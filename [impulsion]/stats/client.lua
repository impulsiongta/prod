-- Stats
local footDistance = 0
local vehicleDistance = 0
local killedNumber = 0
local diedNumber = 0
local shootNumber = 0
local addedMoney = 0
local removedMoney = 0

-- Temp variables
local oldPosition
local oldMoney
local gameTimeAtStart

local function UpdateDistance()
    local position = GetEntityCoords(GetPlayerPed(-1))
    local inVehicle = IsPedInAnyVehicle(GetPlayerPed(-1), false)
    local ped = PlayerPedId()

    if oldPosition then
        if inVehicle then
            vehicleDistance = vehicleDistance + GetDistanceBetweenCoords(oldPosition, position)
        else
            footDistance = footDistance + GetDistanceBetweenCoords(oldPosition, position)
        end
    end

    oldPosition = position

    if IsPedFatallyInjured(ped) then
        oldPosition = nil
    end
end

local function UpdateShots()
    if IsPedShooting(GetPlayerPed(-1)) then
        shootNumber = shootNumber + 1
    end
end

local function AddStatsListener()
    Citizen.CreateThread(function ()        
        while true do
            Citizen.Wait(0)
        
            UpdateDistance()
            UpdateShots()
        end
    end)
end

local function AddStatsPersister()
    Citizen.CreateThread(function ()
        while true do
            Citizen.Wait(30000)
            -- Save every 30 sec 
            TriggerServerEvent('impulsion.stats.save', {
                footDistance = footDistance,
                vehicleDistance = vehicleDistance,
                shootNumber = shootNumber,
                killedNumber = killedNumber,
                diedNumber = diedNumber,
                addedMoney = addedMoney,
                removedMoney = removedMoney,
            })
        end
    end)
end

AddEventHandler('esx:setAccountMoney', function(money)
    if oldMoney then
        if money < oldMoney then
            removedMoney = removedMoney + (oldMoney - money)
        end
        
        if money > oldMoney then
            addedMoney = addedMoney + (money - oldMoney)
        end
    end

    oldMoney = money
end)

AddEventHandler('baseevents:onPlayerDied', function()
    diedNumber = diedNumber + 1
end)

AddEventHandler('baseevents:onPlayerKilled', function()
    killedNumber = killedNumber + 1
end)

-- @TODO Replace this by listening to a correct event
Citizen.CreateThread(function()
    -- main loop thing
    while true do
        Citizen.Wait(50)
        local playerPed = GetPlayerPed(-1)

        if playerPed and playerPed ~= -1 then
            if NetworkIsPlayerActive(PlayerId()) then
                -- Initial wait, ugly, but allow to let the spawning set the player at the correct location before
                -- registration of data 
                Citizen.Wait(10000)
            
                AddStatsListener()
                AddStatsPersister()
                
                return
            end
        end
    end
end)


