local Sessions = {}

ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('es:playerLoaded', function(source, player)
    local identifier = player.getIdentifier()
    local sessionId = MySQL.Sync.fetchScalar('SELECT MAX(session) FROM user_stats WHERE identifier = @identifier', {
        identifier = identifier
    })

    if not sessionId then
        sessionId = 0
    end

    sessionId = sessionId + 1
    Sessions[identifier] = sessionId

    MySQL.Async.execute('INSERT INTO user_stats (identifier, session, start_at) VALUES(@identifier, @session, NOW())', {
        identifier = identifier,
        session = sessionId
    })
end)

AddEventHandler('es:playerDropped', function(user)
    local identifier = user.getIdentifier()
    
    if Sessions[identifier] then
        MySQL.Async.execute('UPDATE user_stats SET end_at = NOW() WHERE identifier = @identifier AND session = @session', {
            identifier = identifier,
            session = Sessions[identifier]
        })
        
        Sessions[identifier] = nil
    end
end)

RegisterServerEvent('impulsion.stats.save')
AddEventHandler('impulsion.stats.save', function (stats)
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    local playerSource = source
    local player = ESX.GetPlayerFromId(playerSource)
    local identifier = player.getIdentifier()

    if Sessions[identifier] then
        MySQL.Async.execute([[
           UPDATE user_stats
           SET
               foot_distance = @footDistance,
               vehicle_distance = @vehicleDistance,
               shot_fired = @shootNumber,
               killed = @killedNumber,
               died = @diedNumber,
               money_added = @addedMoney,
               money_removed = @removedMoney
           WHERE
               identifier = @identifier
               AND
               session = @session
        ]] , {
            ['@footDistance'] = math.floor(stats.footDistance),
            ['@vehicleDistance'] = math.floor(stats.vehicleDistance),
            ['@shootNumber'] = stats.shootNumber,
            ['@killedNumber'] = stats.killedNumber,
            ['@diedNumber'] = stats.diedNumber,
            ['@addedMoney'] = stats.addedMoney,
            ['@removedMoney'] = stats.removedMoney,
            ['@identifier'] = identifier,
            ['@session'] = Sessions[identifier]
        })
    end
end)
