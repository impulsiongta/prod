CREATE TABLE `user_stats` (
  `identifier` VARCHAR(50) NOT NULL,
  `session` INT NOT NULL,
  `start_at` DATETIME NULL,
  `end_at` DATETIME NULL,
  `foot_distance` INT NULL,
  `vehicle_distance` INT NULL,
  `shot_fired` INT NULL,
  `killed` INT NULL,
  `died` INT NULL,
  `money_added` INT NULL,
  `money_removed` INT NULL,
  PRIMARY KEY (`identifier`, `session`));
