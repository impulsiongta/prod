local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil
local GUI                       = {}
GUI.Time                        = 0
local PlayerData              = {}

local UseMask = false
local UseHelmet = false
local UseGlasses = false

local key = {}

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

--Notification joueur
function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, true)
end

--Message text joueur
function Text(text)
		SetTextColour(186, 186, 186, 255)
		SetTextFont(0)
		SetTextScale(0.378, 0.378)
		SetTextWrap(0.0, 1.0)
		SetTextCentre(false)
		SetTextDropshadow(0, 0, 0, 0, 255)
		SetTextEdge(1, 0, 0, 0, 205)
		SetTextEntry("STRING")
		AddTextComponentString(text)
		DrawText(0.017, 0.977)
end

function OpenPersonnelMenu()
	
	ESX.UI.Menu.CloseAll()
	
	ESX.TriggerServerCallback('NB:getUsergroup', function(group)
		playergroup = group

		local elements = {}
		
		table.insert(elements, {label = 'Mes papiers', value = 'menuperso_moi'})
		table.insert(elements, {label = 'Animations', value = 'menuperso_animations'})
		table.insert(elements, {label = 'Attitudes', value = 'menuperso_attitudes'})
		table.insert(elements, {label = 'Clé de voiture', value = 'menuperso_carkey'})
		table.insert(elements, {label = 'GPS Rapide', value = 'menuperso_gpsrapide'})

	--[[	if (IsInVehicle()) then
			local vehicle = GetVehiclePedIsIn( GetPlayerPed(-1), false )
			if ( GetPedInVehicleSeat( vehicle, -1 ) == GetPlayerPed(-1) ) then
				table.insert(elements, {label = 'Véhicule', value = 'menuperso_vehicule'})
			end
		end
	]]
		table.insert(elements, {label = 'Mettre/Retirer son masque', value = 'menuperso_masque'})
		table.insert(elements, {label = 'Mettre/Retirer son casque', value = 'menuperso_casque'})
		table.insert(elements, {label = 'Mettre/Retirer ses lunette', value = 'menuperso_lunette'})

		if PlayerData.job.grade_name == 'boss' then
			table.insert(elements, {label = 'Gestion d\'entreprise', value = 'menuperso_grade'})
		end
				
		if playergroup == 'mod' or playergroup == 'admin' or playergroup == 'superadmin' or playergroup == 'owner' then
			table.insert(elements, {label = 'Modération', value = 'menuperso_modo'})
		end

		table.insert(elements, {label = 'À Propos', value = 'menuperso_propos'})

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'menu_perso',
			{
				title    = 'Menu Personnel',
				align    = 'top-left',
				elements = elements
			},
			function(data, menu)
	
				local elements = {}
				
				if playergroup == 'mod' then
					table.insert(elements, {label = 'TP sur joueur',    					value = 'menuperso_modo_tp_toplayer'})
					table.insert(elements, {label = 'TP joueur sur moi',             		value = 'menuperso_modo_tp_playertome'})
					--table.insert(elements, {label = 'TP sur coordonées [WIP]',			value = 'menuperso_modo_tp_pos'})
					table.insert(elements, {label = 'NoClip',								value = 'menuperso_modo_no_clip'})
					table.insert(elements, {label = 'Mode invincible',					value = 'menuperso_modo_godmode'})
					table.insert(elements, {label = 'Mode fantôme',						value = 'menuperso_modo_mode_fantome'})
					--table.insert(elements, {label = 'Réparer véhicule',					value = 'menuperso_modo_vehicle_repair'})
					--table.insert(elements, {label = 'Faire apparaître un véhicule',		value = 'menuperso_modo_vehicle_spawn'})
					table.insert(elements, {label = 'Retourner le véhicule',				value = 'menuperso_modo_vehicle_flip'})
					--table.insert(elements, {label = 'S\'octroyer de l\'argent',			value = 'menuperso_modo_give_money'})
					--table.insert(elements, {label = 'S\'octroyer de l\'argent (banque)',	value = 'menuperso_modo_give_moneybank'})
					--table.insert(elements, {label = 'S\'octroyer de l\'argent sale',		value = 'menuperso_modo_give_moneydirty'})
					table.insert(elements, {label = 'Afficher/Cacher coordonnées',			value = 'menuperso_modo_showcoord'})
					table.insert(elements, {label = 'Afficher/Cacher noms des joueurs',		value = 'menuperso_modo_showname'})
					table.insert(elements, {label = 'TP sur le marqueur',					value = 'menuperso_modo_tp_marcker'})
					table.insert(elements, {label = 'Soigner la personne',				value = 'menuperso_modo_heal_player'})
					--table.insert(elements, {label = 'Mode spectateur [WIP]',				value = 'menuperso_modo_spec_player'})
					--table.insert(elements, {label = 'Changer l\'apparence',				value = 'menuperso_modo_changer_skin'})
					--table.insert(elements, {label = 'Sauvegarder l\'apparence',			value = 'menuperso_modo_save_skin'})
				end
			
				if playergroup == 'admin' then
					table.insert(elements, {label = 'TP sur joueur',    					value = 'menuperso_modo_tp_toplayer'})
					table.insert(elements, {label = 'TP joueur sur moi',             		value = 'menuperso_modo_tp_playertome'})
					--table.insert(elements, {label = 'TP sur coordonées [WIP]',			value = 'menuperso_modo_tp_pos'})
					table.insert(elements, {label = 'NoClip',								value = 'menuperso_modo_no_clip'})
					table.insert(elements, {label = 'Mode invincible',						value = 'menuperso_modo_godmode'})
					table.insert(elements, {label = 'Mode fantôme',							value = 'menuperso_modo_mode_fantome'})
					table.insert(elements, {label = 'Réparer véhicule',						value = 'menuperso_modo_vehicle_repair'})
					table.insert(elements, {label = 'Faire apparaître un véhicule',			value = 'menuperso_modo_vehicle_spawn'})
					table.insert(elements, {label = 'Retourner le véhicule',				value = 'menuperso_modo_vehicle_flip'})
					table.insert(elements, {label = 'S\'octroyer de l\'argent',				value = 'menuperso_modo_give_money'})
					table.insert(elements, {label = 'S\'octroyer de l\'argent (banque)',	value = 'menuperso_modo_give_moneybank'})
					table.insert(elements, {label = 'S\'octroyer de l\'argent sale',		value = 'menuperso_modo_give_moneydirty'})
					table.insert(elements, {label = 'Afficher/Cacher coordonnées',			value = 'menuperso_modo_showcoord'})
					table.insert(elements, {label = 'Afficher/Cacher noms des joueurs',		value = 'menuperso_modo_showname'})
					table.insert(elements, {label = 'TP sur le marqueur',					value = 'menuperso_modo_tp_marcker'})
					table.insert(elements, {label = 'Soigner la personne',					value = 'menuperso_modo_heal_player'})
					--table.insert(elements, {label = 'Mode spectateur [WIP]',				value = 'menuperso_modo_spec_player'})
					table.insert(elements, {label = 'Changer l\'apparence',					value = 'menuperso_modo_changer_skin'})
					table.insert(elements, {label = 'Sauvegarder l\'apparence',				value = 'menuperso_modo_save_skin'})
				end
			
				if playergroup == 'superadmin' or playergroup == 'owner' then
					table.insert(elements, {label = 'TP sur joueur', 						value = 'menuperso_modo_tp_toplayer'})
					table.insert(elements, {label = 'TP joueur sur moi', 					value = 'menuperso_modo_tp_playertome'})
					table.insert(elements, {label = 'TP sur coordonées [WIP]', 				value = 'menuperso_modo_tp_pos'})
					table.insert(elements, {label = 'NoClip', 								value = 'menuperso_modo_no_clip'})
					table.insert(elements, {label = 'Mode invincible',						value = 'menuperso_modo_godmode'})
					table.insert(elements, {label = 'Mode fantôme', 						value = 'menuperso_modo_mode_fantome'})
					table.insert(elements, {label = 'Réparer véhicule', 					value = 'menuperso_modo_vehicle_repair'})
					table.insert(elements, {label = 'Faire apparaître un véhicule', 		value = 'menuperso_modo_vehicle_spawn'})
					table.insert(elements, {label = 'Retourner le véhicule', 				value = 'menuperso_modo_vehicle_flip'})
					table.insert(elements, {label = 'S\'octroyer de l\'argent', 			value = 'menuperso_modo_give_money'})
					table.insert(elements, {label = 'S\'octroyer de l\'argent (banque)', 	value = 'menuperso_modo_give_moneybank'})
					table.insert(elements, {label = 'S\'octroyer de l\'argent sale', 		value = 'menuperso_modo_give_moneydirty'})
					table.insert(elements, {label = 'Afficher/Cacher coordonnées', 			value = 'menuperso_modo_showcoord'})
					table.insert(elements, {label = 'Afficher/Cacher noms des joueurs', 	value = 'menuperso_modo_showname'})
					table.insert(elements, {label = 'TP sur le marqueur', 					value = 'menuperso_modo_tp_marcker'})
					table.insert(elements, {label = 'Soigner la personne', 					value = 'menuperso_modo_heal_player'})
					table.insert(elements, {label = 'Mode spectateur [WIP]', 				value = 'menuperso_modo_spec_player'})
					table.insert(elements, {label = 'Changer l\'apparence', 				value = 'menuperso_modo_changer_skin'})
					table.insert(elements, {label = 'Sauvegarder l\'apparence', 			value = 'menuperso_modo_save_skin'})
				end

				if data.current.value == 'menuperso_modo' then
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menuperso_modo',
						{
							title    = 'Modération',
							align    = 'top-left',
							elements = elements
						},
						function(data2, menu2)

							if data2.current.value == 'menuperso_modo_tp_toplayer' then
								admin_tp_toplayer()
							end

							if data2.current.value == 'menuperso_modo_tp_playertome' then
								admin_tp_playertome()
							end

							if data2.current.value == 'menuperso_modo_tp_pos' then
								admin_tp_pos()
							end

							if data2.current.value == 'menuperso_modo_no_clip' then
								admin_no_clip()
							end

							if data2.current.value == 'menuperso_modo_godmode' then
								admin_godmode()
							end

							if data2.current.value == 'menuperso_modo_mode_fantome' then
								admin_mode_fantome()
							end

							if data2.current.value == 'menuperso_modo_vehicle_repair' then
								admin_vehicle_repair()
							end

							if data2.current.value == 'menuperso_modo_vehicle_spawn' then
								admin_vehicle_spawn()
							end

							if data2.current.value == 'menuperso_modo_vehicle_flip' then
								admin_vehicle_flip()
							end

							if data2.current.value == 'menuperso_modo_give_money' then
								admin_give_money()
							end

							if data2.current.value == 'menuperso_modo_give_moneybank' then
								admin_give_bank()
							end

							if data2.current.value == 'menuperso_modo_give_moneydirty' then
								admin_give_dirty()
							end

							if data2.current.value == 'menuperso_modo_showcoord' then
								modo_showcoord()
							end

							if data2.current.value == 'menuperso_modo_showname' then
								modo_showname()
							end

							if data2.current.value == 'menuperso_modo_tp_marcker' then
								admin_tp_marcker()
							end

							if data2.current.value == 'menuperso_modo_heal_player' then
								admin_heal_player()
							end

							if data2.current.value == 'menuperso_modo_spec_player' then
								admin_spec_player()
							end

							if data2.current.value == 'menuperso_modo_changer_skin' then
								changer_skin()
							end
							
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
				end
				
				if data.current.value == 'menuperso_vehicule' then
					OpenVehiculeMenu()
				end

				if data.current.value == 'menuperso_moi' then
	
					local elements = {}
					
					table.insert(elements, {label = 'Voir ma carte d\'identité', value = 'menuperso_moi_carte'})
					table.insert(elements, {label = 'Montrer sa carte d\'identité', value = 'menuperso_moi_carte2'})
						
					ESX.UI.Menu.Open(
						
						'default', GetCurrentResourceName(), 'menuperso_moi',
						{
							title    = 'Mes papiers',
							align    = 'top-left',
							elements = elements
						},
						function(data2, menu2)

							if data2.current.value == 'menuperso_moi_carte' then
								TriggerServerEvent('gc:openMeIdentity')
							end

							if data2.current.value == 'menuperso_moi_carte2' then
								local t, distance = GetClosestPlayer()
								if(distance ~= -1 and distance < 3) then
									TriggerServerEvent('gc:openIdentity', GetPlayerServerId(t))
								end
							end
				
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
				end

				if data.current.value == 'menuperso_masque' then

					if UseMask then

						TriggerEvent('skinchanger:getSkin', function(skin)

							TriggerEvent('skinchanger:loadClothes', skin, {
								mask_1 = 0,
								mask_2 = 0
							})
					
						end)
					else
						TriggerEvent('skinchanger:getSkin', function(skin)

							ESX.TriggerServerCallback('esx_mask:getMask', function(hasMask, maskSkin)

								if hasMask then

									TriggerEvent('skinchanger:loadClothes', skin, {
										mask_1 = maskSkin.mask_1,
										mask_2 = maskSkin.mask_2
									})
				
								else
									ESX.ShowNotification('Vous n\'avez ~r~~h~pas de masque')
								end
							end)
						end)
					end
					UseMask  = not UseMask
				end

				if data.current.value == 'menuperso_casque' then

					if UseHelmet then

						TriggerEvent('skinchanger:getSkin', function(skin)

							TriggerEvent('skinchanger:loadClothes', skin, {
								helmet_1 = -1,
								helmet_2 = 0
							})
					
						end)
					else
						TriggerEvent('skinchanger:getSkin', function(skin)

							ESX.TriggerServerCallback('esx_clotheshop:getHelmet', function(hasHelmet, helmetSkin)

								if hasHelmet then

									TriggerEvent('skinchanger:loadClothes', skin, {
										helmet_1 = helmetSkin.helmet_1,
										helmet_2 = helmetSkin.helmet_2
									})
								else
									ESX.ShowNotification('Vous n\'avez ~r~~h~pas de casque')
								end
							end)
						end)
					end
					UseHelmet  = not UseHelmet
				end

				if data.current.value == 'menuperso_lunette' then

					if UseGlasses then

						TriggerEvent('skinchanger:getSkin', function(skin)

							if skin.sex == 0 then
								local player = GetPlayerPed(-1)
								SetPedPropIndex(player, 1, 0, 0, 0) -- Glasses
							else
								local player = GetPlayerPed(-1)
								SetPedPropIndex(player, 1, 5, 0, 0) -- Glasses
							end	
						end)
					else
						TriggerEvent('skinchanger:getSkin', function(skin)

							ESX.TriggerServerCallback('esx_clotheshop:getGlasses', function(hasGlasses, glassesSkin)

								if hasGlasses then

									TriggerEvent('skinchanger:loadClothes', skin, {
										glasses_1 = glassesSkin.glasses_1,
										glasses_2 = glassesSkin.glasses_2
									})
								else
									ESX.ShowNotification('Vous n\'avez ~r~~h~pas de lunette')
								end
							end)
						end)
					end
					UseGlasses  = not UseGlasses
				end

				if data.current.value == 'menuperso_attitudes' then
	
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menuperso_animations',
						{
							title    = 'Animations',
							align    = 'top-left',
							elements = {
								{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

								----------------------------------Attitudes-----------------------------
								{label = 'Homme',  value = 'menuperso_attitudes_homme'},
								{label = 'Femme',  value = 'menuperso_attitudes_femme'},
								{label = 'Dépressif M',  value = 'menuperso_attitudes_depressifm'},
								{label = 'Dépressif F',  value = 'menuperso_attitudes_depressiff'},
								{label = 'Business',  value = 'menuperso_attitudes_business'},
								{label = 'Déterminé',  value = 'menuperso_attitudes_determine'},
								{label = 'Casual',  value = 'menuperso_attitudes_casual'},
								{label = 'Gros',  value = 'menuperso_attitudes_gros'},
								{label = 'Hispter',  value = 'menuperso_attitudes_hispter'},
								{label = 'Blessé',  value = 'menuperso_attitudes_blesse'},
								{label = 'Intimidé',  value = 'menuperso_attitudes_intimide'},
								{label = 'Malheureux',  value = 'menuperso_attitudes_malheureux'},
								{label = 'Muscle',  value = 'menuperso_attitudes_muscle'},
								{label = 'Sombre',  value = 'menuperso_attitudes_sombre'},
								{label = 'Fatigue',  value = 'menuperso_attitudes_fatigue'},
								{label = 'Pressé',  value = 'menuperso_attitudes_presse'},
								{label = 'Fier',  value = 'menuperso_attitudes_fier'},
								{label = 'Petite course',  value = 'menuperso_attitudes_petitecourse'},
								{label = 'Mangeuse d\'Homme',  value = 'menuperso_attitudes_mangeuse'},
								{label = 'Impertinent',  value = 'menuperso_attitudes_impertinent'},
								{label = 'Arrogante',  value = 'menuperso_attitudes_arrogante'},
							},
						},

						function(data2, menu2)

							if data2.current.value == 'menuperso_animations_annuler' then
								ResetPedMovementClipset(PlayerPedId(), 0.1)
							end

							if data2.current.value == 'menuperso_attitudes_homme' then
								RequestAnimSet("move_m@confident")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@confident", true)
							end

							if data2.current.value == 'menuperso_attitudes_femme' then
								RequestAnimSet("move_f@heels@c")
								SetPedMovementClipset(GetPlayerPed(-1), "move_f@heels@c", true)
							end

							if data2.current.value == 'menuperso_attitudes_depressiff' then
								RequestAnimSet("move_f@depressed@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_f@depressed@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_depressifm' then
								RequestAnimSet("move_m@depressed@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@depressed@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_business' then
								RequestAnimSet("move_m@business@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@business@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_determine' then
								RequestAnimSet("move_m@brave@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@brave@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_casual' then
								RequestAnimSet("move_m@casual@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@casual@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_gros' then
								RequestAnimSet("move_m@fat@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@fat@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_hispter' then
								RequestAnimSet("move_m@hipster@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@hipster@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_blesse' then
								RequestAnimSet("move_m@injured")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@injured", true)
							end

							if data2.current.value == 'menuperso_attitudes_intimide' then
								RequestAnimSet("move_m@hurry@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@hurry@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_malheureux' then
								RequestAnimSet("move_m@sad@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@sad@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_muscle' then
								RequestAnimSet("move_m@muscle@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@muscle@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_sombre' then
								RequestAnimSet("move_m@shadyped@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@shadyped@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_fatigue' then
								RequestAnimSet("move_m@buzzed")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@buzzed", true)
							end

							if data2.current.value == 'menuperso_attitudes_presse' then
								RequestAnimSet("move_m@hurry_butch@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@hurry_butch@a", true)
							end

							if data2.current.value == 'menuperso_attitudes_fier' then
								RequestAnimSet("move_m@money")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@money", true)
							end

							if data2.current.value == 'menuperso_attitudes_petitecourse' then
								RequestAnimSet("move_m@quick")
								SetPedMovementClipset(GetPlayerPed(-1), "move_m@quick", true)
							end

							if data2.current.value == 'menuperso_attitudes_mangeuse' then
								RequestAnimSet("move_f@maneater")
								SetPedMovementClipset(GetPlayerPed(-1), "move_f@maneater", true)
							end

							if data2.current.value == 'menuperso_attitudes_impertinent' then
								RequestAnimSet("move_f@sassy")
								SetPedMovementClipset(GetPlayerPed(-1), "move_f@sassy", true)
							end

							if data2.current.value == 'menuperso_attitudes_arrogante' then
								RequestAnimSet("move_f@arrogant@a")
								SetPedMovementClipset(GetPlayerPed(-1), "move_f@arrogant@a", true)
							end
				
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
				end

				if data.current.value == 'menuperso_animations' then

					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menuperso_animations',
						{
							title    = 'Animations',
							align    = 'top-left',
							elements = {
								{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},
								----------------------------------Catégorie------------------------------
								{label = 'Catégorie Utile',  value = 'menuperso_animations_utile'},
								{label = 'Catégorie Amical',  value = 'menuperso_animations_amical'},
								{label = 'Catégorie Dance',  value = 'menuperso_animations_dance'},
								{label = 'Catégorie Vulgaire',  value = 'menuperso_animations_vulgaire'},
								{label = 'Catégorie Sports',  value = 'menuperso_animations_sports'},
								{label = 'Catégorie PEGI21',  value = 'menuperso_animations_pegi21'},
								{label = 'Catégorie Autres',  value = 'menuperso_animations_autres'},
								----------------------------------Animations-----------------------------
								{label = '------------------------------------',  value = nil},
								{label = 'S\'asseoir',  value = 'menuperso_animations_assis'},
								{label = 'S\'asseoir par terre',  value = 'menuperso_animations_assis_sol'},
								{label = 'S\'appuiez',  value = 'menuperso_animations_mur'},
								{label = 'Dormir',  value = 'menuperso_animations_dormir'},
								{label = 'Faire un calin/bisou',  value = 'menuperso_animations_bisou'},
								{label = 'Tendre les bras',  value = 'menuperso_animations_tendre'},
								{label = 'Mort',  value = 'menuperso_animations_mort'},
								{label = 'Mort 2',  value = 'menuperso_animations_mort2'},
								{label = 'Dépressif',  value = 'menuperso_animations_depressif'},
								{label = 'FacePalm',  value = 'menuperso_animations_facepalm'},
								{label = 'Applaudir',  value = 'menuperso_animations_applaudir'},
								{label = 'Café',  value = 'menuperso_animations_cafe'},
								{label = 'Calepin',  value = 'menuperso_animations_calepin'},
							},
						},
						function(data2, menu2)

							if data2.current.value == 'menuperso_animations_annuler' then
								local ped = GetPlayerPed(-1);
								if ped then
									ClearPedTasksImmediately(ped);
								end
							end

							if data2.current.value == 'menuperso_animations_assis' then
								RequestAnimDict("anim@heists@heist_safehouse_intro@phone_couch@male")
								TaskPlayAnim(GetPlayerPed(-1), "anim@heists@heist_safehouse_intro@phone_couch@male", "phone_couch_male_idle" , 8.0, -8, -1, 1, 0, 0, 0, 0)
							end

							if data2.current.value == 'menuperso_animations_assis_sol' then
								animsActionScenario({anim = "WORLD_HUMAN_PICNIC" })
							end

							if data2.current.value == 'menuperso_animations_mur' then
								animsActionScenario({anim = "WORLD_HUMAN_LEANING" })
							end

							if data2.current.value == 'menuperso_animations_dormir' then
								animsActionScenario({anim = "WORLD_HUMAN_BUM_SLUMPED" })
							end

							if data2.current.value == 'menuperso_animations_bisou' then
								animsAction({ lib = "mp_ped_interaction", anim = "kisses_guy_a" })
							end

							if data2.current.value == 'menuperso_animations_tendre' then
								RequestAnimDict("nm@hands")
								TaskPlayAnim(GetPlayerPed(-1), "nm@hands", "flail", 8.0, -8, -1, 49, 0, 0, 0, 0)
							end

							if data2.current.value == 'menuperso_animations_mort' then
								RequestAnimDict("missprologueig_6")
								TaskPlayAnim(GetPlayerPed(-1), "missprologueig_6", "lying_dead_brad" , 8.0, -8, -1, 1, 0, 0, 0, 0)
							end

							if data2.current.value == 'menuperso_animations_mort2' then
								RequestAnimDict("missfbi1")
								TaskPlayAnim(GetPlayerPed(-1), "missfbi1", "cpr_pumpchest_idle", 8.0, -8, -1, 1, 0, 0, 0, 0)
							end

							if data2.current.value == 'menuperso_animations_depressif' then
								animsAction({ lib = "amb@world_human_bum_standing@depressed@idle_a", anim = "idle_a" })
							end

							if data2.current.value == 'menuperso_animations_facepalm' then
								animsAction({ lib = "anim@mp_player_intcelebrationmale@face_palm", anim = "face_palm" })
							end

							if data2.current.value == 'menuperso_animations_applaudir' then
								animsActionScenario({anim = "WORLD_HUMAN_CHEERING" })
							end

							if data2.current.value == 'menuperso_animations_cafe' then
								animsActionScenario({anim = "WORLD_HUMAN_AA_COFFEE" })
							end

							if data2.current.value == 'menuperso_animations_calepin' then
								animsActionScenario({anim = "CODE_HUMAN_MEDIC_TIME_OF_DEATH" })
							end

							if data2.current.value == 'menuperso_animations_utile' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_animations_utile',
									{
										title    = 'Catégorie Utile',
										align    = 'top-left',
										elements = {
											{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

											{label = 'Salut de la main',  value = 'menuperso_animations_salut_de_la_main'},
											{label = 'Salut de la main 2',     value = 'menuperso_animations_salut_de_la_main2'},
											{label = 'Garde à vous',     value = 'menuperso_animations_garde_a_vous'},
											{label = 'S\'allonger sur le ventre',     value = 'menuperso_animations_sallonger_ventre'},
											{label = 'S\'allonger sur le dos',     value = 'menuperso_animations_sallonger_dos'},
											{label = 'À genoux',  value = 'menuperso_animations_genoux'},
											{label = 'Rien de cassé ?',     value = 'menuperso_animations_check'},
											{label = 'Massage cardiaque',     value = 'menuperso_animations_cpr'},
											{label = 'Croiser les bras',  value = 'menuperso_animations_croise_les_bras'},
											{label = 'Croiser les bras 2',     value = 'menuperso_animations_croise_les_bras2'},
											{label = 'Se gratter la tête',  value = 'menuperso_animations_tete'},
											{label = 'Se mettre contre un rebort',     value = 'menuperso_animations_rebord'},
											{label = 'Sérieux',  value = 'menuperso_animations_serieux'},
											{label = 'Balle dans la tête',  value = 'menuperso_animations_Humor_balledanslatete'},

										},
									},
									function(data3, menu3)

										if data3.current.value == 'menuperso_animations_annuler' then
											local ped = GetPlayerPed(-1);
											if ped then
												ClearPedTasks(ped);
											end
										end

										if data3.current.value == 'menuperso_animations_salut_de_la_main' then
											animsAction({ lib = "friends@frj@ig_1", anim = "wave_e" })
										end

										if data3.current.value == 'menuperso_animations_salut_de_la_main2' then
											animsAction({ lib = "friends@frj@ig_1", anim = "wave_a" })
										end

										if data3.current.value == 'menuperso_animations_garde_a_vous' then
											RequestAnimDict("anim@mp_player_intincarsalutestd@ds@")
											TaskPlayAnim(GetPlayerPed(-1), "anim@mp_player_intincarsalutestd@ds@", "idle_a", 8.0, -8, -1, 1, 0, 0, 0, 0)
										end

										if data3.current.value == 'menuperso_animations_sallonger_ventre' then
											animsActionScenario({anim = "WORLD_HUMAN_SUNBATHE" })
										end

										if data3.current.value == 'menuperso_animations_sallonger_dos' then
											animsActionScenario({anim = "WORLD_HUMAN_SUNBATHE_BACK" })
										end

										if data3.current.value == 'menuperso_animations_genoux' then
											RequestAnimDict("misschinese2_crystalmaze")
											TaskPlayAnim(GetPlayerPed(-1), "misschinese2_crystalmaze", "2int_loop_base_taotranslator" , 8.0, -8, -1, 1, 0, 0, 0, 0)
										end

										if data3.current.value == 'menuperso_animations_check' then
											animsAction({ lib = "clothingtrousers", anim = "try_trousers_neutral_b" })
										end

										if data3.current.value == 'menuperso_animations_cpr' then
											RequestAnimDict("mini@cpr@char_a@cpr_str")
											TaskPlayAnim(GetPlayerPed(-1), "mini@cpr@char_a@cpr_str", "cpr_pumpchest", 8.0, -8, -1, 1, 0, 0, 0, 0)
										end

										if data3.current.value == 'menuperso_animations_croise_les_bras' then
											animsAction({ lib = "amb@world_human_window_shop@male@idle_a", anim = "browse_a" })
										end

										if data3.current.value == 'menuperso_animations_croise_les_bras2' then
											animsAction({ lib = "oddjobs@bailbond_hobohang_out_street_c", anim = "base" })
										end

										if data3.current.value == 'menuperso_animations_tete' then
											animsAction({ lib = "clothingtie", anim = "check_out_a" })
										end

										if data3.current.value == 'menuperso_animations_rebord' then
											RequestAnimDict("amb@prop_human_bum_shopping_cart@male@idle_a")
											TaskPlayAnim(GetPlayerPed(-1), "amb@prop_human_bum_shopping_cart@male@idle_a", "idle_a", 8.0, -8, -1, 1, 0, 0, 0, 0)
										end

										if data3.current.value == 'menuperso_animations_serieux' then
											RequestAnimDict("missmic_4premiere")
											TaskPlayAnim(GetPlayerPed(-1), "missmic_4premiere", "movie_prem_01_m_c", 8.0, -8, -1, 1, 0, 0, 0, 0)
										end
										
										if data3.current.value == 'menuperso_animations_Humor_balledanslatete' then
											animsAction({ lib = "mp_suicide", anim = "pistol" })
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_animations_amical' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_animations_amical',
									{
										title    = 'Catégorie Amical',
										align    = 'top-left',
										elements = {
											{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

											{label = 'Bro',     value = 'menuperso_animations_bro'},
											{label = 'Damn',     value = 'menuperso_animations_dam'},
											{label = 'WTF',  value = 'menuperso_animations_wtf'},
										},
									},
									function(data3, menu3)

										if data3.current.value == 'menuperso_animations_annuler' then
											local ped = GetPlayerPed(-1);
											if ped then
												ClearPedTasks(ped);
											end
										end

										if data3.current.value == 'menuperso_animations_bro' then
											animsAction({ lib = "mp_player_int_upperbro_love", anim = "mp_player_int_bro_love_enter" })
										end

										if data3.current.value == 'menuperso_animations_dam' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_damn" })
										end

										if data3.current.value == 'menuperso_animations_wtf' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_shrug_hard" })
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_animations_dance' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_animations_dance',
									{
										title    = 'Catégorie Dance',
										align    = 'top-left',
										elements = {
											{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

											{label = 'Petite danse',  value = 'menuperso_animations_danse1'},
											{label = 'Danse',     value = 'menuperso_animations_danse2'},
											{label = 'Danse 2',     value = 'menuperso_animations_danse3'},
											{label = 'Danse 3',  value = 'menuperso_animations_danse4'},
											{label = 'Autre danse',  value = 'menuperso_animations_danse5'},
											{label = 'Autre danse 2',     value = 'menuperso_animations_danse6'},
											{label = 'Autre danse 3',  value = 'menuperso_animations_danse7'},
											{label = 'Lap-danse',  value = 'menuperso_animations_lapdanse'},
											{label = 'Lap-danse 2',  value = 'menuperso_animations_lapdanse2'},
										},
									},
									function(data3, menu3)

										if data3.current.value == 'menuperso_animations_annuler' then
											local ped = GetPlayerPed(-1);
											if ped then
												ClearPedTasks(ped);
											end
										end

										if data3.current.value == 'menuperso_animations_danse1' then
											animsAction({ lib = "anim@mp_player_intincardancestd@rps@", anim = "idle_a" })
										end

										if data3.current.value == 'menuperso_animations_danse2' then
											animsAction({ lib = "misschinese2_crystalmazemcs1_cs", anim = "dance_loop_tao" })
										end

										if data3.current.value == 'menuperso_animations_danse3' then
											animsActionScenario({anim = "WORLD_HUMAN_PARTYING" })
										end

										if data3.current.value == 'menuperso_animations_danse4' then
											animsAction({ lib = "missfbi3_sniping", anim = "dance_m_default" })
										end

										if data3.current.value == 'menuperso_animations_danse5' then
											animsAction({ lib = "special_ped@mountain_dancer@monologue_2@monologue_2a", anim = "mnt_dnc_angel" })
										end

										if data3.current.value == 'menuperso_animations_danse6' then
											animsAction({ lib = "special_ped@mountain_dancer@monologue_3@monologue_3a", anim = "mnt_dnc_buttwag" })
										end

										if data3.current.value == 'menuperso_animations_danse7' then
											animsAction({ lib = "special_ped@mountain_dancer@monologue_4@monologue_4a", anim = "mnt_dnc_verse" })
										end

										if data3.current.value == 'menuperso_animations_lapdanse' then
											animsAction({ lib = "mini@strip_club@private_dance@part1", anim = "priv_dance_p1" })
										end

										if data3.current.value == 'menuperso_animations_lapdanse2' then
											animsAction({ lib = "mini@strip_club@private_dance@part2", anim = "priv_dance_p2" })
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_animations_vulgaire' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_animations_vulgaire',
									{
										title    = 'Catégorie Vulgaire',
										align    = 'top-left',
										elements = {
											{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

											{label = 'Se gratter les cou*****',  value = 'menuperso_animations_gratte'},
											{label = 'Se gratter les cou***** 2',  value = 'menuperso_animations_gratte2'},
											{label = 'Doigt d\'honneur',  value = 'menuperso_animations_doigt'},
											{label = 'Doigt d\'honneur 2',  value = 'menuperso_animations_doigt2'},
											{label = 'Branlette',  value = 'menuperso_animations_branlette'},
											{label = 'Branlette 2',  value = 'menuperso_animations_branlette2'},
											{label = 'Se curer le nez',  value = 'menuperso_animations_nez'},
											{label = 'Doigt dans le cul',  value = 'menuperso_animations_cul'},
											{label = 'Je t\'encule où...',  value = 'menuperso_animations_ou'},
											{label = 'Par derrière',  value = 'menuperso_animations_derriere'},
										},
									},
									function(data3, menu3)

										if data3.current.value == 'menuperso_animations_annuler' then
											local ped = GetPlayerPed(-1);
											if ped then
												ClearPedTasks(ped);
											end
										end

										if data3.current.value == 'menuperso_animations_gratte' then
											animsAction({ lib = "move_p_m_two_idles@generic", anim = "fidget_scratch_balls" })
										end

										if data3.current.value == 'menuperso_animations_gratte2' then
											animsAction({ lib = "friends@frt@ig_1", anim = "trevor_impatient_wait_2" })
										end

										if data3.current.value == 'menuperso_animations_doigt' then
											animsAction({ lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_01_enter" })
										end
										
										if data3.current.value == 'menuperso_animations_doigt2' then
											animsAction({ lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_02_enter" })
										end

										if data3.current.value == 'menuperso_animations_branlette' then
											animsAction({ lib = "mp_player_intwank", anim = "mp_player_int_wank" })
										end
										
										if data3.current.value == 'menuperso_animations_branlette2' then
											animsAction({ lib = "mp_player_int_upperwank", anim = "mp_player_int_wank_01" })
										end

										if data3.current.value == 'menuperso_animations_nez' then
											animsAction({ lib = "anim@mp_player_intcelebrationmale@nose_pick", anim = "nose_pick" })
										end
										
										if data3.current.value == 'menuperso_animations_cul' then
											animsAction({ lib = "friends@frt@ig_1", anim = "trevor_impatient_wait_3" })
										end

										if data3.current.value == 'menuperso_animations_ou' then
											animsAction({ lib = "anim@mp_player_intcelebrationmale@dock", anim = "dock" })
										end
										
										if data3.current.value == 'menuperso_animations_derriere' then
											animsAction({ lib = "anim@mp_player_intincarair_shaggingbodhi@ds@", anim = "idle_a" })
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_animations_sports' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_animations_sports',
									{
										title    = 'Catégorie Sports',
										align    = 'top-left',
										elements = {
											{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

											{label = 'Étirement',     value = 'menuperso_animations_etirement'},
											{label = 'Étirement 2',     value = 'menuperso_animations_etirement2'},
											{label = 'Étirement 3',     value = 'menuperso_animations_etirement3'},
											{label = 'Étirement 4',     value = 'menuperso_animations_etirement4'},
											{label = 'Pompes',     value = 'menuperso_animations_pompes'},
											{label = 'Yoga',     value = 'menuperso_animations_yoga'},
											{label = 'Abdos',     value = 'menuperso_animations_adbos'},
											{label = 'Altères',     value = 'menuperso_animations_alteres'},
											{label = 'Montrer ses muscles',     value = 'menuperso_animations_muscle'},
										},
									},
									function(data3, menu3)

										if data3.current.value == 'menuperso_animations_annuler' then
											local ped = GetPlayerPed(-1);
											if ped then
												ClearPedTasks(ped);
											end
										end

										if data3.current.value == 'menuperso_animations_pompes' then
											animsActionScenario({ anim = "WORLD_HUMAN_PUSH_UPS" })
										end

										if data3.current.value == 'menuperso_animations_yoga' then
											animsActionScenario({ anim = "WORLD_HUMAN_YOGA" })
										end

										if data3.current.value == 'menuperso_animations_adbos' then
											animsActionScenario({ anim = "WORLD_HUMAN_SIT_UPS" })
										end

										if data3.current.value == 'menuperso_animations_muscle' then
											animsActionScenario({ anim = "WORLD_HUMAN_MUSCLE_FLEX" })
										end

										if data3.current.value == 'menuperso_animations_alteres' then
											animsActionScenario({ anim = "WORLD_HUMAN_MUSCLE_FREE_WEIGHTS" })
										end

										if data3.current.value == 'menuperso_animations_etirement' then
											animsAction({ lib = "mini@triathlon", anim = "idle_f" })
										end

										if data3.current.value == 'menuperso_animations_etirement2' then
											animsAction({ lib = "mini@triathlon", anim = "idle_a" })
										end

										if data3.current.value == 'menuperso_animations_etirement3' then
											animsAction({ lib = "mini@triathlon", anim = "idle_d" })
										end

										if data3.current.value == 'menuperso_animations_etirement4' then
											animsAction({ lib = "mini@triathlon", anim = "idle_e" })
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_animations_pegi21' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_animations_pegi21',
									{
										title    = 'Catégorie PEGI21',
										align    = 'top-left',
										elements = {
											{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

											{label = 'Faire l\'amour (véhicule)',  value = 'menuperso_animations_amour'},
											{label = 'Faire l\'amour (véhicule) 2',     value = 'menuperso_animations_amour2'},
											{label = 'Félation',     value = 'menuperso_animations_amour3'},
											{label = 'Félation (véhicule)',  value = 'menuperso_animations_amour4'},
											{label = 'Félation (véhicule) 2',  value = 'menuperso_animations_amour5'},
										},
									},
									function(data3, menu3)

										if data3.current.value == 'menuperso_animations_annuler' then
											local ped = GetPlayerPed(-1);
											if ped then
												ClearPedTasks(ped);
											end
										end

										if data3.current.value == 'menuperso_animations_amour' then
											animsAction({ lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_female" })
										end

										if data3.current.value == 'menuperso_animations_amour2' then
											animsAction({ lib = "misscarsteal2pimpsex", anim = "pimpsex_hooker" })
										end

										if data3.current.value == 'menuperso_animations_amour3' then
											animsAction({ lib = "mini@prostitutes@sexlow_veh", anim = "low_car_bj_to_prop_p1_female" })
										end

										if data3.current.value == 'menuperso_animations_amour4' then
											animsAction({ lib = "oddjobs@towing", anim = "f_blow_job_loop" })
										end

										if data3.current.value == 'menuperso_animations_amour4' then
											animsAction({ lib = "oddjobs@assassinate@vice@sex", anim = "frontseat_carsex_loop_f" })
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_animations_autres' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_animations_autres',
									{
										title    = 'Catégorie Autres',
										align    = 'top-left',
										elements = {
											{label = 'Annuler l\'animation',  value = 'menuperso_animations_annuler'},

											{label = 'Jumelles',  value = 'menuperso_animations_jumelle'},
											{label = 'BBQ',     value = 'menuperso_animations_bbq'},
											{label = 'Paparazzi',     value = 'menuperso_animations_photo'},
											{label = 'Consierge',  value = 'menuperso_animations_consierge'},
											{label = 'Carte',  value = 'menuperso_animations_carte'},
											{label = 'Bloc-note',     value = 'menuperso_animations_bloc'},
											{label = 'Musicien',  value = 'menuperso_animations_musique'},
											{label = 'DJ',     value = 'menuperso_animations_dj'},
											{label = 'Pêcher',  value = 'menuperso_animations_peche'},
										},
									},
									function(data3, menu3)

										if data3.current.value == 'menuperso_animations_annuler' then
											local ped = GetPlayerPed(-1);
											if ped then
												ClearPedTasks(ped);
											end
										end

										if data3.current.value == 'menuperso_animations_jumelle' then
											animsActionScenario({anim = "WORLD_HUMAN_BINOCULARS" })
										end

										if data3.current.value == 'menuperso_animations_bbq' then
											animsActionScenario({anim = "PROP_HUMAN_BBQ" })
										end

										if data3.current.value == 'menuperso_animations_photo' then
											animsActionScenario({anim = "WORLD_HUMAN_PAPARAZZI" })
										end

										if data3.current.value == 'menuperso_animations_consierge' then
											animsActionScenario({anim = "WORLD_HUMAN_JANITOR" })
										end

										if data3.current.value == 'menuperso_animations_carte' then
											animsActionScenario({anim = "WORLD_HUMAN_TOURIST_MAP" })
										end

										if data3.current.value == 'menuperso_animations_bloc' then
											animsActionScenario({anim = "WORLD_HUMAN_CLIPBOARD" })
										end

										if data3.current.value == 'menuperso_animations_musique' then
											animsActionScenario({anim = "WORLD_HUMAN_MUSICIAN" })
										end

										if data3.current.value == 'menuperso_animations_dj' then
											RequestAnimDict("anim@mp_player_intcelebrationmale@dj")
											TaskPlayAnim(GetPlayerPed(-1), "anim@mp_player_intcelebrationmale@dj", "dj", 8.0, -8, -1, 1, 0, 0, 0, 0)
										end

										if data3.current.value == 'menuperso_animations_peche' then
											animsActionScenario({anim = "WORLD_HUMAN_STAND_FISHING" })
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end
							
							
						end,
						function(data2, menu2)
							menu2.close()
						end
					)

				end

				if data.current.value == 'menuperso_carkey' then
					local elements1 = {}
						for x = 0, #key, 1 do
							if key[x] ~= nil then
								table.insert(elements1,{label = 'Clé de voiture ('..key[x]..')',  value = 'menuperso_key'..key[x]})
							end
						end
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menuperso_key',
						{
							title    = 'Vos clés',
							align    = 'top-left',
							elements = elements1

						},
						function(data2, menu2)
							for k = 0, #key, 1 do
								if key[k] ~= nil then
								    if data2.current.value == 'menuperso_key'..key[k] then
								  		GiveKeyAtPlayer(key[k])
								     end
								end
							end
						end,
						function(data2, menu2)
							menu2.close()
						end
					)

				end
				
				if data.current.value == 'menuperso_gpsrapide' then
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menuperso_gpsrapide',
						{
							title    = 'GPS Rapide',
							align    = 'top-left',
							elements = {
								{label = 'Comissariat',	value = 'menuperso_gpsrapide_comico'},
								{label = 'Hôpital', 		value = 'menuperso_gpsrapide_hopital'},
								{label = 'Garage',  		value = 'menuperso_gpsrapide_garage'},
								{label = 'Concessionnaire',  		value = 'menuperso_gpsrapide_concessionnaire'},
								{label = 'Los Santos Prestige',  	value = 'menuperso_gpsrapide_depanneur'},
								{label = 'Manoir de Jade',  		value = 'menuperso_gpsrapide_manoir'},
							},
						},
						function(data2, menu2)

							if data2.current.value == 'menuperso_gpsrapide_comico' then
								x, y, z = Config.comico.x, Config.comico.y, Config.comico.z
								SetNewWaypoint(x, y, z)
								local source = GetPlayerServerId();
								ESX.ShowNotification("Destination ~y~~h~ajouté au GPS !")
							end

							if data2.current.value == 'menuperso_gpsrapide_hopital' then
								x, y, z = Config.hopital.x, Config.hopital.y, Config.hopital.z
								SetNewWaypoint(x, y, z)
								local source = GetPlayerServerId();
								ESX.ShowNotification("Destination ~y~~h~ajouté au GPS !")
							end

							if data2.current.value == 'menuperso_gpsrapide_concessionnaire' then
								x, y, z = Config.concessionnaire.x, Config.concessionnaire.y, Config.concessionnaire.z
								SetNewWaypoint(x, y, z)
								local source = GetPlayerServerId();
								ESX.ShowNotification("Destination ~y~~h~ajouté au GPS !")
							end

							if data2.current.value == 'menuperso_gpsrapide_depanneur' then
								x, y, z = Config.depanneur.x, Config.depanneur.y, Config.depanneur.z
								SetNewWaypoint(x, y, z)
								local source = GetPlayerServerId();
								ESX.ShowNotification("Destination ~y~~h~ajouté au GPS !")
							end

							if data2.current.value == 'menuperso_gpsrapide_manoir' then
								x, y, z = Config.manoir.x, Config.manoir.y, Config.manoir.z
								SetNewWaypoint(x, y, z)
								local source = GetPlayerServerId();
								ESX.ShowNotification("Destination ~y~~h~ajouté au GPS !")
							end

							if data2.current.value == 'menuperso_gpsrapide_garage' then
								x, y, z = Config.garage.x, Config.garage.y, Config.garage.z
								SetNewWaypoint(x, y, z)
								local source = GetPlayerServerId();
								ESX.ShowNotification("Destination ~y~~h~ajouté au GPS !")
							end

						end,
						function(data2, menu2)
							menu2.close()
						end
					)

				end
				
				if data.current.value == 'menuperso_grade' then
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menuperso_grade',
						{
							title    = 'Gestion d\'entreprise',
							align    = 'top-left',
							elements = {
								{label = 'Recruter',	value = 'menuperso_grade_recruter'},
								{label = 'Virer',		value = 'menuperso_grade_virer'},
								{label = 'Promouvoir',	value = 'menuperso_grade_promouvoir'},
								{label = 'Destituer',	value = 'menuperso_grade_destituer'}
							},
						},
						function(data2, menu2)

							if data2.current.value == 'menuperso_grade_recruter' then
								if PlayerData.job.grade_name == 'boss' then
										local job =  PlayerData.job.name
										local grade = 0
										local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
									if closestPlayer == -1 or closestDistance > 3.0 then
										ESX.ShowNotification("Aucun joueur ~r~~h~à proximité")
									else
										TriggerServerEvent('NB:recruterplayer', GetPlayerServerId(closestPlayer), job,grade)
									end

								else
									Notify("Vous n'avez ~r~~h~pas les droits")

								end
								
							end

							if data2.current.value == 'menuperso_grade_virer' then
								if PlayerData.job.grade_name == 'boss' then
										local job =  PlayerData.job.name
										local grade = 0
										local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
									if closestPlayer == -1 or closestDistance > 3.0 then
										ESX.ShowNotification("Aucun joueur ~r~~h~à proximité")
									else
										TriggerServerEvent('NB:virerplayer', GetPlayerServerId(closestPlayer))
									end

								else
									Notify("Vous n'avez ~r~~h~pas les droits")

								end
								
							end

							if data2.current.value == 'menuperso_grade_promouvoir' then

								if PlayerData.job.grade_name == 'boss' then
										local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
									if closestPlayer == -1 or closestDistance > 3.0 then
										ESX.ShowNotification("Aucun joueur ~r~~h~à proximité")
									else
										TriggerServerEvent('NB:promouvoirplayer', GetPlayerServerId(closestPlayer))
									end

								else
									Notify("Vous n'avez ~r~~h~pas les droits")

								end
								
								
							end

							if data2.current.value == 'menuperso_grade_destituer' then

								if PlayerData.job.grade_name == 'boss' then
										local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
									if closestPlayer == -1 or closestDistance > 3.0 then
										ESX.ShowNotification("Aucun joueur ~r~~h~à proximité")
									else
										TriggerServerEvent('NB:destituerplayer', GetPlayerServerId(closestPlayer))
									end

								else
									Notify("Vous n'avez ~r~~h~pas les droits")

								end
								
								
							end

							
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
				end

				if data.current.value == 'menuperso_propos' then
					ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menuperso_propos',
						{
							title    = 'À Propos',
							align    = 'top-left',
							elements = {
								{label = 'Version 3.0.1',	value = nil},
								{label = 'Développé par Netouss et P2',	value = nil},
							},
						},
						function(data2, menu2)

							if data2.current.value == nil then
							end
							
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
				end
				
			end,
			function(data, menu)
				menu.close()
			end
		)
		
	end)
end

---------------------------------------------------------------------------Vehicule Menu

function OpenVehiculeMenu()
	
	ESX.UI.Menu.CloseAll()
		
	local elements = {}

	if vehiculeON then
		table.insert(elements, {label = 'Couper le moteur',			value = 'menuperso_vehicule_MoteurOff'})
	else
		table.insert(elements, {label = 'Démarrer le moteur',		value = 'menuperso_vehicule_MoteurOn'})
	end
	
	if porteAvantGaucheOuverte then
		table.insert(elements, {label = 'Fermer la porte gauche',	value = 'menuperso_vehicule_fermerportes_fermerportegauche'})
	else
		table.insert(elements, {label = 'Ouvrir la porte gauche',		value = 'menuperso_vehicule_ouvrirportes_ouvrirportegauche'})
	end
	
	if porteAvantDroiteOuverte then
		table.insert(elements, {label = 'Fermer la porte droite',	value = 'menuperso_vehicule_fermerportes_fermerportedroite'})
	else
		table.insert(elements, {label = 'Ouvrir la porte droite',		value = 'menuperso_vehicule_ouvrirportes_ouvrirportedroite'})
	end
	
	if porteArriereGaucheOuverte then
		table.insert(elements, {label = 'Fermer la porte arrière gauche',	value = 'menuperso_vehicule_fermerportes_fermerportearrieregauche'})
	else
		table.insert(elements, {label = 'Ouvrir la porte arrière gauche',		value = 'menuperso_vehicule_ouvrirportes_ouvrirportearrieregauche'})
	end
	
	if porteArriereDroiteOuverte then
		table.insert(elements, {label = 'Fermer la porte arrière droite',	value = 'menuperso_vehicule_fermerportes_fermerportearrieredroite'})
	else
		table.insert(elements, {label = 'Ouvrir la porte arrière droite',		value = 'menuperso_vehicule_ouvrirportes_ouvrirportearrieredroite'})
	end
	
	if porteCapotOuvert then
		table.insert(elements, {label = 'Fermer le capot',	value = 'menuperso_vehicule_fermerportes_fermercapot'})
	else
		table.insert(elements, {label = 'Ouvrir le capot',		value = 'menuperso_vehicule_ouvrirportes_ouvrircapot'})
	end
	
	if porteCoffreOuvert then
		table.insert(elements, {label = 'Fermer le coffre',	value = 'menuperso_vehicule_fermerportes_fermercoffre'})
	else
		table.insert(elements, {label = 'Ouvrir le coffre',		value = 'menuperso_vehicule_ouvrirportes_ouvrircoffre'})
	end
	
	if porteAutre1Ouvert then
		table.insert(elements, {label = 'Fermer autre 1',	value = 'menuperso_vehicule_fermerportes_fermerAutre1'})
	else
		table.insert(elements, {label = 'Ouvrir autre 1',		value = 'menuperso_vehicule_ouvrirportes_ouvrirAutre1'})
	end
	
	if porteAutre2Ouvert then
		table.insert(elements, {label = 'Fermer autre 2',	value = 'menuperso_vehicule_fermerportes_fermerAutre2'})
	else
		table.insert(elements, {label = 'Ouvrir autre 2',		value = 'menuperso_vehicule_ouvrirportes_ouvrirAutre2'})
	end
	
	if porteToutOuvert then
		table.insert(elements, {label = 'Tout fermer',	value = 'menuperso_vehicule_fermerportes_fermerTout'})
	else
		table.insert(elements, {label = 'Tout ouvrir',		value = 'menuperso_vehicule_ouvrirportes_ouvrirTout'})
	end

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'menuperso_vehicule',
		{
			img    = 'menu_vehicule',
			title    = 'Véhicule',
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)
--------------------- GESTION MOTEUR
			if data.current.value == 'menuperso_vehicule_MoteurOn' then
				vehiculeON = true
				SetVehicleEngineOn(GetVehiclePedIsIn( GetPlayerPed(-1), false ), true, false, true)
				SetVehicleUndriveable(GetVehiclePedIsIn( GetPlayerPed(-1), false ), false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_MoteurOff' then
				vehiculeON = false
				SetVehicleEngineOn(GetVehiclePedIsIn( GetPlayerPed(-1), false ), false, false, true)
				SetVehicleUndriveable(GetVehiclePedIsIn( GetPlayerPed(-1), false ), true)
				OpenVehiculeMenu()
			end
--------------------- OUVRIR LES PORTES
			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrirportegauche' then
				porteAvantGaucheOuverte = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 0, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrirportedroite' then
				porteAvantDroiteOuverte = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 1, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrirportearrieregauche' then
				porteArriereGaucheOuverte = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 2, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrirportearrieredroite' then
				porteArriereDroiteOuverte = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 3, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrircapot' then
				porteCapotOuvert = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 4, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrircoffre' then
				porteCoffreOuvert = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 5, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrirAutre1' then
				porteAutre1Ouvert = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 6, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrirAutre2' then
				porteAutre2Ouvert = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 7, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_ouvrirportes_ouvrirTout' then
				porteAvantGaucheOuverte = true
				porteAvantDroiteOuverte = true
				porteArriereGaucheOuverte = true
				porteArriereDroiteOuverte = true
				porteCapotOuvert = true
				porteCoffreOuvert = true
				porteAutre1Ouvert = true
				porteAutre2Ouvert = true
				porteToutOuvert = true
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 0, false, false)
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 1, false, false)
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 2, false, false)
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 3, false, false)
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 4, false, false)
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 5, false, false)
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 6, false, false)
				SetVehicleDoorOpen(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 7, false, false)
				OpenVehiculeMenu()
			end
--------------------- FERMER LES PORTES
			if data.current.value == 'menuperso_vehicule_fermerportes_fermerportegauche' then
				porteAvantGaucheOuverte = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 0, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermerportedroite' then
				porteAvantDroiteOuverte = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 1, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermerportearrieregauche' then
				porteArriereGaucheOuverte = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 2, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermerportearrieredroite' then
				porteArriereDroiteOuverte = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 3, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermercapot' then
				porteCapotOuvert = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 4, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermercoffre' then
				porteCoffreOuvert = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 5, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermerAutre1' then
				porteAutre1Ouvert = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 6, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermerAutre2' then
				porteAutre2Ouvert = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 7, false, false)
				OpenVehiculeMenu()
			end

			if data.current.value == 'menuperso_vehicule_fermerportes_fermerTout' then
				porteAvantGaucheOuverte = false
				porteAvantDroiteOuverte = false
				porteArriereGaucheOuverte = false
				porteArriereDroiteOuverte = false
				porteCapotOuvert = false
				porteCoffreOuvert = false
				porteAutre1Ouvert = false
				porteAutre2Ouvert = false
				porteToutOuvert = false
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 0, false, false)
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 1, false, false)
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 2, false, false)
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 3, false, false)
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 4, false, false)
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 5, false, false)
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 6, false, false)
				SetVehicleDoorShut(GetVehiclePedIsIn( GetPlayerPed(-1), false ), 7, false, false)
				OpenVehiculeMenu()
			end
			
		end,
		function(data, menu)
			menu.close()
		end
	)
end

---------------------------------------------------------------------------Modération

-- GOTO JOUEUR
function admin_tp_toplayer()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez l'ID du joueur...")
	inputgoto = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputgoto == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputgoto = 0
			elseif UpdateOnscreenKeyboard() == 1 then
					inputgoto = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputgoto = 0
			end
		end
		if inputgoto == 2 then
			local gotoply = GetOnscreenKeyboardResult()
			local playerPed = GetPlayerPed(-1)
			local teleportPed = GetEntityCoords(GetPlayerPed(GetPlayerFromServerId(tonumber(gotoply))))
			SetEntityCoords(playerPed, teleportPed)
			
			inputgoto = 0
		end
	end
end)
-- FIN GOTO JOUEUR

-- TP UN JOUEUR A MOI
function admin_tp_playertome()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez l'ID du joueur...")
	inputteleport = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputteleport == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputteleport = 0
			elseif UpdateOnscreenKeyboard() == 1 then
				inputteleport = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputteleport = 0
			end
		end
		if inputteleport == 2 then
			local teleportply = GetOnscreenKeyboardResult()
			local playerPed = GetPlayerFromServerId(tonumber(teleportply))
			local teleportPed = GetEntityCoords(GetPlayerPed(-1))
			SetEntityCoords(playerPed, teleportPed)
			
			inputteleport = 0
		end
	end
end)
-- FIN TP UN JOUEUR A MOI

-- TP A POSITION
function admin_tp_pos()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez la position...")
	inputpos = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputpos == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputpos = 0
			elseif UpdateOnscreenKeyboard() == 1 then
					inputpos = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputpos = 0
			end
		end
		if inputpos == 2 then
			local pos = GetOnscreenKeyboardResult() -- GetOnscreenKeyboardResult RECUPERE LA POSITION RENTRER PAR LE JOUEUR
			local _,_,x,y,z = string.find( pos or "0,0,0", "([%d%.]+),([%d%.]+),([%d%.]+)" )
			
			--SetEntityCoords(GetPlayerPed(-1), x, y, z)
		    SetEntityCoords(GetPlayerPed(-1), x+0.0001, y+0.0001, z+0.0001) -- TP LE JOUEUR A LA POSITION
			inputpos = 0
		end
	end
end)
-- FIN TP A POSITION

-- FONCTION NOCLIP 
local noclip = false
local noclip_speed = 1.0

function admin_no_clip()
  noclip = not noclip
  local ped = GetPlayerPed(-1)
  if noclip then -- activé
    SetEntityInvincible(ped, true)
    SetEntityVisible(ped, false, false)
	Notify("Noclip ~g~activé")
  else -- désactivé
    SetEntityInvincible(ped, false)
    SetEntityVisible(ped, true, false)
	Notify("Noclip ~r~désactivé")
  end
end

function getPosition()
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
  return x,y,z
end

function getCamDirection()
  local heading = GetGameplayCamRelativeHeading()+GetEntityHeading(GetPlayerPed(-1))
  local pitch = GetGameplayCamRelativePitch()

  local x = -math.sin(heading*math.pi/180.0)
  local y = math.cos(heading*math.pi/180.0)
  local z = math.sin(pitch*math.pi/180.0)

  local len = math.sqrt(x*x+y*y+z*z)
  if len ~= 0 then
    x = x/len
    y = y/len
    z = z/len
  end

  return x,y,z
end

function isNoclip()
  return noclip
end

-- noclip/invisible
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    if noclip then
      local ped = GetPlayerPed(-1)
      local x,y,z = getPosition()
      local dx,dy,dz = getCamDirection()
      local speed = noclip_speed

      -- reset du velocity
      SetEntityVelocity(ped, 0.0001, 0.0001, 0.0001)

      -- aller vers le haut
      if IsControlPressed(0,32) then -- MOVE UP
        x = x+speed*dx
        y = y+speed*dy
        z = z+speed*dz
      end

      -- aller vers le bas
      if IsControlPressed(0,269) then -- MOVE DOWN
        x = x-speed*dx
        y = y-speed*dy
        z = z-speed*dz
      end

      SetEntityCoordsNoOffset(ped,x,y,z,true,true,true)
    end
  end
end)
-- FIN NOCLIP

-- GOD MODE
function admin_godmode()
  godmode = not godmode
  local ped = GetPlayerPed(-1)
  
  if godmode then -- activé
		SetEntityInvincible(ped, true)
		Notify("Mode invincible ~g~activé")
	else
		SetEntityInvincible(ped, false)
		Notify("Mode invincible ~r~désactivé")
  end
end
-- FIN GOD MODE

-- INVISIBLE
function admin_mode_fantome()
  invisible = not invisible
  local ped = GetPlayerPed(-1)
  
  if invisible then -- activé
		SetEntityVisible(ped, false, false)
		Notify("Mode fantôme : activé")
	else
		SetEntityVisible(ped, true, false)
		Notify("Mode fantôme : désactivé")
  end
end
-- FIN INVISIBLE

-- Réparer vehicule
function admin_vehicle_repair()

    local ped = GetPlayerPed(-1)
    local car = GetVehiclePedIsUsing(ped)
	
		SetVehicleFixed(car)
		SetVehicleDirtLevel(car, 0.0)

end
-- FIN Réparer vehicule

-- Spawn vehicule
function admin_vehicle_spawn()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez le nom du véhicule...")
	inputvehicle = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputvehicle == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputvehicle = 0
			elseif UpdateOnscreenKeyboard() == 1 then
					inputvehicle = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputvehicle = 0
			end
		end
		if inputvehicle == 2 then
		local vehicleidd = GetOnscreenKeyboardResult()

				local car = GetHashKey(vehicleidd)
				
				Citizen.CreateThread(function()
					Citizen.Wait(10)
					RequestModel(car)
					while not HasModelLoaded(car) do
						Citizen.Wait(0)
					end
                    local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
					veh = CreateVehicle(car, x,y,z, 0.0, true, false)
					SetEntityVelocity(veh, 2000)
					SetVehicleOnGroundProperly(veh)
					SetVehicleHasBeenOwnedByPlayer(veh,true)
					local id = NetworkGetNetworkIdFromEntity(veh)
					SetNetworkIdCanMigrate(id, true)
					SetVehRadioStation(veh, "OFF")
					SetPedIntoVehicle(GetPlayerPed(-1),  veh,  -1)
					Notify("Véhicule livré, bonne route")
				end)
		
        inputvehicle = 0
		end
	end
end)
-- FIN Spawn vehicule

-- flipVehicle
function admin_vehicle_flip()

    local player = GetPlayerPed(-1)
    posdepmenu = GetEntityCoords(player)
    carTargetDep = GetClosestVehicle(posdepmenu['x'], posdepmenu['y'], posdepmenu['z'], 10.0,0,70)
	if carTargetDep ~= nil then
			platecarTargetDep = GetVehicleNumberPlateText(carTargetDep)
	end
    local playerCoords = GetEntityCoords(GetPlayerPed(-1))
    playerCoords = playerCoords + vector3(0, 2, 0)
	
	SetEntityCoords(carTargetDep, playerCoords)
	
	Notify("Voiture retourné")

end
-- FIN flipVehicle

-- GIVE DE L'ARGENT
function admin_give_money()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez le montant à vous octroyer...")
	inputmoney = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputmoney == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputmoney = 0
			elseif UpdateOnscreenKeyboard() == 1 then
					inputmoney = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputmoney = 0
			end
		end
		if inputmoney == 2 then
			local repMoney = GetOnscreenKeyboardResult()
			local money = tonumber(repMoney)
			
			TriggerServerEvent('AdminMenu:giveCash', money)
			inputmoney = 0
		end
	end
end)
-- FIN GIVE DE L'ARGENT

-- GIVE DE L'ARGENT EN BANQUE
function admin_give_bank()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez le montant à vous octroyer...")
	inputmoneybank = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputmoneybank == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputmoneybank = 0
			elseif UpdateOnscreenKeyboard() == 1 then
					inputmoneybank = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputmoneybank = 0
			end
		end
		if inputmoneybank == 2 then
			local repMoney = GetOnscreenKeyboardResult()
			local money = tonumber(repMoney)
			
			TriggerServerEvent('AdminMenu:giveBank', money)
			inputmoneybank = 0
		end
	end
end)
-- FIN GIVE DE L'ARGENT EN BANQUE

-- GIVE DE L'ARGENT SALE
function admin_give_dirty()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez le montant à vous octroyer...")
	inputmoneydirty = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputmoneydirty == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputmoneydirty = 0
			elseif UpdateOnscreenKeyboard() == 1 then
					inputmoneydirty = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputmoneydirty = 0
			end
		end
		if inputmoneydirty == 2 then
			local repMoney = GetOnscreenKeyboardResult()
			local money = tonumber(repMoney)
			
			TriggerServerEvent('AdminMenu:giveDirtyMoney', money)
			inputmoneydirty = 0
		end
	end
end)
-- FIN GIVE DE L'ARGENT SALE

-- Afficher Coord
function modo_showcoord()
	if showcoord then
		showcoord = false
	else
		showcoord = true
	end
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
		
		if showcoord then
			local playerPos = GetEntityCoords(GetPlayerPed(-1))
			local playerHeading = GetEntityHeading(GetPlayerPed(-1))
			Text("~r~X~s~: " ..playerPos.x.." ~b~Y~s~: " ..playerPos.y.." ~g~Z~s~: " ..playerPos.z.." ~y~Angle~s~: " ..playerHeading.."")
		end
		
	end
end)
-- FIN Afficher Coord

-- Afficher Nom
function modo_showname()
	if showname then
		showname = false
	else
		Notify("Ouvrir/Fermer le menu pause pour afficher les noms")
		showname = true
	end
end

Citizen.CreateThread(function()
	while true do
		Wait( 1 )
		if showname then
			for id = 0, 200 do
				if NetworkIsPlayerActive( id ) and GetPlayerPed( id ) ~= GetPlayerPed( -1 ) then
					ped = GetPlayerPed( id )
					blip = GetBlipFromEntity( ped )
					headId = Citizen.InvokeNative( 0xBFEFE3321A3F5015, ped, (GetPlayerServerId( id )..' - '..GetPlayerName( id )), false, false, "", false )
				end
			end
		else
			for id = 0, 200 do
				if NetworkIsPlayerActive( id ) and GetPlayerPed( id ) ~= GetPlayerPed( -1 ) then
					ped = GetPlayerPed( id )
					blip = GetBlipFromEntity( ped )
					headId = Citizen.InvokeNative( 0xBFEFE3321A3F5015, ped, (' '), false, false, "", false )
				end
			end
		end
	end
end)
-- FIN Afficher Nom

-- TP MARCKER
function admin_tp_marcker()
	
	ESX.TriggerServerCallback('NB:getUsergroup', function(group)
		playergroup = group
		
		if playergroup == 'admin' or playergroup == 'superadmin' or playergroup == 'owner' then
			local playerPed = GetPlayerPed(-1)
			local WaypointHandle = GetFirstBlipInfoId(8)
			if DoesBlipExist(WaypointHandle) then
				local coord = Citizen.InvokeNative(0xFA7C7F0AADF25D09, WaypointHandle, Citizen.ResultAsVector())
				--SetEntityCoordsNoOffset(playerPed, coord.x, coord.y, coord.z, false, false, false, true)
				SetEntityCoordsNoOffset(playerPed, coord.x, coord.y, -199.5, false, false, false, true)
				Notify("Téléporté sur le marqueur !")
			else
				Notify("Pas de marqueur sur la carte !")
			end
		end
		
	end)
end
-- FIN TP MARCKER

-- HEAL JOUEUR
function admin_heal_player()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez l'ID du joueur...")
	inputheal = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputheal == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputheal = 0
			elseif UpdateOnscreenKeyboard() == 1 then
				inputheal = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputheal = 0
			end
		end
		if inputheal == 2 then
		local healply = GetOnscreenKeyboardResult()
		TriggerServerEvent('esx_ambulancejob:revive', healply)
		
        inputheal = 0
		end
	end
end)
-- FIN HEAL JOUEUR

-- SPEC JOUEUR
function admin_spec_player()
	DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 120)
	Notify("~b~Entrez l'ID du joueur...")
	inputspec = 1
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if inputspec == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				inputspec = 0
			elseif UpdateOnscreenKeyboard() == 1 then
					inputspec = 2
			elseif UpdateOnscreenKeyboard() == 2 then
				inputspec = 0
			end
		end
		if inputspec == 2 then
		local target = GetOnscreenKeyboardResult()
		
		TriggerEvent('es_camera:spectate', source, target)
		
        inputspec = 0
		end
	end
end)
-- FIN SPEC JOUEUR

---------------------------------------------------------------------------Me concernant
--[[
function openFacture()
	TriggerEvent('NB:closeAllSubMenu')
	TriggerEvent('NB:closeAllMenu')
	TriggerEvent('NB:closeMenuKey')
	
	TriggerEvent('NB:openMenuFactures')
end
]]
---------------------------------------------------------------------------Actions

local playAnim = false
local dataAnim = {}

function animsAction(animObj)
	if (IsInVehicle()) then
		local source = GetPlayerServerId();
		ESX.ShowNotification("Sortez de votre véhicule pour faire cela !")
	else
		Citizen.CreateThread(function()
			if not playAnim then
				local playerPed = GetPlayerPed(-1);
				if DoesEntityExist(playerPed) then -- Ckeck if ped exist
					dataAnim = animObj

					-- Play Animation
					RequestAnimDict(dataAnim.lib)
					while not HasAnimDictLoaded(dataAnim.lib) do
						Citizen.Wait(0)
					end
					if HasAnimDictLoaded(dataAnim.lib) then
						local flag = 0
						if dataAnim.loop ~= nil and dataAnim.loop then
							flag = 1
						elseif dataAnim.move ~= nil and dataAnim.move then
							flag = 49
						end

						TaskPlayAnim(playerPed, dataAnim.lib, dataAnim.anim, 8.0, -8.0, -1, flag, 0, 0, 0, 0)
						playAnimation = true
					end

					-- Wait end annimation
					while true do
						Citizen.Wait(0)
						if not IsEntityPlayingAnim(playerPed, dataAnim.lib, dataAnim.anim, 3) then
							playAnim = false
							TriggerEvent('ft_animation:ClFinish')
							break
						end
					end
				end -- end ped exist
			end
		end)
	end
end
	

function animsActionScenario(animObj)
	if (IsInVehicle()) then
		local source = GetPlayerServerId();
		ESX.ShowNotification("Sortez de votre véhicule pour faire cela !")
	else
		Citizen.CreateThread(function()
			if not playAnim then
				local playerPed = GetPlayerPed(-1);
				if DoesEntityExist(playerPed) then
					dataAnim = animObj
					TaskStartScenarioInPlace(playerPed, dataAnim.anim, 0, false)
					playAnimation = true
				end
			end
		end)
	end
end

-- Verifie si le joueurs est dans un vehicule ou pas
function IsInVehicle()
	local ply = GetPlayerPed(-1)
	if IsPedSittingInAnyVehicle(ply) then
		return true
	else
		return false
	end
end

function changer_skin()
	TriggerEvent('esx_skin:openSaveableMenu', source)
end

function save_skin()
	TriggerEvent('esx_skin:requestSaveSkin', source)
end

--------------Carte d'identité-------------------

function GetPlayers()
    local players = {}

    for i = 0, 31 do
        if NetworkIsPlayerActive(i) then
            table.insert(players, i)
        end
    end

    return players
end

function GetClosestPlayer()
  local players = GetPlayers()
  local closestDistance = -1
  local closestPlayer = -1
  local ply = GetPlayerPed(-1)
  local plyCoords = GetEntityCoords(ply, 0)
  
  for index,value in ipairs(players) do
    local target = GetPlayerPed(value)
    if(target ~= ply) then
      local targetCoords = GetEntityCoords(GetPlayerPed(value), 0)
      local distance = GetDistanceBetweenCoords(targetCoords["x"], targetCoords["y"], targetCoords["z"], plyCoords["x"], plyCoords["y"], plyCoords["z"], true)
      if(closestDistance == -1 or closestDistance > distance) then
        closestPlayer = value
        closestDistance = distance
      end
    end
  end
  
  return closestPlayer, closestDistance
end

---------------------------------------------------------------------------------------------------------
--NB : gestion des menu
---------------------------------------------------------------------------------------------------------

RegisterNetEvent('NB:goTpMarcker')
AddEventHandler('NB:goTpMarcker', function()
	admin_tp_marcker()
end)

RegisterNetEvent('NB:openMenuPersonnel')
AddEventHandler('NB:openMenuPersonnel', function()
	OpenPersonnelMenu()
end)

---------------------------------------------------------------------------------------------------------
-- Gestion des clés de véhicules
---------------------------------------------------------------------------------------------------------

function GiveKeyAtPlayer(key)
	local key = key
	local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
	local closestPlayer1 = (GetPlayerServerId(closestPlayer))
	if closestPlayer ~= nil and key ~= nil and closestDistance < 3.0 then
		TriggerServerEvent('esx:givekey', closestPlayer1,key)
		ESX.ShowNotification('Vous avez donné des clés')
	else
		ESX.ShowNotification('Aucun joueur à proximité')
	end
end

RegisterNetEvent("esx:returnkey")
AddEventHandler("esx:returnkey",function (newkey)
	local i = #key
	local duplicatekey = false
	while i>=0 do
		Wait(50)
		if newkey == key[i] then
			duplicatekey = true
			break
		end
		i = i -1
	end
	if duplicatekey == false then
		table.insert(key,newkey)
		Notify("Vous avez recu une clé de voiture ~r~("..newkey..")")
		TriggerEvent('giveKeyAtKeyMaster', newkey)
	end
end)

AddEventHandler('esx_key:giveVehiclesKey', function(plate)
	local x  = #plate
	Notify(plate[x])
 	table.insert(key, plate[x])
end)