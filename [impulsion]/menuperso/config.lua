local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
Config								= {}

--------------------------------------------------------------------------------------------
-- Config
--------------------------------------------------------------------------------------------
-- GENERAL
Config.general = {
	manettes = false,
}
-- GPS RAPIDE
Config.comico = {
	x = 425.130,
	y = -979.558,
	z = 30.711,
}
Config.hopital = {
	x = 307.76,
	y = -1433.47,
	z = 28.97,
}
Config.concessionnaire = {
	x = -33.777,
	y = -1102.021,
	z = 25.422,
}
Config.depanneur = {
	x = -347.291,
	y = -133.370,
	z = 38.009,
}
Config.manoir = {
	x = -126.66,
	y = 377.79,
	z = 112.76,
}
Config.garage = {
	x = -796.4,
	y = 315.68,
	z = 85.67,
}
-- Ouvrir le menu perso
Config.menuperso = {
	clavier = Keys["F1"],
}
-- Ouvrir le menu job
Config.menujob = {
	clavier = Keys["F6"],
}
-- TP sur le Marker
Config.TPMarker = {
	clavier1 = Keys["LEFTSHIFT"],
	clavier2 = Keys["E"],
	manette1 = Keys["LEFTSHIFT"],
	manette2 = Keys["E"],
}
-- Lever les mains
Config.handsUP = {
	clavier = Keys["X"],
}
-- Pointer du doight
Config.pointing = {
	clavier = Keys["B"],
	manette = Keys["B"],
}
-- S'acroupir
Config.crouch = {
	clavier = Keys["F3"],
	manette1 = Keys["F3"],
}