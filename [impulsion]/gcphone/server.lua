--====================================================================================
-- #Author: Jonathan D @Gannon
--====================================================================================
--====================================================================================
--  
--====================================================================================

local serviceplayer = {}
--====================================================================================
--  Utils
--====================================================================================
ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function getPhoneRandomNumber()
local foundNumber = false
    local phoneNumber = nil

    while not foundNumber do

        phoneNumber = math.random(10000, 99998)
        local result = MySQL.Sync.fetchAll(
            'SELECT COUNT(*) as count FROM users WHERE phone_number = @phoneNumber',
            {
                ['@phoneNumber'] = phoneNumber
            }
        )

        local count  = tonumber(result[1].count)

        if count == 0 then
            foundNumber = true
        end

    end

    return phoneNumber
end
function getSourceFromIdentifier(identifier, cb)
    TriggerEvent("es:getPlayers", function(users)
        for k , user in pairs(users) do
            if user.getIdentifier() == identifier then
                cb(k)
                return
            end
        end
    end)
    cb(nil)
end
function getNumberPhone(identifier)
    local result = MySQL.Sync.fetchAll("SELECT users.phone_number FROM users WHERE users.identifier = @identifier", {
        ['@identifier'] = identifier
    })
    if result[1] ~= nil then
        return result[1].phone_number
    end
    return nil
end

function getIdentifierByPhoneNumber(phone_number)
    local result = MySQL.Sync.fetchAll("SELECT users.identifier FROM users WHERE users.phone_number = @phone_number", {
        ['@phone_number'] = phone_number
    })
    if result[1] ~= nil then
        return result[1].identifier
    end
    return nil
end
--====================================================================================
--  Contacts
--====================================================================================
function getContacts(identifier)
    local result = MySQL.Sync.fetchAll("SELECT phone_users_contacts.id, phone_users_contacts.number, phone_users_contacts.display FROM phone_users_contacts WHERE phone_users_contacts.identifier = @identifier", {
        ['@identifier'] = identifier
    })
    return result
end

function addContact(source, identifier, number, display)
    local sourcePlayer = tonumber(source)
    print(number .. ' ' ..  display)
    MySQL.Sync.execute("INSERT INTO phone_users_contacts (`identifier`, `number`,`display`) VALUES(@identifier, @number, @display)", {
        ['@identifier'] = identifier,
        ['@number'] = number,
        ['@display'] = display,
    })
    notifyContactChange(sourcePlayer, identifier)
end

function updateContact(source, identifier, id, number, display)
    local sourcePlayer = tonumber(source)
    MySQL.Sync.execute("UPDATE phone_users_contacts SET number = @number, display = @display WHERE id = @id", { 
        ['@number'] = number,
        ['@display'] = display,
        ['@id'] = id,
    })
    notifyContactChange(sourcePlayer, identifier)
end

function deleteContact(source, identifier, id)
    local sourcePlayer = tonumber(source)
    MySQL.Sync.execute("DELETE FROM phone_users_contacts WHERE `identifier` = @identifier AND `id` = @id", {
        ['@identifier'] = identifier,
        ['@id'] = id,
    })
    notifyContactChange(sourcePlayer, identifier)
end

function deleteAllContact(identifier)
    MySQL.Sync.execute("DELETE FROM phone_users_contacts WHERE `identifier` = @identifier", {
        ['@identifier'] = identifier
    })
end

function notifyContactChange(source, identifier)
    local sourcePlayer = tonumber(source)
    if sourcePlayer ~= nil then 
        TriggerClientEvent("gcPhone:contactList", sourcePlayer, getContacts(identifier))
    end
end

RegisterServerEvent('gcPhone:addContact')
AddEventHandler('gcPhone:addContact', function(display, phoneNumber)
    local sourcePlayer = tonumber(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    addContact(sourcePlayer, identifier, phoneNumber, display)
end)

RegisterServerEvent('gcPhone:updateContact')
AddEventHandler('gcPhone:updateContact', function(id, display, phoneNumber)
    local sourcePlayer = tonumber(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    updateContact(sourcePlayer, identifier, id, phoneNumber, display)
end)

RegisterServerEvent('gcPhone:deleteContact')
AddEventHandler('gcPhone:deleteContact', function(id)
    local sourcePlayer = tonumber(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    deleteContact(sourcePlayer, identifier, id)
end)
-- esx adaptation
RegisterServerEvent('call:callService')
AddEventHandler('call:callService', function(service, coord, text, player, anonymous)
    local sourcePlayer = tonumber(source)
    local identifier = nil
    if anonymous == false then
        identifier = GetPlayerIdentifiers(source)[1]
    else
        identifier = 'anonymous'
    end
    for i = 1,#serviceplayer,1 do
        if serviceplayer[i].job == service then
            --print(text)
            --print(serviceplayer[i].number)
            if text ~= nil then
                addMessageService(sourcePlayer, identifier, serviceplayer[i].number, os.date('%d/%m %H:%M', os.time()) .. ' ' .. text)
            end
            if coord ~= nil then
                addMessageService(sourcePlayer, identifier, serviceplayer[i].number, coord)
            end
            --addMessage(sourcePlayer, identifier, serviceplayer[1].number, coord)
            --print('send to'..serviceplayer[i].number)
        end
    end
end)

--====================================================================================
--  Messages
--====================================================================================
function getMessages(identifier)
    local result = MySQL.Sync.fetchAll("SELECT phone_messages.* FROM phone_messages LEFT JOIN users ON users.identifier = @identifier WHERE phone_messages.receiver = users.phone_number", {
         ['@identifier'] = identifier
    })
    -- A CHANGER !!!!!!!
    --for k, v in ipairs(result) do  
        --v.time = os.time(v.time) + math.floor(0) - 2*60*60
    --end
    return result
    --return MySQLQueryTimeStamp("SELECT phone_messages.* FROM phone_messages LEFT JOIN users ON users.identifier = @identifier WHERE phone_messages.receiver = users.phone_number", {['@identifier'] = identifier})
end

local MessagesToAdd = {}

AddEventHandler('playerDropped', function()
    local sourcePlayer = tonumber(source)
    if source ~= nil then
        for k,v in pairs(MessagesToAdd)do
            if MessagesToAdd[k] ~= nil then
                MySQL.Sync.execute("INSERT INTO phone_messages (`transmitter`, `receiver`, `message`, `time`, `isRead`, `owner`) VALUES(@transmitter, @receiver, @message, @time, @isRead, @owner)", {
                    ['@transmitter'] = MessagesToAdd[k].transmitter,
                    ['@receiver'] = MessagesToAdd[k].receiver,
                    ['@message'] = MessagesToAdd[k].message,
                    ['@time'] = MessagesToAdd[k].time,
                    ['@isRead'] = MessagesToAdd[k].owner,
                    ['@owner'] = MessagesToAdd[k].owner
                })
            end
        end
    end
end)

local function updatePlayerMessages()
    SetTimeout(6000000, function()
        for k,v in pairs(MessagesToAdd)do
            if MessagesToAdd[k] ~= nil then
                MySQL.Sync.execute("INSERT INTO phone_messages (`transmitter`, `receiver`, `message`, `time`, `isRead`, `owner`) VALUES(@transmitter, @receiver, @message, @time, @isRead, @owner)", {
                    ['@transmitter'] = MessagesToAdd[k].transmitter,
                    ['@receiver'] = MessagesToAdd[k].receiver,
                    ['@message'] = MessagesToAdd[k].message,
                    ['@time'] = MessagesToAdd[k].time,
                    ['@isRead'] = MessagesToAdd[k].owner,
                    ['@owner'] = MessagesToAdd[k].owner
                })
            end
        end
        updatePlayerMessages()
    end)
end
updatePlayerMessages()

local lastmessage = {
    transmitter = 0,
    receiver = 0,
    message = 0,
    time = 0,
    isRead = 0,
    owner = 0,
}

function _internalAddMessage(transmitter, receiver, message, owner)
    local Query = "INSERT INTO phone_messages (`transmitter`, `receiver`,`message`, `isRead`,`owner`) VALUES(@transmitter, @receiver, @message, @isRead, @owner);"
    local Query2 = 'SELECT * from phone_messages WHERE `id` = (SELECT LAST_INSERT_ID());'
    local Parameters = {
        ['@transmitter'] = transmitter,
        ['@receiver'] = receiver,
        ['@message'] = message,
        ['@isRead'] = owner,
        ['@owner'] = owner
    }
    return MySQL.Sync.fetchAll(Query .. Query2, Parameters)[1]
end

function addMessage(source, identifier, phone_number, message)
    local sourcePlayer = tonumber(source)
    local otherIdentifier = getIdentifierByPhoneNumber(phone_number)
    local myPhone = getNumberPhone(identifier)
	if not string.match(message, "GPS") then
	message = os.date('%d/%m %H:%M', os.time()) .. ' ' .. message
	end
    if otherIdentifier ~= nil then 
        local tomess = _internalAddMessage(myPhone, phone_number, message, 0)
        getSourceFromIdentifier(otherIdentifier, function (osou)
            if tonumber(osou) ~= nil then 
                -- TriggerClientEvent("gcPhone:allMessage", osou, getMessages(otherIdentifier))
                TriggerClientEvent("gcPhone:receiveMessage", tonumber(osou), tomess)
            end
        end) 
    end
    local memess = _internalAddMessage(phone_number, myPhone, message, 1)
    -- TriggerClientEvent("gcPhone:allMessage", source, getMessages(identifier))
    TriggerClientEvent("gcPhone:receiveMessage", sourcePlayer, memess)

end
function addMessageService(source, identifier, phone_number, message)
    local sourcePlayer = tonumber(source)
    local otherIdentifier = getIdentifierByPhoneNumber(phone_number)
    local myPhone = getNumberPhone(identifier)
    if otherIdentifier ~= nil then 
        local tomess = _internalAddMessage(myPhone, phone_number, message, 0)
        getSourceFromIdentifier(otherIdentifier, function (osou)
            if tonumber(osou) ~= nil then 
                -- TriggerClientEvent("gcPhone:allMessage", osou, getMessages(otherIdentifier))
                TriggerClientEvent("gcPhone:receiveMessage", tonumber(osou),tomess)
            end
        end) 
    end
end

function setReadMessageNumber(identifier, num)
    local mePhoneNumber = getNumberPhone(identifier)
    MySQL.Sync.execute("UPDATE phone_messages SET phone_messages.isRead = 1 WHERE phone_messages.receiver = @receiver AND phone_messages.transmitter = @transmitter", { 
        ['@receiver'] = mePhoneNumber,
        ['@transmitter'] = num
    })
end

function deleteMessage(msgId)
    MySQL.Sync.execute("DELETE FROM phone_messages WHERE `id` = @id", {
        ['@id'] = msgId
    })
end

function deleteAllMessageFromPhoneNumber(identifier, phone_number)
    local mePhoneNumber = getNumberPhone(identifier)
    MySQL.Sync.execute("DELETE FROM phone_messages WHERE `receiver` = @mePhoneNumber and `transmitter` = @phone_number", {
        ['@mePhoneNumber'] = mePhoneNumber,
        ['@phone_number'] = phone_number
    })
end

function deleteAllMessage(identifier)
    local mePhoneNumber = getNumberPhone(identifier)
    MySQL.Sync.execute("DELETE FROM phone_messages WHERE `receiver` = @mePhoneNumber", {
        ['@mePhoneNumber'] = mePhoneNumber
    })
end

RegisterServerEvent('gcPhone:sendMessage')
AddEventHandler('gcPhone:sendMessage', function(phoneNumber, message, anonymous)
    local sourcePlayer = tonumber(source)
    local identifier = nil
    if anonymous ~= true then
        identifier = GetPlayerIdentifiers(source)[1]
    else
        identifier = 'anonymous'
    end
    --print(identifier)
    addMessage(sourcePlayer, identifier, phoneNumber, message)
end)

RegisterServerEvent('gcPhone:deleteMessage')
AddEventHandler('gcPhone:deleteMessage', function(msgId)
    deleteMessage(msgId)
end)

RegisterServerEvent('gcPhone:deleteMessageNumber')
AddEventHandler('gcPhone:deleteMessageNumber', function(number)
    local sourcePlayer = tonumber(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    deleteAllMessageFromPhoneNumber(identifier, number)
    TriggerClientEvent("gcPhone:allMessage", sourcePlayer, getMessages(identifier))
end)

RegisterServerEvent('gcPhone:deleteAllMessage')
AddEventHandler('gcPhone:deleteAllMessage', function()
    local sourcePlayer = tonumber(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    deleteAllMessage(identifier)
    TriggerClientEvent("gcPhone:allMessage", sourcePlayer, getMessages(identifier))
end)

RegisterServerEvent('gcPhone:setReadMessageNumber')
AddEventHandler('gcPhone:setReadMessageNumber', function(num)
    local identifier = GetPlayerIdentifiers(source)[1]
    setReadMessageNumber(identifier, num)
end)

RegisterServerEvent('gcPhone:deleteALL')
AddEventHandler('gcPhone:deleteALL', function()
    local sourcePlayer = tonumber(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    deleteAllMessage(identifier)
    deleteAllContact(identifier)
    TriggerClientEvent("gcPhone:contactList", sourcePlayer, {})
    TriggerClientEvent("gcPhone:allMessage", sourcePlayer, {})
end)
--====================================================================================
--  OnLoad
--====================================================================================
--esx
--AddEventHandler('playerSpawned', function(spawn)
--      local sourcePlayer = tonumber(source)
--    local identifier = GetPlayerIdentifiers(sourcePlayer)[1]
    --if xPlayer.job == "police" then
--     local myPhoneNumber = getNumberPhone(identifier)
--        table.insert(serviceplayer,{job = 'police',number = myPhoneNumber, identifier = identifier})
        
   -- end
--end)

AddEventHandler('es:playerLoaded',function(source)
    local sourcePlayer = tonumber(source)
    local identifier = GetPlayerIdentifiers(sourcePlayer)[1]
    local myPhoneNumber = getNumberPhone(identifier)
    if myPhoneNumber == nil then
       
        local randomNumberPhone = getPhoneRandomNumber()
        print('TryPhone: ' .. randomNumberPhone)
        MySQL.Sync.execute("UPDATE users SET phone_number = @randomNumberPhone WHERE identifier = @identifier", { 
            ['@randomNumberPhone'] = randomNumberPhone,
            ['@identifier'] = identifier
        })
        myPhoneNumber = getNumberPhone(identifier)

    end
    Citizen.CreateThread(function()
        Wait(3000)
        local xPlayer = ESX.GetPlayerFromId(source)
        local job = xPlayer.getJob()
        local found = false
        for i = 1,#serviceplayer do
            if serviceplayer[i].identifier == xPlayer.identifier then
                found = true
            end
        end
        if not found then
             table.insert(serviceplayer,{job = job.name ,number = myPhoneNumber, identifier = identifier})
        end
    end)

    TriggerClientEvent("gcPhone:myPhoneNumber", sourcePlayer, myPhoneNumber)
    TriggerClientEvent("gcPhone:contactList", sourcePlayer, getContacts(identifier))
    TriggerClientEvent("gcPhone:allMessage", sourcePlayer, getMessages(identifier))
end)
AddEventHandler('gcphone:UpdatePlayerJob', function(job,target)
    local xPlayer = ESX.GetPlayerFromId(target)
    for i = 1, #serviceplayer,1 do
        if serviceplayer[i].identifier == xPlayer.identifier then
            serviceplayer[i].job = job
            break
        end
    end
end)

-- Just For reload
RegisterServerEvent('gcphone:allUpdate')
AddEventHandler('gcphone:allUpdate', function()
    local source = source
    local identifier = GetPlayerIdentifiers(source)[1]
    local mess = getMessages(identifier)
    TriggerClientEvent("gcphone:myPhoneNumber", source, getNumberPhone(identifier))
    TriggerClientEvent("gcphone:allMessage", source, getMessages(identifier))
    TriggerClientEvent("gcphone:contactList", source, getContacts(identifier))  
end)
--esx



---------------------
ESX                       = nil

TriggerEvent('esx:getSharedObject', function(obj)
    ESX = obj
end)


ESX.RegisterServerCallback('gcphone:getItemAmount', function(source, cb, item)
    local xPlayer = ESX.GetPlayerFromId(source)
    local qtty = xPlayer.getInventoryItem(item).count
    --print("phone qtty: " .. qtty)
    cb(qtty)
end)
