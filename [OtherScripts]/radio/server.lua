--RegisterServerEvent("chatCommandEntered")
--RegisterServerEvent('chatMessageEntered')

local station={}

AddEventHandler("chatMessage", function(p, color, msg)
    if msg:sub(1,1) == "/" then
        
        if msg:sub(1,6) == "/radio" then
            local arg=msg:sub(8)
        	if arg==nil or arg=='' then
        		TriggerClientEvent("chatMessage", p, "^1Usage", {0, 0, 0}, "^1/radio URL")
	        else
	        	TriggerClientEvent("playradio1", p, arg)
        	end

        	CancelEvent()
        elseif msg:sub(1,7) == "/volume" then
            local arg=msg:sub(9)
        	if arg==nil or arg=='' then
        		TriggerClientEvent("chatMessage", p, "^1Usage", {0, 0, 0}, "^1/volume (0.0 - 1.0)")
	        else
	        	TriggerClientEvent("changevolume1", p, arg)
        	end

        	CancelEvent()
        elseif msg:sub(1,10) == "/stopradio" then
	        TriggerClientEvent("stopradio1", p)

        	CancelEvent()
        end
    end
end)

RegisterServerEvent('playradio2')
AddEventHandler('playradio2',function(name,url)
 TriggerClientEvent('playradio3',-1,name,url)
 if station[name]==nil then
  station[name]={}
 end
 station[name][1]=url
 station[name][3]=true
end)

RegisterServerEvent('changevolume2')
AddEventHandler('changevolume2',function(name,volume)
 TriggerClientEvent('changevolume3',-1,name,volume)
 if(station[name]==nil) then
  station[name]={}
 end
 station[name][2]=volume
end)

RegisterServerEvent('stopradio2')
AddEventHandler('stopradio2',function(name)
 TriggerClientEvent('stopradio3',-1,name)
 if(station[name]==nil) then
  station[name]={}
 end
 station[name][3]=false
end)

RegisterServerEvent('radiostations')
AddEventHandler('radiostations',function()
 TriggerClientEvent('radiostations',source,station)
end)