
-- settings
local maxdist=18.0
local default_url='http://stream.gclef320kbps.us:9406/live.opus'

local default_volume=1.0
local model=GetHashKey('v_club_vu_deckcase')
local coords={
{-1380.41,-615.579,31.4979}, --bahama mamas
--{-561.9308,280.8893,85.5064} --tequilala
}
--model=GetHashKey('prop_radio_01')

-- don't change below this line
local sourceid=0
local source_name=0
local square_distance=0
local station={}
local volume=.0
local playing=false

RegisterNetEvent("playradio1")
AddEventHandler("playradio1", function(url)
    if sourceid~=0 and square_distance<4.0 then
        TriggerServerEvent("playradio2",source_name,url)
    end
end)

RegisterNetEvent("changevolume1")
AddEventHandler("changevolume1", function(volume)
    if sourceid~=0 and square_distance<4.0 then
        TriggerServerEvent("changevolume2",source_name,volume)
    end
end)

RegisterNetEvent("stopradio1")
AddEventHandler("stopradio1", function()
	if sourceid~=0 and square_distance<4.0 then
        TriggerServerEvent("stopradio2",source_name)
    end
end)

RegisterNetEvent("playradio3")
AddEventHandler("playradio3", function(name,url)
    if station[name]==nil then
        station[name]={url,default_volume,true}
    else
        station[name][1]=url
        station[name][3]=true
    end
    if name==source_name then
        sourceid=0
        source_name=0
    end
end)

RegisterNetEvent("changevolume3")
AddEventHandler("changevolume3", function(name,vol)
    if station[name]==nil then
        station[name]={default_url,vol,true}
    else
        station[name][2]=vol
    end
    if name==source_name then
        volume=vol
    end
end)

RegisterNetEvent("stopradio3")
AddEventHandler("stopradio3", function(name)
	if station[name]==nil then
        station[name]={default_url,default_volume,false}
    else
        station[name][3]=false
    end
    if name==source_name then
        sourceid=0
        source_name=0
    end
end)

RegisterNetEvent("radiostations")
AddEventHandler("radiostations", function(stations)
  for k,v in pairs(stations) do
	if station[k]==nil then
        station[k]={default_url,default_volume,true}
    end
    if v[1]~=nil then station[k][1]=v[1] end
    if v[2]~=nil then station[k][2]=v[2] end
    if v[3]~=nil then station[k][3]=v[3] end
  end
end)

-- local function spawn_radio()
    -- local ped=GetPlayerPed(-1)
    -- local pos=GetEntityCoords(ped)
    -- RequestModel(model)
    -- while not HasModelLoaded(model) do
        -- Wait(0)
    -- end
    -- CreateObject(model,pos.x,pos.y,pos.z, false, false, false)
    -- SetModelAsNoLongerNeeded(model)
-- end

Citizen.CreateThread(function()
    --SetNuiFocus(false,false)
    Wait(2000)
    TriggerServerEvent('radiostations')
    while true do
        Wait(0)
        if IsControlJustPressed(0,86) then
            if sourceid==0 or square_distance>25.0 then
                --spawn_radio()
            elseif square_distance<4.0 then
                SendNUIMessage({
                    showgui = true
                })
                SetNuiFocus(true,true)
            end
        end
    end
end)

local maxdistance_square=maxdist*maxdist
local function calculate_volume(volume,sqd)
    local vol=volume*(1.0-math.sqrt(sqd/maxdistance_square))
    if vol<.0 then
        return .0
    elseif vol>1.0 then
        return 1.0
    else
        return vol
    end
end
local function change_volume(volume)
    SendNUIMessage({
        changevolume = true,
        volume = volume
    })
end
local function change_url(url,volume)
    SendNUIMessage({
        playradio = true,
        sound = url,
        volume = volume
    })
end
local function stop_radio()
    SendNUIMessage({
        stopradio = true
    })
end

RegisterNUICallback('volume', function(data, cb)
  TriggerServerEvent("changevolume2",source_name,data.volume)
  cb('ok')
end)

RegisterNUICallback('play', function(data, cb)
  TriggerServerEvent("playradio2",source_name,data.url)
  cb('ok')
end)

RegisterNUICallback('stop', function(data, cb)
  TriggerServerEvent("stopradio2",source_name)
  cb('ok')
end)

RegisterNUICallback('close', function(data, cb)
  SetNuiFocus(false,false)
  SendNUIMessage({
    hidegui = true
  })
  cb('ok')
end)

-- local entityEnumerator = {
  -- __gc = function(enum)
    -- if enum.destructor and enum.handle then
      -- enum.destructor(enum.handle)
    -- end
    -- enum.destructor = nil
    -- enum.handle = nil
  -- end
-- }

-- local function EnumerateEntities(initFunc, moveFunc, disposeFunc)
  -- return coroutine.wrap(function()
    -- local iter, id = initFunc()
    -- if not id or id == 0 then
      -- disposeFunc(iter)
      -- return
    -- end
    
    -- local enum = {handle = iter, destructor = disposeFunc}
    -- setmetatable(enum, entityEnumerator)
    
    -- local next = true
    -- repeat
      -- coroutine.yield(id)
      -- next, id = moveFunc(iter)
    -- until not next
    
    -- enum.destructor, enum.handle = nil, nil
    -- disposeFunc(iter)
  -- end)
-- end

-- local function EnumerateObjects()
  -- return EnumerateEntities(FindFirstObject, FindNextObject, EndFindObject)
-- end

-- local function trytofind(hash)
    -- for obj in EnumerateObjects() do
        -- if GetEntityModel(obj)==hash then
            -- return obj
        -- end
    -- end
    -- return 0
-- end
local function GetClosestRadio(x,y,z)
    local ret=0
    local sqdist=maxdistance_square
    local rx,ry,rz
    local dx,dy,dz,sqd
    for k,v in pairs(coords) do
        dx,dy,dz=x-v[1],y-v[2],z-v[3]
        sqd=dx*dx+dy*dy+dz*dz
        if sqd<sqdist then
            sqdist=sqd
            ret=k
        end
    end
    if ret==0 then
        local prop=GetClosestObjectOfType(x,y,z,maxdist,model,false, false, false)
        if prop==0 then
            return 0,sqdist,.0,.0,.0
        else
            local pos=GetEntityCoords(prop)
            dx,dy,dz=x-pos.x,y-pos.y,z-pos.z
            sqd=dx*dx+dy*dy+dz*dz
            return prop,sqd,pos.x,pos.y,pos.z
        end
    else
        rx,ry,rz=table.unpack(coords[ret])
        local prop=GetClosestObjectOfType(x,y,z,math.sqrt(sqdist),model,false, false, false)
        if prop~=0 then
            local pos=GetEntityCoords(prop)
            dx,dy,dz=x-pos.x,y-pos.y,z-pos.z
            sqd=dx*dx+dy*dy+dz*dz
            if sqd<sqdist then
                ret=prop
                sqdist=sqd
                rx,ry,rz=pos.x,pos.y,pos.z
            end
        end
    end
    return ret,sqdist,rx,ry,rz
end

Citizen.CreateThread(function()
    local url=''
    while true do
        Wait(100)
        local ped=GetPlayerPed(-1)
        local pos=GetEntityCoords(ped)
        local srcid,sqd,x,y,z=GetClosestRadio(pos.x,pos.y,pos.z)
        if srcid==0 then
            if sourceid~=0 then
                sourceid=0
                print('stopped '..url)
                Citizen.CreateThread(stop_radio)
                url=''
                volume=.0
                source_name=0
                playing=false
            end
        else
            local restart=false
            square_distance=sqd
            if sourceid~=srcid then
                sourceid=srcid
                local new_name=4096*math.floor(x/8+1000)+256*math.floor(y/8+1000)+math.floor(z/8)
                if source_name~=new_name then
                    source_name=new_name
                    local s=station[source_name]
                    if s==nil then
                        station[source_name]={default_url,default_volume,true}
                        s=station[source_name]
                    end
                    if playing~=s[3] or url~=s[1] then
                        url=s[1]
                        playing=s[3]
                        restart=true
                    end
                    volume=s[2]
                end
            end
            if restart then
                if playing then
                    print('starting '..url..' volume '..volume)
                    local vol=calculate_volume(volume,sqd)
                    Citizen.CreateThread(function()change_url(url,vol)end)
                else
                    print('stoped '..url..' volume '..volume)
                    Citizen.CreateThread(stop_radio)
                end
            else
                --print('changing volume '..volume)
                local vol=calculate_volume(volume,sqd)
                Citizen.CreateThread(function()change_volume(vol)end)
            end
        end
    end
end)