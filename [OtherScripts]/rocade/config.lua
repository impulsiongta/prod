steam_err			= "Vous n'avez pas lancé Steam."
whitelist_err		= "Vous n'êtes pas autorisé."
authorized_err      = "Vous n'êtes pas autorisé.\r\n\r\nPour demander l'autorisation : https://impulsiongtavrp.fr"
waiting_str			= "Vous êtes sur la rocade.\r\n\r\nPour connâitre votre position : https://impulsiongtavrp.fr"
rocade_prio_time	= 300	-- Nombre de secondes 
crash_prio_time		= 180	-- Nombre de secondes pendants lesquelles le joueurs garde sa place IG
check_player_time	= 3	-- Nombre de secondes pour vérifier le nombre de joueurs en ligne

print_debug_console	= false	-- Affiche la rocade dans la console
print_debug_file	= true	-- Affiche la rocade dans un fichier
debug_filename		= "rocade.log"

rocade_trace		= "[" .. os.date('%H:%M:%S', os.time()) .. "] [rocade] "

version				= "1.2.0"