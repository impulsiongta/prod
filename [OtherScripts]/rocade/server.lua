ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

version = '2.0.0'

AddEventHandler('onMySQLReady', function()
	resetOnlinePlayers()
end)

AddEventHandler('onResourceStart', function(resource)
	if resource == 'rocade' then
		Wait(2000)
		updateRocade()
	end
end)

AddEventHandler('onResourceStarting', function(resource)
	if resource == 'rocade' then
		print (rocade_trace.."Version = " .. version)

		if (print_debug_console) then
			print (rocade_trace .. "Print rocade console = "..tostring(print_debug_console))
		end
		if (print_debug_file) then
			print (rocade_trace .. "Print rocade file = "..tostring(print_debug_file))
			print (rocade_trace .. "File name = "..debug_filename)
		end
	end
end)

AddEventHandler('playerDropped', function(reason)
	local _source = source
	local playerName = GetPlayerName(_source)
	local steamID = GetPlayerIdentifiers(_source)[1] or ""
	
	print (rocade_trace .. playerName .. " [" .. steamID .. "] s'est deconnecte.")
	
	-- Mise à jour de la BDD
	MySQL.Sync.execute("UPDATE user_whitelist SET online = 0 WHERE identifier=@identifier", {
		['@identifier'] = steamID
	})
	
	-- Si la raison n'est pas une déconnexion
	-- if(reason ~= "Disconnected." and reason ~= "Exiting.") then
		print (rocade_trace .. playerName .. " [" .. steamID .. "] s'est deconnecte : " .. reason)
		
		-- MySQL.Sync.execute("INSERT INTO rocade (identifier, name, status, timeout) VALUES (@identifier, @name, 'crash', DATE_ADD(NOW(), INTERVAL " .. crash_prio_time .. " SECOND)) ON DUPLICATE KEY UPDATE status='crash', timeout=DATE_ADD(NOW(), INTERVAL " .. crash_prio_time .. " SECOND)", {
			-- ['@identifier'] = steamID,
			-- ['@name'] = playerName
		-- })
		
		MySQL.Sync.execute("UPDATE rocade SET status='crash', timeout=DATE_ADD(NOW(), INTERVAL " .. crash_prio_time .. " SECOND) WHERE identifier=@identifier", {
			['@identifier'] = steamID
		})
	-- end
end)

AddEventHandler("playerConnecting", function(playerName, reason, deferrals)
	local _source = source
	local steamID = GetPlayerIdentifiers(_source)[1] or ""
	local result = nil
	
	-- Check si Steam est lancé
	if string.sub(steamID, 1, 8) == "license:" then
		print (rocade_trace .. playerName .. " [" .. steamID .. "] n'a pas lancé Steam.")
		reason(steam_err)
		deferrals.done(steam_err)
		CancelEvent()
		return
	end
	
	-- Check si le joueur est whitelisté
	result = MySQL.Sync.fetchAll("SELECT COUNT(*) as count FROM user_whitelist WHERE `identifier` = @identifier AND `whitelisted` > 0", {
		['@identifier'] = steamID
	})
	
	if(result[1].count == 0) then
		print (rocade_trace .. playerName .. " [" .. steamID .. "] n'est pas whiteliste.")
		reason(whitelist_err)
		deferrals.done(whitelist_err)
		CancelEvent()
		return
	end
	
	-- Check si le joueur est autorisé
	result = MySQL.Sync.fetchAll("SELECT * FROM rocade WHERE `identifier` = @identifier AND `status` <> 'waiting' AND `timeout` >= NOW()", {
		['@identifier'] = steamID
	})
	
	if(#result == 0) then
		print (rocade_trace .. playerName .. " [" .. steamID .. "] n'est pas autorise.")
		
		
		
		-- result = MySQL.Sync.fetchAll("SELECT IFNULL(MAX(rang), 0) as rang FROM rocade WHERE status='waiting'", {
			-- ['@identifier'] = steamID
		-- })
		
		result = MySQL.Sync.fetchAll("SELECT IFNULL(MAX(rang), 0) as rang FROM rocade", {
			['@identifier'] = steamID
		})
		
		MySQL.Sync.execute("INSERT INTO rocade (rang, identifier, name, status) VALUES ("..(result[1].rang + 1)..",@identifier, @name, 'waiting') ON DUPLICATE KEY UPDATE status='waiting', rang="..(result[1].rang + 1), {
			['@identifier'] = steamID,
			['@name'] = playerName
		})
		
		
		
		reason(authorized_err .. " => ROCADE")
		deferrals.done(authorized_err .. " => ROCADE")
		CancelEvent()
		return
	end
	
	-- MySQL.Sync.execute("UPDATE rocade SET status='in_connexion', timeout=DATE_ADD(NOW(), INTERVAL " .. rocade_prio_time .. " SECOND) WHERE identifier=@identifier", {
		-- ['@identifier'] = steamID
	-- })
	MySQL.Sync.execute("UPDATE rocade SET status='in_connexion', timeout=NOW() WHERE identifier=@identifier", {
		['@identifier'] = steamID
	})
	
	print (rocade_trace .. playerName .. " [" .. steamID .. "] se connecte.")
	deferrals.done()
end)

RegisterServerEvent("rocade:playerSpawned")
AddEventHandler("rocade:playerSpawned", function()
	local _source = source
	local playerName = GetPlayerName(_source)
	local steamID = GetPlayerIdentifiers(_source)[1] or ""
	
	print (rocade_trace .. playerName .. " [" .. steamID .. "] est connecte.")
	
	-- Mise à jour de la BDD
	MySQL.Sync.execute("UPDATE user_whitelist SET online = 1 WHERE identifier=@identifier", {
		['@identifier'] = steamID
	})
	
	-- Mise à jour de la BDD
	-- MySQL.Sync.execute("DELETE FROM rocade WHERE identifier=@identifier", {
		-- ['@identifier'] = steamID
	-- })
	
	MySQL.Sync.execute("UPDATE rocade SET status='connected', timeout=NOW() WHERE identifier=@identifier", {
		['@identifier'] = steamID
	})
	
end)

function resetOnlinePlayers()
	local xPlayers = ESX.GetPlayers()
	local steamID = ""
	
	MySQL.Sync.execute("UPDATE user_whitelist SET online = 0 WHERE online = 1", {})

	for i = 1, #xPlayers, 1 do
		steamID = ESX.GetPlayerFromId(xPlayers[i]).identifier or ""
	
		if steamID ~= "" then
			-- Mise à jour de la BDD
			MySQL.Sync.execute("UPDATE user_whitelist SET online = 1 WHERE identifier=@identifier", {
				['@identifier'] = steamID
			})
		end
	end
	
	MySQL.Sync.execute("UPDATE rocade SET status='crash', timeout=DATE_ADD(NOW(), INTERVAL " .. crash_prio_time .. " SECOND) WHERE status<>'waiting'", {})
end



function updateRocade()
	SetTimeout(check_player_time * 1000, function()
		-- local xPlayers = ESX.GetPlayers()
		local max_players_online = GetConvarInt('sv_maxclients', 32)
		
		-- Suppression des joueurs qui ont dépassés le timeout
		MySQL.Sync.execute("DELETE FROM rocade WHERE `status`='crash' AND timeout < NOW()", {})
		
		-- Compte le nombre de joueur en connexion
		result = MySQL.Sync.fetchAll("SELECT IFNULL(COUNT(*), 0) as count FROM rocade WHERE `status`<>'waiting'", {})
		
		-- Authorise les joueurs
		-- if (max_players_online - #xPlayers - result[1].count) > 0 then
			-- MySQL.Sync.execute("UPDATE rocade SET status='authorized', timeout=DATE_ADD(NOW(), INTERVAL " .. rocade_prio_time .. " SECOND) WHERE `status`='waiting' ORDER BY rang LIMIT " .. max_players_online - #xPlayers - result[1].count, {})
		-- end
		if (max_players_online - result[1].count) > 0 then
			MySQL.Sync.execute("UPDATE rocade SET status='authorized', timeout=DATE_ADD(NOW(), INTERVAL " .. rocade_prio_time .. " SECOND) WHERE `status`='waiting' ORDER BY rang LIMIT " .. max_players_online - result[1].count, {})
		end
		
		updateRocade()
	end)
end












function checkOnlinePlayers()
	SetTimeout(check_player_time * 1000, function()
		local xPlayers = ESX.GetPlayers()
		local playerName = ""
		local playerSteam = ""
		local file = nil
		local rocade = nil
		local rocade2 = nil
		
		if xPlayers ~= nil then
			if (print_debug_console or print_debug_file) then
				rocade = rocade_trace .. os.date("%d/%m/%Y %H:%M:%S")
				rocade2 = os.date("%d/%m/%Y %H:%M:%S")
				
				rocade = rocade ..'\r\n'.. rocade_trace..'--------------------------------------------------'
				rocade = rocade ..'\r\n'.. rocade_trace..'Joueurs en ligne : ' .. #xPlayers
				for i = 1, #xPlayers, 1 do
					playerName = GetPlayerName(xPlayers[i]) or ""
					playerSteam = ESX.GetPlayerFromId(xPlayers[i]).identifier or ""

					rocade = rocade ..'\r\n'.. rocade_trace..'     - ' .. playerName .. ' [' .. playerSteam .. ']'
				end
				
				rocade = rocade ..'\r\n'.. rocade_trace..'Joueurs en connexion : ' .. #player_in_connexion
				rocade2 = rocade2 .. '\r\nConnexion :'
				for i = 1, #player_in_connexion, 1 do
					local result = MySQL.Sync.fetchAll("SELECT name FROM users WHERE `identifier` = @identifier",
					{
						['@identifier'] = player_in_connexion[i]
					})
					
					if (#result > 0) then
						playerName = result[1].name
					else
						playerName = ""
					end
					
					rocade = rocade ..'\r\n'.. rocade_trace..'     - ' .. playerName .. ' [' .. player_in_connexion[i] .. ']'
					rocade2 = rocade2 .. '\r\n     - ' .. playerName
				end
				
				rocade = rocade ..'\r\n'.. rocade_trace..'Joueurs en rocade : ' .. #player_waiting
				rocade2 = rocade2 .. '\r\nRocade :'
				for i = 1, #player_waiting, 1 do
					local result = MySQL.Sync.fetchAll("SELECT name FROM users WHERE `identifier` = @identifier",
					{
						['@identifier'] = player_waiting[i]
					})
					
					if (#result > 0) then
						playerName = result[1].name
					else
						playerName = ""
					end
					
					rocade = rocade ..'\r\n'.. rocade_trace..'     - ' .. playerName .. ' [' .. player_waiting[i] .. ']'
					rocade2 = rocade2 .. '\r\n     - ' .. playerName
				end
				
				rocade = rocade ..'\r\n'.. rocade_trace..'Joueurs crash : ' .. #player_crashed
				for i = 1, #player_crashed, 1 do
					local result = MySQL.Sync.fetchAll("SELECT name FROM users WHERE `identifier` = @identifier",
					{
						['@identifier'] = player_crashed[i]
					})
					
					if (#result > 0) then
						playerName = result[1].name
					else
						playerName = ""
					end
					
					rocade = rocade ..'\r\n'.. rocade_trace..'     - ' .. playerName .. ' [' .. player_crashed[i] .. ']'
				end
				rocade = rocade ..'\r\n'.. rocade_trace..'--------------------------------------------------'
			end
			
			if print_debug_console then
				print (rocade)
			end
			
			if print_debug_file then
				file = io.open(debug_filename, "a")
				io.output(file)
				io.write(rocade.."\r\n")
				io.close(file)
			end
			
			--file = io.open("C:\\www\\rocade\\rocade.txt", "w")
			--io.output(file)
			--io.write(rocade2.."\r\n")
			--io.close(file)
			
			onlinePlayers = #xPlayers + #player_in_connexion + #player_crashed
		end
		
		refreshWhitelist()
		checkOnlinePlayers()
	end)
end
--checkOnlinePlayers()
