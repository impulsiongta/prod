API = nil

TriggerEvent("API:getAPI", function(obj) API = obj end)


RegisterServerEvent('voiture:CheckMoneyForVel')

AddEventHandler('voiture:CheckMoneyForVel', function(name, vehicle, price)
     local _source = source

    local vehicle = vehicle
    local name = name
    local price = tonumber(price)

    if (tonumber(API.getMoney(_source)) >= tonumber(price)) then
        API.removeMoney(_source, price)
        TriggerClientEvent('voiture:FinishMoneyCheckForVel', _source, name, vehicle, price)
        TriggerClientEvent("showDoneNotif", _source, "Bonne route !")
    else
        TriggerClientEvent("showErrorNotif", _source, "Vous n'avez pas assez d'argent !")
    end
end)