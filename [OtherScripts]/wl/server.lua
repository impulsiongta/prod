local WL = {}


AddEventHandler("playerConnecting", function(playerName,setCallback)
	local _source = source
	local identifier = getPlayerID(_source)
	local whiteListed = false

	if(#WL > 0) then
		for _,k in pairs(WL) do
			if(k==identifier) then
				whiteListed = true
				break;
			end
		end
	end
	

	if(not whiteListed) then
		setCallback("Vous n'êtes pas whitelist.")
		CancelEvent()
		return
	end
end)




function refreshWL()
	WL = {}

	MySQL.Async.fetchAll("SELECT identifier FROM user_whitelist WHERE `whitelisted` = 1 OR `whitelisted` = 2 OR `whitelisted` = 3", {}, function(result)
		for _,k in pairs(result) do
			table.insert(WL, k.identifier)
		end
	end)

	SetTimeout(600000, refreshWL)
end

MySQL.ready(function()
	refreshWL()
end)


-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
