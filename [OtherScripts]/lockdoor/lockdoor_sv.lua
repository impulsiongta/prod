ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
local doorList = {
    -- Mission Row To locker room & roof
    [1] = { ["objName"] = "v_ilev_ph_gendoor004", ["x"]= 449.69815063477, ["y"]= -986.46911621094,["z"]= 30.689594268799,["locked"]= true,["txtX"]=450.104,["txtY"]=-986.388,["txtZ"]=31.739},
    -- Mission Row Armory
    [2] = { ["objName"] = "v_ilev_arm_secdoor", ["x"]= 452.61877441406, ["y"]= -982.7021484375,["z"]= 30.689598083496,["locked"]= true,["txtX"]=453.079,["txtY"]=-982.600,["txtZ"]=31.739},
    -- Mission Row Captain Office
    [3] = { ["objName"] = "v_ilev_ph_gendoor002", ["x"]= 447.23818969727, ["y"]= -980.63006591797,["z"]= 30.689598083496,["locked"]= true,["txtX"]=447.200,["txtY"]=-980.010,["txtZ"]=31.739},
    -- Mission Row To downstairs right
    [4] = { ["objName"] = "v_ilev_ph_gendoor005", ["x"]= 443.97, ["y"]= -989.033,["z"]= 30.6896,["locked"]= true,["txtX"]=444.020,["txtY"]=-989.445,["txtZ"]=31.739},
    -- Mission Row To downstairs left
    [5] = { ["objName"] = "v_ilev_ph_gendoor005", ["x"]= 445.37, ["y"]= -988.705,["z"]= 30.6896,["locked"]= true,["txtX"]=445.350,["txtY"]=-989.445,["txtZ"]=31.739},
    -- Mission Row Main cells
    [6] = { ["objName"] = "v_ilev_ph_cellgate", ["x"]= 464.0, ["y"]= -992.265,["z"]= 24.9149,["locked"]= true,["txtX"]=463.465,["txtY"]=-992.664,["txtZ"]=25.064},
    -- Mission Row Cell 1
    [7] = { ["objName"] = "v_ilev_ph_cellgate", ["x"]= 462.381, ["y"]= -993.651,["z"]= 24.9149,["locked"]= true,["txtX"]=461.806,["txtY"]=-993.308,["txtZ"]=25.064},
    -- Mission Row Cell 2
    [8] = { ["objName"] = "v_ilev_ph_cellgate", ["x"]= 462.331, ["y"]= -998.152,["z"]= 24.9149,["locked"]= true,["txtX"]=461.806,["txtY"]=-998.800,["txtZ"]=25.064},
    -- Mission Row Cell 3
    [9] = { ["objName"] = "v_ilev_ph_cellgate", ["x"]= 462.704, ["y"]= -1001.92,["z"]= 24.9149,["locked"]= true,["txtX"]=461.806,["txtY"]=-1002.450,["txtZ"]=25.064},
    -- Mission Row Backdoor in
    [10] = { ["objName"] = "v_ilev_gtdoor", ["x"]= 464.126, ["y"]= -1002.78,["z"]= 24.9149,["locked"]= true,["txtX"]=464.100,["txtY"]=-1003.538,["txtZ"]=26.064},
    -- Mission Row Backdoor out
    [11] = { ["objName"] = "v_ilev_gtdoor", ["x"]= 464.18, ["y"]= -1004.31,["z"]= 24.9152,["locked"]= false,["txtX"]=464.100,["txtY"]=-1003.538,["txtZ"]=26.064},
    -- Mission Row Rooftop In
    [12] = { ["objName"] = "v_ilev_gtdoor02", ["x"]= 465.467, ["y"]= -983.446,["z"]= 43.6918,["locked"]= true,["txtX"]=464.361,["txtY"]=-984.050,["txtZ"]=44.834},
    -- Mission Row Rooftop Out
    [13] = { ["objName"] = "v_ilev_gtdoor02", ["x"]= 462.979, ["y"]= -984.163,["z"]= 43.6919,["locked"]= true,["txtX"]=464.361,["txtY"]=-984.050,["txtZ"]=44.834},

    --[12] = { ["objName"] = "v_ilev_ph_door01", ["x"]= 434.691, ["y"]= -981.348,["z"]= 30.71304,["locked"]= true,["txtX"]=434.691,["txtY"]=-981.348,["txtZ"]=31.71304},

    --[13] = { ["objName"] = "v_ilev_ph_door002", ["x"]= 434.691, ["y"]= -982.515,["z"]= 30.71304,["locked"]= true,["txtX"]=434.691,["txtY"]=-982.515,["txtZ"]=31.71304},

    [14] = { ["objName"] = "v_ilev_arm_secdoor", ["x"]= 461.243, ["y"]= -986.013,["z"]= 30.68958,["locked"]= true,["txtX"]=461.243,["txtY"]=-986.013,["txtZ"]=31.68958},   
    --Mission Row Backdoor ToGoOut left
    [15] = { ["objName"] = "v_ilev_rc_door2", ["x"]= 469.254, ["y"]= -1014.541,["z"]= 26.38638,["locked"]= true,["txtX"]=469.254,["txtY"]=-1014.541,["txtZ"]=27.38638}, 
    --Mission Row Backdoor ToGoOut right
    [16] = { ["objName"] = "v_ilev_rc_door2", ["x"]= 468.096, ["y"]= -1014.444,["z"]= 26.38638,["locked"]= true,["txtX"]=468.096,["txtY"]=-1014.444,["txtZ"]=27.38638}, 

    [17] = { ["objName"] = "v_ilev_fibl_door02", ["x"]= 106.033, ["y"]= -743.697,["z"]= 45.7547,["locked"]= true,["txtX"]=106.033,["txtY"]=-743.697,["txtZ"]=46.7547},

    [18] = { ["objName"] = "v_ilev_fibl_door01", ["x"]= 105.737, ["y"]= -745.605,["z"]= 45.7547,["locked"]= true,["txtX"]=105.737,["txtY"]=-745.605,["txtZ"]=46.7547},
}

RegisterServerEvent("lockdoor:updateDoorStatus")
AddEventHandler("lockdoor:updateDoorStatus", function(door)
    doorList = door
    TriggerClientEvent('lockdoor:updateDoorStatusCb', -1, doorList)
end)

ESX.RegisterServerCallback('lockdoor:getStatusDoor', function(source, cb) 
    cb(doorList)
end)
