-- [[-----------------------------------------------------------------------

    -- fivem_doors - Open/Close doors at Mission Row PD
    -- Script By SGTGunner version 1.0

    -- Main Client file

-- ---------------------------------------------------------------------]]--
ESX                           = nil

local PlayerData              = {}
local doorList = {}

RegisterNetEvent('lockdoor:updateDoorStatusCb')
AddEventHandler('lockdoor:updateDoorStatusCb', function(door)
	doorList = door
end)

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(300)
	end
    ESX.TriggerServerCallback('lockdoor:getStatusDoor', function(door)
        doorList = door
    end)
end)

function DrawText3d(x,y,z, text, status)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())

    if onScreen then
        SetTextScale(0.2, 0.2)
        SetTextFont(0)
        SetTextProportional(1)
        if status then
        	SetTextColour(255, 0, 0, 255)
        else
        	SetTextColour(0, 255, 0, 255)
        end
        SetTextDropshadow(0, 0, 0, 0, 55)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end


Citizen.CreateThread(function()
    Wait(1000)
    while true do
        Citizen.Wait(0)
        for i = 1, #doorList do
            local playerCoords = GetEntityCoords( GetPlayerPed(-1) )
            local closeDoor = GetClosestObjectOfType(doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], 1.0, GetHashKey(doorList[i]["objName"]), false, false, false)

            local objectCoordsDraw = GetEntityCoords( closeDoor )
            local playerDistance = GetDistanceBetweenCoords(playerCoords.x, playerCoords.y, playerCoords.z, doorList[i]["x"], doorList[i]["y"], doorList[i]["z"], true)

            if(playerDistance < 1.5) and (PlayerData.job ~= nil and PlayerData.job.name == 'police' or PlayerData.job.name == 'lo') then

                if doorList[i]["locked"] == true then
                    DrawText3d(doorList[i]["txtX"], doorList[i]["txtY"], doorList[i]["txtZ"], "[E] pour ouvrir la porte", doorList[i]["locked"])
                else
                    DrawText3d(doorList[i]["txtX"], doorList[i]["txtY"], doorList[i]["txtZ"], "[E] pour fermer la porte", doorList[i]["locked"])
                end

                if IsControlJustPressed(1,51) then
                    if doorList[i]["locked"] == true then
                        --FreezeEntityPosition(closeDoor, false)
                        if(i==10 or i==11) then
                            doorList[10]["locked"] = false
                            doorList[11]["locked"] = false
                        elseif(i==12 or i==13) then
                            doorList[12]["locked"] = false
                            doorList[13]["locked"] = false
                        elseif(i==4 or i==5) then
                            doorList[4]["locked"] = false
                            doorList[5]["locked"] = false
                        elseif(i==15 or i==16) then
                            doorList[15]["locked"] = false
                            doorList[16]["locked"] = false
                        else
                            doorList[i]["locked"] = false
                        end
                    else
                        --FreezeEntityPosition(closeDoor, true)
                        if(i==10 or i==11) then
                            doorList[10]["locked"] = true
                            doorList[11]["locked"] = true
                        elseif(i==12 or i==13) then
                            doorList[12]["locked"] = true
                            doorList[13]["locked"] = true
                        elseif(i==4 or i==5) then
                            doorList[4]["locked"] = true
                            doorList[5]["locked"] = true
                        elseif(i==15 or i==16) then
                            doorList[15]["locked"] = true
                            doorList[16]["locked"] = true
                        else
                            doorList[i]["locked"] = true
                        end
                    end
                    TriggerServerEvent('lockdoor:updateDoorStatus', doorList)
                    Citizen.Wait(20)
                end
            --else
            end
            FreezeEntityPosition(closeDoor, doorList[i]["locked"])
        end
    end
end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
end)
