-- CONFIG --

-- Ping Limit
pingLimit = 800

-- CODE --

RegisterServerEvent("checkMyPingBro")
AddEventHandler("checkMyPingBro", function()
	ping = GetPlayerPing(source)
	if ping >= pingLimit then
		DropPlayer(source, "Ping trop haut (Limite: " .. pingLimit .. " Ton Ping: " .. ping .. ")")
	end
end)