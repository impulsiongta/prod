--Settings--
API = nil

TriggerEvent("API:getAPI", function(obj) API = obj end)

enableprice = true -- true = carwash is paid, false = carwash is free

price = 200 -- you may edit this to your liking. if "enableprice = false" ignore this one


RegisterServerEvent('vrp_carwash:checkmoney')
AddEventHandler('vrp_carwash:checkmoney', function ()
	local _source = source
	if enableprice == true then
		userMoney = API.getMoney(_source)
		if userMoney >= price then
			API.removeMoney(_source, price)
			TriggerClientEvent('vrp_carwash:success', _source, price)	
		end
	else
		TriggerClientEvent('vrp_carwash:free', _source)
	end
end)