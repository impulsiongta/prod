local vehWeapons = {
	'WEAPON_PUMPSHOTGUN', -- ShotGun
	'WEAPON_CARBINERIFLE', -- Carbine
	'WEAPON_SNIPERRIFLE', -- Sniper
}

local hasBeenInPoliceVehicle = false
local alreadyHaveWeapon = {}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1000)

		if IsPedInAnyPoliceVehicle(GetPlayerPed(-1)) then
			if not hasBeenInPoliceVehicle then
				hasBeenInPoliceVehicle = true
			end
		else
			if hasBeenInPoliceVehicle then
				for i,k in pairs(vehWeapons) do
					if not alreadyHaveWeapon[i] then
						DisablePlayerVehicleRewards(GetPlayerPed(-1))
						RemoveWeaponFromPed(GetPlayerPed(-1), GetHashKey(vehWeapons[i]))
						TriggerEvent('esx:removeWeapon', vehWeapons[i])
					end
				end
				hasBeenInPoliceVehicle = false
			end
		end

		if not IsPedInAnyVehicle(GetPlayerPed(-1)) then
			for i=1,#vehWeapons do
				if HasPedGotWeapon(GetPlayerPed(-1), GetHashKey(vehWeapons[i]), false) == 1 then
					alreadyHaveWeapon[i] = true
				else
					alreadyHaveWeapon[i] = false
				end
			end
		end
	end
end)