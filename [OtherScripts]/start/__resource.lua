resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

client_scripts {

	--'carwash/carwash_client.lua',
	
	"compass/essentials.lua",
	"compass/compass.lua",
	"compass/streetname.lua",

	'heli/heli_client.lua',

	'interior/interiors_client.lua',

	--'logs/client.lua',
	--'logs/config.lua',

	'NeverWanted/nowanted.lua',

	'nocarjack/lock.lua',

	--"pNotify/cl_notify.lua",

	'PoliceVehiclesWeaponDeleter/client.lua',

    'vehicle_control/client.lua',
    'vehicle_control/GUI.lua',

	'vocalChat/vocalchat.lua'
}

server_scripts {

	--'carwash/carwash_server.lua',
	
	'heli/heli_server.lua',
	
	'interior/interiors_server.lua',
	
	--'logs/server.lua',
	--'logs/hashToWeaponsName_s.lua',
	
	--'PoliceVehiclesWeaponDeleter/server.lua'

}

files {


	'chargement/index.html',
    'chargement/loading.css',
    'chargement/video.webm',
    'chargement/placeholder.jpg',

	
    --"pNotify/html/index.html",
    --"pNotify/html/pNotify.js",
    --"pNotify/html/noty.js",
    --"pNotify/html/noty.css",
    --"pNotify/html/themes.css",
    --"pNotify/html/sound-example.wav",

}

loadscreen 'chargement/index.html'

export "SetQueueMax"
export "SendNotification"

--ui_page "pNotify/html/index.html"