-- CONFIG --

-- AFK Kick Time Limit (in seconds)
secondsUntilKick = 900

-- Warn players if 3/4 of the Time Limit ran up
kickWarning = true

-- CODE --

Citizen.CreateThread(function()
	while true do
		Wait(1000)

		playerPed = GetPlayerPed(-1)
		if playerPed then
			currentPos = GetEntityCoords(playerPed, true)

			if currentPos == prevPos then
				if IsControlJustPressed(1, 249) then
					--print(time .. 'secondes')
					time = secondsUntilKick
				else
					if time > 0 then
						if kickWarning and time == math.ceil(secondsUntilKick / 8) then
							TriggerEvent("chatMessage", "WARNING", {255, 0, 0}, "^1Vous serez kick dans " .. math.floor(time/60) .. " minutes et " .. math.fmod(time,60) .. " secondes pour être AFK!")
						end					
						if kickWarning and time == math.ceil(secondsUntilKick / 4) then
							TriggerEvent("chatMessage", "WARNING", {255, 0, 0}, "^1Vous serez kick dans " .. math.floor(time/60) .. " minutes et " .. math.fmod(time,60) .. " secondes pour être AFK!")
						end
						if kickWarning and time == math.ceil(secondsUntilKick / 3) then
							TriggerEvent("chatMessage", "WARNING", {255, 0, 0}, "^1Vous serez kick dans " .. math.floor(time/60) .. " minutes et " .. math.fmod(time,60) .. " secondes pour être AFK!")
						end
						if kickWarning and time == math.ceil(secondsUntilKick / 2) then
							TriggerEvent("chatMessage", "WARNING", {255, 0, 0}, "^1Vous serez kick dans " .. math.floor(time/60) .. " minutes et " .. math.fmod(time,60) .. " secondes pour être AFK!")
						end
						if kickWarning and time == math.ceil(secondsUntilKick / 1.5) then
							TriggerEvent("chatMessage", "WARNING", {255, 0, 0}, "^1Vous serez kick dans " .. math.floor(time/60) .. " minutes et " .. math.fmod(time,60) .. " secondes pour être AFK!")
						end

						time = time - 1
						--print(time .. 'secondes')
					else
						TriggerServerEvent("kickForBeingAnAFKDouchebag")
					end
				end
			else
				time = secondsUntilKick
			end

			prevPos = currentPos
		end
	end
end)