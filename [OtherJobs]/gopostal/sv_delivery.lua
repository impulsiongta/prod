ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


RegisterServerEvent('delivery:sv_getJobId')
AddEventHandler('delivery:sv_getJobId',
  function()
    TriggerClientEvent('delivery:cl_setJobId', source, GetSex(source))
  end
)

--Essential payment functions 

RegisterServerEvent('delivery:success')
AddEventHandler('delivery:success', function(price)
  local _source = source

  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.addMoney(2*price/3)
  xPlayer.addAccountMoney('black_money', price/3)
end)

RegisterServerEvent('delivery:clearItem')
AddEventHandler('delivery:clearItem', function(item)
  local _source = source

  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.setInventoryItem(item, 0)
end)

RegisterServerEvent('delivery:setItem')
AddEventHandler('delivery:setItem', function(item, count)
  local _source = source

  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.setInventoryItem(item, count)
end)

RegisterServerEvent('delivery:removeItem')
AddEventHandler('delivery:removeItem', function(item, count)
  local _source = source

  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.removeInventoryItem(item, count)
end)

