local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local takeVehicle = {x=-1549.45,y=-404.79,z=41.98,a=270.67}
local takeVetement = {x=-1566.36,y=-404.53,z=42.38}
local takeJob = 0


-- RUN SANDWICH

local colorBlipRun = 2

local recolte = {x=2311.89,y=4890.06,z=41.80}
local markerRecolte = {x = 2.001, y = 2.001, z = 0.5001}
local waitingRecolte = 1000

local traitement1 = {x=-102.486,y=6208.335,z=31.025}
local markerTraitement = {x = 1.501, y = 1.501, z = 0.5001}
local waitingTraitement = 1000

local vente = {x=151.784,y=-1478.086,z=29.357}
local markerVente = {x = 2.001, y = 2.001, z = 0.5001}
local waitingVente = 1000
local price = 9

-- RUN MATIERE PREMIERE

local colorBlipRun2 = 75

local recolte2 = {x=-57.90,y=6522.86,z=31.49}
local waitingRecolte2 = 1000

--Entree
local traitement2_1 = {x=-1533.34,y=-453.06,z=35.88}
--Plat
local traitement2_2 = {x=-1535.41,y=-451.22,z=35.88}
--Dessert
local traitement2_3 = {x=-1537.46,y=-449.36,z=35.88}
--Pizza
local traitement2_4 = {x=-1539.69,y=-447.21,z=35.88}

local waitingTraitement2 = 1000

-- RUN BOISSON

local colorBlipAchat = 38

local achat1_1 = {x=148.51,y=237.89,z=106.82}
local achat1_2 = {x=150.59,y=236.99,z=106.86}
local achat1_3 = {x=153.73,y=235.92,z=106.77}
local achat1_4 = {x=155.65,y=235.28,z=106.72}

local markerAchat = {x = 1.501, y = 1.501, z = 0.5001}
local priceAchat = 3
local waitingAchat = 1000
-- COFFRE
local coffre = {x=-98.32,y=367.33,z=113.27}
local coffre_money = {x=-34.28,y=347.7,z=113.92}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}


local phase = 0
local player = -1
local JobAPI = nil

local vehicle = -1
local haveTakeVetement = false

ESX = nil
local PlayerData = {}
local GUI                     = {}
GUI.Time                      = 0
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'tacobell' then
		Config.Zones.Coffre.Type	   = 1
	else
		Config.Zones.Coffre.Type	   = -1
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    player = xPlayer

    while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end
	
	if player.job.name == 'tacobell' then

		Config.Zones.Coffre.Type = 1
		
	else

		Config.Zones.Coffre.Type = -1

	end

	while player == -1 do
		Citizen.Wait(10)
	end

	if(player ~= -1 and player.job ~= nil and player.job.name == "tacobell") then
		if(player.job.grade_name == "boss") then
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'tacobelloff', 'Compte Offshore LS Diner')
		end
	end



	Citizen.CreateThread(function()
		RequestModel("MANOIR")

		company = AddBlipForCoord(-1535.34,-451.06,35.88)
	    SetBlipSprite(company, 304)
	    SetBlipColour(company, 71)
	    SetBlipAsShortRange(company, true)
	    BeginTextCommandSetBlipName("STRING")
	    AddTextComponentString("LS Diner")
	    EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end


		while true do
			Citizen.Wait(5)
			if(player~=-1) then
				if(player.job ~= nil and player.job.name ~= nil  and player.job.name == "tacobell") then
				DrawMarker(1,takeVetement.x,takeVetement.y,takeVetement.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
					
					if(isNear(takeVetement)) then
						if(haveTakeVetements) then
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~reprendre vos vêtements.")
						else
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre vos habits de travail.")
						end

						if(IsControlJustPressed(1, 38)) then
							haveTakeVetements = not haveTakeVetements
							phase = 0

							if(not haveTakeVetements) then
								JobAPI.stop()

								if(DoesEntityExist(vehicle)) then
									DeleteVehicle(vehicle)
								end

								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									TriggerEvent('skinchanger:loadSkin', skin)
								end)
							else
								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									if skin.sex == 0 then
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
									else
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
									end
								end)
							end
						end
					end
					
					if(takeVetement) then

						if(haveTakeVetements) then
							DrawMarker(1,takeVehicle.x,takeVehicle.y,takeVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

							if(isNear(takeVehicle)) then
							
								Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~sortir un véhicule.")
								
								if(IsControlJustPressed(1, 38)) then
									if not IsPedInAnyVehicle(GetPlayerPed(-1), false) then
										local elements = {}
										ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
										  for i=1, #garageVehicles, 1 do
											table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
										  end

										  ESX.UI.Menu.Open(
											'default', GetCurrentResourceName(), 'vehicle_spawner',
											{
											  title    = _U('vehicle_menu'),
											  align    = 'top-left',
											  elements = elements,
											},
											function(data, menu)

											  menu.close()

											  local vehicleProps = data.current.value

											  ESX.Game.SpawnVehicle(vehicleProps.model, takeVehicle, takeVehicle.a, function(vehicle)
												ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
												local playerPed = GetPlayerPed(-1)
												TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
											  end)

											  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'tacobell', vehicleProps)

											end,
											function(data, menu)

											  menu.close()

											  CurrentAction     = 'menu_vehicle_spawner'
											  CurrentActionMsg  = _U('vehicle_spawner')
											  CurrentActionData = {}

											end
										  )

										end, 'tacobell')										
										-- RUN SANDWICH
										JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'viande', colorBlipRun, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte, 'Viande')
										JobAPI.createTraitementMultiple(2, traitement1.x,traitement1.y,traitement1.z, 'viande', 1, 'tacos', 1, colorBlipRun, true, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement, 'Tacos')
										JobAPI.createVenteEntreprise(3, vente.x, vente.y, vente.z, 'tacos', price, "tacobell", colorBlipRun, true, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'bleu')
										
										-- RUN MATIERE PREMIERE (Entree/Plat/Dessert/Pizza
										JobAPI.createRecolte(1, recolte2.x,recolte2.y,recolte2.z, 'matiere_premiere', colorBlipRun2, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte2, 'MatPrem')
										JobAPI.createTraitementMultiple(2, traitement2_1.x,traitement2_1.y,traitement2_1.z, 'matiere_premiere', 1, 'entree', 4, colorBlipRun2, true, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement2, 'marron')										
										JobAPI.createTraitementMultiple(2, traitement2_2.x,traitement2_2.y,traitement2_2.z, 'matiere_premiere', 1, 'plat', 4, colorBlipRun2, false, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement2, 'PlatRepas')
										JobAPI.createTraitementMultiple(2, traitement2_3.x,traitement2_3.y,traitement2_3.z, 'matiere_premiere', 1, 'dessert', 4, colorBlipRun2, false, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement2, 'jaune')
										JobAPI.createTraitementMultiple(2, traitement2_4.x,traitement2_4.y,traitement2_4.z, 'matiere_premiere', 1, 'pizza', 4, colorBlipRun2, false, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement2, 'Pizza')
										
										-- RUN BOISSONS
										if(player.job.grade_name == "boss" or player.job.grade_name == "comptable") then
											JobAPI.createAchatEntreprise(1, achat1_1.x, achat1_1.y, achat1_1.z, 'eau_minerale', priceAchat, 'tacobell', colorBlipAchat, true, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'blanc')
											JobAPI.createAchatEntreprise(3, achat1_2.x, achat1_2.y, achat1_2.z, 'coca', priceAchat, 'tacobell', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'rouge')
											JobAPI.createAchatEntreprise(3, achat1_3.x, achat1_3.y, achat1_3.z, 'redbull', priceAchat, 'tacobell', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'bleu')
											JobAPI.createAchatEntreprise(3, achat1_4.x, achat1_4.y, achat1_4.z, 'cafe', priceAchat, 'tacobell', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'marron')
										end																	
									else
										local vehicleIsIn = GetVehiclePedIsIn(GetPlayerPed(-1), false)
										local vehicleProps = ESX.Game.GetVehicleProperties(vehicleIsIn)
										TriggerServerEvent('esx_society:putVehicleInGarage', 'tacobell', vehicleProps)
										ESX.Game.DeleteVehicle(vehicleIsIn)
										JobAPI.stop()
									end
								end	
							end
						end
					end
				end
			end
		end

	end)

end)



function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function spawnVehicle(name, tabl)

	hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1) 	

    Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
    SetVehicleOnGroundProperly(Truck)
    SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
   	SetVehicleEngineOn(Truck, true, false, false)

   	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck

end

AddEventHandler('manoirdejade:hasEnteredMarker', function(zone)

	if zone == 'Coffre' and player.job and player.job.name == 'tacobell' then
		CurrentAction     = 'manoirdejade_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end
	
end)
			
AddEventHandler('manoirdejade:hasExitedMarker', function(zone)

	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
	
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('manoirdejade:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('manoirdejade:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 200, false, true, 2, false, false, false, false)
			end
		end
	end
end)


RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = 'LS Diner',
		number     = 'tacobell',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAouSURBVHjaxFdnWBVnFn6/abfSmyCXLsIaNAIRYyIaCyLGZE2yGjVFn42a8mw0bixP3HTdEI0uRlNMsSRGscQo1iCKBRuKKCJNBURqlHYL90799geIMcm6+OyPnR8z85yZ78w77znnPecjlFL8Pw/u/dUroChAazMFYQCWBUABhifgdCwAQOAFLF+0AN9++QWmTJ36Oyerv2nE/p0OtFmPCL3+NF++suM8DXv0M5T3+g4fTVoBT/4npBSDjDsSGJMaJk6u1Mwbl6UvrwAA7n7QUkpBnQAIAF3nhYLipedM6BOSj8KjLdK27Fh4pWw2NOBk7PRrD3kVrl8Va5ei+2YmyAlDF/gMFHXhJCJ22tfL0pfjvgC4md0wf8ECfLRzCSADSnknEA8vN5KzNZcWt5fgbNuBJ+a/MXxmX11DX0aLCLJbrxrNHA+PQCfcfMPhHeiB197ev8g7ctiN7hD0FABDGNTX16L1g3qwBgAzAJcN8PfuTdeu+pIJ6lO7bOPSD+caNCea64rRct0Fl5WBqKrQNAYckbH4s5LMvOr2DJMunwGg3XcIAEAPgAkHgj0fx8h+OSi8ZIGVK1wxe/rY2WD0kFptkEUKncDDpOcgdsgoKHEWbll5/PNTLuNaN32A1m/aLKabAUEngBBqpFTUCGEkQkABEEIIJQyhAAgIoberhZgBBoCrfg72llYxAQYmeeEM82xoDNo0a0fF5av17kLLmZ17i13XK5qvWCVaGxfNjuofFzh4bGRkSm5xTdbGvH0bMW9FJ4CywmJwBvNAH9/gd+3Nzb6UY1RQEMIQKtuJBoCw7h7FLDATgCJmAjAB4pVThPPtxc5NM77eJrocn8zLXjXc9uL6j/K2Vb37VS9p+8EKFBS1IqJfZFj6/Ii1vpZwwRD1EMRPT1g3rt6xsZsBtc1KNn+z7sSEl17bHuBuXuN0dgAEIJQBpZ2Zzyr0oegnJ/zYUHF5r2NlM1BfSwh4mthrvPLw0C+i3vxb9F/3NHI7/MIyZL1vEWpqDVA0AwAZOrtT55RUp80uCwa7C6IqO+/KrXHDhmDhM6PQ7uhY1zu8z1FoAEs4cBwLQcfDYNBB0RQ8EBo2l2lpYkbYQrENn2LclBBEP/vOwwezTSWl9vqsxDpP5vDuZsZwMh6b3mrH5XJbl6DIAAW6Tr8XIqNeoOHBQdiw65jswdAVluDgZFlVCcdyYBkGhBBooNDLyvBnn3xqZEh5x8HQ04SkitdoBe8Myalr01yJjzl1tpcQ32swWALsqkmHIr8DwKNLNO6hhKXVtTh69CxqKkrRUF2594233j+u0zHJDGHAdAEAKDRNY+KCoufJrHZwbsMZPFl1i4S0eVTWu3vuSMh2Z9fHutjdgaoElgB5CnCB9Ky8K+t/wYH8iwAAWZLUy+UVS729vWA06GA26GE26uFmMsJkMgKKbXRHXtMotbhGyzcTsuXU3y8lXKveNaT1NMOVlgL7i4D9JcC1SgB8zwBkbt70a7HFvm3f7bXb23O9PNzgZtbD3ayHm0mAh1kPd1YH02j2jfCnjVi66EMtNknvdNVbZZ3gIRNWlsBTQLkEOLIBGHrWjLZu3QqOu6NH2T//jMsXLyyJHJf2GIEGQgAQCk2jUGUNgp/vGBM3KCksJfzMhpTZ2L+kGY71RQBlAcIDtzYBWjsA422xuzeA1NTUuwxRUVGIiYk5lPzwoAMWS+9UWVFBQTtLklXBa5Rto+rcjIyMSUG9e0ONHYjK1FbIe5yAeAhw5PX47/9QilVVhSiKyMk5/MGsWa+kqKqLoQAoUQFCAEWGJST4qaXpywacLzh38U4wTYDGdrZJkB4DYH5rOHnyJABg7569py5dvLzFYDSCYVgQwoGAAcMQEAoubdz413lBd2eh5gDg+iOX92Zgzpw5dxkOHToEAFBUGes2fL04PXb5BIawek3TuqVEkkX0jek7MS4ubvH5gnNVd1bLAJQuYns2aXErV678jw/LykpLDufmrEtLS3vFZnMCVAOlBJoGyKpkThyR9lphC3mTyCK02qKuj9q7BKirrpR7R+S/8nX6VF663WFtJUSFpqmQZRmyrMHucCI0LOKF8VNn+Ov0+u73BU7CsQMU5YUMso9LcAtTiCz+DwDy8/Nrii5eWMlxDERRhlNS4ZRkOEUZRHL4BdnKpscF8OAFodOhHrg+uANXHnSiZYA3uBgDqznBdLJDAY1h7gsAAJw4fjSjsfFmjUNSYXfJsDslSIoKu90B38DAZ1VKeIZlAQZ4fgtFo4cDZWiHIXktrCdlA2uAjnRNkHodb+2U9x6OZAZPf5y6UNGeNFJc4uPFrHE4XFBVFZIkg2GAytLS7QUn8xQA6DPIBL79EdzaoIMkUXLp5n7qyYcHcAZVAABFphBc7deeHhL7q3Y8fjzi4+O7Dbfv3d3dYbFYkPjISPhaopB95MT3za32IkkU4XK54HK6oBh0V7KP564GQM2w4NFQCVeneOCZaYswa+7rOBe0EwlJaqIbzwOUAaDgqy25bdtPlNwBsCcrC/Pnz4NOEJCQkGCcOHHSVACwWCxISkpC+sKXMTDUE+cObnXWNDX9gxJC7R0dICzB9ZrqfwYIwe3DBibDDm8Ql4zm6H2Y12cRypdNoyF+AWFD4zFLVAl4gUdZcUMTF1WdM/HLX+UABdDR4YSfvz9mzJgZGxEe9cmghKGhQYG9IUkiQsPCsW1nFt7bfAARyam7qWDYJYkuyCD5dbnHMl/uN5jvlxgyLCpCe8DNNoCL9w8kHUKV35ZV8oTnH2n/MSaYD1QVwGRkkblp9+dmP782W8b7d3Ig7+JlVNc1IWbgo1EMR77lBa7XnLmzsy5eKpj5/YYNZ/bsy8HZs6cx+bmpeGxYMma8+uMynbt3asHpY8vKy+yun13HIzPWmLM/fvsp0dY0vEHP39LapSIvqbUxwFZbBk0jMLhz2LbjyuG1W2pXpDz+F8b+fGV3l2IHjhyT3HSz5YnIqD4LCSHhoiK1OCSnF2VNI/1Co0IYvcljf9bOqm2bNiqiy4V9u7NujB2b1jhiaNKOlFGTlOrrp8fPfJF7RvDvr3MLjfQx+Op8eUUxq/abcDVXobVdtmYeda3JOlH3iq+ZtcYnvskOmVyi9fcZ1zllf5593OtWs0Oo+6VF9lQl1dOdpYThaLOVsC43sxDur6c/vLeouaDgjAYAfn6+qLtaB95dwGEcxeJ3Xg2Lrmt8bnL/ERZH2KBI0SV5UMf11tLKG3V1RMk/VR5yoDy38fqYtH1aYqQ3dOI1+Jg8Mf2trhAQhmnVKO3aDACEENyuU0VR8NvdM8/zoFwHMrduwJR/LYWv+4AbZbmjF2ujO6AbfYLx0XkyzmMRSuP5RFi9G1ALoH9bnOC5fbh0c9IPsIQ4Ab2uu2Uz97szUhWCnz7pwPoX3DCh6GMknk1ireo1tqBpBjEe/DOxXfJQjjyYhzCfKvYB1ovcOv0e2pVSCXQMNO33Q8q/BwB9W4jgmEg2zAAAAABJRU5ErkJggg=='
	}
	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

function OpenMobileManoirActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_manoir_actions',
		{
			title    = 'LS Diner',
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_tacobell', 'LS Diner', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)
		
		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and player.job and player.job.name == 'tacobell' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'manoirdejade_actions_menu' then
				    CoffreMenu()
				end

				CurrentAction = nil
				GUI.Time = GetGameTimer()

			end
		end
		if IsControlPressed(0,  Keys['F6']) and player.job ~= nil and player.job.name == 'tacobell' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_manoir_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobileManoirActionsMenu()
			GUI.Time = GetGameTimer()
		end
		
	end
end)

function CoffreMenu()

	local elements = {
	{label = _U('deposit_stock'), value = 'put_stock'},
    	{label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if player.job ~= nil and player.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_action'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        		OpenPutStocksMenu()
      		end
			if data.current.value == 'get_stock' then
		        OpenGetStocksMenu()
		    end
		    if data.current.value == 'boss_actions' then
		        TriggerEvent('esx_society:openBossMenu', 'tacobell', function(data, menu)
		        menu.close()
				end)
			end

			CurrentAction     = 'manoirdejade_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end


function OpenGetStocksMenu()

  ESX.TriggerServerCallback('manoirdejade:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
		if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('manoirdejade_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('manoirdejade:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('manoirdejade:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('manoirdejade:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end