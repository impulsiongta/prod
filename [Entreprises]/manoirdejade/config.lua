Config = {}
Config.DrawDistance = 50
Config.MarkerColor = {r = 0, g = 128, b = 255}
Config.Locale = 'fr'

Config.Zones = {

	Coffre = {
		Pos   = {x = -1549.46, y = -435.60, z = 35.88},
		Size  = {x = 1.501, y = 1.501, z = 0.5001},
		Type  = 1
	}
}