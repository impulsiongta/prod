local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local takeAlcools = {x=-1386.97,y=-608.242,z=30.3195}
local takeVehicle = {x=-1452.55,y=-627.86,z=30.68,a=33.13}
local takeJob = 0

--RUN ALCOOL
local colorBlipRun = 2

local recolte = {x=-760.496,y=-2585.79,z=13.8785}
local markerRecolte = {x = 2.001, y = 2.001, z = 0.5001}
local waitingRecolte = 2000

local traitement = {x=1017.4,y=-2515.45,z=28.3029}
local traitement2 = {x=1018.4,y=-2516.45,z=28.3029}
local markerTraitement = {x = 2.001, y = 2.001, z = 0.5001}
local waitingTraitement = 2000


local vente  = {x=-565.14,y=305.57,z=83.189}
local vente2 = {x=-565.54,y=303.26,z=83.17}
local vente3 = {x=-565.87,y=300.51,z=83.08}
local vente4 = {x=-564.71,y=298.00,z=83.06}
local vente5 = {x=-561.56,y=297.48,z=83.02}
local markerVente = {x = 1.501, y = 1.501, z = 0.5001}
local waitingVente = 400
local price = 1.60


-- RUN VIN

local colorBlipRun2 = 27

local recolte2_1 = {x=-1912.93,y=2074.54,z=140.38}
local recolte2_2 = {x=-1910.68,y=2072.78,z=140.38}
local recolte2_3 = {x=-1908.54,y=2070.76,z=140.38}

local waitingRecolte2 = 1000


-- RUN BOISSON

local colorBlipAchat = 38
local colorBlipAchat2 = 1

local achat1_1 = {x=148.51,y=237.89,z=106.82}
local achat1_2 = {x=150.59,y=236.99,z=106.86}
local achat1_3 = {x=153.73,y=235.92,z=106.77}
local achat1_4 = {x=155.65,y=235.28,z=106.72}

--plateau traiteur
local achat1_5 = {x=-875.66,y=-2109.76,z=9.91}

local markerAchat = {x = 1.501, y = 1.501, z = 0.5001}
local priceAchat = 3
local waitingAchat = 1000


local society = {x=-1386.08,y=-627.313,z=30.8196}
local coffre = {x=-1390.23,y=-600.212,z=30.3196}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}

local vehicle = -1

local player = -1
local JobAPI = nil

ESX = nil
local PlayerData = {}
local GUI                     = {}
GUI.Time                      = 0
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil

Farm = {}

local stop = false
local blipRecolte = -1
local blipTraitement = -1
local blipTraitement2 = -1
local blipVente = -1
local count = 0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'tequilala' then

		Config.Zones.BahaActions.Type = 1
		Config.Zones.BahaCraft.Type = 1

	else

		Config.Zones.BahaActions.Type = -1
		Config.Zones.BahaCraft.Type = -1
		
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)

    player = xPlayer

  	if player.job.name == 'tequilala' then

		Config.Zones.BahaActions.Type = 1
		Config.Zones.BahaCraft.Type = 1
		
	else

		Config.Zones.BahaActions.Type = -1
		Config.Zones.BahaCraft.Type = -1

	end
	
    while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end

	while player == -1 do
		Citizen.Wait(10)
	end
	if(player ~= -1 and player.job ~= nil and player.job.name == "tequilala") then
		--JobAPI.createTakeItem(takeAlcools.x,takeAlcools.y,takeAlcools.z, 'Stockage des l\'alcools',{'mojito', 'vodka', 'cafe' },{'Mojito', 'Vodka', 'Café'})
		if(player.job.grade_name == "boss" ) then
			--JobAPI.createSocietyMenu(society.x,society.y,society.z, 'tequilala', 'Bahama Mamas')
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'tequilalaoff', 'Compte Offshore Bahama Mamas')
		end
	end

	Citizen.CreateThread(function()
		RequestModel("BAHAMA")

		company = AddBlipForCoord(-1389.48, -591.75, 29.34)
	    SetBlipSprite(company, 93)
		SetBlipColour (company, 27)
	    SetBlipAsShortRange(company, true)
	    BeginTextCommandSetBlipName("STRING")
	    AddTextComponentString("Bahama Mamas")
	    EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end
		while true  do
			Citizen.Wait(0)
			if(player ~= -1 and player.job ~= nil and player.job.name == "tequilala") then
				--DrawMarker(1,takeAlcools.x,takeAlcools.y,takeAlcools.z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)
				DrawMarker(1,takeVehicle.x,takeVehicle.y,takeVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
			end
		end 

	end)


	Citizen.CreateThread(function()
		while player == -1 do
			Citizen.Wait(10)
		end
		while true do
			Citizen.Wait(0)
			if(player ~= -1) then
				if(player.job ~= nil and player.job.name == "tequilala") then
					if(isNear(takeVehicle)) then
							
						Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ accéder au garage.")
						
						if(IsControlJustPressed(1, 38)) then
							if not IsPedInAnyVehicle(GetPlayerPed(-1), false) then
								local elements = {}
								ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
								  for i=1, #garageVehicles, 1 do
									table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
								  end

								  ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'vehicle_spawner',
									{
									  title    = _U('vehicle_menu'),
									  align    = 'top-left',
									  elements = elements,
									},
									function(data, menu)

									  menu.close()

									  local vehicleProps = data.current.value

									  ESX.Game.SpawnVehicle(vehicleProps.model, takeVehicle, 34.0, function(vehicle)
										ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
										local playerPed = GetPlayerPed(-1)
										TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
									  end)

									  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'tequilala', vehicleProps)

									end,
									function(data, menu)

									  menu.close()

									  CurrentAction     = 'menu_vehicle_spawner'
									  CurrentActionMsg  = _U('vehicle_spawner')
									  CurrentActionData = {station = station, partNum = partNum}

									end
								  )

								end, 'tequilala')
								takeJob = takeJob + 1
								if takeJob == 1 then
									-- RUN ALCOOL
									JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'alcool', colorBlipRun, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte, 'Alcool')


									traitement = AddBlipForCoord(-1389.48, -591.75, 29.34)
									SetBlipSprite(traitement, 16 + 2)
									SetBlipColour(traitement, colorBlipRun)
									SetBlipDisplay(traitement, 4)

									traitement2 = AddBlipForCoord(-1389.48, -591.75, 29.34)
									SetBlipSprite(traitement2, 16 + 2)
									SetBlipColour(traitement2, colorBlipRun2)
									SetBlipDisplay(traitement2, 4)


									JobAPI.createVenteEntreprise(3, vente.x, vente.y, vente.z, 'whisky', price, 'tequilala', colorBlipRun, true, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'Whisky')
									JobAPI.createVenteEntreprise(3, vente2.x, vente2.y, vente2.z, 'vodka', price, 'tequilala', colorBlipRun, false, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'Vodka')
									JobAPI.createVenteEntreprise(3, vente3.x, vente3.y, vente3.z, 'mojito', price, 'tequilala', colorBlipRun, false, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'Mojito')
									JobAPI.createVenteEntreprise(3, vente4.x, vente4.y, vente4.z, 'biere', price, 'tequilala', colorBlipRun, false, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'Biere')
									JobAPI.createVenteEntreprise(3, vente5.x, vente5.y, vente5.z, 'bieresansalcool', price, 'tequilala', colorBlipRun, false, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'BiereNoAlc')

									-- RUN VIGNE
									JobAPI.createRecolte(1, recolte2_1.x,recolte2_1.y,recolte2_1.z, 'vin_rouge', colorBlipRun2, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte2, 'VinRouge')
									JobAPI.createRecolte(2, recolte2_2.x,recolte2_2.y,recolte2_2.z, 'jus_de_raisin', colorBlipRun2, false, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte2, 'JusRaisin')
									JobAPI.createRecolte(3, recolte2_3.x,recolte2_3.y,recolte2_3.z, 'champagne', colorBlipRun2, false, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte2, 'Champagne')

									-- RUN BOISSONS
									if(player.job.grade_name == "boss" or player.job.grade_name == "chef_equipe") then
										JobAPI.createAchatEntreprise(1, achat1_1.x, achat1_1.y, achat1_1.z, 'eau_minerale', priceAchat, 'tequilala', colorBlipAchat, true, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'blanc')
										JobAPI.createAchatEntreprise(1, achat1_2.x, achat1_2.y, achat1_2.z, 'coca', priceAchat, 'tequilala', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'rouge')
										JobAPI.createAchatEntreprise(1, achat1_3.x, achat1_3.y, achat1_3.z, 'redbull', priceAchat, 'tequilala', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'bleu')
										JobAPI.createAchatEntreprise(1, achat1_4.x, achat1_4.y, achat1_4.z, 'cafe', priceAchat, 'tequilala', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'marron')
										JobAPI.createAchatEntreprise(1, achat1_5.x, achat1_5.y, achat1_5.z, 'plateau_traiteur', priceAchat, 'tequilala', colorBlipAchat2, true, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'PlatRepas')
									end							
								end
							
							else
								local vehicleIsIn = GetVehiclePedIsIn(GetPlayerPed(-1), false)
								local vehicleProps = ESX.Game.GetVehicleProperties(vehicleIsIn)
								TriggerServerEvent('esx_society:putVehicleInGarage', 'tequilala', vehicleProps)
								ESX.Game.DeleteVehicle(vehicleIsIn)
								JobAPI.stop()
								SetBlipDisplay(traitement, 0)
								traitement = -1
								SetBlipDisplay(traitement2, 0)
								traitement2 = -1
								takeJob = takeJob - 1
							end
						end
					end
				end
			end
		end
	end)
end)

function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function spawnVehicle(name, tabl)

	hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1) 	

    Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
    SetVehicleOnGroundProperly(Truck)
    SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
   	SetVehicleEngineOn(Truck, true, false, false)
    
   	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck

end

function OpenCloakroomMenu()

	local elements = {
		{label = _U('clothes_civil'), value = 'citizen_wear'},
		{label = _U('clothes_travail'), value = 'travail_wear'},
		{label = _U('clothes_salle'), value = 'salle_wear'},
		{label = _U('deposit_stock'), value = 'put_stock'},
    	{label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if player.job ~= nil and player.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_actions'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'citizen_wear' then

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
					TriggerEvent('skinchanger:loadSkin', skin)
				end)

			end

			if data.current.value == 'travail_wear' then

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

					if skin.sex == 0 then
						TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
					else
						TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
					end
				end)
			end
		
			if data.current.value == 'salle_wear' then

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

					if skin.sex == 0 then
						SetPedComponentVariation(GetPlayerPed(-1), 4, 25, 0, 0)--Jean
						SetPedComponentVariation(GetPlayerPed(-1), 6, 10, 0, 0)--Chaussure
						SetPedComponentVariation(GetPlayerPed(-1), 8, 93, 1, 0)--Sous pull
						SetPedComponentVariation(GetPlayerPed(-1), 3, 11, 0, 0)--bras
						SetPedComponentVariation(GetPlayerPed(-1), 11, 11, 1, 0)--Veste
					else
						SetPedComponentVariation(GetPlayerPed(-1), 4, 6, 0, 0)--Jean
						SetPedComponentVariation(GetPlayerPed(-1), 6, 19, 3, 0)--Chaussure
						SetPedComponentVariation(GetPlayerPed(-1), 8, 24, 0, 0)--Sous pull
						SetPedComponentVariation(GetPlayerPed(-1), 3, 0, 0, 0)--bras
						SetPedComponentVariation(GetPlayerPed(-1), 11, 28, 3, 0)--Veste
					end
				end)
			end
			if data.current.value == 'put_stock' then
        		OpenPutStocksMenu()
      		end
			if data.current.value == 'get_stock' then
		        OpenGetStocksMenu()
		    end
		    if data.current.value == 'boss_actions' then
		        TriggerEvent('esx_society:openBossMenu', 'tequilala', function(data, menu)
		        menu.close()
				end)
			end

			CurrentAction     = 'baha_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end

function OpenMobileBahamasActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_bahama_actions',
		{
			title    = _U('bahama'),
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_tequilala', 'Bahama Mamas', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

function OpenBahamaCraftMenu()

	if player.job ~= nil then

		local elements = {
			{label = _U('vin'), value = 'vinchaud'},
			{label = _U('whisky'), value = 'whisky'},
			{label = _U('vodka'), value = 'vodka'},
			{label = _U('mojito'), value = 'mojito'},
			{label = _U('biere'), value = 'biere'},
			{label = _U('bieresa'), value = 'bieresansalcool'},
		}

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'bahama_craft',
			{
				title    = _U('craft'),
				align    = 'top-left',
				elements = elements
			},
			function(data, menu)
			
				if data.current.value == 'vinchaud' then
					TriggerServerEvent('esx_bahama:startCraftVin')
					TriggerServerEvent('esx_bahama:stopCraftWhisky')
					TriggerServerEvent('esx_bahama:stopCraftVodka')
					TriggerServerEvent('esx_bahama:stopCraftMojito')
					TriggerServerEvent('esx_bahama:stopCraftBiere')
					TriggerServerEvent('esx_bahama:stopCraftBiereSA')
				end
				
				if data.current.value == 'whisky' then
					TriggerServerEvent('esx_bahama:startCraftWhisky')
					TriggerServerEvent('esx_bahama:stopCraftVin')
					TriggerServerEvent('esx_bahama:stopCraftVodka')
					TriggerServerEvent('esx_bahama:stopCraftMojito')
					TriggerServerEvent('esx_bahama:stopCraftBiere')
					TriggerServerEvent('esx_bahama:stopCraftBiereSA')
				end
				
				if data.current.value == 'vodka' then
					TriggerServerEvent('esx_bahama:startCraftVodka')
					TriggerServerEvent('esx_bahama:stopCraftVin')
					TriggerServerEvent('esx_bahama:stopCraftWhisky')
					TriggerServerEvent('esx_bahama:stopCraftMojito')
					TriggerServerEvent('esx_bahama:stopCraftBiere')
					TriggerServerEvent('esx_bahama:stopCraftBiereSA')
				end
				
				if data.current.value == 'mojito' then
					TriggerServerEvent('esx_bahama:startCraftMojito')
					TriggerServerEvent('esx_bahama:stopCraftVin')
					TriggerServerEvent('esx_bahama:stopCraftWhisky')
					TriggerServerEvent('esx_bahama:stopCraftVodka')
					TriggerServerEvent('esx_bahama:stopCraftBiere')
					TriggerServerEvent('esx_bahama:stopCraftBiereSA')
				end
				
				if data.current.value == 'biere' then
					TriggerServerEvent('esx_bahama:startCraftBiere')
					TriggerServerEvent('esx_bahama:stopCraftVin')
					TriggerServerEvent('esx_bahama:stopCraftWhisky')
					TriggerServerEvent('esx_bahama:stopCraftVodka')
					TriggerServerEvent('esx_bahama:stopCraftMojito')
					TriggerServerEvent('esx_bahama:stopCraftBiereSA')
				end
				
				if data.current.value == 'bieresansalcool' then
					TriggerServerEvent('esx_bahama:startCraftBiereSA')
					TriggerServerEvent('esx_bahama:stopCraftVin')
					TriggerServerEvent('esx_bahama:stopCraftWhisky')
					TriggerServerEvent('esx_bahama:stopCraftVodka')
					TriggerServerEvent('esx_bahama:stopCraftMojito')
					TriggerServerEvent('esx_bahama:stopCraftBiere')
				end

			end,
			
			function(data, menu)
				menu.close()
				CurrentAction     = 'baha_craft_menu'
				CurrentActionMsg  = _U('open_craft')
				CurrentActionData = {}
			end
		)
	else
		ESX.ShowNotification (_U("craft_grade"))
	end
end

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)
		
		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and player.job and player.job.name == 'tequilala' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'baha_actions_menu' then
				    OpenCloakroomMenu()
				end
				
				if CurrentAction == 'baha_craft_menu' then
					OpenBahamaCraftMenu()
				end

				CurrentAction = nil
				GUI.Time = GetGameTimer()

			end
		end

		if IsControlPressed(0,  Keys['F6']) and player.job ~= nil and player.job.name == 'tequilala' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_bahama_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobileBahamasActionsMenu()
			GUI.Time = GetGameTimer()
		end

	end
end)

AddEventHandler('esx_bahama:hasEnteredMarker', function(zone)

	if zone == 'BahaActions' and player.job and player.job.name == 'tequilala' then
		CurrentAction     = 'baha_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end
	
	if zone == 'BahaCraft' then
		CurrentAction     = 'baha_craft_menu'
		CurrentActionMsg  = _U('open_craft')
		CurrentActionData = {}
	end
end)
			
AddEventHandler('esx_bahama:hasExitedMarker', function(zone)

	if zone == 'BahaCraft' then
		TriggerServerEvent('esx_bahama:stopCraftVin')
	end

	if zone == 'BahaCraft' then
		TriggerServerEvent('esx_bahama:stopCraftWhisky')
	end
	
	if zone == 'BahaCraft' then
		TriggerServerEvent('esx_bahama:stopCraftVodka')
	end
	
	if zone == 'BahaCraft' then
		TriggerServerEvent('esx_bahama:stopCraftMojito')
	end
	
	if zone == 'BahaCraft' then
		TriggerServerEvent('esx_bahama:stopCraftBiere')
	end
	
	if zone == 'BahaCraft' then
		TriggerServerEvent('esx_bahama:stopCraftBiereSA')
	end

	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('esx_bahama:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('esx_bahama:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 200, false, true, 2, false, false, false, false)
			end
		end
	end
end)


RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = 'Bar / Club',
		number     = 'tequilala',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAjGSURBVHjarFdbjF1VGf7+tda+nXP2OXPOzJzOTKfT0mFKb0NDCwhFQKRFQwgqojyIygPGaKKGEKoRY/BBlIRgNb6JT4oaFQgxVaMWbEAqpQ6Uy/RCy8B0Znrmcs6cyz6XfVnr92GAFOm0Rfie9sPOv771/bdvETPjbTy26it4D4gAZoABUgJgBhsGSQEOE0AgA9BKkmI1lBxg4msKOy683RsuWLOPjO2JFzqfBiE5PeRnpx5+51vh/wEzODF9sOQVVsH9mN2fvczuTQ+zEhkWBH90xcz834+242qYEba4jDX2A3zGUO+fgOEcg26we9O3pTb3XZ0eKRTJs2ASA+4kTVXwbK1NnzPY9e+wFBiz2PkRmO+G4f98MAIMsDbryZFfz2ws3pLdNriS0hY603XTeu5kJZwPVO7ivqxjtKi/MIPsloFNPTvX7Z/fM56H0d9GZO4EUP//CQhcK3LOvbkrh67OrC+6wYlyXH/5VBy1Qg+MHhFq2P0+gvE5BOOzSVRqqN6bNg7lLhl8qbpvYgsTXQ7gH+8Ne163p0/I3vTuwg0j16XW9biN8Vl0JspW1OikIOkkaVPPrCtC2RLtNypAj6eSIOyqPzs54K3Nr5RZp06EzURLNf2+CBDjWtWTeqjw8bWjVm9Klf70CsKpRc6N9rNq6qfkQrhbJGSn1uQQTtcjXevMwJOAJRAvtnq5nRStvOcys8fMOL3rzk2AsV7knJ93XXfBBqvgydk9hxG14xhMLDOWka40ENguU9IVKRvt6VqdYt4HhmECtGCw5i4AWdY8x5rB+vwJZIWrdme3D22y+3wq7TmsdbXzCDGOaW0EMzFJsYKF2GnnXEBDx7PNBjPGYFhDA05vBlDSixuhZsI4E8BnS8HbEi3JRPekL+7f6W9YQfN/O5roudb9VIvvQMwLb/1LYFMkY3KUsqFbUUc3owoRykiMsFwLuYv7OZqtq2ihuU+AXhQgCND5KECj1orUt7q2rxLVFyY5fL2ymxvR9wEYikybDAADAJQBASLjgJthEwkbKPEFS5MsXL0WSZg064dKmjQmpGsxQPif89/bhiSIIMUP/G2r/KQToXZg+hkKzXcgCACyAAokJTgxYCktSAmVsqCD2IKtNtqrsqmejw7DHcyivP+NkFw1p7LOZxCaNAg/AVA9uwKMLVYxfWP6ol4s7n+zbRbDu0DQWOqhbihaLV0FE8fMWktYAmRJJGGch03pzGgfwROovDAF1eV2F669YMTb0OuxTbeSJb4JwF1WAWIAtrwzs6Ho6KCN9rHyHmGLl8AACQFIsZGJemXKAmuAQ03kSpAiJJ2YhdamfWS+Xn1mIkrCJEuSPH9khfKGuzeYdvjX1pHyLUT0CIATZ06BEimk5c3umjyCI7MhN6NHiUQMQQCz4LT8stBGSN8Gh5qhDZFjA0QJt5KFsNGpthdafxEGj0vfvkHnnXvrh6aIWNv+xv6RcLpRM9X4+tMJvDsFggZUxu4TnkI4VV8giMNggIgAW17PrrxJKAGVd0y8EJCBgXAkpBAanURJI7XjOAvKtktoJo9RostwFIITZXBiut2hHJPAVcvXgOG88hxBIJhGeAqCykvFR2mTt++lMHHsgSysnJe0JqtsGCBLAAIqCeMeuy+zqfuTF+0SafU9kIhlgkXYAhwZhHNB4vZmXACDyxPQXGOYDhEDTAagBEzgrHUPJF0jSSJ9UVGHpWYcVZoGRFiqB5YEQtcVQ0YVnJyOE4vAlhGUAhEEABPrBeGqdMxJe/lBBJ7Vi+0SIKC6UxuQsT6FfucbJmvtEk1N/pY+dnvS1frBk4tQgkgRlO8gaUZQ+RTbK31TefaN100zfpglrTGuKC65KQM751Z0qDPQGF+WABEFcbXzbFhuwd9Y9NmlnxpLPiiC2PPX9xp/U3+1dmhGhbONV9hTtrQkrExK62oIpy9Luh6h82b1X4LEfqSsz5EtLWgGpKq7g10ymgsgDB072xzQHJnfN16eqbtruzk7UpSOtHTXtlWdritXB40jc1Z9bGYvpew2DECuguVb7XixycJX0EFMSLgufee7nLdvZmZQU0epke4SWaovnK5XGPzisnOAAZAQT7eOLvy2NpC9w798yM4kDDKaFw9Oon5w+o8iNn8wRfeXaMZQeS+RnlOJKi3lDRdcYpZIqy8lPbavBZjm27Nuj8ddl60SzfFSLiq3/ixIvHwuR1RDpO+v7JuIgtfmd4i0I+O54DU9GzwqQE/At+5iS/QpTfAGCzXTjkRSbh9PquGIN5BzpBJuUg/nVUu/4vT6Kwo71w5G1Y5dHZuZRsIPgyg8pyUjpVZR3rkqjqKVVGm/isXwbinEUdjyZpN3vkqxgbRkOz2cDxqvzmY41g+FE5Xt/qUDt3ZdtTqJS83IXuFvc9bks0mt0y4/+dq0rrZ/KCQdeGuLLTOKiQCwYJtu47TYCgbYllfKjv48S1Hibvs+dmUvnWq3UuuLHeGofOt4+agQ8tGw1Hi+8vTEFv/SoRF7qLAS7QTNw3OLtbGZQ0m5+TMlxeNnsubqjD7IcJXaBiIGQIDpcXexKzIgBmrRpAXRSW8dKAYT5VQ0F/xGCDHJwGRw8NTXOhPV24VvDXAjrsTV9hhrfsKyrGPn9y6gpZVEkRkTQVJnV2WlAdDWGQ0uUz3aKxqaczsuvFF5tl977uST0Pgdi6VVL4XcG8/UDkBQEVI2JYmSscQ5zPaZHVFRpuzF7ksHd6MV7aJ6PC5eb3xRBOZ49iNDO/ytK/3F/RPluNR8gASVTr8AWbJBtjxBSpTe8hBnxbsV4HcKhC3PSvzR/tHWsfKLrdkgtPv9H/ub+1anNxdz1bHJZv35qfsItBcfEGcoQgDA82G102lN1rYWdqxz/Vboy6x3IYxG+ZkT08GBqQcQml9ACP2hEjjNsL2aVNsPzj117K70cM8l5Citg9JkOFnbl5wKfsWJ/icJivEhYLmnWUzMvzanmmNBKdjEJADDx2FwDIZrSwYFHwr+OwAE4Wi/iDWo5gAAAABJRU5ErkJggg=='
	}
	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('bahama:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
    	if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('bahama_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('bahama:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('bahama:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do
	--print(json.encode(items[i]))
      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('bahama:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end