Config = {}
Config.DrawDistance = 120.0
Config.MarkerColor = {r = 0, g = 128, b = 255}
Config.Locale = 'fr'

Config.Zones = {

	BahaActions = {
		Pos   = {x = -1389.55, y = -591.65, z = 29.40},
		Size = {x = 1.201, y = 1.201, z = 0.5001},
		Type  = 1
	},
	
	BahaCraft = {
		Pos   = {x = -1391.99, y = -604.07, z = 29.35},
		Size  = {x = 1.501, y = 1.501, z = 0.5001},
		Type  = 1
	}
}