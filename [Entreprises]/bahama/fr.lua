Locales['fr'] = {

	--Cloakroom
	
		['cloakroom']					= 'Vestiaire',
		['clothes_civil']				= 'Tenue civil',
		['clothes_travail']				= 'Tenue de travail',
		['clothes_salle']				= 'Tenue de service de salle',

		['open_menu']					= 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le vestiaire',
		['bahama']						= 'Bahama Mamas',
		['bahama_stock']				= 'Bahama Stock',
		['deposit_stock']				= 'Déposer Stock',
  		['withdraw_stock']				= 'Prendre Stock',
		['quantity']					= 'quantité',
		['invalid_quantity']			= 'quantité invalide',
		['inventory']					= 'inventaire',
  		['boss_actions']				= 'Menu Patron',
		['you_removed']               = 'vous avez retiré x',
    	['you_added']                 = 'Vous avez ajouté x',
	['vehicle_spawner'] 		  = 'appuez sur ~INPUT_CONTEXT~ pour ~g~~h~sortir un véhicule',
	['vehicle_menu'] = 'véhicule',
	--Craft

		['craft']						= 'Appareil',
		['open_craft']					= 'Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~faire des boissons spéciale',
		['craft_grade']					= 'Vous n\'êtes ~r~pas assez expérimenté~s~ pour effectuer cette action',
		
		['prepaannule']					= 'Préparation ~r~~h~annulé',
		
		['craftvin_rouge']				= 'Vous n\'avez ~r~~h~plus de vin rouge',
		['prepavin_rouge']				= 'Préparation du ~b~~h~Vin chaud~s~ ...',
		
		['prepabiere'] 					= 'Préparation ~b~~h~de la bière~s~ ...',
		['prepabieresa'] 				= 'Préparation ~b~~h~de la bière sans alcool~s~ ...',
		['prepavodka'] 					= 'Préparation ~b~~h~de vodka~s~ ...',
		['prepawhisky'] 				= 'Préparation ~b~~h~du whisky~s~ ...',
		['prepamojito'] 				= 'Préparation ~b~~h~du mojito~s~ ...',
		
		['craftstop'] 					= 'Vous n\'avez ~r~~h~plus d\'alcool',
	
		['craftstopvin'] 					= 'Vous n\'avez ~r~~h~plus de vin rouge',
	
		['vin'] 						= 'Vin Chaud',
		['biere'] 						= 'Bière',
		['bieresa'] 					= 'Bière sans alcool',
		['whisky'] 						= 'Whisky',
		['vodka'] 						= 'Vodka',
		['mojito'] 						= 'Mojito',

}
