ESX = nil
PlayersCraftingVin    	= {}
PlayersCraftingWhisky   = {}
PlayersCraftingVodka    = {}
PlayersCraftingMojito   = {}
PlayersCraftingBiere  	= {}
PlayersCraftingBiereSA  = {}


TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'tequilala', 'Bahama Mamas', 'society_tequilala', 'society_tequilala', 'society_tequilala', {type = 'private'})
TriggerEvent('esx_society:registerSociety', 'tequilalaoff', 'Compte Offshore Bahama Mamas', 'society_tequilalaoff', 'society_tequilalaoff', 'society_tequilalaoff', {type = 'private'})

----------------------------------------CRAFT VIN CHAUD-----------------------------------------

local function CraftVin(source)

	SetTimeout(2000, function()

		if PlayersCraftingVin[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local VinQuantity = xPlayer.getInventoryItem('vin_rouge').count
			local vin_chaudQuantity = xPlayer.getInventoryItem('vin_chaud').count

			if VinQuantity <= 0 or vin_chaudQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('craftstopvin'))		
			else   
                xPlayer.removeInventoryItem('vin_rouge', 1)
                xPlayer.addInventoryItem('vin_chaud', 5)

				CraftVin(source)
			end
		end
	end)
end

RegisterServerEvent('esx_bahama:startCraftVin')
AddEventHandler('esx_bahama:startCraftVin', function()
	local _source = source
	PlayersCraftingVin[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('prepavin_rouge'))
	CraftVin(_source)
end)

RegisterServerEvent('esx_bahama:stopCraftVin')
AddEventHandler('esx_bahama:stopCraftVin', function()
	local _source = source
	PlayersCraftingVin[_source] = false
	TriggerClientEvent('esx:showNotification', _source, _U('prepaannule'))
end)

----------------------------------------CRAFT Whisky---------------------------------------------

local function CraftWhisky(source)

	SetTimeout(2000, function()

		if PlayersCraftingWhisky[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local AlcoolQuantity = xPlayer.getInventoryItem('alcool').count
			local whiskyQuantity = xPlayer.getInventoryItem('whisky').count

			if AlcoolQuantity <= 0 or whiskyQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('craftstop'))		
			else   
                xPlayer.removeInventoryItem('alcool', 1)
                xPlayer.addInventoryItem('whisky', 5)

				CraftWhisky(source)
			end
		end
	end)
end

RegisterServerEvent('esx_bahama:startCraftWhisky')
AddEventHandler('esx_bahama:startCraftWhisky', function()
	local _source = source
	PlayersCraftingWhisky[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('prepawhisky'))
	CraftWhisky(_source)
end)

RegisterServerEvent('esx_bahama:stopCraftWhisky')
AddEventHandler('esx_bahama:stopCraftWhisky', function()
	local _source = source
	PlayersCraftingWhisky[_source] = false
	TriggerClientEvent('esx:showNotification', _source, _U('prepaannule'))
end)

----------------------------------------CRAFT vodka---------------------------------------------

local function CraftVodka(source)

	SetTimeout(2000, function()

		if PlayersCraftingVodka[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local AlcoolQuantity = xPlayer.getInventoryItem('alcool').count
			local vodkaQuantity = xPlayer.getInventoryItem('vodka').count

			if AlcoolQuantity <= 0 or vodkaQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('craftstop'))		
			else   
                xPlayer.removeInventoryItem('alcool', 1)
                xPlayer.addInventoryItem('vodka', 5)

				CraftVodka(source)
			end
		end
	end)
end

RegisterServerEvent('esx_bahama:startCraftVodka')
AddEventHandler('esx_bahama:startCraftVodka', function()
	local _source = source
	PlayersCraftingVodka[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('prepavodka'))
	CraftVodka(_source)
end)

RegisterServerEvent('esx_bahama:stopCraftVodka')
AddEventHandler('esx_bahama:stopCraftVodka', function()
	local _source = source
	PlayersCraftingVodka[_source] = false
	TriggerClientEvent('esx:showNotification', _source, _U('prepaannule'))
end)

----------------------------------------CRAFT mojito---------------------------------------------

local function CraftMojito(source)

	SetTimeout(2000, function()

		if PlayersCraftingMojito[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local AlcoolQuantity = xPlayer.getInventoryItem('alcool').count
			local mojitoQuantity = xPlayer.getInventoryItem('mojito').count

			if AlcoolQuantity <= 0 or mojitoQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('craftstop'))		
			else   
                xPlayer.removeInventoryItem('alcool', 1)
                xPlayer.addInventoryItem('mojito', 5)

				CraftMojito(source)
			end
		end
	end)
end

RegisterServerEvent('esx_bahama:startCraftMojito')
AddEventHandler('esx_bahama:startCraftMojito', function()
	local _source = source
	PlayersCraftingMojito[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('prepamojito'))
	CraftMojito(_source)
end)

RegisterServerEvent('esx_bahama:stopCraftMojito')
AddEventHandler('esx_bahama:stopCraftMojito', function()
	local _source = source
	PlayersCraftingMojito[_source] = false
	TriggerClientEvent('esx:showNotification', _source, _U('prepaannule'))
end)


----------------------------------------CRAFT BIERE---------------------------------------------

local function CraftBiere(source)

	SetTimeout(2000, function()

		if PlayersCraftingBiere[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local AlcoolQuantity = xPlayer.getInventoryItem('alcool').count
			local biereQuantity = xPlayer.getInventoryItem('biere').count

			if AlcoolQuantity <= 0 or biereQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('craftstop'))		
			else   
                xPlayer.removeInventoryItem('alcool', 1)
                xPlayer.addInventoryItem('biere', 5)

				CraftBiere(source)
			end
		end
	end)
end

RegisterServerEvent('esx_bahama:startCraftBiere')
AddEventHandler('esx_bahama:startCraftBiere', function()
	local _source = source
	PlayersCraftingBiere[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('prepabiere'))
	CraftBiere(_source)
end)

RegisterServerEvent('esx_bahama:stopCraftBiere')
AddEventHandler('esx_bahama:stopCraftBiere', function()
	local _source = source
	PlayersCraftingBiere[_source] = false
	TriggerClientEvent('esx:showNotification', _source, _U('prepaannule'))
end)

----------------------------------------CRAFT BIERE SANS ALCOOL---------------------------------------------

local function CraftBiereSA(source)

	SetTimeout(2000, function()

		if PlayersCraftingBiereSA[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local AlcoolQuantity = xPlayer.getInventoryItem('alcool').count
			local bieresansalcoolQuantity = xPlayer.getInventoryItem('bieresansalcool').count

			if AlcoolQuantity <= 0 or bieresansalcoolQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('craftstop'))		
			else   
                xPlayer.removeInventoryItem('alcool', 1)
                xPlayer.addInventoryItem('bieresansalcool', 5)

				CraftBiereSA(source)
			end
		end
	end)
end

RegisterServerEvent('esx_bahama:startCraftBiereSA')
AddEventHandler('esx_bahama:startCraftBiereSA', function()
	local _source = source
	PlayersCraftingBiereSA[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('prepabieresa'))
	CraftBiereSA(_source)
end)

RegisterServerEvent('esx_bahama:stopCraftBiereSA')
AddEventHandler('esx_bahama:stopCraftBiereSA', function()
	local _source = source
	PlayersCraftingBiereSA[_source] = false
	TriggerClientEvent('esx:showNotification', _source, _U('prepaannule'))
end)

-----------------------------------------------TELEPHONE---------------------------------------------------

TriggerEvent('esx_phone:registerNumber', 'tequilala', 'Alerte Bahama Mamas', true, true)

----------------------------------
---- Ajout Gestion Stock Boss ----
----------------------------------

RegisterServerEvent('bahama:getStockItem')
AddEventHandler('bahama:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_tequilala', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('bahama:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_tequilala', function(inventory)
    cb(inventory.items)
  end)

end)

-------------
-- AJOUT 2 --
-------------

RegisterServerEvent('bahama:putStockItems')
AddEventHandler('bahama:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_tequilala', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('bahama:putStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_tequilala', function(inventory)
    cb(inventory.items)
  end)

end)

ESX.RegisterServerCallback('bahama:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)