Config = {}
Config.DrawDistance = 120.0
Config.MarkerColor = {r = 0, g = 128, b = 255}
Config.Locale = 'fr'

Config.Zones = {

	Coffre = {
		Pos   = {x = 2456.153, y = 4993.582, z = 46.81},
		Size  = {x = 1.501, y = 1.501, z = 0.5001},
		Type  = 1
	}
}