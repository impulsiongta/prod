Locales['fr'] = {

	--Cloakroom
	
		['cloakroom'] = 'Coffre',

		['open_menu'] = 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le coffre',
		['fermier'] = 'Ferme',
		['fermier_stock'] = 'Ferme Stock',
		['deposit_stock']             = 'Déposer Stock',
  		['withdraw_stock']            = 'Prendre Stock',
		['quantity']                  = 'quantité',
		['invalid_quantity']          = 'quantité invalide',
		['inventory']                 = 'inventaire',
		['boss_action']               = 'Menu patron',
		['you_removed']               = 'vous avez retiré x',
    		['you_added']                 = 'Vous avez ajouté x',
		['vehicle_spawner'] 		  = 'appuez sur ~INPUT_CONTEXT~ pour ~g~~h~sortir un véhicule',
		['vehicle_menu'] = 'véhicule',
}
