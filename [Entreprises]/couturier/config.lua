Config = {}
Config.DrawDistance              = 120.0
Config.MarkerColor               = {r = 0, g = 128, b = 255}
Config.Locale                    = 'fr'

Config.Zones = {

	Coffre = {
		Pos   	= { x = 706.63, y = -965.85, z = 30.443},
		Size 	= { x = 1.101, y = 1.101, z = 0.5001},
		Type  	= 1
	},

	garageOut = {
		Pos   	= { x = 743.08, y = -948.07, z = 25.63},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= -1
	},
	garageIn = {
		Pos   	= { x = 1109.94, y = -3161.10, z = -37.51},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= -1
	},
	garageGoOut = {
		Pos   	= { x = 1110.13, y = -3164.73, z = -37.51},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= 1
	},
	garageGoIn = {
		Pos   	= { x = 740.16, y = -948.68, z = 25.63},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= 1
	},
	garageOutFoot = {
		Pos   	= { x = 719.97, y = -967.33, z = 30.39},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= -1
	},
	garageInFoot = {
		Pos   	= { x = 1121.72, y = -3152.93, z = -37.06},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= -1
	},
	garageGoOutFoot = {
		Pos   	= { x = 1120.34, y = -3152.34, z = -37.06},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= 1
	},
	garageGoInFoot = {
		Pos   	= { x = 720.53, y = -969.24, z = 30.39},
		Size 	= { x = 1.101, y = 1.101, z = 0.5000},
		Type  	= 1
	}
}