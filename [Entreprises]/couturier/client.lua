local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local takeVehicle = {x=722.553,y=-982.69,z=24.147,a=270.709}
local depositVehicle = {x=718.553,y=-981.69,z=24.147,a=270.709}
local takeVetement = {x=706.63,y=-960.85,z=30.443}

local colorBlipRun = 2

local recolte = {x=2309.0532,y=4886.21,z=41.808}
local markerRecolte = {x = 2.001, y = 2.001, z = 0.5001}
local waitingRecolte = 2000

local traitement1 = {x=-589.065,y=-1587.855,z=26.751}
local markerTraitement1 = {x = 2.001, y = 2.001, z = 0.5001}
local waitingTraitement1 = 2000

local traitement2 = {x=739.399,y=-969.995,z=24.644}
local markerTraitement2 = {x = 2.001, y = 2.001, z = 0.5001}
local waitingTraitement2 = 2000

local vente = {x=-700.349,y=-147.137,z=37.846}
local markerVente = {x = 2.001, y = 2.001, z = 0.5001}
local waitingVente = 2000
local price = 12


local coffre = {x=706.670,y=-965.801,z=30.395}
local coffre_money = {x=720.855,y=-965.46,z=30.395}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}

local phase = 0
local player = -1
local JobAPI = nil

local vehicle = -1
local haveTakeVetement = false

ESX = nil
local PlayerData = {}
local GUI                     = {}
GUI.Time                      = 0
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil

Farm = {}

local stop = false
local blipRecolte = -1
local blipTraitement = -1
local blipTraitement2 = -1
local blipVente = -1
local count = 0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'couturier' then
		Config.Zones.Coffre.Type	   = 1
		Config.Zones.garageGoOut.Type	   = 1
		Config.Zones.garageGoIn.Type	   = 1
		Config.Zones.garageGoOutFoot.Type	   = 1
		Config.Zones.garageGoInFoot.Type	   = 1

	else
		Config.Zones.Coffre.Type	   = -1
		Config.Zones.garageGoOut.Type	   = -1
		Config.Zones.garageGoIn.Type	   = -1
		Config.Zones.garageGoOutFoot.Type	   = -1
		Config.Zones.garageGoInFoot.Type	   = -1
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	playerJob = xPlayer.job.name
    playerGrade = xPlayer.job.grade
    player = xPlayer

	while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end
	if player.job.name == 'couturier' then
		Config.Zones.Coffre.Type	   = 1
		Config.Zones.garageGoOut.Type	   = 1
		Config.Zones.garageGoIn.Type	   = 1
		Config.Zones.garageGoOutFoot.Type	   = 1
		Config.Zones.garageGoInFoot.Type	   = 1
	else
		Config.Zones.Coffre.Type	   = -1
		Config.Zones.garageGoOut.Type	   = -1
		Config.Zones.garageGoIn.Type	   = -1
		Config.Zones.garageGoOutFoot.Type	   = -1
		Config.Zones.garageGoInFoot.Type	   = -1
	end



	while player == -1 do
		Citizen.Wait(10)
	end


	if(player ~= -1 and player.job ~= nil and player.job.name == "couturier") then
		--JobAPI.createCoffre(coffre.x,coffre.y,coffre.z,"couturier")
		if (player.job.grade_name == "boss") then
			--JobAPI.createSocietyMenu(coffre_money.x,coffre_money.y,coffre_money.z, 'couturier', 'Old Leather')
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'couturieroff', 'Compte Offshore Old Leather')
		end
	end

	Citizen.CreateThread(function()
		--RequestModel("ASHRO")

		company = AddBlipForCoord(takeVetement.x, takeVetement.y, takeVetement.z)
	    SetBlipSprite(company, 366)
		SetBlipColour(company, 21)
	    SetBlipAsShortRange(company, true)
	    BeginTextCommandSetBlipName("STRING")
	    AddTextComponentString("Old Leather")
	    EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end


		while true do
			Citizen.Wait(0)
			if(player~=-1 ) then
				if(player.job ~= nil and player.job.name ~= nil and player.job.name == "couturier") then
					DrawMarker(1,takeVetement.x,takeVetement.y,takeVetement.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
					if(isNear(Config.Zones.Coffre.Pos)) then
						--ESX.Game.Utils.DrawText3D(Config.Zones.Coffre.Pos, 'COUCOU ICI LE COFFRE', 2)
					end

					if(isNear(takeVetement)) then
						if(haveTakeVetements) then
							--ESX.Game.Utils.DrawText3D(takeVetement, 'COUCOU ICI LES vêtements', 1)
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~reprendre vos vêtements.")
						else
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre vos habits de travail.")
						end

						if(IsControlJustPressed(1, 38)) then
							haveTakeVetements = not haveTakeVetements
							phase = 0

							if(not haveTakeVetements) then
								JobAPI.stop()

								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									TriggerEvent('skinchanger:loadSkin', skin)
								end)
							else
								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									if skin.sex == 0 then
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
									else
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
									end
								end)
							end
						end
					end
					
					if(takeVetement) then
					
						if(haveTakeVetements) then
							DrawMarker(1,takeVehicle.x,takeVehicle.y,takeVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
							DrawMarker(1,depositVehicle.x,depositVehicle.y,depositVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

							if(isNear(takeVehicle)) then
							
								Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~sortir un véhicule.")
								
								if(IsControlJustPressed(1, 38)) then
							
									
									local elements = {}
									ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
									  for i=1, #garageVehicles, 1 do
										table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
									  end

									  ESX.UI.Menu.Open(
										'default', GetCurrentResourceName(), 'vehicle_spawner',
										{
										  title    = _U('vehicle_menu'),
										  align    = 'top-left',
										  elements = elements,
										},
										function(data, menu)

										  menu.close()

										  local vehicleProps = data.current.value

										  ESX.Game.SpawnVehicle(vehicleProps.model, takeVehicle, 270.0, function(vehicle)
											ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
											local playerPed = GetPlayerPed(-1)
											TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
										  end)

										  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'couturier', vehicleProps)

										end,
										function(data, menu)

										  menu.close()

										  CurrentAction     = 'menu_vehicle_spawner'
										  CurrentActionMsg  = _U('vehicle_spawner')
										  CurrentActionData = {station = station, partNum = partNum}

										end
									  )

									end, 'couturier')
									JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'cuir_brut', colorBlipRun, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte, 'bleu')
									JobAPI.createTraitementMultiple(2, traitement1.x,traitement1.y,traitement1.z, 'cuir_brut', 1, 'cuir_tane', 1, colorBlipRun, true, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement1, 'bleu')
									JobAPI.createTraitementMultiple(3, traitement2.x,traitement2.y,traitement2.z, 'cuir_tane', 1, 'veste_cuir', 1, colorBlipRun, true, markerTraitement2.x, markerTraitement2.y, markerTraitement2.z, waitingTraitement2, 'bleu')
									JobAPI.createVenteEntreprise(4, vente.x, vente.y, vente.z, 'veste_cuir', price, 'couturier', colorBlipRun, true, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'bleu')
								end							
							else
									local vehicleIsIn = GetVehiclePedIsIn(GetPlayerPed(-1), false)
									local vehicleProps = ESX.Game.GetVehicleProperties(vehicleIsIn)
									TriggerServerEvent('esx_society:putVehicleInGarage', 'couturier', vehicleProps)
									ESX.Game.DeleteVehicle(vehicleIsIn)
									JobAPI.stop()
							end
						end	
					end

				end
			end
		end
	end)
end)


function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function spawnVehicle(name, tabl)

	hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1) 	

    Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
    SetVehicleOnGroundProperly(Truck)
    SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
   	SetVehicleEngineOn(Truck, true, false, false)

   	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck

end

AddEventHandler('couturier:hasEnteredMarker', function(zone)

	if zone == 'Coffre' and player.job and player.job.name == 'couturier' then
		CurrentAction     = 'couturier_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}

	elseif zone == 'garageGoIn' and player.job and player.job.name == 'couturier' then
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetEntityCoords(GetVehiclePedIsUsing(GetPlayerPed(-1)), Config.Zones.garageIn.Pos.x, Config.Zones.garageIn.Pos.y, Config.Zones.garageIn.Pos.z, false, false, false, false)
			SetTimeout(2000, function()
				SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageIn.Pos.x, Config.Zones.garageIn.Pos.y, Config.Zones.garageIn.Pos.z, false, false, false, false)
				SetEntityHeading(GetVehiclePedIsUsing(GetPlayerPed(-1)), 357.46)
			end)
		else
			SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageIn.Pos.x, Config.Zones.garageIn.Pos.y, Config.Zones.garageIn.Pos.z, false, false, false, false)
			SetEntityHeading(GetPlayerPed(-1), 357.46)
		end

	elseif zone == 'garageGoOut' and player.job and player.job.name == 'couturier' then
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetEntityCoords(GetVehiclePedIsUsing(GetPlayerPed(-1)), Config.Zones.garageOut.Pos.x, Config.Zones.garageOut.Pos.y, Config.Zones.garageOut.Pos.z, false, false, false, false)
			SetTimeout(2000, function()
				SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageOut.Pos.x, Config.Zones.garageOut.Pos.y, Config.Zones.garageOut.Pos.z, false, false, false, false)
				SetEntityHeading(GetVehiclePedIsUsing(GetPlayerPed(-1)), 267.00)
			end)
		else
			SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageOut.Pos.x, Config.Zones.garageOut.Pos.y, Config.Zones.garageOut.Pos.z, false, false, false, false)
			SetEntityHeading(GetPlayerPed(-1), 267.00)
		end
		
	elseif zone == 'garageGoInFoot' and player.job and player.job.name == 'couturier' then
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetEntityCoords(GetVehiclePedIsUsing(GetPlayerPed(-1)), Config.Zones.garageInFoot.Pos.x, Config.Zones.garageInFoot.Pos.y, Config.Zones.garageInFoot.Pos.z, false, false, false, false)
			SetTimeout(2000, function()
				SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageInFoot.Pos.x, Config.Zones.garageInFoot.Pos.y, Config.Zones.garageInFoot.Pos.z, false, false, false, false)
				SetEntityHeading(GetVehiclePedIsUsing(GetPlayerPed(-1)), 358.27)
			end)
		else
			SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageInFoot.Pos.x, Config.Zones.garageInFoot.Pos.y, Config.Zones.garageInFoot.Pos.z, false, false, false, false)
			SetEntityHeading(GetPlayerPed(-1), 358.27)
		end
		
	elseif zone == 'garageGoOutFoot' and player.job and player.job.name == 'couturier' then
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetEntityCoords(GetVehiclePedIsUsing(GetPlayerPed(-1)), Config.Zones.garageOutFoot.Pos.x, Config.Zones.garageOutFoot.Pos.y, Config.Zones.garageOutFoot.Pos.z, false, false, false, false)
			SetTimeout(2000, function()
				SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageOutFoot.Pos.x, Config.Zones.garageOutFoot.Pos.y, Config.Zones.garageOutFoot.Pos.z, false, false, false, false)
				SetEntityHeading(GetVehiclePedIsUsing(GetPlayerPed(-1)), 357.97)
			end)
		else
			SetEntityCoords(GetPlayerPed(-1), Config.Zones.garageOutFoot.Pos.x, Config.Zones.garageOutFoot.Pos.y, Config.Zones.garageOutFoot.Pos.z, false, false, false, false)
			SetEntityHeading(GetPlayerPed(-1), 357.97)
		end
	end
	
end)
			
AddEventHandler('couturier:hasExitedMarker', function(zone)

	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
	
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('couturier:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('couturier:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

				if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
					DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 200, false, true, 2, false, false, false, false)
			end
		end
	end
end)
function OpenMobileCouturierActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_couturier_actions',
		{
			title    = 'Old Leather',
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_couturier', 'Old Leather', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)

		if IsControlPressed(0,  Keys['F6']) and player.job ~= nil and player.job.name == 'couturier' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_couturier_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobileCouturierActionsMenu()
			GUI.Time = GetGameTimer()
		end

		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and player.job and player.job.name == 'couturier' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'couturier_actions_menu' then
					CoffreMenu()
				end				

				CurrentAction = nil
				GUI.Time      = GetGameTimer()

			end
		end
	end
end)

function CoffreMenu()

	local elements = {
		{label = _U('deposit_stock'), value = 'put_stock'},
    	{label = _U('withdraw_stock'), value = 'get_stock'}
	}
	print(player.job.grade_name)
	if player.job ~= nil and player.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_action'), value = 'boss_actions'})
	end
	
	if player.job ~= nil and player.job.grade_name == 'chief' then
	    table.insert(elements, {label = _U('boss_action'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        			OpenPutStocksMenu()
      			end

			if data.current.value == 'get_stock' then
		        	OpenGetStocksMenu()
		      	end

		      	if data.current.value == 'boss_actions' then
		        	TriggerEvent('esx_society:openBossMenu', 'couturier', function(data, menu)
		          	menu.close()
		        	end)
		      	end

			CurrentAction     = 'couturier_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end


function OpenGetStocksMenu()

  ESX.TriggerServerCallback('couturier:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
		if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('couturier_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('couturier:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('couturier:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('couturier:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end