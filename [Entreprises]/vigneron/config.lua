Config = {}
Config.DrawDistance              = 100.0
Config.MarkerColor               = {r = 0, g = 128, b = 255}
Config.Locale                    = 'fr'

Config.Zones = {

	Coffre = {
		Pos   = {x = -1928.818, y = 2059.444, z = 140.838},
		Size = {x = 1.001, y = 1.001, z = 0.5001},
		Type  = 1
	}
}