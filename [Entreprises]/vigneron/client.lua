local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

JobAPI = nil

TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)

local player = -1

local takeVetement = {x=-1889.305,y=2051.001,z=140.985}
local takeVehicle = {x=-1910.92,y=2041.66,z=140.74,a=194.07}

local coffre = {x=-1928.818,y=2059.444,z=140.838}
local coffre_money = {x=-1909.44,y=2082.446,z=140.384}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}

local colorBlipRun = 2

local recolte = {x=-1899.408,y=2129.708,z=125.354}
local markerRecolte = {x = 2.001, y = 2.001, z = 0.5001}
local waitingRecolte = 2000

local traitement = {x=838.528,y=-1929.002,z=28.976}
local markerTraitement = {x = 2.001, y = 2.001, z = 0.5001}
local waitingTraitement = 2000

local ventes = {
	{x=-3005.165,y=78.773,z=11.608},
	{x=-1849.478,y=-325.431,z=48.146}
}
local markerVente = {x = 2.001, y = 2.001, z = 0.5001}
local waitingVente = 2000
local price = 14


local currentVehicle = ""

local phase = 0
local haveTakeVetements = false
local vehicle = -1

local blips = {}
ESX = nil
local PlayerData = {}
local GUI                     = {}
GUI.Time                      = 0

local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)



RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'vigneron' then
		Config.Zones.Coffre.Type	   = 1
	else
		Config.Zones.Coffre.Type	   = -1
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)

    playerJob = xPlayer.job.name
    playerGrade = xPlayer.job.grade
    player = xPlayer

	while player == -1 do
		Wait(10)
	end
	
	if player.job.name == 'vigneron' then
		Config.Zones.Coffre.Type	   = 1
	else
		Config.Zones.Coffre.Type	   = -1
	end

	while JobAPI == nil do
		Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end

	if(player ~= -1 and player.job ~= nil and player.job.name == "vigneron") then
			--JobAPI.createCoffre(coffre.x,coffre.y,coffre.z,"vigneron")
		if (player.job.grade_name == "boss") then
			JobAPI.createSocietyMenu(coffre_money.x,coffre_money.y,coffre_money.z, 'vigneron', 'Gordon’s Wine & Co')
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'vigneronoff', 'Compte Offshore Gordon’s Wine & Co')
		end
	end


	Citizen.CreateThread(function()

		company = AddBlipForCoord(takeVetement.x, takeVetement.y, takeVetement.z)
		SetBlipSprite(company, 93)
		SetBlipAsShortRange(company, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Gordon’s Wine & Co")
		EndTextCommandSetBlipName(company)


		while true do
			Citizen.Wait(0)
			if(player ~= -1) then
				if(player.job ~= nil and player.job.name == "vigneron") then
					DrawMarker(1,takeVetement.x,takeVetement.y,takeVetement.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

					if(haveTakeVetements) then
                    	DrawMarker(1,takeVehicle.x,takeVehicle.y,takeVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
					end

					if(player.job.grade_name == "boss") then
						DrawMarker(1,pc.x,pc.y,pc.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
					end
				end
			end
		end

	end)


	Citizen.CreateThread(function()

		while true do
			Citizen.Wait(0)

			if(player ~= -1) then
				if(player.job ~= nil and player.job.name ~= nil and player.job.name == "vigneron") then
					if(isNear(takeVetement)) then
						if(haveTakeVetements) then
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~reprendre vos vêtements.")
						else
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre vos habits de travail.")
						end

						if(IsControlJustPressed(1, 38)) then
							haveTakeVetements = not haveTakeVetements
							phase = 0

							if(not haveTakeVetements) then
								JobAPI.stop()

								if(DoesEntityExist(vehicle)) then
									DeleteVehicle(vehicle)
								end

								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									TriggerEvent('skinchanger:loadSkin', skin)
								end)
							else
								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									if skin.sex == 0 then
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
									else
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
									end
								end)
							end
						end
					end

					if(takeVetement) then

						if(isNear(takeVehicle)) then
							if(currentVehicle ~= "mule3") then
								Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre votre camion.")

								if(IsControlJustPressed(1, 38)) then

									if(DoesEntityExist(vehicle)) then
										DeleteVehicle(vehicle)
									end

									currentVehicle = "mule3"
									spawnVehicle("mule3", takeVehicle)

									local newBlip = blips[#blips+1]


									JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'raisin', colorBlipRun, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte, 'JusRaisin')
									JobAPI.createTraitementMultiple(2, traitement.x,traitement.y,traitement.z, 'raisin', 1, 'vin_rouge', 1, colorBlipRun, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement, 'VinRouge')
									for _,k in pairs(ventes) do
										JobAPI.createVenteEntreprise(3, k.x, k.y, k.z, 'vin_rouge', price, "vigneron", colorBlipRun, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'VinRouge')
									end
								end
							else
								Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ranger votre camion.")

								if(IsControlJustPressed(1, 38)) then

									if(DoesEntityExist(vehicle)) then
										DeleteVehicle(vehicle)
									end

									JobAPI.stop()

									currentVehicle = ""
									phase = 0
								end
							end
						end
					end
				end
			end
		end
	end)
end)




function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function spawnVehicle(name, tabl)
	local hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1)

	local Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
	SetVehicleOnGroundProperly(Truck)
	SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
	SetVehicleEngineOn(Truck, true, false, false)

	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck
end

function OpenMobileVigneronActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_vigneron_actions',
		{
			title    = 'Gordon’s Wine & Co',
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_vigneron', 'Gordon’s Wine & Co', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

AddEventHandler('vigneron:hasEnteredMarker', function(zone)

	if zone == 'Coffre' and player.job and player.job.name == 'vigneron' then
		CurrentAction     = 'vigneron_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end
end)
			
AddEventHandler('vigneron:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('vigneron:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('vigneron:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 200, false, true, 2, false, false, false, false)
			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)

		if IsControlPressed(0,  Keys['F6']) and player.job ~= nil and player.job.name == 'vigneron' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_vigneron_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobileVigneronActionsMenu()
			GUI.Time = GetGameTimer()
		end
		
		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and player.job and player.job.name == 'vigneron' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'vigneron_actions_menu' then
					CoffreMenu()
				end				

				CurrentAction = nil
				GUI.Time      = GetGameTimer()

			end
		end

	end
end)


function CoffreMenu()

	local elements = {
		{label = _U('deposit_stock'), value = 'put_stock'},
    	{label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if PlayerData.job ~= nil and PlayerData.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_actions'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('vigneron_stock'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        			OpenPutStocksMenu()
      			end

			if data.current.value == 'get_stock' then
		        	OpenGetStocksMenu()
		      	end

		      	if data.current.value == 'boss_actions' then
		        	TriggerEvent('esx_society:openBossMenu', 'vigneron', function(data, menu)
		          	menu.close()
		        	end)
		      	end

			CurrentAction     = 'vigneron_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('vigneron:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
    	if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('vigneron_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('vigneron:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('vigneron:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('vigneron:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end