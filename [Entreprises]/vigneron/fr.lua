Locales['fr'] = {

	['open_menu'] = 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le coffre',
	['vigneron_stock'] = 'Vigneron Stock',
	['deposit_stock']             = 'Déposer Stock',
  	['withdraw_stock']            = 'Prendre Stock',
	['quantity']                  = 'quantité',
	['invalid_quantity']          = 'quantité invalide',
	['inventory']                 = 'inventaire',
}