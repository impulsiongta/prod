local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local takeVehicle = {x=-1098.37,y=-257.06,z=37.68, a=157.15}
local takeVetement = {x=-1057.15,y=-239.12,z=39.73}


local colorBlipRun = 2

-- local recolte = {x=-580.15,y=5343.48,z=70.24}
local markerRecolte = {x = 2.001, y = 2.001, z = 0.5001}
local waitingRecolte = 2000

-- local traitement = {x=910.76,y=-2486.76,z=28.48}
local markerTraitement = {x = 2.001, y = 2.001, z = 0.5001}
local waitingTraitement = 2000

-- local vente = {x=-1044.03,y=-237.46,z=37.96}
local markerVente = {x = 2.001, y = 2.001, z = 0.5001}
local waitingVente = 2000
local price = 17

local coffre = {x=-1069.1,y=-246.63,z=44.02}
local coffre_money = {x=-1050.27,y=-228.97,z=44.02}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}

local player = -1
local JobAPI = nil

local vehicle = -1
local haveTakeVetement = false

ESX = nil
local PlayerData = {}
local GUI                     = {}
GUI.Time                      = 0
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'lossantostimes' then
		Config.Zones.Coffre.Type	   = 1
	else
		Config.Zones.Coffre.Type	   = -1
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	player = xPlayer

	while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end
	
	if player.job.name == 'lossantostimes' then

		Config.Zones.Coffre.Type = 1
		
	else

		Config.Zones.Coffre.Type = -1

	end

	while player == -1 do
		Citizen.Wait(10)
	end

	if(player ~= -1 and player.job ~= nil and  player.job.name == "lossantostimes") then
		--JobAPI.createCoffre(coffre.x,coffre.y,coffre.z,"lossantostimes")
		if (player.job.grade_name == "boss") then
			--JobAPI.createSocietyMenu(coffre_money.x,coffre_money.y,coffre_money.z, 'lossantostimes', 'Los Santos Times')
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'lossantostimesoff', 'Compte Offshore Los Santos Times')
		end
	end


	Citizen.CreateThread(function()
		RequestModel("RUMPO2")

		local company = AddBlipForCoord(takeVetement.x, takeVetement.y, takeVetement.z)
		SetBlipSprite(company, 135)
		SetBlipScale(company, 1.0)
		SetBlipAsShortRange(company, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Los Santos Times")
		EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end


		while true do
			Citizen.Wait(0)
			if(player~=-1) then
				if(player.job ~= nil and player.job.name == "lossantostimes") then
					DrawMarker(1,takeVetement.x,takeVetement.y,takeVetement.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

					if(isNear(takeVetement)) then
						if(haveTakeVetement) then
							Info("Appuyez sur ~g~E~w~ pour ~g~~h~reprendre vos vêtements.")
						else
							Info("Appuyez sur ~g~E~w~ pour ~g~~h~prendre votre service.")
						end

						if(IsControlJustPressed(1, 38)) then
							haveTakeVetement = not haveTakeVetement
						end
					end


					if(haveTakeVetement) then
						DrawMarker(1,takeVehicle.x,takeVehicle.y,takeVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

						if(isNear(takeVehicle)) then
							if(DoesEntityExist(vehicle)) then
								Info("Appuyez sur ~g~E~w~ pour ~g~~h~rentrer le véhicule.")
							else
								Info("Appuyez sur ~g~E~w~ pour ~g~~h~sortir le véhicule.")
							end

							if(IsControlJustPressed(1, 38)) then
								if(DoesEntityExist(vehicle)) then
									DeleteVehicle(vehicle)
									JobAPI.stop()
								else
									spawnVehicle("RUMPO2",takeVehicle)
									SetVehicleLivery(vehicle, 0)
									JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'rouleau_de_papier', colorBlipRun, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte, 'bleu')
									JobAPI.createTraitementMultiple(2, traitement.x,traitement.y,traitement.z, 'rouleau_de_papier', 1, 'journaux', 1, colorBlipRun, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement, 'bleu')
									JobAPI.createVenteEntreprise(3, vente.x, vente.y, vente.z, 'journaux', price, 'lossantostimes', colorBlipRun, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'bleu')
								end
							end
						end
					end
				end
			end
		end

	end)
end)



function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function spawnVehicle(name, tabl)
	local hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1)
	local Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
	SetVehicleOnGroundProperly(Truck)
	SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
	SetVehicleEngineOn(Truck, true, false, false)

	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck
end

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = 'LS Times',
		number     = 'lossantostimes',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAS6SURBVHjatJfda1NZEMB/9yY3bUMSk7U119o8pIgVHwqisaBEWH0QhEX7sn+EsF+wu7rWr92a6voii30Ri/jQJ6FCfZCK+yCLxUDVWpSorQ3sw7a0SD8kiUlzc2cf9t6QxqSf2YG595yZc+bMnZkzMxc2Bz8BVzYjwFmB5j558uRZXdf9mUwmD6CqKh6Pp/7JkyfDL1++7LfWfQNcs8Y54BI1gqbu7u74u3fvTCmBhw8fzpw4caLXWvMdIGV4kVpCa2vr19lsVkRE7t27lwDcFuv7CofbeGG956jVGNls9p+lpSUA5ubmFoAM8C1wfQV5vwLna6KAqqpORVHssWGZ/Y81yPwNOLeZIKwEESC6jg/rBkygp1YK1JcTAoEA27ZtQ1VVTNNkcXGRfD5PKBTizZs35HK5mBUXVzaqQLYaY/fu3fj9fpLJJCKCpmkEAgE8Hg8iQigU4v3791gWUIHYRhT4qhKxubkZj8dDPB5nz5491NfX43A4WFxcZGRkhGAwyPbt20u3XLbesfUE4TmgqxIjGAwyNjZGU1MTTqeTFy9eMDIygoiwd+9eUqkUdvCWKdG1VgXOW0H0GSiKgsvlIp/PYxgGIkJbWxvHjx9nYmICEcHj8WAYRqXtl4FfVnPBRTulikiRaI9FhEKhgKqqzM/P4/V6uXPnDrlcjgcPHpBMJtm/fz/z8/PV3NpjBebVSsxLpVlt69atkkqlRESkr6+vSA8Gg9Lc3Fych8Nh6ezslNbWVgHE7/dLXV2drJAtBThTKXkUFxw5ckRu3LhRrAMLCwsyMDAgd+/elfv378vw8LB0dHQsExoOh6W9vX21g0vxZwAF+N2e2HDz5k0ikQipVOo/PzmduN3uZW6Ympqit7eXqakpAoEAhmGwY8cOfD6ffQWLcVPqztnZWd6+fWtPL9gP2Qj6fD65deuWrAcGBgZKZVxXLfOfWUs69Pl8HD58mJaWFgA+fvxIPp8H4Pbt2xw7doyDBw/y7NkzACYnJzl06BBHjx7l9OnTGIZBXV2dLe4a8EOp/DOrfXEoFJKdO3dKJBIRVVUFkMHBQXn69KkEg0Hx+/0CyNDQkIiIjI6OLts/ODho065WygNXK93TshJNoVBgenqa9vZ2AEZHR+nr62NpaYnGxka7ehbf9ti2Ujwej692ztlqFvB6vXLgwAEBZNeuXdLR0SG6rktLS4tEo1FxuVzidDrl8ePHIiIyNjZWtJSFPSWNTdVa0GPdjsvljE+fPqFpGj6fj3Q6TTqdxu12Y5omz58/p7GxEV3XV0pCXWstRrGyQgKAYRikUin27dvHzMwMXq+36BZd18lkMrx+/ZpcLlcpDa+rUyqtC5+5QlGU4ljTNHG5XMv4jx49sl1QUFW1ezMNid3ZLLNEaWKxr2EVSK/SQ1bvCcvc0bWJBrt+swrYAbRmH4qIWTqthQJ2IK3W92eBC4qiTNulwDTN7EobHOs051/WF31Z5WdmKBqNjp86daqzoaFB3bJliyuZTDpM02z48OHD34BRqx+ni5XKaywW+3N8fFwSiUTm1atX6UQikZmYmJD+/v5ZVVWbqDGUNjA/Wqn3C03Twk6ns4iapoUdDofO/wQ9VureMPw7AEQ0hrYho9D4AAAAAElFTkSuQmCC'
	}
	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

AddEventHandler('lossantostimes:hasEnteredMarker', function(zone)

	if zone == 'Coffre' and player.job and player.job.name == 'lossantostimes' then
		CurrentAction     = 'lossantostimes_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end
	
end)
			
AddEventHandler('lossantostimes:hasExitedMarker', function(zone)

	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
	
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('lossantostimes:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('lossantostimes:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 200, false, true, 2, false, false, false, false)
			end
		end
	end
end)

function OpenMobileLosSantosTimesActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_lossantostimes_actions',
		{
			title    = 'Los Santos Times',
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_lossantostimes', 'Los Santos Times', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)
		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and player.job and player.job.name == 'lossantostimes' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'lossantostimes_actions_menu' then
				    CoffreMenu()
				end

				CurrentAction = nil
				GUI.Time = GetGameTimer()

			end
		end
		if IsControlPressed(0,  Keys['F6']) and player.job ~= nil and player.job.name == 'lossantostimes' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_lossantostimes_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobileLosSantosTimesActionsMenu()
			GUI.Time = GetGameTimer()
		end

	end
end)

function CoffreMenu()

	local elements = {
	{label = _U('deposit_stock'), value = 'put_stock'},
    	{label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if player.job ~= nil and player.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_action'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        		OpenPutStocksMenu()
      		end
			if data.current.value == 'get_stock' then
		        OpenGetStocksMenu()
		    end
		    if data.current.value == 'boss_actions' then
		        TriggerEvent('esx_society:openBossMenu', 'lossantostimes', function(data, menu)
		        menu.close()
				end)
			end

			CurrentAction     = 'lossantostimes_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end


function OpenGetStocksMenu()

  ESX.TriggerServerCallback('lossantostimes:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
    	if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('lossantostimes_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('lossantostimes:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('lossantostimes:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('lossantostimes:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end