ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('esx_phone:registerNumber', 'lossantostimes', 'Alerte Los Santos Times', true, true)
TriggerEvent('esx_society:registerSociety', 'lossantostimes', 'Los Santos Times', 'society_lossantostimes', 'society_lossantostimes', 'society_lossantostimes', {type = 'private'})
TriggerEvent('esx_society:registerSociety', 'lossantostimesoff', 'Compte Offshore Los Santos Times', 'society_lossantostimesoff', 'society_lossantostimesoff', 'society_lossantostimesoff', {type = 'private'})
----------------------------------
---- Ajout Gestion Stock Boss ----
----------------------------------

RegisterServerEvent('lossantostimes:getStockItem')
AddEventHandler('lossantostimes:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_lossantostimes', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('lossantostimes:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_lossantostimes', function(inventory)
    cb(inventory.items)
  end)

end)

-------------
-- AJOUT 2 --
-------------

RegisterServerEvent('lossantostimes:putStockItems')
AddEventHandler('lossantostimes:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_lossantostimes', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('lossantostimes:putStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_lossantostimes', function(inventory)
    cb(inventory.items)
  end)

end)

ESX.RegisterServerCallback('lossantostimes:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)