Config = {}
Config.DrawDistance = 120.0
Config.MarkerColor = {r = 0, g = 128, b = 255}
Config.Locale = 'fr'

Config.Zones = {

	Coffre = {
		Pos   = {x=-1069.1,y=-246.63,z=44.02},
		Size  = {x = 1.501, y = 1.501, z = 0.5001},
		Type  = 1
	}
}