Config = {}
Config.DrawDistance = 120.0
Config.MarkerColor = {r = 0, g = 128, b = 255}
Config.Locale = 'fr'

Config.Zones = {

	Coffre = {
		Pos   = {x = -52.813, y = -2525.26, z = 7.401},
		Size  = {x = 1.501, y = 1.501, z = 0.5001},
		Type  = 1
	}
}