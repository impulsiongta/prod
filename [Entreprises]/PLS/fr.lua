Locales['fr'] = {

	--Cloakroom
	
		['cloakroom'] = 'Coffre',

		['open_menu'] 				= 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le coffre',
		['PLS'] 					= 'Petrol Los Santos',
		['PLS_stock'] 				= 'Petrol Los Santos Stock',
		['deposit_stock']             = 'Déposer Stock',
  		['withdraw_stock']            = 'Prendre Stock',
		['quantity']                  = 'quantité',
		['invalid_quantity']          = 'quantité invalide',
		['inventory']                 = 'inventaire',
		['boss_action']               = 'Menu patron',
		['you_removed']               = 'vous avez retiré x',
    	['you_added']                 = 'Vous avez ajouté x',
}
