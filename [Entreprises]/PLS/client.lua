local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local takeService = {x=-44.04,y=-2519.924,z=7.395}
local spawnVeh = {x=-32.81,y=-2532.456,z=6.01,a=51.434}
local spawnTrailer = {x=-49.119,y=-2545.725,z=6.01,a=334.768}

local colorBlipRun = 2

local recolte = {x=182.551,y=-2212.977,z=5.951}
local markerRecolte = {x = 4.001, y = 4.001, z = 0.5001}
local waitingRecolte = 3000

local traitement = {x=1623.111,y=-2368.597,z=93.022}
local markerTraitement = {x = 8.001, y = 8.001, z = 0.5001}
local waitingTraitement = 3000

local vente = {x=638.82,y=254.82,z=103.152}
local markerVente = {x = 4.001, y = 4.001, z = 0.5001}
local waitingVente = 3000
local price = 12

local exchangeItem = {x=-127.33,y=-2535.66,z=6.00}
local markerExchangeItem = {x = 4.001, y = 4.001, z = 0.5001}
local waitingExchangeItem = 15000

local coffre_money = {x=-48.396,y=-2508.862,z=7.396}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}
local coffre = {x=-52.813,y=-2525.26,z=7.401}

local haveTakeVetement = false

local vehicle = -1
local trainer = -1

local blipVente = -1
local stop = false

local player = -1

local JobAPI = nil
local phase = 0

ESX = nil
local PlayerData = {}
local GUI                     = {}
GUI.Time                      = 0

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'PLS' then
		Config.Zones.Coffre.Type	   = 1
	else
		Config.Zones.Coffre.Type	   = -1
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	player = xPlayer

	while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end
	
	if player.job.name == 'PLS' then

		Config.Zones.Coffre.Type = 1
		
	else

		Config.Zones.Coffre.Type = -1

	end

	while player == -1 do
		Citizen.Wait(10)
	end

	if(player ~= -1 and player.job ~= nil and player.job.name == "PLS") then
		--JobAPI.createCoffre(coffre.x,coffre.y,coffre.z,"PLS")
		if (player.job.grade_name == "boss") then
			--JobAPI.createSocietyMenu(coffre_money.x,coffre_money.y,coffre_money.z, 'PLS', 'Petrol Los Santos')
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'PLSoff', 'Compte Offshore Petrol Los Santos')
		end
	end



	Citizen.CreateThread(function()
		RequestModel("CAMIONPLS")
		RequestModel("TANKER")

		company = AddBlipForCoord(takeService.x, takeService.y, takeService.z)
	    SetBlipSprite(company, 415)
	    SetBlipAsShortRange(company, true)
	    BeginTextCommandSetBlipName("STRING")
	    AddTextComponentString("PLS")
	    EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end
		
		while true do
			Citizen.Wait(0)
			if(player ~= -1) then
				if(player.job ~= nil and player.job.name ~= nil  and player.job.name == "PLS") then
					DrawMarker(1,takeService.x,takeService.y,takeService.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
					
					if(isNear(takeService)) then
						if(haveTakeVetements) then
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~reprendre vos vêtements.")
						else
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre vos habits de travail.")
						end

						if(IsControlJustPressed(1, 38)) then
							haveTakeVetements = not haveTakeVetements
							phase = 0

							if(not haveTakeVetements) then
								JobAPI.stop()
								stop = true
								if(DoesEntityExist(vehicle)) then
									DeleteVehicle(vehicle)
								end

								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									TriggerEvent('skinchanger:loadSkin', skin)
								end)
							else
								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									if skin.sex == 0 then
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
									else
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
									end
								end)
							end
						end
					end
					
					if(takeService) then

						if(haveTakeVetements) then
							DrawMarker(1,spawnVeh.x,spawnVeh.y,spawnVeh.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

							if(isNear(spawnVeh)) then
								if(DoesEntityExist(vehicle)) then
									Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ranger votre camion")
								else
									Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre votre camion")
								end


								if(IsControlJustPressed(1, 38)) then
									if(DoesEntityExist(vehicle)) then
										DeleteVehicle(vehicle)
										DeleteVehicle(trainer)
										JobAPI.stop()
										stop = true
									else
										spawnVehicle("camionpls", spawnVeh)
										JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'petrole_brut', colorBlipRun, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte, 'bleu')
										JobAPI.createTraitementMultiple(2, traitement.x,traitement.y,traitement.z, 'petrole_brut', 1, 'petrole', 1, colorBlipRun, true, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement, 'bleu')
										createVente(3, vente.x, vente.y, vente.z, 'petrole', price)
										JobAPI.exchangeItem(4, exchangeItem.x,exchangeItem.y,exchangeItem.z, 'petrole', 'bidon_essence', 12, colorBlipRun, true, markerExchangeItem.x, markerExchangeItem.y, markerExchangeItem.z, waitingExchangeItem, 'bleu')
									end
								end
							end
						end
					end
				end
			end

		end

	end)



	Citizen.CreateThread(function()

		while true do
			Citizen.Wait(0)
			if(player ~= -1 and player.job ~= nil and player.job.name == "PLS") then
				if(DoesEntityExist(vehicle)) then
					if(not IsEntityAttached(trainer)) then
						if(stop == false) then
							JobAPI.stop()
							stop = true
						end	

						Info("Veuillez attacher ~y~~h~votre remorque.")
					else
						if(stop == true) then
							JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'petrole_brut', colorBlipRun, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte)
							JobAPI.createTraitementMultiple(2, traitement.x,traitement.y,traitement.z, 'petrole_brut', 1, 'petrole', 1, colorBlipRun, true, markerTraitement.x, markerTraitement.y, markerTraitement.z, waitingTraitement)
							createVente(3, vente.x, vente.y, vente.z, 'petrole', price)
							JobAPI.exchangeItem(4, exchangeItem.x,exchangeItem.y,exchangeItem.z, 'petrole', 'bidon_essence', 10, colorBlipRun, true, markerExchangeItem.x, markerExchangeItem.y, markerExchangeItem.z, waitingExchangeItem)
						end
					end

					if(IsEntityDead(vehicle) or IsEntityDead(trainer)) then
						JobAPI.stop()
						stop = true
					end
				end
			end
		end

	end)
end)



local count = 0
function createVente(number, x,y,z, item1, price)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false

	blipVente  = AddBlipForCoord(x,y,z)
	SetBlipSprite(blipVente, 16 + number)
	SetBlipColour(blipVente, colorBlipRun)
	SetBlipDisplay(blipVente, 4)

	while not isInArea and not stop do
		Citizen.Wait(0)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,markerVente.x,markerVente.y,markerVente.z,0,128,255,200,0,0,0,0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

		if(distance < markerVente.x) then
			isInArea = true
		end
	end
	local isOnVente = false
	
	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,markerVente.x,markerVente.y,markerVente.z,0,128,255,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > markerVente.x) then
				isInArea = false
			end

			if(not isOnVente) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~vendre.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter de vendre.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnVente = not isOnVente
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item1)
	Wait(50)
	while isInArea and not stop do
		Citizen.Wait(waitingVente)
		if(isOnVente) then
			if(count > 0) then
				count = count-1
				TriggerServerEvent("Farm:addAccountMoney", price, "PLS")
				TriggerServerEvent("farm:removeItem", item1)
				TriggerServerEvent("fuel:addFuel",10)
			else
				isInArea = false
			end
		end
	end

	SetBlipDisplay(blipVente, 0)
	blipVente = -1

	if(not stop) then
		createVente(number, x,y,z, item1, price)
	end
end)
end


RegisterNetEvent("farm:sendItem")
AddEventHandler("farm:sendItem", function(c)
	count = c
end)



function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function spawnVehicle(name, tabl)

	hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1) 	

    Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
    SetVehicleOnGroundProperly(Truck)
    SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
   	SetVehicleEngineOn(Truck, true, false, false)

   	hashTrainer = GetHashKey("TANKER")
   	local coords = GetOffsetFromEntityInWorldCoords(Truck, -2.0,0.0,0.0)
   	Trainer = CreateVehicle(hashTrainer,  spawnTrailer.x,spawnTrailer.y,spawnTrailer.z, spawnTrailer.a, true, false)
   	SetVehicleOnGroundProperly(Trainer)
    
   	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck
	trainer = Trainer

end

AddEventHandler('PLS:hasEnteredMarker', function(zone)

	if zone == 'Coffre' and player.job and player.job.name == 'PLS' then
		CurrentAction     = 'PLS_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end
	
end)
			
AddEventHandler('PLS:hasExitedMarker', function(zone)

	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
	
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('PLS:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('PLS:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 200, false, true, 2, false, false, false, false)
			end
		end
	end
end)
function OpenMobilePLSActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_pls_actions',
		{
			title    = 'Petrol Los Santos',
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_PLS', 'Petrol Los Santos', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)
		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and player.job and player.job.name == 'PLS' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'PLS_actions_menu' then
				    CoffreMenu()
				end

				CurrentAction = nil
				GUI.Time = GetGameTimer()

			end
		end
		if IsControlPressed(0,  Keys['F6']) and player.job ~= nil and player.job.name == 'PLS' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_pls_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobilePLSActionsMenu()
			GUI.Time = GetGameTimer()
		end

	end
end)

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = 'Petrol LS',
		number     = 'PLS',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAPFSURBVHjaxJdNaF1FFMd/Z2Yy7yXNB4TWthiRYrVWNOhCKnZRpTG0sbaCOy1FxIW0oKXQnZvaljaLKIjiB8WNYApBUBF1UbBBUbEbSayJuGiNCtZosHnNx7v3zoyLd/t8Sd97ufnQHHiLN2fmzm/mnP+ZGQkhsJpmqjV+fbAN8REgkjZVUiqgF9gLJPN8180DDmhK+/8F9D3w5vRAJgDAAj3AYeAH4GCF72ngKeAAMCWgA0AIiEBAAKaBQ8Az6ZjNwKtANgAJ/mOQR9K/ncBpYCwFOwIcB85VG5voRoyb2QE8Ps+lM4eA4CwwC7wP/CzCTc4zlnj25DQNAd4RwKHwQTDioKGR2AOwFZF+QmivEhayhuB5oAvYDrwAbAUOJZ7DeUNTCJwBmqutULliJ4SNy0pCYAhYD/Sl4X1UCfevsXT4UsrtD4DCoyWU4h5P0yAQUItSQb3e3wCjFUnZUU2xadKBqEVPXg9gLfAhcPt/XQdqAewCHga0ADnN9XVm/7CA1UsHyJOWocjB6J8BuwgIoyDx8NNEWHBMLQBfdgoc+zzmvSFHa35hBK3AauHkYMLZYUfeLD0JcQFaLBzf2UD/cMLARUdbHQgt0GiEE+djfi94jjxoiPzSZFi22QTuWiec7rYc/TSi2cLeLZqim1c9U4BTX8QMX/G8tc/SbKGYLBMAoBDBfRsVL3VZTp6PGR0PaAWVsjQKfpsMjIw7XtuToy0H08nCeWOyZnWhGHhok+KrXzSf/Jiw/VZF7OZm/eAlz4kuy82t8PdstqQ1i5FW5Eor3bFJcarbUiiWtkAEjMBzH8XlfrLMUlzTfIDYQaFY+pUBUun5lShEcdD8X1b9PkD9a5qkMVfy7w4oWUEAI77mxEaVttlXHkRpaEIo+ZcNUGtyrWD8WmDwsmfsaoSr4BSB0T88u+8I5IxiJg4rByBAa154+0LC0BVPb7dFScBXzGE1/DqpeOPbmA0twrYOVVbJsgFa80L/sOPscMLrj+XYshaKyY1B33YLKCW8eC6ib5flnvVCIaovSVP9JBJUmoh5Ax+MOM5ciOnbbdncXrvIzCaw707NVARHP4t4ucdyW7vMKViZZOiCKp9sEzMw8H3CsZ2Wzg2KawusaHI28GSn5om7De9+l6BkCTtQBvGlHXilx9JiyRTTkEIcuFdTKOoFD6NaotGVNb7JlMprVgtA4mBNw5xWkxlA55pGKl9kbgnPxzBnXADRVxcTgi8T0/ysTqb2p9czXylJ6lTKGy9hQQXREz6/rrf6K2yVX8eKVbZ/BgCjLU7ye5V9jAAAAABJRU5ErkJggg=='
	}
	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

RegisterNetEvent('PLS:bidon_essence')
AddEventHandler('PLS:bidon_essence', function(xPlayer)
local _source = source
local playerPed = GetPlayerPed(-1)

	Citizen.CreateThread(function()
	
	local x,y,z = table.unpack(GetEntityCoords(playerPed))
	local veh = GetClosestVehicle(x, y, z, 4.001, 0, 70)
	
		if veh == 0 then
			veh = GetVehiclePedIsIn(playerPed, false)
		end
		
		if(veh ~= nil and veh ~= 0 and GetVehicleNumberPlateText(veh) ~= nil) then
		
		  Info('Remplissage en cours')
		  GiveWeaponToPed(GetPlayerPed(-1), 0x34A67B97, 1,  0, true)
		  
		  RequestAnimDict("weapon@w_sp_jerrycan")
		  while not HasAnimDictLoaded("weapon@w_sp_jerrycan") do
			Citizen.Wait(100)
		  end
		  TaskPlayAnim(playerPed,"weapon@w_sp_jerrycan","fire", 8.0, -8, -1, 49, 0, 0, 0, 0)
		  local done = false

		  while done == false do

			SetVehicleUndriveable(veh, true)
			SetVehicleEngineOn(veh, false, false, false)
			Wait(5000)

			TriggerEvent("advancedFuel:setEssence", 100, GetVehicleNumberPlateText(veh), GetDisplayNameFromVehicleModel(GetEntityModel(veh)))
			xPlayer.removeInventoryItem('bidon_essence', 1)
			RemoveWeaponFromPed(GetPlayerPed(-1), 0x34A67B97)
			done = true
		  end
		  TaskPlayAnim(playerPed,"weapon@w_sp_jerrycan","fire_outro", 8.0, -8, -1, 49, 0, 0, 0, 0)
		  Wait(500)
		  ClearPedTasks(playerPed)

		  SetVehicleEngineOn(veh, true, false, false)
		  SetVehicleUndriveable(veh, false)
		  ESX.ShowNotification('Remplissage terminé.')
		  ESX.ShowNotification('Vous avez utiliser un ~r~~h~ Bidon d\'essence')
		else
			ESX.ShowNotification('Pas de véhicule autour de vous.')
		end
	end)
end)

function CoffreMenu()

	local elements = {
	{label = _U('deposit_stock'), value = 'put_stock'},
    {label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if player.job ~= nil and player.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_action'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        		OpenPutStocksMenu()
      		end
			if data.current.value == 'get_stock' then
		        OpenGetStocksMenu()
		    end
		    if data.current.value == 'boss_actions' then
		        TriggerEvent('esx_society:openBossMenu', 'PLS', function(data, menu)
		        menu.close()
				end)
			end

			CurrentAction     = 'PLS_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end


function OpenGetStocksMenu()

  ESX.TriggerServerCallback('PLS:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
		if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('PLS_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('PLS:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('PLS:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('PLS:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end