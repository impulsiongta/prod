local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local takeVehicle = {x=711.91,y=4110.00,z=31.29} -- 18.17
local spawnVehicle2 = {x=713.0,y=4085.00,z=40.00, a=94.00}
local takeVehicle2 = {x=713.55,y=4174.88,z=40.70,a=294.58} -- 186.8
local takeVetement = {x=778.42,y=4184.90,z=41.7}

-- RUN POISSONS

local colorBlipRun = 2

local recolte = {x=42.04,y=4091.95,z=31.15}
local markerRecolte = {x = 45.001, y = 45.001, z = 6.5001}
local waitingRecolte = 2000

local traitement1 = {x=908.17,y=-1724.47,z=32.16}
local markerTraitement1 = {x = 2.001, y = 2.001, z = 0.5001}
local waitingTraitement1 = 2000

local traitement2 = {x=-637.93,y=-1252.36,z=11.37}
local traitement3 = {x=-640.53,y=-1252.09,z=11.37}
local traitement4 = {x=-642.75,y=-1251.15,z=11.37}
local traitement5 = {x=-635.38,y=-1252.25,z=11.37}

local markerTraitement2 = {x = 2.001, y = 2.001, z = 0.5001}
local waitingTraitement2 = 500

local vente1 = {x=-697.74,y=-869.347,z=23.72}
local vente2 = {x=-701.07,y=-883.23,z=23.78}
local vente3 = {x=-708.18,y=-885.45,z=23.80}
local vente4 = {x=-698.12,y=-858.85,z=23.66}

local markerVente = {x = 2.001, y = 2.001, z = 0.5001}
local waitingVente = 500
local price = 4

-- RUN JUS DE FRUIT

local colorBlipRun2 = 47
--JUS_DE_FRUIT
local recolte2 = {x=2299.83,y=4880.20,z=41.80}
local waitingRecolte2 = 1000

-- PurJus
local traitement2_1 = {x=-556.69,y=274.07,z=83.01}

-- Punch
local traitement2_2 = {x=-554.03,y=273.09,z=82.99}



-- RUN BOISSON

local colorBlipAchat = 2

local achat1_1 = {x=148.51,y=237.89,z=106.82}
local achat1_2 = {x=150.59,y=236.99,z=106.86}
local achat1_3 = {x=153.73,y=235.92,z=106.77}
local achat1_4 = {x=155.65,y=235.28,z=106.72}

local markerAchat = {x = 1.501, y = 1.501, z = 0.5001}
local priceAchat = 3
local waitingAchat = 1000
--COFFRE
local coffre = {x=-1600.97,y=5204.5,z=4.31}
local coffre_money = {x=-1593.65,y=5205.06,z=4.31}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}
--local pc = {x=-1604.34,y=5197.06,z=4.36}

local phase = 0
local player = -1
local JobAPI = nil

local vehicle = -1
local haveTakeVetement = false

ESX = nil
local PlayerData = {}
local GUI                     = {}
GUI.Time                      = 0

local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'pecheur' then
		Config.Zones.Coffre.Type	   		= 1
		Config.Zones.DeleteBateau.Type	   	= 1
	else
		Config.Zones.Coffre.Type	   		= -1
		Config.Zones.DeleteBateau.Type	   	= -1
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	playerJob = xPlayer.job.name
    playerGrade = xPlayer.job.grade
    player = xPlayer

	if player.job.name == 'pecheur' then
		Config.Zones.Coffre.Type	   		= 1
		Config.Zones.DeleteBateau.Type	   	= 1
	else
		Config.Zones.Coffre.Type	   		= -1
		Config.Zones.DeleteBateau.Type	  	= -1
	end

    while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end


	while player == -1 do
		Citizen.Wait(10)
	end

	if(player ~= -1 and player.job ~= nil and  player.job.name == "pecheur") then
		if (player.job.grade_name == "boss") then
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'pecheuroff', 'Compte Offshore Poisson d\'or')
		end
	end

	Citizen.CreateThread(function()
		RequestModel("TUG")
		RequestModel("BENSON")

		local company = AddBlipForCoord(takeVetement.x, takeVetement.y, takeVetement.z)
		SetBlipSprite(company, 317)
		SetBlipColour (company, 4)
		SetBlipAsShortRange(company, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Poisson d\'or")
		EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end


		while true do
			Citizen.Wait(10)
			if(player~=-1) then
				if(player.job ~= nil and player.job.name == "pecheur") then
					DrawMarker(1,takeVetement.x,takeVetement.y,takeVetement.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

					if(isNear(takeVetement)) then
						if(haveTakeVetements) then
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~reprendre vos vêtements.")
						else
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre vos habits de travail.")
						end

						if(IsControlJustPressed(1, 38)) then
							haveTakeVetements = not haveTakeVetements
							phase = 0

							if(not haveTakeVetements) then
								JobAPI.stop()

								if(DoesEntityExist(vehicle)) then
									DeleteVehicle(vehicle)
								end

								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									TriggerEvent('skinchanger:loadSkin', skin)
								end)
							else
								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									if skin.sex == 0 then
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
									else
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
									end
								end)
							end
						end
					end
					
					if(takeVetement) then
					
						if(haveTakeVetements) then
							DrawMarker(1,takeVehicle.x,takeVehicle.y,takeVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
							DrawMarker(1,takeVehicle2.x,takeVehicle2.y,takeVehicle2.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
							if(isNear(takeVehicle)) then
								if(DoesEntityExist(vehicle)) then
									Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~rentrer le véhicule.")
								else
									Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~sortir un bateau.")
								end

								if(IsControlJustPressed(1, 38)) then
									if(DoesEntityExist(vehicle)) then
										DeleteVehicle(vehicle)
										--JobAPI.stop()
									else
										spawnVehicle("TUG",spawnVehicle2)
										JobAPI.createRecolte(1, recolte.x,recolte.y,recolte.z, 'fish', colorBlipRun, true, markerRecolte.x, markerRecolte.y, markerRecolte.z, waitingRecolte, 'FiletPoisson')
									end
								end
							end


							if(isNear(takeVehicle2)) then
							
								Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~sortir un véhicule.")
								
								if(IsControlJustPressed(1, 38)) then
									if not IsPedInAnyVehicle(GetPlayerPed(-1), false) then
										local elements = {}
										ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
										  for i=1, #garageVehicles, 1 do
											table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
										  end

										  ESX.UI.Menu.Open(
											'default', GetCurrentResourceName(), 'vehicle_spawner',
											{
											  title    = _U('vehicle_menu'),
											  align    = 'top-left',
											  elements = elements,
											},
											function(data, menu)

											  menu.close()

											  local vehicleProps = data.current.value

											  ESX.Game.SpawnVehicle(vehicleProps.model, takeVehicle2, takeVehicle2.a, function(vehicle)
												ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
												local playerPed = GetPlayerPed(-1)
												TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
											  end)

											  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'pecheur', vehicleProps)

											end,
											function(data, menu)

											  menu.close()

											  CurrentAction     = 'menu_vehicle_spawner'
											  CurrentActionMsg  = _U('vehicle_spawner')
											  CurrentActionData = {}

											end
										  )

										end, 'pecheur')
										-- RUN POISSONS
										JobAPI.createTraitementMultiple(2, traitement1.x,traitement1.y,traitement1.z, 'fish', 1, 'filet_poisson', 1, colorBlipRun, true, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement1, 'FiletPoisson')
										JobAPI.createTraitementMultiple(3, traitement2.x,traitement2.y,traitement2.z, 'filet_poisson', 1, 'sushi', 4, colorBlipRun, true, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement1, 'Sushi')
										JobAPI.createTraitementMultiple(3, traitement3.x,traitement3.y,traitement3.z, 'filet_poisson', 1, 'plateau_fruit_mer', 4, colorBlipRun, false, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement1, 'FruitsMer')
										JobAPI.createTraitementMultiple(3, traitement4.x,traitement4.y,traitement4.z, 'filet_poisson', 1, 'gambas', 4, colorBlipRun, false, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement1, 'Gambas')
										JobAPI.createTraitementMultiple(3, traitement5.x,traitement5.y,traitement5.z, 'filet_poisson', 1, 'makki', 4, colorBlipRun, false, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement1, 'Maki')
																				
										JobAPI.createVenteEntreprise(4, vente1.x, vente1.y, vente1.z, 'sushi', price, 'pecheur', colorBlipRun, true, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'Sushi')
										JobAPI.createVenteEntreprise(4, vente2.x, vente2.y, vente2.z, 'makki', price, 'pecheur', colorBlipRun, false, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'Maki')
										JobAPI.createVenteEntreprise(4, vente3.x, vente3.y, vente3.z, 'plateau_fruit_mer', price, 'pecheur', colorBlipRun, false, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'FruitsMer')
										JobAPI.createVenteEntreprise(4, vente4.x, vente4.y, vente4.z, 'gambas', price, 'pecheur', colorBlipRun, false, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'Gambas')
										
										-- RUN JUS DE FRUITS
										JobAPI.createRecolte(1, recolte2.x,recolte2.y,recolte2.z, 'jus_de_fruit', colorBlipRun2, true, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingRecolte2, 'JusOrange')
										JobAPI.createTraitementMultiple(2, traitement2_1.x,traitement2_1.y,traitement2_1.z, 'jus_de_fruit', 1, 'pur_jus', 2, colorBlipRun2, true, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement2, 'JusRaisin')
										JobAPI.createTraitementMultiple(2, traitement2_2.x,traitement2_2.y,traitement2_2.z, 'jus_de_fruit', 1, 'punch', 2, colorBlipRun2, false, markerTraitement1.x, markerTraitement1.y, markerTraitement1.z, waitingTraitement2, 'Punch')
									
										-- RUN BOISSONS
										if(player.job.grade_name == "boss") then
											JobAPI.createAchatEntreprise(1, achat1_1.x, achat1_1.y, achat1_1.z, 'eau_minerale', priceAchat, 'pecheur', colorBlipAchat, true, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'blanc')
											JobAPI.createAchatEntreprise(3, achat1_2.x, achat1_2.y, achat1_2.z, 'coca', priceAchat, 'pecheur', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'rouge')
											JobAPI.createAchatEntreprise(3, achat1_3.x, achat1_3.y, achat1_3.z, 'redbull', priceAchat, 'pecheur', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'bleu')
											JobAPI.createAchatEntreprise(3, achat1_4.x, achat1_4.y, achat1_4.z, 'cafe', priceAchat, 'pecheur', colorBlipAchat, false, markerAchat.x, markerAchat.y, markerAchat.z, waitingAchat, 'marron')
										end																
									else
										local vehicleIsIn = GetVehiclePedIsIn(GetPlayerPed(-1), false)
										local vehicleProps = ESX.Game.GetVehicleProperties(vehicleIsIn)
										TriggerServerEvent('esx_society:putVehicleInGarage', 'pecheur', vehicleProps)
										ESX.Game.DeleteVehicle(vehicleIsIn)
										JobAPI.stop()
										--takeJob = takeJob - 1
									end
								end	
							end							
						end
					end
				end
			end
		end

	end)
end)



function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function spawnVehicle(name, tabl)
	local hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1)
	local Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
	SetVehicleOnGroundProperly(Truck)
	SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
	SetVehicleEngineOn(Truck, true, false, false)

	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck
end

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = 'Poisson d\'Or',
		number     = 'pecheur',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAdCAYAAABWk2cPAAAEEElEQVRIiZWXXYhVZRSGn2ftfc78qElSqF0ohilFIGgJEf0TBJUEERTkRZHedBMUSFB4UVAQ1UXdRHcVRUFXgUQalIhQphAaRRGIQWNkqTGjozNzdnz77KPHM3vPz4IF+2y+vd5vvev3WBQFgzJ9cYLO1PlZ7/vEAtrAkNAB0uGZwUPRGiFvL5n1cV5n8ff973Li0IeNgMBKimIbeC9wRvkMOQhM9h9cc+t2Nty3a2GgmlTqWACWAU+qzwLj4DIt7gKfgRK49qN+ibqXeQRZSFirq0PvjPC3iHghgrfU5eojapYue0lLUmrsN94muh/UeNtRp4Al6gik5yLFtT140HrMOUATvRXNAzIG7FOeA3YDoxrjFMUeukk1L2gtvf3ANTKufKkeCd0U4bDyOnBgELRJGj3t3XQoz8p8nZm5guZ/tPQ4gZxWvyeYmHXpqPdpTlCq2CbwgdheBM6meCrXgSsq1gbored3Tnr7pdXKyJPXXZlUj2fhvxFekwV35LkjrVz6NWuw3gQaVRO4QtLNE3DSLIvDhkdT3A13CBt6zvVKpsbEnKA2sePlev0lwr0RngpdZ7g7y2N1ulC7nTE0lNNq1ZtvAk19tNPKs15ME/xQ73wV3Wnhc8N9kTkZ4TbhRUgxnlsaY5rAIiupS3qtsLZysr/+jgtvh34bOq3uRJ6fD7gWNIGlxKn4XQLeb5SZOt0DTWeyLNLlDqmvRvhdRLRCn0Z2VD164aDJWJZZNX4eCB0FTvSfKWNbMYEcUF+O8KeIWB66XXm40am6l17WVerjBgfVmSua+UCSKftDXgo9FeHa0CeE9QsG7VlB7wnZJGWGlt6PDOeMDrcYbuflJOqTDvJ1hB+F5oY3oZsXB9plcKtplIVbUvq3W3k3wSpN2T3c7mp6zrM4H8HeiLgQepW6cuGgXQqXqRvT+DJ4E7ixrm2GafZemr+d0L+UiQhznT3uGkGrBErTY2m3TFyjvq882M3mvrNhv2q4IsKRKHOAqYWDdvW8eLZMIPg5jS9hqd0atMzekDyVTkSP8lzdWrFzTj1TZ79xyqgTyjHlbnV96FTox6TZrpfLqgp+QblTXV8UPEpRpN8nlV8XQW8Z07SW7AHHUnzR3ejtEbYTWM2o3JjaoLLZcFw9KBxbsKc96oBDyifCLuEW5TXg02pLGKt69NXAFuExwoeE6aLgByg+AP9bFL2VTArvKauRp4DbEtVQ0vZ3VZvLgRsM11TpcJiieAP8sWmIz7s5AH8Cr5hAZGfVpVaVw6bc3Qy7k2gC/IKieAc8XFDM2vgXA5rkDyhr9StgK3AzuE7SUuZp4ajwDXCkkJPzLWjWbfGdqXN0pvv/y1xam4NuwbeqC1vuwZT1eCHNWPpWfLNhojU6y/7/NhbWm8q80VsAAAAASUVORK5CYII='
	}
	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

function OpenMobilePecheurActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_pecheur_actions',
		{
			title    = 'Poisson d\'or',
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_pecheur', 'Poisson d\'or', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

AddEventHandler('pecheur:hasEnteredMarker', function(zone)
	if player.job and player.job.name == 'pecheur' then
		if zone == 'Coffre'then
			CurrentAction     = 'pecheur_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}
		elseif zone == 'DeleteBateau' then 
			CurrentAction     = 'pecheur_actions_delete'
			CurrentActionMsg  = _U('delete_menu')
			CurrentActionData = {}
		end
	end
	
end)
			
AddEventHandler('pecheur:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(10)
		
		if IsControlPressed(0,  Keys['F6']) and player.job and player.job.name == 'pecheur' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_pecheur_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobilePecheurActionsMenu()
			GUI.Time = GetGameTimer()
		end

		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E'])  and player.job and player.job.name == 'pecheur' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'pecheur_actions_menu' then
					CoffreMenu()
				elseif CurrentAction == 'pecheur_actions_delete' then
					ESX.Game.DeleteVehicle(GetVehiclePedIsIn(GetPlayerPed(-1), false))
				end

				CurrentAction = nil
				GUI.Time      = GetGameTimer()

			end
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(10)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('pecheur:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('pecheur:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(10)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 200, false, true, 2, false, false, false, false)
			end
		end
	end
end)



function CoffreMenu()

	local elements = {
	{label = _U('deposit_stock'), value = 'put_stock'},
    {label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if player.job ~= nil and player.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_action'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        			OpenPutStocksMenu()
      			end

			if data.current.value == 'get_stock' then
		        	OpenGetStocksMenu()
		      	end

		      	if data.current.value == 'boss_actions' then
		        	TriggerEvent('esx_society:openBossMenu', 'pecheur', function(data, menu)
		          	menu.close()
		        	end)
		      	end

			CurrentAction     = 'pecheur_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('pecheur:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
    	if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('pecheur_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('pecheur:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('pecheur:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('pecheur:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end