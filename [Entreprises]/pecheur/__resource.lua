dependency 'essentialmode'

client_scripts {
	'@es_extended/locale.lua',
	'fr.lua',
	'config.lua',
	'client.lua'
}

server_scripts {
	'@es_extended/locale.lua',
	'fr.lua',
	'config.lua',
	'server.lua'
}