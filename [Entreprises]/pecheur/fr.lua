Locales['fr'] = {

	['cloakroom'] 					= 'Coffre',
	['open_menu'] 					= 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le coffre',
	['pecheur'] 					= 'Le Poisson D\'Or',
	['pecheur_stock'] 				= 'Pecheur Stock',
	['deposit_stock']             	= 'Deposer Stock',
  	['withdraw_stock']            	= 'Prendre Stock',
	['quantity']                  	= 'quantite',
	['invalid_quantity']          	= 'quantite invalide',
	['inventory']                 	= 'inventaire',
	['boss_action']               	= 'Menu patron',
	['you_removed']               	= 'Vous avez retir� x',
	['you_added']                 	= 'Vous avez ajout� x',
	['delete_menu']					= 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~supprimer le v�hicule',
	['vehicle_spawner'] 		  = 'appuez sur ~INPUT_CONTEXT~ pour ~g~~h~acc�der au garage',
	['vehicle_menu'] = 'v�hicule',
}