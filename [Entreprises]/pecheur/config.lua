Config = {}
Config.DrawDistance = 50
Config.MarkerColor = {r = 0, g = 128, b = 255}
Config.Locale = 'fr'

Config.Zones = {

	Coffre = {
		Pos   	= { x = 722.9, y = 4189.9, z = 41.0},
		Size	= { x = 1.501, y = 1.501, z = 0.5001},
		Type  	= 1
	},
	DeleteBateau = {
		Pos   	= { x = 697.18, y = 4091.71, z = 30.82},
		Size	= { x = 10.001, y = 10.001, z = 1.0001},
		Type  	= 1
	}
}