ESX = nil



local player = -1

local result = -1
local count = 0

local start = false



RegisterNetEvent("esx_humanes:sendItem")
AddEventHandler("esx_humanes:sendItem", function(c)
	count = c
end)


AddEventHandler("playerSpawned", function()
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	start = true
end)

--local prise_service = {x=3513.302,y=3755.161,z=29.963}
local chargement_camions = {x=3620.698,y=3736.592,z=28.69, a=243.096} 
local sortie_vehicles = {x=3583.9,y=3672.211,z=33.889,a=243.096}
local take_vetements = {x=3555.01,y=3660.138,z=28.122}
local recolte = {x=3559.573,y=3671.787,z=28.122}
local retake_vetement = {x=3556.232,y=3684.855,z=28.122}

local boss_computer = {x=3536.183,y=3659.273,z=28.122}
local boss_coffre = {x=3608.22,y=3727.867,z=29.689}

local hopital = {
  [1] = {["name"] = "Hôpital",["x"]=-261.992340087891,["y"]=6333.66162109375,["z"]=32.4210891723633},
  [2] = {["name"] = "Hôpital",["x"]=1864.72619628906,["y"]=3702.21899414063,["z"]=33.4716033935547},
  [3] = {["name"] = "Hôpital",["x"]=-654.125671386719,["y"]=309.119750976563,["z"]=82.9213256835938},
  [4] = {["name"] = "Hôpital",["x"]=-910.559753417969,["y"]=-335.803131103516,["z"]=38.979133605957},
  [5] = {["name"] = "Hôpital",["x"]=-427.751342773438,["y"]=-328.891357421875,["z"]=33.1089820861816},
  [6] = {["name"] = "Hôpital",["x"]=319.811828613281,["y"]=-557.3583984375,["z"]=28.7437915802002},
  [7] = {["name"] = "Hôpital",["x"]=375.525451660156,["y"]=-1443.37231445313,["z"]=29.4315452575684},
  [8] = {["name"] = "Hôpital",["x"]=1136.01513671875,["y"]=-1599.89086914063,["z"]=34.6925392150879},
}

local currentHopital = -1
local currentBlip = -1

local isInJob = true
local phase = 0

local isOnRecolte = false
local isOnVente = false
local hasStartedRecolte = false


local pcActivate = false
local coffreActivate = false
local currentMenu = 0

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    player = xPlayer

    hashTruck = GetHashKey("boxville3")
	RequestModel(hashTruck)

	Citizen.CreateThread(function()
		
		company = AddBlipForCoord(take_vetements.x, take_vetements.y, take_vetements.z)
	    SetBlipSprite(company, 499)
	    SetBlipAsShortRange(company, true)
	    BeginTextCommandSetBlipName("STRING")
	    AddTextComponentString("Humane's Lab")
	    EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end
		
		if(player ~= -1 and player.job ~= nil and  player.job.name == "humane" and player.job.grade_name == "boss") then
			JobAPI.createSocietyMenu(3541.529,3667.697,27.122, 'humane', 'Humane\'s Lab')
		end

		while true do
			Citizen.Wait(0)

			if(player ~= -1 and player ~= nil and start) then
				if(player.job ~= nil and player.job.name == "humane") then
					--DrawMarker(1,prise_service.x,prise_service.y,prise_service.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
					DrawMarker(1,chargement_camions.x,chargement_camions.y,chargement_camions.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
					DrawMarker(1,sortie_vehicles.x,sortie_vehicles.y,sortie_vehicles.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
					if(phase == 0) then
						DrawMarker(1,take_vetements.x,take_vetements.y,take_vetements.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
					else
						DrawMarker(1,recolte.x,recolte.y,recolte.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
						DrawMarker(1,retake_vetement.x,retake_vetement.y,retake_vetement.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
					end

					if(player.job.grade_name == "boss") then
						DrawMarker(1,boss_computer.x,boss_computer.y,boss_computer.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
						DrawMarker(1,boss_coffre.x,boss_coffre.y,boss_coffre.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
					end

				end
			end
		end

	end)


		
	Citizen.CreateThread(function()
		while player == -1 do
			Citizen.Wait(10)
		end
		while true do
			Citizen.Wait(0)
			if(player ~= -1 and player ~= nil and start) then
				if(player.job ~= nil and player.job.name ~= nil and player.job.name == "humane") then
					local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), take_vetements.x,take_vetements.y,take_vetements.z, true)

					if(distance < 4 and phase ==0) then
						Info("Appuyez sur ~g~E~w~ pour mettre la blouse.")

						if(IsControlJustPressed(1, 38)) then
							SetPedComponentVariation(GetPlayerPed(-1), 11, 23, 3, 0)
							SetPedComponentVariation(GetPlayerPed(-1), 3, 84, 1, 0)
							SetPedComponentVariation(GetPlayerPed(-1), 4, 0, 14, 0)
							SetPedComponentVariation(GetPlayerPed(-1), 6, 1, 15, 0)
							SetPedComponentVariation(GetPlayerPed(-1), 8, 1, 0, 0)
							SetPedComponentVariation(GetPlayerPed(-1), 1, 46, 0, 0)

							phase = 1
						end
					end

					distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), recolte.x,recolte.y,recolte.z, true)

					if(distance < 5 and phase == 1) then
						if(isOnRecolte) then
								Info("Appuyez sur ~g~E~w~ pour arrêter de récolter")
						else
								Info("Appuyez sur ~g~E~w~ pour récolter")
						end
						if(IsControlJustPressed(1, 38)) then
							isOnRecolte = not isOnRecolte
							TriggerServerEvent("esx_humanes:getItemQ", "medikit")
							if(isOnRecolte) then
								startRecolteSystem()
							end
						end
					end

					distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), retake_vetement.x,retake_vetement.y,retake_vetement.z, true)

					if(distance < 5 and phase == 1) then
						Info("Appuyez sur ~g~E~w~ pour reprendre vos habits.")

						if(IsControlJustPressed(1, 38)) then
							ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
								TriggerEvent('skinchanger:loadSkin', skin)
							end)

							phase = 0
						end
					end


					distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), chargement_camions.x,chargement_camions.y,chargement_camions.z, true)

					if(distance < 5 and phase == 0 and currentHopital == -1) then

						Info("Appuyez sur ~g~E~w~ pour charger la cargaison.")

						if(IsControlJustPressed(1, 38)) then
							local random = math.random(1,#hopital)

							local x = hopital[random]["x"]
							local y = hopital[random]["y"]
							local z = hopital[random]["z"]

							currentHopital = {x=x,y=y,z=z}
							TriggerServerEvent("esx_humanes:getItemQ", "medikit")
							Wait(50)
							isOnVente = not isOnVente
							spawnVehicle()
							launchMission()
						end
					end

					if(player.job.grade_name == "boss") then
						distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), boss_computer.x,boss_computer.y,boss_computer.z, true)

						if(distance < 5) then
							if(not pcActivate) then
								Info("Appuyez sur ~g~E~w~ pour allumer l'ordinateur.")

							else
								Info("Appuyez sur ~g~E~w~ pour éteindre l'ordinateur.")
								renderBossPC()
							end


							if(IsControlJustPressed(1, 38)) then
								pcActivate = not pcActivate
							end
						end

						distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), boss_coffre.x,boss_coffre.y,boss_coffre.z, true)

						if(distance < 3) then
							if(not coffreActivate) then
								Info("Appuyez sur ~g~E~w~ pour ouvrir le coffre.")
							else
								Info("Appuyez sur ~g~E~w~ pour fermer le coffre.")
								renderCoffre()
							end

							if(IsControlJustPressed(1, 38)) then
								TriggerServerEvent("esx_coffre:getInventory", "humane")
								Wait(50)
								coffreActivate = not coffreActivate
							end
						end
					end

					if(currentHopital ~= -1) then
						distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), currentHopital.x,currentHopital.y,currentHopital.z, true)
						DrawMarker(1,currentHopital.x,currentHopital.y,currentHopital.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
						if(distance <5) then
							if(not isOnVente) then
								Info("Appuyez sur ~g~E~w~ pour vendre les médikits.")
							else
								Info("Appuyez sur ~g~E~w~ pour arrêter de vendre les médikits.",3000)
							end

							if(IsControlJustPressed(1, 38)) then
								TriggerServerEvent("esx_humanes:getItemQ", "medikit")
								Wait(50)
								isOnVente = not isOnVente
							end
						end
					end
				end
			end
		end

	end)

end)


-- 11 23 3
		-- 3 84 1
		-- 4 0 14
		-- 6 1 15
		-- 8 1 0
		-- 1 46 0



function startRecolteSystem()
	Citizen.CreateThread(function()

		while isOnRecolte do
			Citizen.Wait(3000)
			print(count)
			if(count < 32) then
				TriggerServerEvent("esx_humanes:addItem", "medikit", 1)
				count = count +1
			else
				isOnRecolte = false
			end
		end
	end)
end

function spawnVehicle()

	hashTruck = GetHashKey("boxville3")
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1) 	

    Truck = CreateVehicle(hashTruck,  chargement_camions.x,chargement_camions.y,chargement_camions.z, chargement_camions.a, true, false)
    SetVehicleOnGroundProperly(Truck)
    SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
   	SetVehicleEngineOn(Truck, true, false, false)
    
   	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck

end



function launchMission()
	currentBlip  = AddBlipForCoord(currentHopital.x,currentHopital.y,currentHopital.z)
	SetBlipSprite(currentBlip, 280)
	SetBlipColour(currentBlip, 2)
	SetBlipRoute(currentBlip, true)
	SetBlipDisplay(currentBlip, 4)

	local distance = GetDistanceBetweenCoords(currentHopital.x,currentHopital.y,currentHopital.z, GetEntityCoords(GetPlayerPed(-1)), true)

	Citizen.CreateThread(function()
		local stop = false
		while not stop do
			Citizen.Wait(0)
			distance = GetDistanceBetweenCoords(currentHopital.x,currentHopital.y,currentHopital.z, GetEntityCoords(GetPlayerPed(-1)), true)
			if(distance < 5) then
				SetBlipRoute(currentBlip, false)
				SetBlipDisplay(currentBlip, 0)
				if(isOnVente) then
					if(count ~= 0) then
						TriggerServerEvent("esx_humanes:removeItem", "medikit", 1)
						TriggerEvent("API:addMoney", 32)
					else
						currentHopital = -1
						isOnVente = false
					end
					Citizen.Wait(3000)
				end
			else
				SetBlipRoute(currentBlip, true)
				SetBlipDisplay(currentBlip, 4)
			end
		end
	end)
end




RegisterNetEvent("esx_humanes:sendRequest")
AddEventHandler("esx_humanes:sendRequest", function(senderName, coords, id)
	Citizen.CreateThread(function()
		local requestCoords = -1

		local stop = false
		local accepted = false
		local timeout = 0
		TriggerEvent("showWarnNotif", senderName.." a demandé une livraison ! (Appuyez sur E pour accepter).")

		while not stop do
			Citizen.Wait(0)
			if(timeout<10000) then
				timeout = timeout+1
				if(IsControlJustPressed(1, 38)) then
					TriggerServerEvent("esx_jobs:sendNotif", id, "Un livreur est en route !")
					requestCoords  = AddBlipForCoord(coords.x,coords.y,coords.z)
		        	SetBlipSprite(requestCoords, 280)
		        	SetBlipColour(requestCoords, 2)
		        	SetBlipRoute(requestCoords, true)
					SetBlipDisplay(requestCoords, 4)
					timeout = 10000
				end
			else
				local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), coords.x,coords.y,coords.z, true)

				if(distance < 5) then
					SetBlipDisplay(requestCoords, 0)
					SetBlipRoute(requestCoords, false)
					stop = true
				end
			end
		end 
	end)
end)



function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end




local arrayCount = 0
local countMin = 0
local countMax = 23

local playersList = {}
function renderBossPC()

	if(currentMenu == 0) then
		TriggerEvent("GUIHumane:Title", "Gérer les effectifs")

		TriggerEvent("GUIHumane:Option", "Recruter quelqu'un", function(cb)
			if(cb) then
				currentMenu = 1
				TriggerServerEvent("esx_humanes:getPlayersByJob", "unemployed")
				countMin = 0
				countMax = 23
			end
		end)

		TriggerEvent("GUIHumane:Option", "Virer quelqu'un", function(cb)
			if(cb) then
				currentMenu = 2
				TriggerServerEvent("esx_humanes:getPlayersByJob", "humane")
				countMin = 0
				countMax = 23
			end
		end)

		TriggerEvent("GUIHumane:Option", "Augmenter de grade quelqu'un", function(cb)
			if(cb) then
				currentMenu = 3
				TriggerServerEvent("esx_humanes:getPlayersByJob", "humane")
				countMin = 0
				countMax = 23
			end
		end)

		TriggerEvent("GUIHumane:Option", "Descendre d'un grade quelqu'un", function(cb)
			if(cb) then
				currentMenu = 4
				TriggerServerEvent("esx_humanes:getPlayersByJob", "humane")
				countMin = 0
				countMax = 23
			end
		end)
	elseif(currentMenu == 1) then

		TriggerEvent("GUIHumane:Option", "< Retour", function(cb)
			if(cb) then
				currentMenu = 0
			end
		end)

			for id, name in pairs(playersList) do
				if(type(name) ~= "table") then
					TriggerEvent("GUIHumane:Option", name, function(cb)
						if(cb) then
							TriggerServerEvent("esx_humanes:setJobOfId", id, "humane", 0)
						end
					end)
				end
			end


	elseif(currentMenu == 2) then

		TriggerEvent("GUIHumane:Option", "< Retour", function(cb)
			if(cb) then
				currentMenu = 0
			end
		end)

		
			arrayCount = #playersList
			for id, name in pairs(playersList) do
				if(type(name) ~= "table") then
					TriggerEvent("GUIHumane:Option", name, function(cb)
						if(cb) then
							TriggerServerEvent("esx_humanes:setJobOfId", id, "unemployed", 0)
						end
					end)
				end
			end


	elseif(currentMenu == 3) then

		TriggerEvent("GUIHumane:Option", "< Retour", function(cb)
			if(cb) then
				currentMenu = 0
			end
		end)

			arrayCount = #playersList
			for id, name in pairs(playersList) do
				if(type(name) ~= "table") then
					TriggerEvent("GUIHumane:Option", name, function(cb)
						if(cb) then
							TriggerServerEvent("esx_humanes:setJobOfId", id, "humane", -1,1)
						end
					end)
				end
			end


	else

		TriggerEvent("GUIHumane:Option", "< Retour", function(cb)
			if(cb) then
				currentMenu = 0
			end
		end)

			arrayCount = #playersList
			for id, name in pairs(playersList) do
				if(type(name) ~= "table") then
					TriggerEvent("GUIHumane:Option", name, function(cb)
						if(cb) then
							TriggerServerEvent("esx_humanes:setJobOfId", id, "humane", -1,2)
						end
					end)
				end
			end

	end

	TriggerEvent("GUIHumane:Update")
end

RegisterNetEvent("API:setPlayersResult")
AddEventHandler("API:setPlayersResult",function(list)
	playersList = list
end)


local menuID = 0 
local inventory = -1
local toGet = 1
function renderCoffre()

	if(menuID == 0) then
		TriggerEvent("GUIHumane:Title", "Contenu du coffre")

		TriggerEvent("GUIHumane:Option", "Déposer un item", function(cb)
			if(cb) then
				TriggerServerEvent("esx_coffre:getPlayerInventory", "humane")
				toGet = 1
				menuID = 1
			end
		end)

		TriggerEvent("GUIHumane:Int", "Quantité : ", toGet, 1, 32, function(cb)
			toGet = cb
		end)

		if(toGet~=-1 and inventory ~= -1) then
			for ind,k in pairs(inventory) do
				if(type(k) == "number") then
					if( k > 0) then
						TriggerEvent("GUIHumane:Option", ind, function(cb)
							if(cb) then
								if(k >= toGet) then
									TriggerServerEvent("esx_coffre:recupItem", "humane", ind,toGet)
									menu = false
								end
							end
						end)
					end
				end
			end
		end
	else
		TriggerEvent("GUIHumane:Option", "< Retour", function(cb)
			if(cb) then
				TriggerServerEvent("esx_coffre:getInventory", "humane")
				toGet = 0
				menuID = 0
			end
		end)

		TriggerEvent("GUIHumane:Int", "Quantité : ", toGet, 1, 32, function(cb)
			toGet = cb
		end)

		for i,k in pairs(inventory) do
			if(type(k) ~= "number") then
				TriggerEvent("GUIHumane:Option", k.it.." "..k.q, function(cb)
					if(cb) then
						if(toGet <= k.q) then
							TriggerServerEvent("esx_coffre:addItem", "humane", i, toGet)
							toGet = 1
							menuID = 0
						end
					end
				end)
			end
		end
	end

	TriggerEvent("GUIHumane:Update")
end

RegisterNetEvent("esx_coffre:sendPlayerInventaire")
AddEventHandler("esx_coffre:sendPlayerInventaire", function(inv)
	inventory = inv
end)


RegisterNetEvent("esx_coffre:getCoffreInventory")
AddEventHandler("esx_coffre:getCoffreInventory", function(inv)
	inventory = inv
end)


RegisterNetEvent("GUI:requestGo")
AddEventHandler("GUI:requestGo", function(current, cb)
	local changed = false
	if(current > 23 and countMax+1<=arrayCount) then
		countMin = countMin+1
		countMax = countMax+1
		changed = true
	end

	cb(changed)
end)