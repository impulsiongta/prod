local players = {}

ESX = nil
API = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent("API:getAPI", function(obj) API = obj end)


local coffre = {x=3608.22,y=3727.867,z=29.689}
local coffreFunc = nil




RegisterServerEvent("esx_humanes:addPlayer")
AddEventHandler("esx_humanes:addPlayer", function()
	local _source = source

	players[_source] = GetPlayerName(_source)

end)


AddEventHandler("playerDisconnect", function()
	local _source = source
	local newList = {}

	for i,k in pairs(players) do
		newList[i] = k
	end

	players = {}
	players = newList
end)



RegisterServerEvent("esx_humanes:getPlayerInfos")
AddEventHandler("esx_humanes:getPlayerInfos", function()
	local _source = source
	API.getPlayersInfos(_source, function(result)
		TriggerClientEvent("API:setResult", _source, result)
	end)
end)



RegisterServerEvent("esx_humanes:getPlayers")
AddEventHandler("esx_humanes:getPlayers", function()
	local _source = source
	players = API.getPlayers()
	TriggerClientEvent("API:setPlayersResult", _source, players)
end)

RegisterServerEvent("esx_humanes:getPlayersByJob")
AddEventHandler("esx_humanes:getPlayersByJob", function(job)
	local _source = source
	
	players = API.getPlayersByJob(job)

	TriggerClientEvent("API:setPlayersResult", _source, players)
end)




RegisterServerEvent("esx_coffre:getInventory")
AddEventHandler("esx_coffre:getInventory", function(name)
	local _source = source
	coffreFunc = API.getCoffre(coffre.x,coffre.y,coffre.z)
	if(name == "humane") then
		local items = coffreFunc.getItems()
		TriggerClientEvent("esx_coffre:getCoffreInventory",_source,items)
	end
end)


RegisterServerEvent("esx_coffre:recupItem")
AddEventHandler("esx_coffre:recupItem", function(name, item, quantity)
	local _source = source
	coffreFunc = API.getCoffre(coffre.x,coffre.y,coffre.z)
	if(name == "humane") then
		coffreFunc.recupItem(item,quantity, _source)
	end
end)



RegisterServerEvent("esx_coffre:addItem")
AddEventHandler("esx_coffre:addItem", function(name, item, quantity)
	local _source = source
	coffreFunc = API.getCoffre(coffre.x,coffre.y,coffre.z)
	if(name == "humane") then
		coffreFunc.addItem(item,quantity, _source)
	end
end)


RegisterServerEvent("esx_coffre:getPlayerInventory")
AddEventHandler("esx_coffre:getPlayerInventory", function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	local inv = xPlayer.getInventory()

	local inv_sended = {}
	for i,k in pairs(inv) do
		if(k.count > 0) then
			inv_sended[k.name] = {it = k.label, q = k.count}
		end
	end

	TriggerClientEvent("esx_coffre:sendPlayerInventaire", _source, inv_sended)

end)







TriggerEvent('esx_phone:registerCallback', function(source, phoneNumber, message, anon)

	local xPlayer  = ESX.GetPlayerFromId(source)
	local xPlayers = ESX.GetPlayers()

	if phoneNumber == 'humanes' then
		for i=1, #xPlayers, 1 do

			local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])

			if xPlayer2.job.name == 'humanes' then
				local source2 = xPlayer2.source
				TriggerClientEvent("esx_humanes:sendRequest", source2, GetPlayerName(source), xPlayer.get('coords'), _source)
			end
		end
	end

end)



RegisterServerEvent('esx_humanes:getItemQ')
AddEventHandler('esx_humanes:getItemQ', function(name)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	local itemArray = xPlayer.getInventoryItem(name)
	TriggerClientEvent("esx_humanes:sendItem",_source, itemArray.count)
end)


RegisterServerEvent('esx_humanes:addItem')
AddEventHandler('esx_humanes:addItem', function(itemName, quantity)
	local _source = source
	API.addItem(_source, itemName,quantity)
end)


RegisterServerEvent('esx_humanes:removeItem')
AddEventHandler('esx_humanes:removeItem', function(itemName, quantity)
	local _source = source
	API.removeItem(_source, itemName,quantity)
end)



RegisterServerEvent("esx_humanes:setJobOfId")
AddEventHandler("esx_humanes:setJobOfId", function(id, job, grade, t)
	local _source = id

	if(grade == -1) then
		if(t == 1) then
			API.getPlayersInfos(_source, function(result)
				API.setJob(_source,job,result.job.grade+1)
			end)
				
		else
			API.getPlayersInfos(_source, function(result)
				API.setJob(_source,job,result.job.grade-1)
			end)
		end
	else
		API.setJob(_source,job,grade)
	end
	
end)