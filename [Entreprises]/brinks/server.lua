ESX = nil
----------------------------------
local brink = false
local convoi = {}
----------------------------------
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('esx_phone:registerNumber', 'brinks', 'Alerte Brinks', true, true)

TriggerEvent('esx_society:registerSociety', 'brinks', 'Brinks', 'society_brinks', 'society_brinks', 'society_brinks', {type = 'private'})
TriggerEvent('esx_society:registerSociety', 'brinksoff', 'Compte Offshore Brinks', 'society_brinksoff', 'society_brinksoff', 'society_brinksoff', {type = 'private'})

------------------------------------------
function get3DDistance(x1, y1, z1, x2, y2, z2)
	return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2) + math.pow(z1 - z2, 2))
end

RegisterServerEvent('brinks:toofar')
AddEventHandler('brinks:toofar', function(brinkss)
	local source = source
	local xPlayers = ESX.GetPlayers()
	brink = false

	if(convoi[source])then
		TriggerClientEvent('brinks:toofarlocal', source)
		convoi[source] = nil
		TriggerClientEvent('esx:showNotification', source, _U('brinks_has_cancelled') .. Stores[brinkss].nameofstore)
	end
end)

RegisterServerEvent('brinks:recolte')
AddEventHandler('brinks:recolte', function(brinkss)

	local source = source
	local xPlayer = ESX.GetPlayerFromId(source)
	local xPlayers = ESX.GetPlayers()

	if Stores[brinkss] then

		local store = Stores[brinkss]

		if (os.time() - store.lastrobbed) < 600 and store.lastrobbed ~= 0 then

			TriggerClientEvent('esx:showNotification', source, _U('already_brinks') .. (600 - (os.time() - store.lastrobbed)) .. _U('seconds'))
			return
		end

		TriggerClientEvent('esx:showNotification', source, _U('started_to_brinks') .. store.nameofstore)
		TriggerClientEvent('esx:showNotification', source, _U('hold_pos'))
		TriggerClientEvent('brinks:currentlyrecolte', source, brinkss)
		Stores[brinkss].lastrobbed = os.time()
		convoi[source] = brinkss
		local savedSource = source
		SetTimeout(30000, function()

			if(convoi[savedSource])then

				brink = false
				--TriggerClientEvent('brinks:robbery_successful', savedSource, job)
				TriggerClientEvent('brinks:brinks_complete', savedSource, job)
				
				if(xPlayer)then

					xPlayer.addInventoryItem('sac_dargent', 5)
				end
			end
		end)
	end
end)

----------------------------------
---- Ajout Gestion Stock Boss ----
----------------------------------

RegisterServerEvent('brinks:getStockItem')
AddEventHandler('brinks:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_brinks', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('brinks:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_brinks', function(inventory)
    cb(inventory.items)
  end)

end)

-------------
-- AJOUT 2 --
-------------

RegisterServerEvent('brinks:putStockItems')
AddEventHandler('brinks:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_brinks', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('brinks:putStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_brinks', function(inventory)
    cb(inventory.items)
  end)

end)

ESX.RegisterServerCallback('brinks:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)