Locales['fr'] = {

	['brinks_cancelled'] = 'Le ramassage va être annulé, ~r~~h~vous n\'allez avoir aucun sacs',
	
	['open_menu'] = 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le coffre',
		
	['store_brinks'] = 'Superette Brinks',
	['brinks_stock'] = 'Brinks Stock',
	['deposit_stock']             = 'Déposer Stock',
  	['withdraw_stock']            = 'Prendre Stock',
	['quantity']                  = 'quantité',
	['invalid_quantity']          = 'quantité invalide',
	['inventory']                 = 'inventaire',
	['you_removed']               = 'vous avez retiré x',
    ['you_added']                 = 'Vous avez ajouté x',
	['brinks_of'] = 'Ramassage des sacs : ~g~',
	['seconds_remaining'] = '~w~ secondes restantes',
	['brinks_has_cancelled'] = 'Le ramassage ~r~a été annulé : ~b~',
	
	['already_brinks'] = 'Les sacs de cette superette ~r~~h~on déjà était récolté. Veuillez attendre : ~y~',
	['seconds'] = ' secondes.',
	
	['press_to_brinks'] = 'appuyez sur ~INPUT_CONTEXT~ pour ~g~prendre les sacs ~b~~h~à ',
	
	['brinks_successful'] = 'Vous avez pris ~y~',
	['brinks_successful2'] = ' sacs',
	
	['started_to_brinks'] = 'vous avez commencé ~y~à ramasser les sacs ~b~~h~à ',
	['do_not_move'] = ', ~r~~h~ne vous éloignez pas !',

	['hold_pos'] = 'Le ramassage des sacs va prendre ~g~~h~30 secondes',
	
	['brinks_complete'] = 'Vous avez ~g~~h~tout les sacs',
	
	
	['stockade'] = 'Fourgon blindé Brink\'s',
	['xls'] = 'XLS Blindé',
	['veh_menu'] = 'garage brink\'s',
	['veh_spawn'] = 'appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~accéder au garage de la Brink\'s',
	['store_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~ranger le véhicule dans le garage de la Brink\'s',
	['vehicle_spawner'] 		  = 'appuez sur ~INPUT_CONTEXT~ pour ~g~~h~accéder au garage',
	['vehicle_menu'] = 'véhicule',
}
