Config = {}
Config.DrawDistance              = 100.0
Config.MarkerColor               = {r = 0, g = 128, b = 255}
Config.Locale                    = 'fr'

Config.Zones = {

	VehicleSpawner = {
		Pos   = {x = -19.89,y=-705.8, z = 31.34},
		Size = {x = 2.001, y = 2.001, z = 0.5001},
		Type  = 1
	},

	VehicleDeleter = {
		Pos   = {x = -36.62, y = -700.51, z = 31.34},
		Size  = {x = 2.001, y = 2.001, z = 0.5001},
		Type  = 1
	},
	
	Coffre = {
		Pos   = {x = 3.01, y = -659.39, z = 32.50},
		Size = {x = 1.501, y = 1.501, z = 0.5001},
		Type  = 1
	}
}

--------------------SUPERETTES--------------------------
Stores = {
	["clinton_twentyfourseven"] = {
		position = { ['x'] = 378.03, ['y'] = 332.93, ['z'] = 103.57 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Clinton Avenue)",
		lastrobbed = 0
	},
	["palomino_twentyfourseven"] = {
		position = { ['x'] = 2557.13, ['y'] = 380.56, ['z'] = 108.62 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Palomino Freeway)",
		lastrobbed = 0
	},
	["ineseno_twentyfourseven"] = {
		position = { ['x'] = -3038.9, ['y'] = 584.18, ['z'] = 7.91 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Ineseno Road)",
		lastrobbed = 0
	},
	["barbareno_twentyfourseven"] = {
		position = { ['x'] = -3242.33, ['y'] = 999.68, ['z'] = 12.83 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Barbareno Road)",
		lastrobbed = 0
	},
	["route68_twentyfoursever"] = {
		position = { ['x'] = 546.24, ['y'] = 2663.41, ['z'] = 42.16 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Route 68)",
		lastrobbed = 0
	},
	["sandyshores_twentyfoursever"] = {
		position = { ['x'] = 1959.63, ['y'] = 3748.42, ['z'] = 32.34 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Sandy Shores)",
		lastrobbed = 0
	},
	["senora_twentyfoursever"] = {
		position = { ['x'] = 2673.54, ['y'] = 3286.28, ['z'] = 55.24 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Senora Freeway)",
		lastrobbed = 0
	},
	["route1_twentyfourseven"] = {
		position = { ['x'] = 1734.56, ['y'] = 6420.24, ['z'] = 35.04 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Route 1)",
		lastrobbed = 0
	},
	["innocence_twentyfourseven"] = {
		position = { ['x'] = 24.18, ['y'] = -1347.31, ['z'] = 29.5 },
		--sacs = math.random(1,1),
		nameofstore = "24/7. (Innocence Boulevard & Elgin Avenue)",
		lastrobbed = 0
	},
	["grove_ltd"] = {
		position = { ['x'] = -46.41, ['y'] = -1758.3, ['z'] = 29.42 },
		--sacs = math.random(1,1),
		nameofstore = "LTD Gasoline. (Grove Street)",
		lastrobbed = 0
	},
	["mirror_ltd"] = {
		position = { ['x'] = 1165.23, ['y'] = -322.48, ['z'] = 69.21 },
		--sacs = math.random(1,1),
		nameofstore = "LTD Gasoline. (Mirror Park Boulevard)",
		lastrobbed = 0
	},
	["littleseoul_ltd"] = {
		position = { ['x'] = -705.52, ['y'] = -913.35, ['z'] = 19.22 },
		--sacs = math.random(1,1),
		nameofstore = "LTD Gasoline. (Little Seoul)",
		lastrobbed = 0
	},
	["rockforddrive_ltd"] = {
		position = { ['x'] = -1819.79, ['y'] = 794.71, ['z'] = 138.08 },
		--sacs = math.random(1,1),
		nameofstore = "LTD Gasoline. (North Rockford Drive)",
		lastrobbed = 0
	},
	["grapseed_ltd"] = {
		position = { ['x'] = 1707.4, ['y'] = 4919.71, ['z'] = 42.06 },
		--sacs = math.random(1,1),
		nameofstore = "LTD Gasoline. (Grapeseed Main Street)",
		lastrobbed = 0
	},
	["elrancho_liquor"] = {
		position = { ['x'] = 1133.77, ['y'] = -982.5, ['z'] = 46.42 },
		--sacs = math.random(1,1),
		nameofstore = "Robs Liquor. (El Rancho Boulevard)",
		lastrobbed = 0
	},
	["sanandreas_liquor"] = {
		position = { ['x'] = -1221.63, ['y'] = -908.72, ['z'] = 12.33 },
		--sacs = math.random(1,1),
		nameofstore = "Robs Liquor. (San Andreas Avenue)",
		lastrobbed = 0
	},
	["prosperity_liquor"] = {
		position = { ['x'] = -1485.85, ['y'] = -377.61, ['z'] = 40.16 },
		--sacs = math.random(1,1),
		nameofstore = "Robs Liquor. (Prosperity Street)",
		lastrobbed = 0
	},
	["ocean_liquor"] = {
		position = { ['x'] = -2965.88, ['y'] = 390.95, ['z'] = 15.04 },
		--sacs = math.random(1,1),
		nameofstore = "Robs Liquor. (Great Ocean Higway)",
		lastrobbed = 0
	},
	["route68_liquor"] = {
		position = { ['x'] = 1168.7, ['y'] = 2717.88, ['z'] = 37.16 },
		--sacs = math.random(1,1),
		nameofstore = "Robs Liquor. (Route 68)",
		lastrobbed = 0
	},
--------------------MAGASIN DE VETEMENTS----------------

	["innocence_vetement"] = {
		position = { ['x'] = 73.62, ['y'] = -1392.3, ['z'] = 29.38 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Innocence Boulevard)",
		lastrobbed = 0
	},
	["portola_vetement"] = {
		position = { ['x'] = -708.67, ['y'] = -151.17, ['z'] = 37.42 },
		--sacs = math.random(1,1),
		nameofstore = "Ponsonbys. (Portola Drive)",
		lastrobbed = 0
	},
	["lagunas_vetement"] = {
		position = { ['x'] = -165.47, ['y'] = -303.26, ['z'] = 39.73 },
		--sacs = math.random(1,1),
		nameofstore = "Ponsonbys. (Las Lagunas Blvd)",
		lastrobbed = 0
	},
	["sinner_vetement"] = {
		position = { ['x'] = 427.15, ['y'] = -806.51, ['z'] = 29.49 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Sinner Street)",
		lastrobbed = 0
	},
	["palomino_vetement"] = {
		position = { ['x'] = -822.86, ['y'] = -1071.94, ['z'] = 11.33 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Palomino Avenue)",
		lastrobbed = 0
	},
	["rockford_vetement"] = {
		position = { ['x'] = -1448.54, ['y'] = -238.11, ['z'] = 49.81 },
		--sacs = math.random(1,1),
		nameofstore = "Ponsonbys. (Rockford Drive)",
		lastrobbed = 0
	},
	["paleto_vetement"] = {
		position = { ['x'] = 5.75, ['y'] = 6510.96, ['z'] = 31.88 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Paleto Boulevard)",
		lastrobbed = 0
	},
	["paleto_vetement"] = {
		position = { ['x'] = 5.75, ['y'] = 6510.96, ['z'] = 31.88 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Paleto Boulevard)",
		lastrobbed = 0
	},
	["hawick_vetement"] = {
		position = { ['x'] = 118.87, ['y'] = -232.37, ['z'] = 54.56 },
		--sacs = math.random(1,1),
		nameofstore = "Suburban. (Hawick Avenue)",
		lastrobbed = 0
	},
	["grapeseed_vetement"] = {
		position = { ['x'] = 1695.65, ['y'] = 4822.61, ['z'] = 42.06 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Grapeseed Main Street)",
		lastrobbed = 0
	},
	["route68_vetement"] = {
		position = { ['x'] = 617.37, ['y'] = 2773.36, ['z'] = 42.09 },
		--sacs = math.random(1,1),
		nameofstore = "Suburban. (Route 68)",
		lastrobbed = 0
	},
	["route682_vetement"] = {
		position = { ['x'] = 1196.67, ['y'] = 2711.95, ['z'] = 38.22 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Route 68)",
		lastrobbed = 0
	},
	["rockford2_vetement"] = {
		position = { ['x'] = -1182.45, ['y'] = -764.93, ['z'] = 17.33 },
		--sacs = math.random(1,1),
		nameofstore = "Suburban. (Rockford Drive North)",
		lastrobbed = 0
	},
	["great_vetement"] = {
		position = { ['x'] = -3178.36, ['y'] = 1035.82, ['z'] = 20.86 },
		--sacs = math.random(1,1),
		nameofstore = "Suburban. (Great Ocean Highway)",
		lastrobbed = 0
	},
	["route683_vetement"] = {
		position = { ['x'] = -1102.29, ['y'] = 2712.1, ['z'] = 19.11 },
		--sacs = math.random(1,1),
		nameofstore = "Discount Store. (Route 68)",
		lastrobbed = 0
	},
--------------------COIFFEUR/BARBIER--------------------

	["madwayne_coiffeur"] = {
		position = { ['x'] = -808.88, ['y'] = -179.89, ['z'] = 37.57 },
		--sacs = math.random(1,1),
		nameofstore = "Bob Mulét. (Mad Wayne Drive)",
		lastrobbed = 0
	},
	["carson_coiffeur"] = {
		position = { ['x'] = 141.19, ['y'] = -1706.21, ['z'] = 29.29 },
		--sacs = math.random(1,1),
		nameofstore = "Herr Kutz Barber. (Carson Avenue)",
		lastrobbed = 0
	},
	["magellan_coiffeur"] = {
		position = { ['x'] = -1278.46, ['y'] = -1119.29, ['z'] = 6.99 },
		--sacs = math.random(1,1),
		nameofstore = "Beachcombover Barbers. (Magellan Avenue)",
		lastrobbed = 0
	},
	["niland_coiffeur"] = {
		position = { ['x'] = 1931.3, ['y'] = 3734.78, ['z'] = 32.84 },
		--sacs = math.random(1,1),
		nameofstore = "O'Sheas Barbers. (Niland Avenue)",
		lastrobbed = 0
	},
	["mirror_coiffeur"] = {
		position = { ['x'] = 1215.78, ['y'] = -475.85, ['z'] = 66.21 },
		--sacs = math.random(1,1),
		nameofstore = "Herr Kutz Barber. (Mirror Park Blvd)",
		lastrobbed = 0
	},
	["hawick_coiffeur"] = {
		position = { ['x'] = -36.33, ['y'] = -155.62, ['z'] = 57.08 },
		--sacs = math.random(1,1),
		nameofstore = "Hair On Hawick. (Hawick Avenue)",
		lastrobbed = 0
	},
	["duluoz_coiffeur"] = {
		position = { ['x'] = -276.7, ['y'] = 6223.72, ['z'] = 31.7 },
		--sacs = math.random(1,1),
		nameofstore = "Herr Kutz Barber. (Duluoz Avenue)",
		lastrobbed = 0
	},
--------------------SALON DE TATOUAGE-------------------

	["innocence_tatouage"] = {
		position = { ['x'] = 1326.46, ['y'] = -1652.67, ['z'] = 52.28 },
		--sacs = math.random(1,1),
		nameofstore = "Los Santos Tattoos. (Innocence Blvd)",
		lastrobbed = 0
	},
	["aguija_tatouage"] = {
		position = { ['x'] = -1150.46, ['y'] = -1426.36, ['z'] = 4.95 },
		--sacs = math.random(1,1),
		nameofstore = "The PIT. (Aguja Street)",
		lastrobbed = 0
	},
	["vinewood_tatouage"] = {
		position = { ['x'] = 320.92, ['y'] = 183.81, ['z'] = 103.59 },
		--sacs = math.random(1,1),
		nameofstore = "Blazing Tattoo. (Vinewood Blvd)",
		lastrobbed = 0
	},
	["barbareno_tatouage"] = {
		position = { ['x'] = -3173.22, ['y'] = 1074.34, ['z'] = 20.83 },
		--sacs = math.random(1,1),
		nameofstore = "Inkinc Tattoos. (Barbareno Road)",
		lastrobbed = 0
	},
	["zancudo_tatouage"] = {
		position = { ['x'] = 1862.07, ['y'] = 3748.24, ['z'] = 33.03 },
		--sacs = math.random(1,1),
		nameofstore = "Body Art Tattoos. (Zancudo Avenue)",
		lastrobbed = 0
	},
	["duluoz_tatouage"] = {
		position = { ['x'] = -292.2, ['y'] = 6197.3, ['z'] = 31.49 },
		--sacs = math.random(1,1),
		nameofstore = "The Fastest Gun. (Duluoz Avenue)",
		lastrobbed = 0
	},
--------------------STATION SERVICE-------------------

	["xero_route68"] = {
		position = { ['x'] = 46.45, ['y'] =  2789.11, ['z'] = 57.87 },
		--sacs = math.random(1,1),
		nameofstore = "Xero (Route 68)",
		lastrobbed = 0
	},
	["general_route68"] = {
		position = { ['x'] = 265.86, ['y'] = 2598.2814, ['z'] = 44.83 },
		--sacs = math.random(1,1),
		nameofstore = "General store (Route 68)",
		lastrobbed = 0
	},
	["cafe_route68"] = {
		position = { ['x'] = 1039.53, ['y'] = 2664.75, ['z'] = 39.55 },
		--sacs = math.random(1,1),
		nameofstore = "Café (Route 68)",
		lastrobbed = 0
	},
	["store_route68"] = {
		position = { ['x'] = 1200.68, ['y'] = 2655.91, ['z'] = 37.85 },
		--sacs = math.random(1,1),
		nameofstore = "Store (Route 68)",
		lastrobbed = 0
	},
	["senora_way_rex_diner"] = {
		position = { ['x'] = 2562.02, ['y'] = 2590.82, ['z'] = 38.08 },
		--sacs = math.random(1,1),
		nameofstore = "Rex\'s Diner (Senora way)",
		lastrobbed = 0
	},
	["route_13_earls"] = {
		position = { ['x'] = 2662.29, ['y'] = 3265.55, ['z'] = 55.24 },
		--sacs = math.random(1,1),
		nameofstore = "Earl's (Route 13)",
		lastrobbed = 0
	},
	["drmarket_marina"] = {
		position = { ['x'] = 1997.02, ['y'] =  3780.41, ['z'] = 32.18 },
		--sacs = math.random(1,1),
		nameofstore = "Dr. Market (Marina)",
		lastrobbed = 0
	},
	["grapespeed_ltd"] = {
		position = { ['x'] = 1702.47, ['y'] = 4916.47, ['z'] = 42.07 },
		--sacs = math.random(1,1),
		nameofstore = "Grapespeed LTD",
		lastrobbed = 0
	},
	["great_ocean_highway_globe_oil"] = {
		position = { ['x'] = 1706.17, ['y'] = 6425.84, ['z'] = 32.77 },
		--sacs = math.random(1,1),
		nameofstore = "Great Ocean Highway Globe Oil",
		lastrobbed = 0
	},
	["paleto_boulevard_don_store"] = {
		position = { ['x'] = 162.07, ['y'] = 6636.59, ['z'] = 31.55 },
		--sacs = math.random(1,1),
		nameofstore = "Paleto boulevard Don\'s Store",
		lastrobbed = 0
	},
	["paleto_boulevard_xero"] = {
		position = { ['x'] = -92.69, ['y'] = 6409.94, ['z'] = 31.64 },
		--sacs = math.random(1,1),
		nameofstore = "Paleto boulevard Xero",
		lastrobbed = 0
	},
	["route_68_ron"] = {
		position = { ['x'] = -2544.08, ['y'] = 2316.09, ['z'] = 33.21 },
		--sacs = math.random(1,1),
		nameofstore = "Route 68 Ron",
		lastrobbed = 0
	},
	["route11ltd"] = {
		position = { ['x'] = -1828.23, ['y'] = 800.09, ['z'] = 138.16 },
		--sacs = math.random(1,1),
		nameofstore = "Route 11 LTD",
		lastrobbed = 0
	},
	["rockfortdriveron"] = {
		position = { ['x'] = -1427.84, ['y'] = -268.32, ['z'] = 46.22 },
		--sacs = math.random(1,1),
		nameofstore = "Rockfort Drive Ron",
		lastrobbed = 0
	},
	["delperrofreewayxero"] = {
		position = { ['x'] = -2073.12, ['y'] = -327.32, ['z'] = 13.31 },
		--sacs = math.random(1,1),
		nameofstore = "Del Perro Freeway Xero",
		lastrobbed = 0
	},
	["lindsaycircusltd"] = {
		position = { ['x'] = -702.87, ['y'] = -917.40, ['z'] = 19.21 },
		--sacs = math.random(1,1),
		nameofstore = "Lindsay Circus LTD",
		lastrobbed = 0
	},
	["calaisavenuexero"] = {
		position = { ['x'] = -531.52, ['y'] = -1221.15, ['z'] = 18.45 },
		--sacs = math.random(1,1),
		nameofstore = "Calais Avenue Xero",
		lastrobbed = 0
	},
	["grovestreetltd"] = {
		position = { ['x'] = -40.92, ['y'] = -1747.83, ['z'] = 29.32 },
		--sacs = math.random(1,1),
		nameofstore = "Grove Street LTD",
		lastrobbed = 0
	},
	["capitalboulevardxero"] = {
		position = { ['x'] = 289.47, ['y'] = -1266.92, ['z'] = 29.44 },
		--sacs = math.random(1,1),
		nameofstore = "Capital Boulevard Xero",
		lastrobbed = 0
	},
	["vespicciboulevard"] = {
		position = { ['x'] =  818.15, ['y'] = -1040.76, ['z'] = 26.75 },
		--sacs = math.random(1,1),
		nameofstore = "Vespicci Boulevard",
		lastrobbed = 0
	},
	["capitalboulevardron"] = {
		position = { ['x'] = 1211.17, ['y'] = -1389.16, ['z'] = 35.37 },
		--sacs = math.random(1,1),
		nameofstore = "Capital Boulevard Ron",
		lastrobbed = 0
	},
	["mirrorparkboulevardltd"] = {
		position = { ['x'] = 1160.73, ['y'] = -311.91, ['z'] = 69.27 },
		--sacs = math.random(1,1),
		nameofstore = "Mirror Park Boulevard LTD",
		lastrobbed = 0
	},
	["clintonavenuebeautystore"] = {
		position = { ['x'] = 646.35, ['y'] = 267.38, ['z'] = 103.25 },
		--sacs = math.random(1,1),
		nameofstore = "Clinton Avenue Beauty Store",
		lastrobbed = 0
	},
	["palominofreewayron"] = {
		position = { ['x'] =  2559.16, ['y'] = 373.88, ['z'] = 108.62 },
		--sacs = math.random(1,1),
		nameofstore = "Palomino Freeway Ron",
		lastrobbed = 0
	},
--------------------AMMUNATION-------------------

	["lindsaycircus"] = {
		position = { ['x'] = -666.56, ['y'] = -933.73, ['z'] = 21.82 },
		--sacs = math.random(1,1),
		nameofstore = "Lindsay Circus",
		lastrobbed = 0
	},
	["popularstreet"] = {
		position = { ['x'] = 818.07, ['y'] = -2155.38, ['z'] = 29.61 },
		--sacs = math.random(1,1),
		nameofstore = "Popular Street",
		lastrobbed = 0
	},
	["algonquinboulevard"] = {
		position = { ['x'] = 1689.72, ['y'] = 3758.13, ['z'] = 34.70 },
		--sacs = math.random(1,1),
		nameofstore = "Algonquin Boulevard",
		lastrobbed = 0
	},
	["greatoceanhighway"] = {
		position = { ['x'] = -334.53, ['y'] = 6081.86, ['z'] = 31.45 },
		--sacs = math.random(1,1),
		nameofstore = "Great Ocean Highway",
		lastrobbed = 0
	},
	["spanishavenue"] = {
		position = { ['x'] =  255.06, ['y'] = -46.46, ['z'] = 69.94 },
		--sacs = math.random(1,1),
		nameofstore = "Spanish Avenue",
		lastrobbed = 0
	},
	["adamsappleboulevard"] = {
		position = { ['x'] = 13.92, ['y'] = -1106.28, ['z'] = 29.79 },
		--sacs = math.random(1,1),
		nameofstore = "Adam's Apple Boulevard",
		lastrobbed = 0
	},
	["palominofreeway"] = {
		position = { ['x'] = 2572.20, ['y'] = 292.58, ['z'] = 108.73 },
		--sacs = math.random(1,1),
		nameofstore = "Palomino Freeway",
		lastrobbed = 0
	},
	["route68"] = {
		position = { ['x'] = -1122.08, ['y'] = 2696.76, ['z'] = 18.55 },
		--sacs = math.random(1,1),
		nameofstore = "Route 68",
		lastrobbed = 0
	},
	["vespucciboulevard"] = {
		position = { ['x'] =  846.68, ['y'] = -1035.26, ['z'] = 28.34 },
		--sacs = math.random(1,1),
		nameofstore = "Vespucci Boulevard",
		lastrobbed = 0
	}
}
