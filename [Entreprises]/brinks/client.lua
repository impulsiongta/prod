local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

--local takeVehicle = {x=-19.89,y=-705.8,z=32.34,a=340.31}
local takeVetement = {x=11.32,y=-661.48,z=33.45}

--[[
local recolte = {
{x=-1485.93,y=-377.7,z=40.16},
{x=-46.44,y=-1758.199,z=29.42}
}
]]

local colorBlipRun = 2

local traitement = {x=-589.065,y=-1587.855,z=26.751}

local vente = {x=-5.41,y=-669.87,z=32.34}
local markerVente = {x = 2.001, y = 2.001, z = 0.5001}
local waitingVente = 1000
local price = 17

local coffre = {x=3.01,y=-659.39,z=33.45}
local coffre_money = {x=8.18,y=-658.37,z=33.45}
local coffre_money2 = {x=248.25,y=222.53,z=106.29}

local phase = 0
local player = -1
local JobAPI = nil

local vehicle = -1
local haveTakeVetement = false
local PlayerData = {}
ESX = nil
local GUI                     = {}
GUI.Time                      = 0
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil

---------------------------------
local brinksup = false
local store = ""
local secondsRemaining = 0
---------------------------------

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'brinks' then

		Config.Zones.VehicleSpawner.Type   = 1
		Config.Zones.VehicleDeleter.Type   = 1
		Config.Zones.Coffre.Type	   = 1

	else

		Config.Zones.VehicleSpawner.Type   = -1
		Config.Zones.VehicleDeleter.Type   = -1
		Config.Zones.Coffre.Type	   = -1
	end

end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)

    playerJob = xPlayer.job.name
    playerGrade = xPlayer.job.grade
    player = xPlayer

  	if player.job.name == 'brinks' then

		Config.Zones.VehicleSpawner.Type   = 1
		Config.Zones.VehicleDeleter.Type   = 1
		Config.Zones.Coffre.Type	   = 1

	else

		Config.Zones.VehicleSpawner.Type   = -1
		Config.Zones.VehicleDeleter.Type   = -1
		Config.Zones.Coffre.Type	   = -1
		
	end
	
    while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end


	while player == -1 do
		Citizen.Wait(10)
	end

	if(player ~= -1 and player.job ~= nil and player.job.name == "brinks") then
		--JobAPI.createCoffre(coffre.x,coffre.y,coffre.z,"brinks")
		if (player.job.grade_name == "boss") then
			JobAPI.createSocietyMenu(coffre_money.x,coffre_money.y,coffre_money.z, 'brinks', 'Brink\'s')
			JobAPI.createSocietyMenu2(coffre_money2.x,coffre_money2.y,coffre_money2.z, 'brinksoff', 'Compte Offshore Brink\'s')
		end
	end

	Citizen.CreateThread(function()
		RequestModel("STOCKADE")

		company = AddBlipForCoord(-56.99, -688.1, 32.34)
	    SetBlipSprite(company, 461)
		SetBlipColour(company, 20)
	    SetBlipAsShortRange(company, true)
	    BeginTextCommandSetBlipName("STRING")
	    AddTextComponentString("Brink's")
	    EndTextCommandSetBlipName(company)

		while player == -1 do
			Citizen.Wait(10)
		end


		while true do
			Citizen.Wait(0)
			if(player~=-1 ) then
				if(player.job ~= nil and player.job.name ~= nil and player.job.name == "brinks") then
					DrawMarker(1,takeVetement.x,takeVetement.y,takeVetement.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

					if(isNear(takeVetement)) then
						if(haveTakeVetements) then
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~reprendre vos vêtements")
						else
							Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre votre équipements")
						end

						if(IsControlJustPressed(1, 38)) then
							haveTakeVetements = not haveTakeVetements
							phase = 0

							if(not haveTakeVetements) then
								JobAPI.stop()

								--if(DoesEntityExist(vehicle)) then
								--	DeleteVehicle(vehicle)
								--end

								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									TriggerEvent('skinchanger:loadSkin', skin)
									local playerPed = GetPlayerPed(-1)
									
									local weapon = GetHashKey("weapon_smg")
									local weapon2 = GetHashKey("weapon_combatpistol")
									RemoveWeaponFromPed(playerPed, weapon)
									RemoveWeaponFromPed(playerPed, weapon2)
									
									SetPedArmour(playerPed, 0)
									ClearPedBloodDamage(playerPed)
									ResetPedVisibleDamage(playerPed)
									ClearPedLastWeaponDamage(playerPed)
									brinksup = true
								end)
							else
								ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
									if skin.sex == 0 then
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
									else
										TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
									end
									
									local playerPed = GetPlayerPed(-1)
									
									local weapon = GetHashKey("weapon_smg")
									local weapon2 = GetHashKey("weapon_combatpistol")
									GiveWeaponToPed(playerPed, weapon, 250, false)
									GiveWeaponToPed(playerPed, weapon2, 250, false)
									
									SetPedArmour(playerPed, 50)
									--SetEntityHealth(playerPed, 200)
									ClearPedBloodDamage(playerPed)
									ResetPedVisibleDamage(playerPed)
									ClearPedLastWeaponDamage(playerPed)
									createrecolte()
									JobAPI.createVenteEntreprise(1, vente.x, vente.y, vente.z, 'sac_dargent', price, 'brinks', colorBlipRun, true, markerVente.x, markerVente.y, markerVente.z, waitingVente, 'SacArgent')
									brinksup = false
									incircle = false
								end)
							end
						end
					end
					
					--[[if(takeVetement) then
					
						if(haveTakeVetements) then
							DrawMarker(1,takeVehicle.x,takeVehicle.y,takeVehicle.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

							if(isNear(takeVehicle)) then
								if(DoesEntityExist(vehicle)) then
									Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~rentrer le fourgon blindé")
								else
									Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~sortir le fourgon blindé")
								end

								if(IsControlJustPressed(1, 38)) then
									if(DoesEntityExist(vehicle)) then
										DeleteVehicle(vehicle)
									else
										spawnVehicle("STOCKADE",takeVehicle)
										local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), false)
										SetVehicleColours(vehicle, 12, 13)
										SetVehicleDirtLevel(vehicle, 0)
									end
								end
							end
						end
					end]]
				end
			end
		end
	end)
end)

function SetVehicleMaxMods(vehicle)

  local props = {
    modEngine       = 2,
    modTransmission = 2,
  }

  ESX.Game.SetVehicleProperties(vehicle, props)

end

function OpenVehicleSpawnerMenu()

	local elements = {}
	ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)
	  for i=1, #garageVehicles, 1 do
		table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
	  end

	  ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'vehicle_spawner',
		{
		  title    = _U('vehicle_menu'),
		  align    = 'top-left',
		  elements = elements,
		},
		function(data, menu)

		  menu.close()

		  local vehicleProps = data.current.value

		  ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawner.Pos, 345.0, function(vehicle)
			ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
			local playerPed = GetPlayerPed(-1)
			TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
		  end)

		  TriggerServerEvent('esx_society:removeVehicleFromGarage', 'brinks', vehicleProps)

		end,
		function(data, menu)

		  menu.close()

		  CurrentAction     = 'menu_vehicle_spawner'
		  CurrentActionMsg  = _U('vehicle_spawner')
		  CurrentActionData = {}

		end
	  )

	end, 'brinks')
	
end

AddEventHandler('brinks:hasEnteredMarker', function(zone)

	if zone == 'VehicleSpawner' and player.job and player.job.name == 'brinks' then
		CurrentAction     = 'vehicle_spawner_menu'
		CurrentActionMsg  = _U('veh_spawn')
		CurrentActionData = {}
	end

	if zone == 'VehicleDeleter' and player.job and player.job.name == 'brinks' then

		local playerPed = GetPlayerPed(-1)
		local coords    = GetEntityCoords(playerPed)

		if IsPedInAnyVehicle(playerPed,  false) then

			local vehicle, distance = ESX.Game.GetClosestVehicle({
				x = coords.x,
				y = coords.y,
				z = coords.z
			})

			if distance ~= -1 and distance <= 1.0 then

				CurrentAction     = 'delete_vehicle'
				CurrentActionMsg  = _U('store_veh')
				CurrentActionData = {vehicle = vehicle}
			end
		end
	end
	if zone == 'Coffre' and player.job and player.job.name == 'brinks' then
		CurrentAction     = 'brinks_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end
end)
			
AddEventHandler('brinks:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('brinks:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('brinks:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
			end
		end
	end
end)

-----------------------------------------------------------------------

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
	    SetTextOutline()
	end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

RegisterNetEvent('brinks:currentlyrecolte')
AddEventHandler('brinks:currentlyrecolte', function(brinkss)
	brinksup = true
	store = brinkss
	secondsRemaining = 30
end)

RegisterNetEvent('brinks:toofarlocal')
AddEventHandler('brinks:toofarlocal', function(brinkss)
	brinksup = false
	ESX.ShowNotification(_U('brinks_cancelled'))
	robbingName = ""
	secondsRemaining = 0
	incircle = false
end)

--[[
RegisterNetEvent('brinks:robbery_successful')
AddEventHandler('brinks:robbery_successful', function(brinkss)
	brinksup = false
	ESX.ShowNotification(_U('brinks_successful') .. Stores[store].reward .. _U('brinks_successful2'))
	store = ""
	secondsRemaining = 0
	incircle = false
end)
]]--

RegisterNetEvent('brinks:brinks_complete')
AddEventHandler('brinks:brinks_complete', function(brinkss)
	brinksup = false
	ESX.ShowNotification(_U('brinks_complete'))
	store = ""
	secondsRemaining = 0
	incircle = false
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if brinksup then
			Citizen.Wait(1000)
			if(secondsRemaining > 0)then
				secondsRemaining = secondsRemaining - 1
			end
		end
	end
end)

--[[
Citizen.CreateThread(function()
	for k,v in pairs(Stores)do
		local ve = v.position

		local blip = AddBlipForCoord(ve.x, ve.y, ve.z)
		SetBlipSprite(blip, 461)
		SetBlipColour(blip, 1)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(_U('store_brinks'))
		EndTextCommandSetBlipName(blip)
	end
end)
]]--

incircle = false

function createrecolte()
Citizen.CreateThread(function()
	while true do
		local pos = GetEntityCoords(GetPlayerPed(-1), true)

		for k,v in pairs(Stores)do
			local pos2 = v.position

			if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) < 15.0)then
				if not brinksup then
					DrawMarker(1, v.position.x, v.position.y, v.position.z - 1, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.0001, 0, 128, 255, 200, 0, 0, 0,0)

					if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) < 1.0)then
						if incircle == false then
							DisplayHelpText(_U('press_to_brinks') .. v.nameofstore)
						end
						incircle = true
						if IsControlJustReleased(1, 51) then
							TriggerServerEvent('brinks:recolte', k)
						end
					elseif(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) > 1.0)then
						incircle = false
					end
				end
			end
		end

		if brinksup then

			drawTxt(0.68, 1.45, 1.0,1.0,0.4, _U('brinks_of') .. secondsRemaining .. _U('seconds_remaining'), 255, 255, 255, 255)

			local pos2 = Stores[store].position

			if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) > 7.5)then
				TriggerServerEvent('brinks:toofar', store)
			end
		end

		Citizen.Wait(0)
	end
end)
end

---------------------------------------------------------------------------------
function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end

--[[
function spawnVehicle(name, tabl)

	hashTruck = GetHashKey(name)
	RequestModel(hashTruck)

	local playerPed = GetPlayerPed(-1) 	

    Truck = CreateVehicle(hashTruck,  tabl.x,tabl.y,tabl.z, tabl.a, true, false)
    SetVehicleOnGroundProperly(Truck)
    SetVehRadioStation(Truck, "OFF")
	SetPedIntoVehicle(playerPed, Truck, -1)
   	SetVehicleEngineOn(Truck, true, false, false)

   	TriggerEvent("advancedFuel:setEssence", 75, GetVehicleNumberPlateText(GetVehiclePedIsUsing(GetPlayerPed(-1))), GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(GetPlayerPed(-1)))))

	vehicle = Truck

end
]]

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name       = 'Brinks',
		number     = 'brinks',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAijSURBVHjavFdtTFTZGX7uvfPBMCCzA7IMouAYB3CR0i0apSpBCT+aIDuudWsght3VpUsCfqTbGE36p+6aqBm3plF3jRjDbqhpjG1GCmEsJVJQoFg74zj4wccMEQYFhw7MzP04957+GQgCtqzd9k3ur3PPeZ736znnBf57+yGArwBcA1AMQI3/g+kA/AzAHxiGEUtKSuj69espAAqgB0ANgJX/C+BMAL8E4DGZTPSjjz6iXV1dEqWUhEIh8fr16+GysjIaExNDAbwEcOH7IKIGUATgdwCmc3JyqM1mo8+fP5copQpdaLLL5YpYrdaZiOS+KfDbAA4BcMbExNA9e/bQtrY2IoqiTJdg5eXlFMBf3gT4xwCuAhjJzMykp06don19ffI8b8nY2JhACJEWAw+Hw5LZbKYAjn/X/P5ZrVbLu3btog0NDTQUCs0FUHw+n3jmzBmhoKCANxgMYn5+fqStrU2YTyASiUgWi4UCOPldCHxtMpno3bt3CaV0NsyKopD29naxqqoqYjQapWheZ7/s7Gw+HA4viERNTY0CoB+AcSngiQAmv/jii1fyOzg4SDZv3iwAkOeAyhkZGSLHcQoAmpCQQMbHxxcQcLvdYnx8PAVgB6D6TwQOLVu2jHq9XjL3kImJCWIwGMgMeGZmptTa2ioQQniLxUIA0MLCwiAhhF+sFq5cuRKJ7r3w78DjATw9ePDgYi2lfPbZZzOH0HXr1pEnT57wVVVVgkqlUgDQ2NhYqaCgIOx2u4XF9h89epSP7v/V6whU6fV6Ojo6Ki7mRW9vr8gwjAyAMgyj1NbWBgHIarVa1mg0s6kpLi4WFUVZzAmyb98+Mfpf0RxcPYAfAMAtrVZLjx8/zhNCFvQ4IYRs2bJFmAEqKyuLNDQ0hL1er1hfXz/jHV2xYoUUCoUW1YhQKCTl5ORQAPei4DEAHAD+DgDZAH4NQLZarQLP8wsK6ty5czMeUK1WS0ZGRgSPxyPt3r17tivy8/MJIUR5nTA5HA5RrVZTAL8B0PbJJ59Qo9H4t7mp2AeAnD9/fkEunz17JhmNxtlitFgs4tzwx8fHk6amJnERPZCbm5uF4eFhqa2tLaLX60l2dja9efMmbWxspCzLdsyvh/q8vDxlMbl9//33xfkaAEDesWOH0NPTI1BKFY/HI9ntdvHGjRu8zWabtlqtUxzHicXFxeG9e/dG7HZ7RBRFsbm5WUhJSaFRxX3FDiclJdFAILCAQFNTEw9AAUB1Oh3Zs2cP39raKsyI1rVr14R33303lJ6ezgMIAHiYn58fSU5OJt9++224ubk5YrPZImVlZXxRUdGk0WgUARyYLxASpRSKoixola1bt6qLi4vFwsJCtry8HKtXr9YAYKLLysmTJ6nNZmO7u7s5u92uy8rKSr58+bJSUlKidHR0KCqVio2NjWU2bdqEycnJ2K6uriCAP80noOI4DizL0vkE9Ho963A4WI/Hw/A8z8wBBwD2yJEj9Msvv8TY2BhxuVyBvr6+ZACTWq12dPny5aapqSlZpVKpHA7Hi9bW1lQApwE8Y+fhUEopKKXMfAI8z9NPP/2Ufvzxx/yuXbvEzs5Oae763r17YwKBgKarq2ua5/nnGRkZXoZhYnmeTwgGg4rJZJJ0Op08Pj6+DEAHgLNYRKN5QRAgCIICYJac2+2mR48e5W/evKm+ffs253A40NLSIhcUFKgHBgZQV1cXqa+vH0tLS5MLCwt1Pp/v7cTERCUrK0t6+fKl6Ha7ydDQkPbGjRsygGUAGgCIixEIhMNhBAIBpKSkwOl0ks8//zzidDrHvF6vXF5enlFZWclIkkSSkpJ0Dx8+nGhvb58sLS1NCIVCyvDwsL6ysvKtjo6OaY7jhtevXx+TmJj4ls/nI+3t7Zrc3Nywy+UCpXRwNufzCDgJIcLQ0BCbnJxMysvL8eDBAyUuLi4+OztbL8uyemBg4CkAedWqVcu6urpCfr/fl5ubuzkYDMb7/f6QyWSaTk9P1/b09KwcHh6+a7FY8rRabdLZs2eZx48fxzudzmcAZgWIi4Z6pqjUAH7a19eX0NTUJO7evZuuXbtW29/fr33y5EnkwYMH7IYNG0IcxyE/P395YWGhwe/3KwCU0tLSmJaWFn5oaCjBYDDwd+7cCQSDwQlCSOLFixcNpaWlTEVFhWpqauorAH+crV4AlQBcRqOxMzU1tWfbtm0pH374oXL16lWVJEnsxMSEXFFRoWzfvv0ly7K0qKgobe3atSljY2O0pKSErl69eqXL5dILgqAaGhr65+3btx89evRoas2aNbqLFy+WPH78OH3btm1MdXU1HRkZmQbw9SttB+Aex3Gp1dXVhpqaGikhIYHp7e1VamtrqcFgYPLy8li/3y/HxcWtUBTF6fF48hITE592d3ezLS0tmbIsTw8MDPgvXbqUZDKZVBs3bkyxWq1vWa1WFcMwAKCcPn2aXLhwQRO9kp/OJTDTbhtZlv39unXrVmk0mrDZbKabNm1SqdVq9v79+7LZbFZ98803YwzDhM1mc0ZOTo586tSp3nfeeSc1PT2dGxwclHfu3Jl84MAB/Zo1a2YPHx0dlU+cOMGcP3+ejV5Ch+a399x+XwXAptVqf1JaWqrr7+8naWlpoZUrV2obGxv5zMzMmPHxcVar1VKDwSA6nc4XFoslobq6OuG9995TqVSv1DO9deuWtH//fo3X6w1GX8e/XerjdAOASwDuxcXFhQFQtVod2Lp169SxY8cEm80WPnz4cNjtdr/26q2rq+M5jqMAbgHIetPBRBUdPO+xLEs/+OAD/v79+zylVKCUzrwdic/nE+/cuSPZ7fZwZ2en0NjYyGs0GgqgbimDKrMEIkYAvwCwH8Byi8UCs9kMnU6HkZERuN1uTE9PA0AEQAzLsoyiKA8B/AgA/30QmLHU6Pi9PTq2AcALAP8A8NfoMJoE4OcAegGcW8qh/xoAzb75OCI1u3QAAAAASUVORK5CYII='
	}
	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

function OpenMobileBrinksActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'mobile_brinks_actions',
		{
			title    = 'Brinks',
			align    = 'top-left',
			elements = {
				{label = 'Facturation',    value = 'billing'},
			}
		},
		function(data, menu)

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else

							menu.close()

							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun ~r~~h~joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_brinks', 'Brink\'s', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)
			menu.close()
		end
	)

end

Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)
		
		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and player.job and player.job.name == 'brinks' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'vehicle_spawner_menu' then
					OpenVehicleSpawnerMenu()
				end
				
				if CurrentAction == 'delete_vehicle' then
					local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
					TriggerServerEvent('esx_society:putVehicleInGarage', 'brinks', vehicleProps)
					ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
				end
				if CurrentAction == 'brinks_actions_menu' then
					CoffreMenu()
				end				

				CurrentAction = nil
				GUI.Time      = GetGameTimer()

			end
		end

		if IsControlPressed(0,  Keys['F6']) and player.job ~= nil and player.job.name == 'brinks' and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mobile_brinks_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenMobileBrinksActionsMenu()
			GUI.Time = GetGameTimer()
		end

	end
end)

function CoffreMenu()

	local elements = {
		{label = _U('deposit_stock'), value = 'put_stock'},
    		{label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if PlayerData.job ~= nil and PlayerData.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_actions'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('brinks_stock'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        			OpenPutStocksMenu()
      			end

			if data.current.value == 'get_stock' then
		        	OpenGetStocksMenu()
		      	end

		      	if data.current.value == 'boss_actions' then
		        	TriggerEvent('esx_society:openBossMenu', 'brinks', function(data, menu)
		          	menu.close()
		        	end)
		      	end

			CurrentAction     = 'brinks_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('brinks:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
    	if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('brinks_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('brinks:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('brinks:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('brinks:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end