ui_page "libs/index.html"

client_script "client.lua"
server_script 'server.lua'
files {
    "libs/index.html",
    "libs/assets/css/style.css",
    "libs/assets/fonts/MaterialIcons-Regular.eot",
	"libs/assets/fonts/MaterialIcons-Regular.ttf",
	"libs/assets/fonts/MaterialIcons-Regular.woff",
	"libs/assets/fonts/MaterialIcons-Regular.woff2",
	"libs/assets/fonts/SignPainter-HouseScript.eot",
	"libs/assets/fonts/SignPainter-HouseScript.woff2",
	"libs/assets/fonts/SignPainter-HouseScript.woff",
	"libs/assets/fonts/SignPainter-HouseScript.ttf",
	"libs/assets/fonts/SignPainter-HouseScript.svg",
	"libs/assets/fonts/PricedownBl-Regular.woff",
	"libs/assets/fonts/PricedownBl-Regular.ttf",
	"libs/assets/fonts/PricedownBl-Regular.svg",
    "libs/assets/js/index.js",
	"files/logo.png"
}
