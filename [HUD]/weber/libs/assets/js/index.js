// ____  _  _  ____  ____  _  _ 
//(_  _)( \( )(  _ \( ___)( \/ )
// _)(_  )  (  )(_) ))__)  )  ( 
//(____)(_)\_)(____/(____)(_/\_)

// 1. Variables
// 4. Envoie
// 5. Fonctions
// 6. Data
// 7. Réceptions

// __      __     _____  _____          ____  _      ______  _____ 
// \ \    / /\   |  __ \|_   _|   /\   |  _ \| |    |  ____|/ ____|
//  \ \  / /  \  | |__) | | |    /  \  | |_) | |    | |__  | (___  
//   \ \/ / /\ \ |  _  /  | |   / /\ \ |  _ <| |    |  __|  \___ \ 
//    \  / ____ \| | \ \ _| |_ / ____ \| |_) | |____| |____ ____) |
//     \/_/    \_\_|  \_\_____/_/    \_\____/|______|______|_____/ 

//  ______ _   ___      ______ _____ ______ 
// |  ____| \ | \ \    / / __ \_   _|  ____|
// | |__  |  \| |\ \  / / |  | || | | |__   
// |  __| |     | \ \/ /| |  | || | |  __|  
// | |____| |\  |  \  / | |__| || |_| |____ 
// |______|_| \_|   \/   \____/_____|______|

function sendData(name, data) {
    $.post("http://weber/" + name, JSON.stringify(data), function(data) {});
}

//  ______ ____  _   _  _____ _______ _____ ____  _   _  _____ 
// |  ____/ __ \| \ | |/ ____|__   __|_   _/ __ \| \ | |/ ____|
// | |__ | |  | |  \| | |       | |    | || |  | |  \| | (___  
// |  __|| |  | |     | |       | |    | || |  | |     |\___ \ 
// | |   | |__| | |\  | |____   | |   _| || |__| | |\  |____) |
// |_|    \____/|_| \_|\_____|  |_|  |_____\____/|_| \_|_____/ 

$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

function FinishNotif(id) {
  id.addClass('hidden');
}

function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

//  _____       _______       
// |  __ \   /\|__   __|/\    
// | |  | | /  \  | |  /  \   
// | |  | |/ /\ \ | | / /\ \  
// | |__| / ____ \| |/ ____ \ 
// |_____/_/    \_\_/_/    \_\

function sendnotif(title, message, spec, color, icon, position, expire, idnotif, animenter, animexit) {
	var elements = '<div class="kyl_notif" id="'+ idnotif +'" data-color="'+ color +'" data-positionnotif="'+ position +'" id="js-timer">';
	
	if (icon == false) {
	} 
	else if (icon == true) {
		elements += '<div class="kyl_notif-icon">';	
		if (color == 'red') {
			elements += '<i class="icons">announcement</i>';
		}
		else if (color == 'rose') {
			elements += '<i class="icons">insert_emoticon</i>';
		}
		else if (color == 'purple') {
			elements += '<i class="icons">lightbulb_outline</i>';
		}
		else if (color == 'blue') {
			elements += '<i class="icons">live_help</i>';
		}
		else if (color == 'green') {
			elements += '<i class="icons">done</i>';
		}
		else if (color == 'green') {
			elements += '<i class="icons">done_all</i>';
		}
		else if (color == 'green') {
			elements += '<i class="icons">thumb_up</i>';
		}
		else if (color == 'yellow') {
			elements += '<i class="icons">new_releases</i>';
		}
		else if (color == 'amber') {
			elements += '<i class="icons">warning</i>';
		}
		else if (color == 'orange') {
			elements += '<i class="icons">priority_high</i>';
		}
		else if (color == 'dark_orange') {
			elements += '<i class="icons">report</i>';
		}
		else {
			elements += '<i class="icons">crop_square</i>';
		}
		elements += '</div>';
	} else {
	elements += '<div class="kyl_notif-icon">';
	elements += '<i class="icons">'+ icon +'</i>';
	elements += '</div>';
	}
	
	elements += '<div class="kyl_notif-body">';
	
	if (title == false) {
	} else {
	elements += '<h4>'+ title +'</h4>';
	}
	
	if (message == false) {
	} else {
	elements += '<p>'+ message +'</p>';
	}
	
	if (spec == false) {
	} else {
	elements += '<p class="kyl_italic">'+ spec +'</p>';
	}
	
	elements += '</div></div>';
	
	$("#body").append(elements);
	
	if (animenter == true) {
		$('#' + idnotif).animateCss('bounceIn');
	}
	else if (animenter == false) {
	}
	else {
		$('#' + idnotif).animateCss(animenter);
	}
	
	if (expire == true) {
		setTimeout(function() {
			if (animexit == true) {
				$('#' + idnotif).hide('bounceOut');
			}
			else if (animexit == false) {
				$('#' + idnotif).hide();
			}
			else {
				$('#' + idnotif).hide(animexit);
			}
		}, 5000);
	} 
	
	else if (expire == false) {
	} 
	
	else {
		setTimeout(function() {
			if (animexit == true) {
				$('#' + idnotif).hide('bounceOut');
			}
			else if (animexit == false) {
				$('#' + idnotif).hide();
			}
			else {
				$('#' + idnotif).hide(animexit);
			}
		}, expire);
	}
}

function logo(height, width, position, image) {
	var elements = '<img height="'+ height +'" width="'+ width +'" data-positionlogo="'+ position +'" class="kyl_logo" src="'+ image +'"/>';
	$("#body").append(elements);
}

function titleheader(title) {
	$(".kyl-title-header").text(title);
	$(".kyl-title-header").show();
}

function headericon(column1, column2, column3, column4, column5, column6) {
	if (column1 != false) {
		$(".iconcolumn1").empty()
		var elements = '<img height="24" width="24" style="vertical-align:middle; position:inline;" src="'+ column1 +'"/>';
		$(".iconcolumn1").append(elements);
	}
	
	if (column2 != false) {
		$(".iconcolumn2").empty()
		var elements = '<img height="24" width="24" style="vertical-align:middle; position:inline;" src="'+ column2 +'"/>';
		$(".iconcolumn2").append(elements);
	}
	
	if (column3 != false) {
		$(".iconcolumn3").empty()
		var elements = '<img height="24" width="24" style="vertical-align:middle; position:inline;" src="'+ column3 +'"/>';
		$(".iconcolumn3").append(elements);
	}
	
	if (column4 != false) {
		$(".iconcolumn4").empty()
		var elements = '<img height="24" width="24" style="vertical-align:middle; position:inline;" src="'+ column4 +'"/>';
		$(".iconcolumn4").append(elements);
	}
	
	if (column5 != false) {
		$(".iconcolumn5").empty()
		var elements = '<img height="24" width="24" style="vertical-align:middle; position:inline;" src="'+ column5 +'"/>';
		$(".iconcolumn5").append(elements);
	}
	
	if (column6 != false) {
		$(".iconcolumn6").empty()
		var elements = '<img height="24" width="24" style="vertical-align:middle; position:inline;" src="'+ column6 +'"/>';
		$(".iconcolumn6").append(elements);
	}
}

function headertext(column1, number1, column2, number2, column3, number3, column4, number4, column5, number5, column6, number6) {
	if (column1 != false) {
		if (number1 == true) {
			$(".valuecolumn1").empty()
			var elements = numberWithSpaces(column1);
			$(".valuecolumn1").append(elements);
		}
		else {
			$(".valuecolumn1").empty()
			var elements = column1;
			$(".valuecolumn1").append(elements);
		}
	}
	
	if (column2 != false) {
		if (number1 == true) {
			$(".valuecolumn2").empty()
			var elements = numberWithSpaces(column2);
			$(".valuecolumn2").append(elements);
		}
		else {
			$(".valuecolumn2").empty()
			var elements = column2;
			$(".valuecolumn2").append(elements);
		}
	}
	
	if (column3 != false) {
		if (number1 == true) {
			$(".valuecolumn3").empty()
			var elements = numberWithSpaces(column3);
			$(".valuecolumn3").append(elements);
		}
		else {
			$(".valuecolumn3").empty()
			var elements = column3;
			$(".valuecolumn3").append(elements);
		}
	}
	
	if (column4 != false) {
		if (number1 == true) {
			$(".valuecolumn4").empty()
			var elements = numberWithSpaces(column4);
			$(".valuecolumn4").append(elements);
		}
		else {
			$(".valuecolumn4").empty()
			var elements = column4;
			$(".valuecolumn4").append(elements);
		}
	}
	
	if (column5 != false) {
		$(".valuecolumn5").empty()
		var elements = column5;
		$(".valuecolumn5").append(elements);
		if (number1 == true) {
			$(".valuecolumn5").empty()
			var elements = numberWithSpaces(column5);
			$(".valuecolumn5").append(elements);
		}
		else {
			$(".valuecolumn5").empty()
			var elements = column5;
			$(".valuecolumn5").append(elements);
		}
	}
	
	if (column6 != false) {
		if (number1 == true) {
			$(".valuecolumn6").empty()
			var elements = numberWithSpaces(column6);
			$(".valuecolumn6").append(elements);
		}
		else {
			$(".valuecolumn6").empty()
			var elements = column6;
			$(".valuecolumn6").append(elements);
		}
	}
}

function headersymbole(column1, column2, column3, column4, column5, column6) {
	if (column1 != false) {
		$(".symbolecolumn1").empty()
		var elements = column1;
		$(".symbolecolumn1").append(elements);
	}
	
	if (column2 != false) {
		$(".symbolecolumn2").empty()
		var elements = column2;
		$(".symbolecolumn2").append(elements);
	}
	
	if (column3 != false) {
		$(".symbolecolumn3").empty()
		var elements = column2;
		$(".symbolecolumn3").append(elements);
	}
	
	if (column4 != false) {
		$(".symbolecolumn4").empty()
		var elements = column4;
		$(".symbolecolumn4").append(elements);
	}
	
	if (column5 != false) {
		$(".symbolecolumn5").empty()
		var elements = column5;
		$(".symbolecolumn5").append(elements);
	}
	
	if (column6 != false) {
		$(".symbolecolumn6").empty()
		var elements = column6;
		$(".symbolecolumn6").append(elements);
	}
}

function createmenu(menuid, menuname) {
		var elements = '<div id="'+ menuid +'" class="sidebar">';
			elements += '<p class="menu menu-title">'+ menuname +'</p>';
			elements += '</div>';

		$("#body").append(elements);
}

function pagemenu(menuid, pageid, parent) {
		if (parent == false) {
			var elements = '<div id="'+ pageid +'">';
		}
		else {
			var elements = '<div id="'+ pageid +'" data-parent="'+ parent +'">';
		}

		elements += '</div>';

		$("#"+ menuid).append(elements);
}

function itemenu(menuid, pageid, itemname, submenu, action, callback) {
		if (submenu != false) {
			var elements = '<p class="menu" data-sub="'+ submenu +'">'+ itemname +'</p>';
		}
		if (action != false) {
			var elements = '<p class="menu" data-action="'+ action +'">'+ itemname +'</p>';
		}

		$("#"+ pageid).append(elements);
}

//  _____    __   _____ ______ _____ _______ _____ ____  _   _  _____ 
// |  __ \ _/_/_ / ____|  ____|  __ \__   __|_   _/ __ \| \ | |/ ____|
// | |__) | ____| |    | |__  | |__) | | |    | || |  | |  \| | (___  
// |  _  /|  _| | |    |  __| |  ___/  | |    | || |  | |     |\___ \ 
// | | \ \| |___| |____| |____| |      | |   _| || |__| | |\  |____) |
// |_|  \_\_____|\_____|______|_|      |_|  |_____\____/|_| \_|_____/ 

$(document).ready(function(){
	window.addEventListener("message", function(event) {
        var data = event.data

		if (data.sendnotif) {
            sendnotif(data.title, data.message, data.spec, data.color, data.icon, data.position, data.expire, data.idnotif, data.animenter, data.animexit)
        }
	
		else if (data.logo) {
            logo(data.height, data.width, data.position, data.image)
        }
		
		else if (data.titleheader) {
            titleheader(data.title)
        }
		
		else if (data.headericon) {
            headericon(data.column1, data.column2, data.column3, data.column4, data.column5, data.column6)
        }
		
		else if (data.headertext) {
            headertext(data.column1, data.number1, data.column2, data.number2, data.column3, data.number3, data.column4, data.number4, data.column5, data.number5, data.column6, data.number6)
        }
		
		else if (data.headersymbole) {
            headersymbole(data.column1, data.column2, data.column3, data.column4, data.column5, data.column6)
        }
		
		else if (data.progressbar) {
            progressbar(data.progress_width, data.progress_height, data.progress_color, data.bar_width, data.bar_height, data.bar_bordercolor, data.bar_borderwidth)
        }

        else if (data.createmenu) {
            createmenu(data.menuid, data.menuname)
        }

        else if (data.pagemenu) {
            pagemenu(data.menuid, data.pageid, data.parent)
        }

        else if (data.itemenu) {
            itemenu(data.menuid, data.pageid, data.itemname, data.submenu, data.action, data.callback)
        }
    })
})