ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if ESX == nil then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
end


TriggerEvent("es:addGroup", "mod", "user", function(group) end)

-- Modify if you want, btw the _admin_ needs to be able to target the group and it will work
local groupsRequired = {
	kill = "mod",
	noclip = "mod",
	freeze = "mod",
	tp_here = "mod",
	["goto"] = "mod",
	heal = "mod",
	kick = "mod",
	ban = "superadmin",
	revive = "mod"
}

local banned = ""
local bannedTable = {}

function loadBans()
	banned = LoadResourceFile("es_admin2", "data/bans.txt") or ""
	if banned then
		local b = stringsplit(banned, "\n")
		for k,v in ipairs(b) do
			bannedTable[v] = true
		end
	end

	if GetConvar("es_admin2_globalbans", "0") == "1" then
		PerformHttpRequest("http://essentialmode.com/bans.txt", function(err, rText, headers)
			local b = stringsplit(rText, "\n")
			for k,v in pairs(b)do
				bannedTable[v] = true
			end
		end)
	end
end

function isBanned(id)
	return bannedTable[id]
end

function banUser(id)
	banned = banned .. id .. "\n"
	SaveResourceFile("es_admin2", "data/bans.txt", banned, -1)
	bannedTable[id] = true
end

AddEventHandler('playerConnecting', function(user, set)
	for k,v in ipairs(GetPlayerIdentifiers(source))do
		if isBanned(v) then
			set(GetConvar("es_admin_banreason", "Vous avez été banni du serveur."))
			CancelEvent()
			break
		end
	end
end)

RegisterServerEvent('es_admin:all')
AddEventHandler('es_admin:all', function(type)
	local Source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		TriggerEvent('es:canGroupTarget', user.getGroup(), "admin", function(available)
			if available or user.getGroup() == "superadmin" then
				LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:' .. type)
				if type == "kill_all" then TriggerClientEvent('es_admin:quick', -1, 'kill') end
				if type == "tp_here_all" then TriggerClientEvent('es_admin:quick', -1, 'tp_here', Source) end
				if type == "heal_all" then TriggerClientEvent('es_admin:quick', -1, 'heal') end
				if type == "revive_all" then TriggerClientEvent('es_admin:quick', -1, 'revive') end
			else
				TriggerClientEvent('chatMessage', Source, "SYSTEM", {255, 0, 0}, "Vous n'avez pas la permission de faire ça.")
			end
		end)
	end)
end)

RegisterServerEvent('es_admin:quick')
AddEventHandler('es_admin:quick', function(id, type)
	local Source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		TriggerEvent('es:getPlayerFromId', id, function(target)
			TriggerEvent('es:canGroupTarget', user.getGroup(), groupsRequired[type], function(available)
				TriggerEvent('es:canGroupTarget', user.getGroup(), target.getGroup(), function(canTarget)
					if canTarget and available then
						LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:' .. type .. ' sur ' .. target.getIdentifier())
						if type == "ban" then
							for k,v in ipairs(GetPlayerIdentifiers(id))do
								banUser(v)
							end
							DropPlayer(id, GetConvar("es_admin_banreason", "Vous avez été banni du serveur."))
-- Déwhitlist
						end
						if type == "kill" then TriggerClientEvent('es_admin:quick', id, type) end
						if type == "noclip" then TriggerClientEvent('es_admin:quick', id, type) end
						if type == "freeze" then TriggerClientEvent('es_admin:quick', id, type) end
						-- if type == "crash" then TriggerClientEvent('es_admin:quick', id, type) end
						if type == "tp_here" then TriggerClientEvent('es_admin:quick', id, type, Source) end
						if type == "goto" then TriggerClientEvent('es_admin:quick', Source, type, id) end
						if type == "heal" then TriggerClientEvent('es_admin:quick', id, type) end
						if type == "kick" then DropPlayer(id, 'Kicked by es_admin GUI') end
						if type == "revive" then TriggerClientEvent('es_admin:quick', id, type) end
					else
						if not available then
							TriggerClientEvent('chatMessage', Source, 'SYSTEM', {255, 0, 0}, "Votre groupe ne peut pas utiliser cette commande.")
						else
							TriggerClientEvent('chatMessage', Source, 'SYSTEM', {255, 0, 0}, "Vous n'avez pas la permission de faire ça.")
						end
					end
				end)
			end)
		end)
	end)
end)

AddEventHandler('es:playerLoaded', function(Source, user)
	TriggerClientEvent('es_admin:setGroup', Source, user.getGroup())
end)

RegisterServerEvent('es_admin:set')
AddEventHandler('es_admin:set', function(t, USER, GROUP)
	local Source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		TriggerEvent('es:getPlayerFromId', USER, function(target)
			TriggerEvent('es:canGroupTarget', user.getGroup(), "admin", function(available)
				if available then
					if ESX == nil then
						TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
					end
					if t == "group" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Player not found")
						else
							TriggerEvent("es:getAllGroups", function(groups)
								if(groups[GROUP])then
									TriggerEvent("es:setPlayerData", USER, "group", GROUP, function(response, success)
										LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:set' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
										TriggerClientEvent('es_admin:setGroup', USER, GROUP)
										TriggerClientEvent('chatMessage', -1, "CONSOLE", {0, 0, 0}, "Groupe de ^2^*" .. GetPlayerName(tonumber(USER)) .. "^r^0 a été modifié en ^2^*" .. GROUP .. "^0.")
									end)
								else
									TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Groupe non trouvé.")
								end
							end)
						end
					elseif t == "level" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Joueur non trouvé.")
						else
							GROUP = tonumber(GROUP)
							if(GROUP ~= nil and GROUP > -1)then
								TriggerEvent("es:setPlayerData", USER, "permission_level", GROUP, function(response, success)
									if(true)then
										LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:set' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
										TriggerClientEvent('chatMessage', -1, "CONSOLE", {0, 0, 0}, "Niveau de permission de ^2" .. GetPlayerName(tonumber(USER)) .. "^0 a été modifié en ^2" .. tostring(GROUP) .. "^0.")
									end
								end)	
							else
								TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
							end
						end
					elseif t == "money" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Joueur non trouvé.")
						else
							GROUP = tonumber(GROUP)
							if(GROUP ~= nil and GROUP > -1)then
								TriggerEvent('es:getPlayerFromId', USER, function(target)
									LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:set' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
									target.setMoney(GROUP)
								end)
							else
								TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
							end
						end
					elseif t == "bank" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Joueur non trouvé.")
						else
							GROUP = tonumber(GROUP)
							if(GROUP ~= nil and GROUP > -1)then
								TriggerEvent('es:getPlayerFromId', USER, function(target)
									LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:set' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
									target.setBankBalance(GROUP)
								end)
							else
								TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
							end
						end
					elseif t == "black" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Joueur non trouvé.")
						else
							local xPlayer = ESX.GetPlayerFromId(USER)
							GROUP = tonumber(GROUP)
							if(GROUP ~= nil and GROUP > -1)then
								TriggerEvent('es:getPlayerFromId', USER, function(target)
									LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:set' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
									xPlayer.setAccountMoney('black_money', GROUP)
								end)
							else
								TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
							end
						end
					end
				else
					TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Vous devez être admin pour faire ça.")
				end
			end)
		end)
	end)	
end)

RegisterServerEvent('es_admin:add')
AddEventHandler('es_admin:add', function(t, USER, GROUP)
	local Source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		TriggerEvent('es:getPlayerFromId', USER, function(target)
			TriggerEvent('es:canGroupTarget', user.getGroup(), "admin", function(available)
				if available then
					if ESX == nil then
						TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
					end
					if t == "money" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Joueur non trouvé.")
						else
							GROUP = tonumber(GROUP)
							if(GROUP ~= nil)then
								TriggerEvent('es:getPlayerFromId', USER, function(target)
									LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:add' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
									target.addMoney(GROUP)
								end)
							else
								TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
							end
						end
					elseif t == "bank" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Joueur non trouvé.")
						else
							GROUP = tonumber(GROUP)
							if(GROUP ~= nil)then
								TriggerEvent('es:getPlayerFromId', USER, function(target)
									LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:add' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
									target.addBank(GROUP)
								end)
							else
								TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
							end
						end
					elseif t == "black" then
						if(GetPlayerName(USER) == nil)then
							TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Joueur non trouvé.")
						else
							local xPlayer = ESX.GetPlayerFromId(USER)
							GROUP = tonumber(GROUP)
							if(GROUP ~= nil)then
								TriggerEvent('es:getPlayerFromId', USER, function(target)
									LogsAdmin.print(user.getIdentifier() .. ' execute es_admin2:add' .. t .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. GROUP)
									xPlayer.addAccountMoney('black_money', GROUP)
								end)
							else
								TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
							end
						end
					end
				else
					TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Vous devez être admin pour faire ça.")
				end
			end)
		end)
	end)	
end)

-- Rcon commands
AddEventHandler('rconCommand', function(commandName, args)
	local Source = source
	if commandName == 'setadmin' then
		if ESX == nil then
			TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		end
		if #args ~= 2 then
				RconPrint("Usage : setadmin [user-id] [permission-level]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end
		
		TriggerEvent("es:setPlayerData", tonumber(args[1]), "permission_level", tonumber(args[2]), function(response, success)
			RconPrint(response)
			
			TriggerEvent('es:getPlayerFromId', Source, function(user)
				TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
					LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
				end)
			end)
			if(true)then
				print(args[1] .. " " .. args[2])
			TriggerClientEvent('es:setPlayerDecorator', tonumber(args[1]), 'rank', tonumber(args[2]), true)
			TriggerClientEvent('chatMessage', -1, "CONSOLE", {0, 0, 0}, "Niveau de permission de ^2" .. GetPlayerName(tonumber(args[1])) .. "^0 a été modifié en ^2 " .. args[2] .. "^0.")
			end
		end)

		CancelEvent()
	elseif commandName == 'setgroup' then
		if #args ~= 2 then
				RconPrint("Usage : setgroup [user-id] [group]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

		TriggerEvent("es:getAllGroups", function(groups)

			if(groups[args[2]])then
				TriggerEvent("es:setPlayerData", tonumber(args[1]), "group", args[2], function(response, success)

					TriggerEvent('es:getPlayerFromId', Source, function(user)
						TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
							LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
						end)
					end)
					if(true)then
					TriggerClientEvent('es:setPlayerDecorator', tonumber(args[1]), 'group', tonumber(args[2]), true)
					TriggerClientEvent('chatMessage', -1, "CONSOLE", {0, 0, 0}, "Groupe de ^2^*" .. GetPlayerName(tonumber(args[1])) .. "^r^0 a été modifié en ^2^*" .. args[2] .. "^0.")
					end
				end)
			else
				RconPrint("Ce groupe n'existe pas.\n")
			end
		end)

		CancelEvent()
	elseif commandName == 'giverole' then
		if #args < 2 then
				RconPrint("Usage : giverole [user-id] [role]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

			TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
				table.remove(args, 1)
				user.giveRole(table.concat(args, " "))
				
				TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
					LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. table.concat(args, " "))
				end)
				
				TriggerClientEvent("chatMessage", user.get('source'), "SYSTEM", {255, 0, 0}, "Vous avez donné le rôle : ^2" .. table.concat(args, " "))
			end)

		CancelEvent()
	elseif commandName == 'removerole' then
		if #args < 2 then
				RconPrint("Usage : removerole [user-id] [role]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté\n")
			CancelEvent()
			return
		end

			TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
				table.remove(args, 1)
				user.removeRole(table.concat(args, " "))
				
				TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
					LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. table.concat(args, " "))
				end)
				
				TriggerClientEvent("chatMessage", user.get('source'), "SYSTEM", {255, 0, 0}, "Vous avez supprimé le rôle : ^2" .. table.concat(args, " "))
		end)

		CancelEvent()
	elseif commandName == 'setmoney' then
		if #args ~= 2 then
				RconPrint("Usage : setmoney [user-id] [money]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

		TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
			if(user)then
				if tonumber(args[2]) > -1 then
					TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
						LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
					end)
					
					user.setMoney(tonumber(args[2]))

					RconPrint("Money set")
					TriggerClientEvent('chatMessage', tonumber(args[1]), "CONSOLE", {0, 0, 0}, "Votre argent a été modifié : ^2^*$" .. tonumber(args[2]) .. "^0.")
				else
					TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
				end
			end
		end)

		CancelEvent()
	elseif commandName == 'addmoney' then
		if #args ~= 2 then
				RconPrint("Usage : addmoney [user-id] [money]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

		TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
			if(user)then
				TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
					LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
				end)
				
				user.addMoney(tonumber(args[2]))

				RconPrint("Money add")
				TriggerClientEvent('chatMessage', tonumber(args[1]), "CONSOLE", {0, 0, 0}, "Votre argent a été crédité de : ^2^*$" .. tonumber(args[2]) .. "^0.")
			end
		end)

		CancelEvent()
	elseif commandName == 'setbank' then
		if #args ~= 2 then
				RconPrint("Usage : setbank [user-id] [money]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

		TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
			if(user)then
				TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
					LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
				end)
				
				user.setBankBalance(tonumber(args[2]))

				RconPrint("Blank set")
				TriggerClientEvent('chatMessage', tonumber(args[1]), "CONSOLE", {0, 0, 0}, "Votre argent en banque a été modifié : ^2^*$" .. tonumber(args[2]) .. "^0.")
			end
		end)

		CancelEvent()
	elseif commandName == 'addbank' then
		if #args ~= 2 then
				RconPrint("Usage : addbank [user-id] [money]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

		TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
			if(user)then
				TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
					LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
				end)
				
				user.addBank(tonumber(args[2]))

				RconPrint("Blank add")
				TriggerClientEvent('chatMessage', tonumber(args[1]), "CONSOLE", {0, 0, 0}, "Votre argent en banque a été crédité de : ^2^*$" .. tonumber(args[2]) .. "^0.")
			end
		end)

		CancelEvent()
	elseif commandName == 'setblack' then
		if #args ~= 2 then
				RconPrint("Usage : setblack [user-id] [money]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

		local xPlayer = ESX.GetPlayerFromId(tonumber(args[1]))
		
		TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
			if(user)then
				if tonumber(args[2]) > -1 then
					TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
						LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
					end)
					
					xPlayer.setAccountMoney('black_money', tonumber(args[2]))

					RconPrint("Black money set")
					TriggerClientEvent('chatMessage', tonumber(args[1]), "CONSOLE", {0, 0, 0}, "Votre argent sale a été modifié : ^2^*$" .. tonumber(args[2]) .. "^0.")
				else
					TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Nombre invalide rentré.")
				end
			end
		end)

		CancelEvent()
	elseif commandName == 'addblack' then
		if #args ~= 2 then
				RconPrint("Usage : addblack [user-id] [money]\n")
				CancelEvent()
				return
		end

		if(GetPlayerName(tonumber(args[1])) == nil)then
			RconPrint("Joueur non connecté.\n")
			CancelEvent()
			return
		end

		local xPlayer = ESX.GetPlayerFromId(tonumber(args[1]))
		
		TriggerEvent("es:getPlayerFromId", tonumber(args[1]), function(user)
			if(user)then
				TriggerEvent('es:getPlayerFromId', tonumber(args[1]), function(target)
					LogsAdmin.print('rcon execute ' .. commandName .. ' sur ' .. target.getIdentifier() .. ' valeur : ' .. args[2])
				end)
				
				xPlayer.addAccountMoney('black_money', tonumber(args[2]))

				RconPrint("Black money add")
				TriggerClientEvent('chatMessage', tonumber(args[1]), "CONSOLE", {0, 0, 0}, "Votre sale argent a été crédité de : ^2^*$" .. tonumber(args[2]) .. "^0.")
			end
		end)

		CancelEvent()
	end
end)

-- Default commands
TriggerEvent('es:addCommand', 'admin', function(source, args, user)
	LogsAdmin.print(user.getIdentifier() .. ' execute /admin')
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Niveau : ^*^2 " .. tostring(user.get('permission_level')))
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Groupe : ^*^2 " .. user.getGroup())
end, {help = "Shows what admin level you are and what group you're in"})

-- Default commands
TriggerEvent('es:addCommand', 'report', function(source, args, user)
	LogsAdmin.print(user.getIdentifier() .. ' execute /report valeur : ' .. table.concat(args, " "))
	TriggerClientEvent('chatMessage', source, "REPORT", {255, 0, 0}, " (^2" .. GetPlayerName(source) .." | "..source.."^0) " .. table.concat(args, " ") .. "^0.")

	TriggerEvent("es:getPlayers", function(pl)
		for k,v in pairs(pl) do
			TriggerEvent("es:getPlayerFromId", k, function(user)
				if(user.getPermissions() > 0 and k ~= source)then
					TriggerClientEvent('chatMessage', k, "REPORT", {255, 0, 0}, " (^2" .. GetPlayerName(source) .." | "..source.."^0) " .. table.concat(args, " ") .. "^0.")
				end
			end)
		end
	end)
end, {help = "Report a player or an issue", params = {{name = "report", help = "What you want to report"}}})

-- Append a message
function appendNewPos(msg)
	-- local file = io.open('resources/[essential]/es_admin/positions.txt', "a")
	-- newFile = msg
	-- file:write(newFile)
	-- file:flush()
	-- file:close()
end

-- Do them hashes
function doHashes()
  -- lines = {}
  -- for line in io.lines("resources/[essential]/es_admin/input.txt") do
  	-- lines[#lines + 1] = line
  -- end

  -- return lines
end


RegisterServerEvent('es_admin:givePos')
AddEventHandler('es_admin:givePos', function(str)
	appendNewPos(str)
end)

-- Noclip
TriggerEvent('es:addGroupCommand', 'noclip', "admin", function(source, args, user)
	LogsAdmin.print(user.getIdentifier() .. ' execute /noclip')
	TriggerClientEvent("es_admin:noclip", source)
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Activer ou désactiver noclip"})

-- Kicking
TriggerEvent('es:addGroupCommand', 'kick', "mod", function(source, args, user)
	if args[1] then
		if(GetPlayerName(tonumber(args[1])))then
			local player = tonumber(args[1])

			-- User permission check
			TriggerEvent("es:getPlayerFromId", player, function(target)

				local reason = args
				table.remove(reason, 1)
				if(#reason == 0)then
					reason = "Kicked : Vous avez été expulsé du serveur."
				else
					reason = "Kicked : " .. table.concat(reason, " ")
				end

				LogsAdmin.print(user.getIdentifier() .. ' execute /kick sur ' .. target.getIdentifier() .. ' valeur : ' .. reason)
				TriggerClientEvent('chatMessage', -1, "SYSTEM", {255, 0, 0}, "Joueur ^2" .. GetPlayerName(player) .. "^0 a été expulsé (^2" .. reason .. "^0).")
				DropPlayer(player, reason)
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Expulser un joueur avec une raison spécifique ou sans raison", params = {{name = "userid", help = "ID du joueur"}, {name = "reason", help = "La raison de l'expulsion du joueur"}}})

-- Announcing
TriggerEvent('es:addGroupCommand', 'announce', "mod", function(source, args, user)
	LogsAdmin.print(user.getIdentifier() .. ' execute /announce valeur : ' .. table.concat(args, " "))
	TriggerClientEvent('chatMessage', -1, "ANNOUNCEMENT", {255, 0, 0}, "" .. table.concat(args, " "))
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Passer une annonce sur l'ensemble du serveur", params = {{name = "announcement", help = "Message de l'annonce"}}})

-- Freezing
local frozen = {}
TriggerEvent('es:addGroupCommand', 'freeze', "mod", function(source, args, user)
	if args[1] then
		if(GetPlayerName(tonumber(args[1])))then
			local player = tonumber(args[1])

			-- User permission check
			TriggerEvent("es:getPlayerFromId", player, function(target)

				if(frozen[player])then
					frozen[player] = false
				else
					frozen[player] = true
				end

				TriggerClientEvent('es_admin:freezePlayer', player, frozen[player])

				local state = "libéré"
				if(frozen[player])then
					state = "figé"
				end

				LogsAdmin.print(user.getIdentifier() .. ' execute /freeze sur ' .. target.getIdentifier() .. ' valeur : ' .. state)
				TriggerClientEvent('chatMessage', player, "SYSTEM", {255, 0, 0}, "Vous avez été " .. state .. " par ^2" .. GetPlayerName(source) .. "^0.")
				TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Joueur ^2" .. GetPlayerName(player) .. "^0 a été " .. state .. ".")
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Figer ou libérer un joueur", params = {{name = "userid", help = "ID du joueur"}}})

-- tp_here
local frozen = {}
TriggerEvent('es:addGroupCommand', 'tp_here', "mod", function(source, args, user)
	if args[1] then
		if(GetPlayerName(tonumber(args[1])))then
			local player = tonumber(args[1])

			-- User permission check
			TriggerEvent("es:getPlayerFromId", player, function(target)

				LogsAdmin.print(user.getIdentifier() .. ' execute /tp_here sur ' .. target.getIdentifier() .. ' x=' .. user.getCoords().x .. ' y=' .. user.getCoords().y .. ' z=' .. user.getCoords().z)
				TriggerClientEvent('es_admin:teleportUser', target.get('source'), user.getCoords().x, user.getCoords().y, user.getCoords().z)

				TriggerClientEvent('chatMessage', player, "SYSTEM", {255, 0, 0}, "Vous avez été téléporté par ^2" .. GetPlayerName(source) .. "^0.")
				TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Joueur ^2" .. GetPlayerName(player) .. "^0 a été téléporté.")
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Téléporter un joueur à vous", params = {{name = "userid", help = "ID du joueur"}}})

-- heal
local frozen = {}
TriggerEvent('es:addGroupCommand', 'heal', "mod", function(source, args, user)
	if args[1] then
		if(GetPlayerName(tonumber(args[1])))then
			local player = tonumber(args[1])

			-- User permission check
			TriggerEvent("es:getPlayerFromId", player, function(target)

				LogsAdmin.print(user.getIdentifier() .. ' execute /heal sur ' .. target.getIdentifier())
				TriggerClientEvent('es_admin:heal', player)

				TriggerClientEvent('chatMessage', player, "SYSTEM", {255, 0, 0}, "Vous avez été soigné par ^2" .. GetPlayerName(source) .. "^0.")
				TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Joueur ^2" .. GetPlayerName(player) .. "^0 a été soigné.")
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Soigner un joueur", params = {{name = "userid", help = "ID du joueur"}}})

-- Goto
TriggerEvent('es:addGroupCommand', 'goto', "mod", function(source, args, user)
	if args[1] then
		if(GetPlayerName(tonumber(args[1])))then
			local player = tonumber(args[1])

			-- User permission check
			TriggerEvent("es:getPlayerFromId", player, function(target)
				if(target)then

					LogsAdmin.print(user.getIdentifier() .. ' execute /goto sur ' .. target.getIdentifier() .. ' x=' .. target.getCoords().x .. ' y=' .. target.getCoords().y .. ' z=' .. target.getCoords().z)
					TriggerClientEvent('es_admin:teleportUser', source, target.getCoords().x, target.getCoords().y, target.getCoords().z)

					TriggerClientEvent('chatMessage', player, "SYSTEM", {255, 0, 0}, "Vous avez été téléporté par ^2" .. GetPlayerName(source))
					TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Téléporté au joueur ^2" .. GetPlayerName(player) .. "^0.")
				end
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Teleport to a user", params = {{name = "userid", help = "ID du joueur"}}})

-- Kill yourself
-- TriggerEvent('es:addCommand', 'die', function(source, args, user)
	-- TriggerClientEvent('es_admin:kill', source)
	-- TriggerClientEvent('chatMessage', source, "", {0,0,0}, "^1^*Vous vous êtes suicidé.")
-- end, {help = "Suicide"})

-- Killing
TriggerEvent('es:addGroupCommand', 'kill', "admin", function(source, args, user)
	if args[1] then
		if(GetPlayerName(tonumber(args[1])))then
			local player = tonumber(args[1])

			-- User permission check
			TriggerEvent("es:getPlayerFromId", player, function(target)
				LogsAdmin.print(user.getIdentifier() .. ' execute /kill sur ' .. target.getIdentifier())
				TriggerClientEvent('es_admin:kill', player)

				TriggerClientEvent('chatMessage', player, "SYSTEM", {255, 0, 0}, "Vous avez été tué par ^2" .. GetPlayerName(source) .. "^0.")
				TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Joueur ^2" .. GetPlayerName(player) .. "^0 a été tué.")
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
		end
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect !")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Tuer un joueur", params = {{name = "userid", help = "ID du joueur"}}})

-- Position
TriggerEvent('es:addGroupCommand', 'pos', "owner", function(source, args, user)
	LogsAdmin.print(user.getIdentifier() .. ' execute /pos')
	TriggerClientEvent('es_admin:givePosition', source)
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Save position to file"})

TriggerEvent('es:addGroupCommand', 'setskin', "owner", function(source, args, user)
	if args[1] then
		LogsAdmin.print(user.getIdentifier() .. ' execute /setskin valeur : ' .. args[1])
		TriggerClientEvent('es_admin:setskin', source, args[1])
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Skin manquant !")
	end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante !")
end, {help = "Set skin"})

function stringsplit(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t={} ; i=1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end

loadBans()