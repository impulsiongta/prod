ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
API = nil
TriggerEvent("API:getAPI", function(obj) API = obj end)

RegisterServerEvent("farm:getItemCount")
AddEventHandler("farm:getItemCount", function(item)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	local itemArray = xPlayer.getInventoryItem(item)
	TriggerClientEvent("farm:sendItem",_source, itemArray.count)
end)

RegisterServerEvent("farm:getItemCount2")
AddEventHandler("farm:getItemCount2", function(item)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	local itemArray = xPlayer.getInventoryItem(item)
	TriggerClientEvent("farm:sendItem2",_source, itemArray.count)
end)

RegisterServerEvent("farm:addItem")
AddEventHandler("farm:addItem", function(item)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.addInventoryItem(item,1)
end)

RegisterServerEvent("farm:removeItem")
AddEventHandler("farm:removeItem", function(item)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem(item, 1)
end)

RegisterServerEvent("farm:addMoney")
AddEventHandler("farm:addMoney", function(price)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	xPlayer.addMoney(price)
end)


RegisterServerEvent("farm:addSocietyMoney")
AddEventHandler("farm:addSocietyMoney", function(price, society)

	TriggerServerEvent('esx_society:depositMoney', society, price)

end)



RegisterServerEvent("jobsAPI:getPlayersByJob")
AddEventHandler("jobsAPI:getPlayersByJob", function(job)
	local _source = source
	
	players = API.getPlayersByJob(job)

	TriggerClientEvent("API:setPlayersResult", _source, players)
end)




RegisterServerEvent("jobsAPI:setJobOfId")
AddEventHandler("jobsAPI:setJobOfId", function(id, job, grade, t)
	local _source = id

	if(grade == -1) then
		if(t == 1) then
			API.getPlayersInfos(_source, function(result)
				API.setJob(_source,job,result.job.grade+1)
			end)
				
		else
			API.getPlayersInfos(_source, function(result)
				API.setJob(_source,job,result.job.grade-1)
			end)
		end
	else
		API.setJob(_source,job,grade)
	end
	
end)








--[[--------------------------------------
				COFFRE PART
--------------------------------------]]--

local coffreList = {}


RegisterServerEvent("farm:getInventory")
AddEventHandler("farm:getInventory", function(x,y,z)
	local _source = source
	coffreFunc = API.getCoffre(x,y,z)
	local items = coffreFunc.getItems()
	TriggerClientEvent("farm:getCoffreInventory",_source,items)

end)


RegisterServerEvent("farm:recupItem")
AddEventHandler("farm:recupItem", function(item, quantity,x,y,z)
	local _source = source
	coffreFunc = API.getCoffre(x,y,z)
	coffreFunc.recupItem(item,quantity, _source)
end)



RegisterServerEvent("farm:addcItem")
AddEventHandler("farm:addcItem", function(item, quantity,x,y,z)
	local _source = source
	local id = x+y+z
	coffreFunc = API.getCoffre(x,y,z)
	coffreFunc.addItem(item,quantity, _source)
end)


RegisterServerEvent("farm:getPlayerInventory")
AddEventHandler("farm:getPlayerInventory", function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	local inv = xPlayer.getInventory()

	local inv_sended = {}
	for i,k in pairs(inv) do
		if(k.count > 0) and k.canRemove then
			inv_sended[k.name] = {it = k.label, q = k.count}
		end
	end

	TriggerClientEvent("farm:sendPlayerInventaire", _source, inv_sended)

end)


local alreadyCreated = {}
RegisterServerEvent("farm:createCoffre")
AddEventHandler("farm:createCoffre", function(x,y,z,name)
	local isIn = false

	for _,k in pairs(alreadyCreated) do
		if(k==name) then
			isIn = true
			break;
		end
	end

	if(not isIn) then
		API.createCoffre(name,x,y,z,name, 2)
		table.insert(alreadyCreated, name)
	end
end)



RegisterServerEvent("API:getItemFromSociety")
AddEventHandler("API:getItemFromSociety", function(item, quantity)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	xPlayer.addInventoryItem(item,quantity)
end)


RegisterServerEvent("jobs:getPlayerInfos")
AddEventHandler("jobs:getPlayerInfos", function()
	local _source = source
	API.getPlayersInfos(_source, function(result)
		TriggerClientEvent("API:setResult", _source, result)
	end)
end)

RegisterServerEvent("Farm:addAccountMoney")
AddEventHandler("Farm:addAccountMoney", function(price, name)

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_'..name, function(account)

		account.addMoney(price)

	end)

end)

RegisterServerEvent("Farm:removeAccountMoney")
AddEventHandler("Farm:removeAccountMoney", function(price, name)

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_'..name, function(account)

		account.removeMoney(price)

	end)

end)