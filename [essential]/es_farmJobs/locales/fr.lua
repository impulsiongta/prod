Locales['fr'] = {

	['remove_comp_money'] = 'Retirer argent société',
	['deposit_money'] = 'Déposer argent',
	['launder_money'] = 'Blanchir argent',
	['withdraw_amount'] = 'Montant du retrait',
	['amount_invalid'] = 'Montant ~r~~h~invalide',
	['deposit_amount'] = 'Montant du dépôt',
	['launder_amount'] = 'Montant à blanchir',
	['press_to_open'] = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu',

}
