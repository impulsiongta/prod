ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if ESX == nil then
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
end

Farm = {}

local stop = false
local blipRecolte = -1
local blipTraitement = -1
local blipTraitement2 = -1
local blipTraitementMultiple = -1
local blipexchangeItem = -1
local blipVente = -1
local blipVenteEntreprise = -1
local blipAchatEntreprise = -1
local count = 0
local count2 = 0
local listcouleur = {
["blanc"]		= { ['r'] = 255, ['g'] = 255, ['b'] = 255},
["rouge"]		= { ['r'] = 255, ['g'] = 51, ['b'] = 51},
["bleu"]		= { ['r'] = 0, ['g'] = 128, ['b'] = 255},
["marron"]		= { ['r'] = 255, ['g'] = 128, ['b'] = 255},
["orange"]		= { ['r'] = 255, ['g'] = 128, ['b'] = 0},
["vert"]		= { ['r'] = 51, ['g'] = 255, ['b'] = 51},
["violet"]		= { ['r'] = 178, ['g'] = 102, ['b'] = 255},
["jaune"]		= { ['r'] = 255, ['g'] = 255, ['b'] = 51},
["LaitChoco"]	= { ['r'] = 99,  ['g'] = 33,  ['b'] = 5},
["Lait"]		= { ['r'] = 255, ['g'] = 255, ['b'] = 255},
["Champagne"]	= { ['r'] = 255, ['g'] = 187, ['b'] = 80},
["VinRouge"]	= { ['r'] = 145, ['g'] = 30,  ['b'] = 45},
["JusRaisin"]	= { ['r'] = 145, ['g'] = 48,  ['b'] = 106},
["JusOrange"]	= { ['r'] = 255, ['g'] = 146, ['b'] = 0},
["Punch"]		= { ['r'] = 255, ['g'] = 94,  ['b'] = 24},
["Pain"]		= { ['r'] = 207, ['g'] = 119, ['b'] = 0},
["PainComplet"] = { ['r'] = 140, ['g'] = 80,  ['b'] = 0},
["Donuts"]		= { ['r'] = 232, ['g'] = 74,  ['b'] = 142},
["Croissant"]	= { ['r'] = 255, ['g'] = 255, ['b'] = 51},
["FruitsMer"]	= { ['r'] = 224, ['g'] = 63,  ['b'] = 32},
["Sushi"]		= { ['r'] = 224, ['g'] = 99,  ['b'] = 114},
["Gambas"]		= { ['r'] = 255, ['g'] = 93,  ['b'] = 43},
["Maki"]		= { ['r'] = 23,  ['g'] = 8,   ['b'] = 4},
["Biere"]		= { ['r'] = 189, ['g'] = 99,  ['b'] = 36},
["Whisky"]		= { ['r'] = 92,  ['g'] = 39,  ['b'] = 13},
["Alcool"]		= { ['r'] = 196, ['g'] = 191, ['b'] = 180},
["MatPrem"]		= { ['r'] = 95,  ['g'] = 138, ['b'] = 196},
["PlatRepas"]	= { ['r'] = 57,  ['g'] = 196, ['b'] = 149},
["Vodka"]		= { ['r'] = 255,  ['g'] = 241, ['b'] = 235},
["Mojito"]		= { ['r'] = 62,  ['g'] = 255, ['b'] = 101},
["BiereNoAlc"]	= { ['r'] = 255,  ['g'] = 233, ['b'] = 146},
["Cookies"]		= { ['r'] = 178,  ['g'] = 82, ['b'] = 32},
["CacaoChaud"]	= { ['r'] = 87,  ['g'] = 40, ['b'] = 15},
["Viande"]		= { ['r'] = 255,  ['g'] = 186, ['b'] = 189},
["Tacos"]		= { ['r'] = 255,  ['g'] = 242, ['b'] = 189},
["Pizza"]		= { ['r'] = 255,  ['g'] = 175, ['b'] = 94},
["FiletPoisson"]= { ['r'] = 255,  ['g'] = 89, ['b'] = 85},
["SacArgent"]	= { ['r'] = 0,  ['g'] = 255, ['b'] = 0},
["Blé"]	= { ['r'] = 255,  ['g'] = 158, ['b'] = 13}
}

Farm.createRecolte = function(number, x,y,z, item, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipRecolte  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipRecolte, 16 + number)
		SetBlipColour(blipRecolte, blipcolor)
		SetBlipDisplay(blipRecolte, 4)
	end

	while not isInArea and not stop do
		Citizen.Wait(0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnRecolte = false

	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnRecolte) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~récolter.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter de récolter.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnRecolte = not isOnRecolte
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item)
	Wait(50)
	while isInArea and not stop do
		Citizen.Wait(waiting)

		if(isOnRecolte) then
			if(count < 100) then
				count = count+1
				TriggerServerEvent("farm:addItem", item)
			else
				isInArea = false
			end
		end
	end
	
	if(blipRecolte ~= nil) then
		SetBlipDisplay(blipRecolte, 0)
		blipRecolte = - 1
		RemoveBlip(blipRecolte)
	end

	if(not stop) then
		Farm.createRecolte(number, x,y,z, item, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	end
end)
end


Farm.createTraitement = function(number, x,y,z, item1, item2, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipTraitement  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipTraitement, 16 + number)
		SetBlipColour(blipTraitement, blipcolor)
		SetBlipDisplay(blipTraitement, 4)
	end

	while not isInArea and not stop do
		Citizen.Wait(0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnTraite = false

	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnTraite) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~traiter.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter de traiter.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnTraite = not isOnTraite
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item1)
	Wait(50)
	while isInArea and not stopRecolte do
		Citizen.Wait(waiting)
		if(isOnTraite) then
			if(count > 0) then
				count = count-1
				TriggerServerEvent("farm:removeItem", item1)
				TriggerServerEvent("farm:addItem", item2)
			else
				isInArea = false
			end
		end
	end
	
	if(blipTraitement ~= nil) then 
		SetBlipDisplay(blipTraitement, 0)
		blipTraitement = -1
	end
	
	if(not stop) then
		Farm.createTraitement(number,x,y,z, item1, item2, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	end
end)
end

Farm.createTraitement2 = function(number, x,y,z, item1, item2, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipTraitement2  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipTraitement2, 16 + number)
		SetBlipColour(blipTraitement2, blipcolor)
		SetBlipDisplay(blipTraitement2, 4)
	end

	while not isInArea and not stop do
		Citizen.Wait(0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnTraite = false

	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnTraite) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~traiter.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter de traiter.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnTraite = not isOnTraite
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item1)
	Wait(50)
	while isInArea and not stopRecolte do
		Citizen.Wait(waiting)
		if(isOnTraite) then
			if(count > 0) then
				count = count-1
				TriggerServerEvent("farm:removeItem", item1)
				TriggerServerEvent("farm:addItem", item2)
			else
				isInArea = false
			end
		end
	end
	
	if(blipTraitement2 ~= nil) then 
		SetBlipDisplay(blipTraitement2, 0)
		blipTraitement2 = -1
	end

	if(not stop) then
		Farm.createTraitement2(number,x,y,z, item1, item2, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	end
end)
end

Farm.createTraitementMultiple = function(number, x,y,z, item1, qtty1, item2, qtty2, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipTraitementMultiple  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipTraitementMultiple, 16 + number)
		SetBlipColour(blipTraitementMultiple, blipcolor)
		SetBlipDisplay(blipTraitementMultiple, 4)
	end

	while not isInArea and not stop do
		Citizen.Wait(0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnTraite = false

	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnTraite) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~traiter.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter de traiter.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnTraite = not isOnTraite
			end
		end
	end)
	
	TriggerServerEvent("farm:getItemCount", item1)
	Wait(100)
	TriggerServerEvent("farm:getItemCount2", item2)
	Wait(100)
	while isInArea and not stopRecolte do
		
		Citizen.Wait(waiting)
		if(isOnTraite) then
			if(count > 0) then
				count = count-1
				Citizen.Wait(50)
				if (count2 <= 100-qtty2) then
					count2 = count2+qtty2
					for i=1,qtty1 do 
						TriggerServerEvent("farm:removeItem", item1)
					end
					for i=1,qtty2 do 
						TriggerServerEvent("farm:addItem", item2)
					end
				else
					isInArea = false
				end
			else
				isInArea = false
			end
		end
	end

	if(blipTraitementMultiple ~= nil) then 
		SetBlipDisplay(blipTraitementMultiple, 0)
		blipTraitementMultiple = -1
	end

	if(not stop) then
		Farm.createTraitementMultiple(number,x,y,z, item1, qtty1, item2, qtty2, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	else
		SetBlipDisplay(blipTraitementMultiple, 0)
		blipTraitementMultiple = -1
	end
end)
end

Farm.exchangeItem = function(number, x,y,z, item1, item2, qttytoexchange, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipexchangeItem  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipexchangeItem, 16 + number)
		SetBlipColour(blipexchangeItem, blipcolor)
		SetBlipDisplay(blipexchangeItem, 4)
	end

	while not isInArea and not stop do
		Citizen.Wait(0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0, sizemarkeurx, sizemarkeury, sizemarkeurz,r,g,b,200,0,0,0,0)
		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnExchange = false

	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0, sizemarkeurx, sizemarkeury, sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnExchange) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~échanger.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter d'échanger.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnExchange = not isOnExchange
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item1)
	Wait(50)
	while isInArea and not stopRecolte do
		Citizen.Wait(waiting)
		if(isOnExchange) then
			if(count >= qttytoexchange) then
				count = count-qttytoexchange				
				for i=1,qttytoexchange do
					TriggerServerEvent("farm:removeItem", item1)
				end
				if string.find(tostring(item2), "WEAPON") then
					GiveWeaponToPed(GetPlayerPed(-1), GetHashKey(item2), 1, false)
				else
					TriggerServerEvent("farm:addItem", item2)
				end				
			else
				isInArea = false
			end
		end
	end

	if(blipexchangeItem ~= nil) then
	SetBlipDisplay(blipexchangeItem, 0)
	blipexchangeItem = -1
	end

	if(not stop) then
		Farm.exchangeItem(number,x,y,z, item1, item2, qttytoexchange, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	end
end)
end

Farm.createVente = function(number, x,y,z, item1, price, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		--print(json.encode(listcouleur))
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipVente  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipVente, 16 + number)
		SetBlipColour(blipVente, blipcolor)
		SetBlipDisplay(blipVente, 4)
	end


	while not isInArea and not stop do
		Citizen.Wait(0)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnVente = false
	
	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnVente) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~vendre.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter de vendre.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnVente = not isOnVente
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item1)
	Wait(50)
	while isInArea and not stop do
		Citizen.Wait(waiting)
		if(isOnVente) then
			if(count > 0) then
				count = count-1
				TriggerServerEvent("farm:addMoney", price)
				TriggerServerEvent("farm:removeItem", item1)
			else
				isInArea = false
			end
		end
	end

	if(blipVente ~= nil) then
	SetBlipDisplay(blipVente, 0)
	blipVente = -1
	end

	if(not stop) then
		Farm.createVente(number,x,y,z, item1, price, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	end
end)
end

Farm.createVenteEntreprise = function(number, x,y,z, item1, price, society, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		--print(json.encode(listcouleur))
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipVenteEntreprise  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipVenteEntreprise, 16 + number)
		SetBlipColour(blipVenteEntreprise, blipcolor)
		SetBlipDisplay(blipVenteEntreprise, 4)
	end


	while not isInArea and not stop do
		Citizen.Wait(0)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnVente = false
	
	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnVente) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~vendre.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter de vendre.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnVente = not isOnVente
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item1)
	Wait(50)
	while isInArea and not stop do
		Citizen.Wait(waiting)
		if(isOnVente) then
			if(count > 0) then
				count = count-1
				TriggerServerEvent("Farm:addAccountMoney", price, society)
				TriggerServerEvent("farm:removeItem", item1)
			else
				isInArea = false
			end
		end
	end

	if(blipVenteEntreprise ~= nil) then
	SetBlipDisplay(blipVenteEntreprise, 0)
	blipVenteEntreprise = -1
	end

	if(not stop) then
		Farm.createVenteEntreprise(number, x,y,z, item1, price, society, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	end
end)
end

Farm.createAchatEntreprise = function(number, x,y,z, item1, price, society, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
Citizen.CreateThread(function()
	stop = false
	local isInArea = false
	local r,g,b = 0


	if listcouleur[couleur] ~= nil then
		--print(json.encode(listcouleur))
		r = listcouleur[couleur].r
		g = listcouleur[couleur].g
		b = listcouleur[couleur].b
	end
	
	if(showed == true) then
		blipAchatEntreprise  = AddBlipForCoord(x,y,z)
		SetBlipSprite(blipAchatEntreprise, 16 + number)
		SetBlipColour(blipAchatEntreprise, blipcolor)
		SetBlipDisplay(blipAchatEntreprise, 4)
	end

	while not isInArea and not stop do
		Citizen.Wait(0)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

		if(distance < sizemarkeurx) then
			isInArea = true
		end
	end
	local isOnAchat = false
	
	Citizen.CreateThread(function()
		while isInArea  and not stop do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,sizemarkeurx,sizemarkeury,sizemarkeurz,r,g,b,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > sizemarkeurx) then
				isInArea = false
			end

			if(not isOnAchat) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~acheter.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~arrêter d'acheter.")
			end

			if(IsControlJustPressed(1,  38)) then
				isOnAchat = not isOnAchat
			end
		end
	end)

	TriggerServerEvent("farm:getItemCount", item1)
	Wait(50)
	while isInArea and not stop do
		TriggerServerEvent("farm:getItemCount", item1)
		Citizen.Wait(waiting)
		if(isOnAchat) then
			if(count <= 100) then
				print('count : ' .. count)
				count = count-1
				TriggerServerEvent("Farm:removeAccountMoney", price, society)
				TriggerServerEvent("farm:addItem", item1)
			else
				isInArea = false
			end
		end
	end

	if(blipAchatEntreprise ~= nil) then
	SetBlipDisplay(blipAchatEntreprise, 0)
	blipAchatEntreprise = -1
	end

	if(not stop) then
		Farm.createAchatEntreprise(number, x,y,z, item1, price, society, blipcolor, showed, sizemarkeurx, sizemarkeury, sizemarkeurz, waiting, couleur)
	end
end)
end


--[[------------------------------------------------------------------------------------------

										PATRON PC PART
------------------------------------------------------------------------------------------]]--

local countMin = 0
local countMax = 23
local arrayCount = 0

local playersList = {}

Farm.createPatronPC = function(x,y,z, job)
Citizen.CreateThread(function()

	local inArea = false
	while not inArea do
		Citizen.Wait(0)

		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), x,y,z, true)

		if(distance < 1) then
			inArea = true
		end
	end
	local inPC = false

	Citizen.CreateThread(function()

		while inArea do
			Citizen.Wait(0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > 1) then
				inArea = false
			end

			if(not inPC) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~allumer l'ordinateur.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~éteindre l'ordinateur.")
			end

			if(IsControlJustPressed(1,  38)) then
				inPC = not inPC
			end
		end

	end)

	local currentMenu = 0
	while inArea do
		Citizen.Wait(0)
		if(inPC) then
			if(currentMenu == 0) then
				TriggerEvent("GUIJobsAPI:Title", "Gérer les effectifs")

				TriggerEvent("GUIJobsAPI:Option", "Recruter quelqu'un", function(cb)
					if(cb) then
						currentMenu = 1
						TriggerServerEvent("jobsAPI:getPlayersByJob", "unemployed")
						countMin = 0
						countMax = 23
					end
				end)

				TriggerEvent("GUIJobsAPI:Option", "Virer quelqu'un", function(cb)
					if(cb) then
						currentMenu = 2
						TriggerServerEvent("jobsAPI:getPlayersByJob", job)
						countMin = 0
						countMax = 23
					end
				end)

				TriggerEvent("GUIJobsAPI:Option", "Augmenter de grade quelqu'un", function(cb)
					if(cb) then
						currentMenu = 3
						TriggerServerEvent("jobsAPI:getPlayersByJob", job)
						countMin = 0
						countMax = 23
					end
				end)

				TriggerEvent("GUIJobsAPI:Option", "Descendre d'un grade quelqu'un", function(cb)
					if(cb) then
						currentMenu = 4
						TriggerServerEvent("jobsAPI:getPlayersByJob", job)
						countMin = 0
						countMax = 23
					end
				end)
			elseif(currentMenu == 1) then

				TriggerEvent("GUIJobsAPI:Option", "< Retour", function(cb)
					if(cb) then
						currentMenu = 0
					end
				end)

					for id, name in pairs(playersList) do
						if(type(name) ~= "table") then
							TriggerEvent("GUIJobsAPI:Option", name, function(cb)
								if(cb) then
									TriggerServerEvent("jobsAPI:setJobOfId", id, job, 0)
								end
							end)
						end
					end


			elseif(currentMenu == 2) then

				TriggerEvent("GUIJobsAPI:Option", "< Retour", function(cb)
					if(cb) then
						currentMenu = 0
					end
				end)

					
					arrayCount = #playersList
					for id, name in pairs(playersList) do
						if(type(name) ~= "table") then
							TriggerEvent("GUIJobsAPI:Option", name, function(cb)
								if(cb) then
									TriggerServerEvent("jobsAPI:setJobOfId", id, "unemployed", 0)
								end
							end)
						end
					end


			elseif(currentMenu == 3) then

				TriggerEvent("GUIJobsAPI:Option", "< Retour", function(cb)
					if(cb) then
						currentMenu = 0
					end
				end)

					arrayCount = #playersList
					for id, name in pairs(playersList) do
						if(type(name) ~= "table") then
							TriggerEvent("GUIJobsAPI:Option", name, function(cb)
								if(cb) then
									TriggerServerEvent("jobsAPI:setJobOfId", id, job, -1,1)
								end
							end)
						end
					end
			
			else

				TriggerEvent("GUIJobsAPI:Option", "< Retour", function(cb)
					if(cb) then
						currentMenu = 0
					end
				end)

					arrayCount = #playersList
					for id, name in pairs(playersList) do
						if(type(name) ~= "table") then
							TriggerEvent("GUIJobsAPI:Option", name, function(cb)
								if(cb) then
									TriggerServerEvent("jobsAPI:setJobOfId", id, job, -1,2)
								end
							end)
						end
					end
			end
			TriggerEvent("GUIJobsAPI:Update")
		end
	end
	Farm.createPatronPC(x,y,z, job)
end)
end





local inventory = -1
local toGet = 1

Farm.createCoffre = function(x,y,z, name)
TriggerServerEvent("farm:createCoffre",x,y,z,name)
Citizen.CreateThread(function()
	

	local inArea = false
	while not inArea do
		Citizen.Wait(0)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), x,y,z, true)

		if(distance < 1) then
			inArea = true
		end
	end
	local inCoffre = false

	Citizen.CreateThread(function()

		while inArea do
			Citizen.Wait(0)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)

			if(distance > 1) then
				inArea = false
			end

			if(not inCoffre) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le coffre.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~fermer le coffre.")
			end

			if(IsControlJustPressed(1,  38)) then
				TriggerServerEvent("farm:getInventory", x,y,z)
				inCoffre = not inCoffre
			end
		end

	end)

	local menuID = 0
	countMin = 0
	countMax = 23
	while inArea do
		Citizen.Wait(0)
		if(inCoffre) then
			if(menuID == 0) then
				TriggerEvent("GUIJobsAPI:Title", "Contenu du coffre")

				TriggerEvent("GUIJobsAPI:Option", "Déposer un item", function(cb)
					if(cb) then
						TriggerServerEvent("farm:getPlayerInventory", x,y,z)
						toGet = 1
						menuID = 1
					end
				end)

				TriggerEvent("GUIJobsAPI:Int", "Quantité : ", toGet, 1, 100, function(cb)
					toGet = cb
				end)

				if(toGet~=-1 and inventory ~= -1) then
					for ind,k in pairs(inventory) do
						if(type(k) == "number") then
							if( k > 0) then
								TriggerEvent("GUIJobsAPI:Option", ind.." ("..k..")", function(cb)
									if(cb) then
										if(k >= toGet) then
											TriggerServerEvent("farm:recupItem", ind,toGet,x,y,z)
											inCoffre = false
										end
									end
								end)
							end
						end
					end
				end
			else
				TriggerEvent("GUIJobsAPI:Option", "< Retour", function(cb)
					if(cb) then
						TriggerServerEvent("farm:getInventory", x,y,z)
						toGet = 0
						menuID = 0
					end
				end)

				TriggerEvent("GUIJobsAPI:Int", "Quantité : ", toGet, 1, 100, function(cb)
					toGet = cb
				end)

				if(inventory ~= -1) then
					for i,k in pairs(inventory) do
						if(type(k) ~= "number") then
							TriggerEvent("GUIJobsAPI:Option", k.it.." ("..k.q..")", function(cb)
								if(cb) then
									if(toGet <= k.q) then
										TriggerServerEvent("farm:addcItem", i, toGet,x,y,z)
										TriggerServerEvent("farm:getInventory", x,y,z)
										toGet = 1
										menuID = 0
									end
								end
							end)
						end
					end
				end
			end

			TriggerEvent("GUIJobsAPI:Update")
		end
	end
	Farm.createCoffre(x,y,z, name)
end)

end

RegisterNetEvent("farm:sendPlayerInventaire")
AddEventHandler("farm:sendPlayerInventaire", function(inv)
	inventory = inv
end)


RegisterNetEvent("farm:getCoffreInventory")
AddEventHandler("farm:getCoffreInventory", function(inv)
	inventory = inv
end)





Farm.stop = function()
	stop = true
end

function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


RegisterNetEvent("farm:sendItem")
AddEventHandler("farm:sendItem", function(c)
	count = c
end)

RegisterNetEvent("farm:sendItem2")
AddEventHandler("farm:sendItem2", function(c)
	count2 = c
end)

RegisterNetEvent("API:getJobAPI")
AddEventHandler("API:getJobAPI", function(obj)
	obj(Farm)
end)



RegisterNetEvent("API:setPlayersResult")
AddEventHandler("API:setPlayersResult",function(list)
	playersList = {}
	playersList = list
end)



--[[---------------------------------------------------------
					SOCIETY MENUS
---------------------------------------------------------]]--

local sharedAccountMoney = 0


Farm.createSocietyMenu = function(x,y,z, name, menuName)
	RegisterNetEvent('esx_addonaccount:setMoney')
	AddEventHandler('esx_addonaccount:setMoney', function(accountName, money)
		if accountName == 'society_' .. name then
			sharedAccountMoney = money
		end
	end)
	
	if ESX == nil then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end
	
	Citizen.CreateThread(function()
		local menuShowed = false

		TriggerServerEvent('esx_society:updateShowMoney', name)

		while true do
			Citizen.Wait(0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)
			if(distance > 1 and menuShowed) then
				ESX.UI.Menu.CloseAll()
				menuShowed = false
			end
	
			if(distance<1) then
				if(menuShowed) then
					Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~fermer.")
				else
					Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~accèder aux fonds de l'entreprise.")
				end
	
				if(IsControlJustPressed(1, 38)) then
					menuShowed = not menuShowed
	
					if(menuShowed) then
						renderMenu(name, menuName .. " $" .. tostring(sharedAccountMoney))
					else
						ESX.UI.Menu.CloseAll()
					end
				end
			end
		end
	end)
end


function renderMenu(name, menuName)
	local _name = name
	local elements = {}
		
	if ESX == nil then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end
	

  	table.insert(elements, {label = 'Retirer argent société', value = 'withdraw_society_money'})
  	table.insert(elements, {label = 'Déposer argent',        value = 'deposit_money'})
  	--table.insert(elements, {label = 'Blanchir argent',        value = 'wash_money'})

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'realestateagent',
		{
			title    = menuName,
			align = 'top-left',
			elements = elements
		},
		function(data, menu)

			if data.current.value == 'withdraw_society_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'withdraw_society_money_amount',
					{
						title = 'Montant du retrait'
					},
					function(data, menu)
						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:withdrawMoney', _name, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

			if data.current.value == 'deposit_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'deposit_money_amount',
					{
						title = 'montant du dépôt'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:depositMoney', _name, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

			if data.current.value == 'wash_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'wash_money_amount',
					{
						title = 'Blanchir argent'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:washMoney', _name, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)

			menu.close()
		end
	)
end

--------------COMPTE OFFSHORE ENTREPRISE-------------------

Farm.createSocietyMenu2 = function(x,y,z, name, menuName)
	RegisterNetEvent('esx_addonaccount:setMoney')
	AddEventHandler('esx_addonaccount:setMoney', function(accountName, money)
		if accountName == 'society_' .. name then
			sharedAccountMoney = money
		end
	end)
		
	if ESX == nil then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end
	
	Citizen.CreateThread(function()
		local menuShowed = false

		TriggerServerEvent('esx_society:updateShowMoney', name)

		while true do
			Citizen.Wait(0)
			local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
			DrawMarker(1,x,y,z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)
			if(distance > 1 and menuShowed) then
				ESX.UI.Menu.CloseAll()
				menuShowed = false
			end
	
			if(distance<1) then
				if(menuShowed) then
					Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~fermer")
				else
					Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~accèder au compte offshore de l'entreprise")
				end

				if(IsControlJustPressed(1, 38)) then
					menuShowed = not menuShowed
	
					if(menuShowed) then
						renderMenu2(name, menuName .. " $" .. tostring(sharedAccountMoney))
					else
						ESX.UI.Menu.CloseAll()
					end
				end
			end
		end
	end)
end

function renderMenu2(name, menuName)
	local _name = name
	local elements = {}
	
	
	if ESX == nil then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end
	
  	table.insert(elements, {label = 'retirer argent société', value = 'withdraw_society_money'})
  	table.insert(elements, {label = 'déposer argent',        value = 'deposit_money'})
  	--table.insert(elements, {label = 'blanchir argent',        value = 'wash_money'})

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'realestateagent',
		{
			title    = menuName,
			align = 'top-left',
			elements = elements
		},
		function(data, menu)

			if data.current.value == 'withdraw_society_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'withdraw_society_money_amount',
					{
						title = 'Montant du retrait'
					},
					function(data, menu)
						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:withdrawMoney', _name, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)
			end

			if data.current.value == 'deposit_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'deposit_money_amount',
					{
						title = 'Montant du dépôt'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:depositMoney', _name, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

			if data.current.value == 'wash_money' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'wash_money_amount',
					{
						title = 'blanchir argent'
					},
					function(data, menu)

						local amount = tonumber(data.value)

						if amount == nil then
							ESX.ShowNotification('Montant ~r~~h~invalide')
						else
							menu.close()
							TriggerServerEvent('esx_society:washMoney', _name, amount)
						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

		end,
		function(data, menu)

			menu.close()
		end
	)
end

Farm.createTakeItem = function(x,y,z,menuName, itemsArray, itemsToGuiArray)
Citizen.CreateThread(function()
	local menuShowed = false
		
	if ESX == nil then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end
	
	while true do
		Citizen.Wait(0)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),x,y,z, true)
		DrawMarker(1,x,y,z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)
		if(distance > 1 and menuShowed) then
			ESX.UI.Menu.CloseAll()
			menuShowed = false
		end

		if(distance<1) then
			if(menuShowed) then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~fermer.")
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~accèder aux marchandises.")
			end

			if(IsControlJustPressed(1, 38)) then
				menuShowed = not menuShowed

				if(menuShowed) then
					renderTakeItemMenu(menuName, itemsArray, itemsToGuiArray)
				else
					ESX.UI.Menu.CloseAll()
				end
			end
		end
	end
end)
end

function renderTakeItemMenu(menuName, itemsArray, itemsToGuiArray)
	local _name = name
	local elements = {}
		
	if ESX == nil then
	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
	end
	
	for i,k in pairs(itemsToGuiArray) do
  		table.insert(elements, {label = k, value = itemsArray[i]})
  	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'renderTakeItemMenu',
		{
			title    = menuName,
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			for _,k in pairs( itemsArray)do
				if data.current.value == k then

					ESX.UI.Menu.Open(
						'dialog', GetCurrentResourceName(), 'renderTakeItemMenu',
						{
							title = 'Montant'
						},
						function(data, menu)

							local amount = tonumber(data.value)

							if amount == nil then
								ESX.ShowNotification('Montant ~r~~h~invalide')
							else
								menu.close()
								TriggerServerEvent('API:getItemFromSociety', k, amount)
							end

						end,
						function(data, menu)
							menu.close()
						end
					)

				end
			end
		end,
		function(data, menu)

			menu.close()
		end
	)
end

RegisterNetEvent("GUIJobsAPI:requestGo")
AddEventHandler("GUIJobsAPI:requestGo", function(current, cb)
	local changed = false
	if(current > 23 and countMax+1<=arrayCount) then
		countMin = countMin+1
		countMax = countMax+1
		changed = true
	end

	cb(changed)
end)

Citizen.CreateThread(function()

	while true do
		Citizen.Wait(0)
		TriggerServerEvent("jobs:getPlayerInfos")
		Citizen.Wait(3000)
	end

end)