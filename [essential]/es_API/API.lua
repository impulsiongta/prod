ESX = nil 
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

APIFunction = {}

RegisterServerEvent("API:getAPI")
AddEventHandler("API:getAPI", function(cb)
	cb(APIFunction)
end)


coffres = {}

APIFunction.getItemCount = function(source, itemName)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	return xPlayer.getInventoryItem(itemName).count

end




APIFunction.addItem = function(source, itemName, quantity)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.addInventoryItem(itemName, quantity)

end

APIFunction.removeItem = function(source, itemName, quantity)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem(itemName, quantity)

end

APIFunction.getMoney = function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	return xPlayer.getMoney()
	
end

APIFunction.addMoney = function(source, nb)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.addMoney(nb)
	TriggerEvent("API:askToUpdate")
end

APIFunction.removeMoney = function(source, nb)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeMoney(nb)
	TriggerEvent("API:askToUpdate")
end

APIFunction.removeAllMoney = function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.setMoney(0)
	xPlayer.set('black_money',0)

	print(xPlayer.getMoney())
	TriggerEvent("API:askToUpdate")
end


APIFunction.getPlayers = function()
	local _source = source
	local xPlayers = ESX.GetPlayers()
	local playerList = {}	

	for i=1, #xPlayers, 1 do
		local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])
		local source2 = xPlayer2.source

		playerList[source2] = GetPlayerName(xPlayer2.source)
	end

	return playerList
end



APIFunction.getPlayersByJob = function(job)
	local _source = source
	local xPlayers = ESX.GetPlayers()
	local playerList = {}	

	for i=1, #xPlayers, 1 do
		local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])
		local currentJob = xPlayer2.job.name
		local source2 = xPlayer2.source

		if(job == currentJob) then
			playerList[source2] = GetPlayerName(xPlayer2.source)
		end
	end

	return playerList
end


APIFunction.getPlayersInfos = function(source, cb)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	cb(xPlayer)

end


APIFunction.setJob = function(source,job,grade)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.setJob(job, grade)
end

APIFunction.createCoffre = function(nameID,x,y,z,job,grade)
	
	MySQL.Async.fetchAll("SELECT * FROM coffre WHERE nameID = @nameID", {['@nameID'] = nameID}, function(result)
		if(result[1] == nil) then
			MySQL.Async.execute("INSERT INTO coffre (nameID, job,grade) VALUES (@nameID,@job,@grade)", {['@nameID'] = nameID, ['@job'] = job, ['@grade'] = grade})

			local id = x+y+z
			coffres[id] = createCoffre(nameID, {}, job, grade)
		else

			MySQL.Async.fetchAll("SELECT * FROM coffre_inventory WHERE nameID = @nameID", {['@nameID'] = nameID}, function(result)

				local itemsData = {}
				for _,k in pairs(result) do
					itemsData[k.item] = k.quantity
				end

				local id = x+y+z
				coffres[id] = createCoffre(nameID, itemsData, job, grade)
			end)
		end
	end)

end


APIFunction.getCoffre = function(x,y,z)
	local id = x+y+z
	return coffres[id]
end




function createCoffre(nameID, itemsData, job, grade)
	local self = {}

	self.name = nameID
	self.itemsData = itemsData
	self.job = job
	self.grade = grade

	self.getItems = function()
		return self.itemsData
	end

	self.getRequirements = function()
		return self.job, self.grade
	end

	self.addItem = function(items, quantity, source)
		local _source = source
		if(self.itemsData[items] == nil) then
			self.itemsData[items] = quantity

			MySQL.Async.execute("INSERT INTO coffre_inventory (nameID, item,quantity) VALUES (@nameID, @item,@quantity)", {['@nameID'] = self.name, ['@item'] = items, ['@quantity'] = quantity})
		else
			local count = self.itemsData[items]

			self.itemsData[items] = quantity + count

			MySQL.Async.execute("UPDATE coffre_inventory SET quantity = @quantity WHERE nameID = @nameID AND item = @item", {['@quantity'] = self.itemsData[items], ['@nameID'] = self.name, ['@item'] = items})
		end

		APIFunction.removeItem(_source, items, quantity)
	end

	self.recupItem = function(items, quantity, source)
		local _source = source
		if(self.itemsData[items] ~= nil) then
			if(self.itemsData[items] >= quantity) then
				local count = self.itemsData[items]

				self.itemsData[items] = count - quantity
				MySQL.Async.execute("UPDATE coffre_inventory SET quantity = @quantity WHERE nameID = @nameID AND item = @item", {['@quantity'] = self.itemsData[items], ['@nameID'] = self.name, ['@item'] = items})
				APIFunction.addItem(_source, items, quantity)
				return true
			end
		end
		return false
	end 
	return self
end