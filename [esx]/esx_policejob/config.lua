Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 0, g = 128, b = 255 }
Config.EnablePlayerManagement     = false
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = true
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale                     = 'fr'

Config.PoliceStations = {

  LSPD = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      {name = 'WEAPON_SMOKEGRENADE',     price = 187},
      {name = 'WEAPON_FLARE',            price = 125},
      {name = 'WEAPON_NIGHTSTICK',       price = 500},
      {name = 'WEAPON_COMBATPISTOL',     price = 1875},
      {name = 'WEAPON_PISTOL',           price = 1250},
      {name = 'WEAPON_SMG',        	 price = 3125},
      {name = 'WEAPON_COMBATPDW',        price = 6000},
      {name = 'WEAPON_CARBINERIFLE',     price = 4375},
      {name = 'WEAPON_STUNGUN',          price = 1000},
      {name = 'WEAPON_FLASHLIGHT',       price = 375},
      {name = 'WEAPON_FIREEXTINGUISHER', price = 625},
      {name = 'WEAPON_FLAREGUN',         price = 750},
      {name = 'WEAPON_SNIPERRIFLE',      price = 6250},
      {name = 'GADGET_PARACHUTE',        price = 2500}
    },

    AuthorizedVehicles = {
      { name = 'police',  label = 'Véhicule de patrouille 1' },
      { name = 'police2', label = 'Véhicule de patrouille 2' },
      { name = 'police3', label = 'Véhicule de patrouille 3' },
      { name = 'police4', label = 'Véhicule civil' },
      { name = 'policeb', label = 'Moto' },
      { name = 'policet', label = 'Van de transport' },
    },
	
    AuthorizedHelicopters = {
	{name = 'polmav' , label = 'Helicoptere de police'},
	{name = 'buzzard2', label = 'Hélicoptère d\'intervention'},
    },
	
    Cloakrooms = {
      { x = 452.600, y = -993.306, z = 29.750 },
    },

    Armories = {
      { x = 458.554, y = -979.773, z = 29.689 },
    },

    Vehicles = {
      {
        Spawner    = { x = 454.69, y = -1017.4, z = 27.430 },
        SpawnPoint = { x = 438.42, y = -1018.3, z = 27.757 },
        Heading    = 90.0,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 456.0, y = -984.12, z = 42.69 },
        SpawnPoint = { x = 449.3, y = -981.14, z = 42.69 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = 462.74, y = -1014.4, z = 27.065 },
      { x = 462.40, y = -1019.7, z = 27.104 },
    },
	
    HelicoDeleters = {	
	  {x = 449.3, y = -981.14, z = 42.69}
    },

    BossActions = {
      { x = 448.417, y = -973.208, z = 29.689 }
    },

  },

}
