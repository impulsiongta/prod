Config = {}
Config.Locale = 'fr'

Config.PoliceNumberRequired = 2
Config.TimerBeforeNewRob = 1800 -- seconds

Stores = {
	-- ["ocean_liquor"] = {
	-- 	position = { ['x'] = -2959.33715820313, ['y'] = 388.214172363281, ['z'] = 14.0432071685791 },
	-- 	reward = math.random(3000,5000),
	-- 	nameofstore = "Robs Liquor. (Great Ocean Higway)",
	--	secondsRemaining = 45, -- seconds
	-- 	lastrobbed = 0
	-- },
	["sanandreas_liquor"] = {
		position = { ['x'] = -1219.85607910156, ['y'] = -916.276550292969, ['z'] = 11.3262157440186 },
		reward = math.random(3000,5000),
		nameofstore = "Robs Liquor. (San Andreas Avenue)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	},
	["grove_ltd"] = {
		position = { ['x'] = -43.4035377502441, ['y'] = -1749.20922851563, ['z'] = 29.421012878418 },
		reward = math.random(3000,5000),
		nameofstore = "LTD Gasoline. (Grove Street)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	},
	["mirror_ltd"] = {
		position = { ['x'] = 1160.67578125, ['y'] = -314.400451660156, ['z'] = 69.2050552368164 },
		reward = math.random(500,5000),
		nameofstore = "LTD Gasoline. (Mirror Park Boulevard)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	},
	["prosperity_liquor"] = {
		position = { ['x'] = -1479.22, ['y'] = -374.597, ['z'] = 39.1633 },
		reward = math.random(3000,5000),
		nameofstore = "Robs Liquor. (Prosperity Street)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	},
	-- ["rockforddrive_ltd"] = {
	-- 	position = { ['x'] = -1828.42, ['y'] = 799.294, ['z'] = 138.173 },
	-- 	reward = math.random(3000,5000),
	-- 	nameofstore = "LTD Gasoline. (North Rockford Drive)",
	--	secondsRemaining = 45, -- seconds
	-- 	lastrobbed = 0
	-- },
	["littleseoul_ltd"] = {
		position = { ['x'] = -709.17022705078, ['y'] = -904.21722412109, ['z'] = 19.215591430664 },
		reward = math.random(3000,5000),
		nameofstore = "LTD Gasoline. (Little Seoul)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	},
	["clinton_twentyfourseven"] = {
		position = { ['x'] = 379.983, ['y'] = 332.192, ['z'] = 103.566 },
		reward = math.random(3000,5000),
		nameofstore = "24/7. (Clinton Avenue)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	},
	-- ["palomino_twentyfourseven"] = {
	-- 	position = { ['x'] = 2550.19, ['y'] = 385.45, ['z'] = 108.623 },
	-- 	reward = math.random(3000,5000),
	-- 	nameofstore = "24/7. (Palomino Freeway)",
	--	secondsRemaining = 45, -- seconds
	-- 	lastrobbed = 0
	-- },
	-- ["ineseno_twentyfourseven"] = {
	-- 	position = { ['x'] = -3047.2, ['y'] = 586.331, ['z'] = 7.90893 },
	-- 	reward = math.random(3000,5000),
	-- 	nameofstore = "24/7. (Ineseno Road)",
	--	secondsRemaining = 45, -- seconds
	-- 	lastrobbed = 0
	-- },
	["innocence_twentyfourseven"] = {
		position = { ['x'] = 28.5155, ['y'] = -1340.12, ['z'] = 29.497 },
		reward = math.random(3000,5000),
		nameofstore = "24/7. (Innocence Boulevard & Elgin Avenue)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	},
	["elrancho_liquor"] = {
		position = { ['x'] = 1126.3, ['y'] = -980.948, ['z'] = 45.4157 },
		reward = math.random(3000,5000),
		nameofstore = "Robs Liquor. (El Rancho Boulevard)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	}--[[,
	["barbareno_twentyfourseven"] = {
		position = { ['x'] = -3249.3, ['y'] = 1004.76, ['z'] = 12.8307 },
		reward = math.random(3000,5000),
		nameofstore = "24/7. (Barbareno Road)",
		secondsRemaining = 45, -- seconds
		lastrobbed = 0
	}--]]

	-------------------NORD---------------------------
	
--	["sandyshores_twentyfoursever"] = {
--		position = { ['x'] = 1961.24682617188, ['y'] = 3749.46069335938, ['z'] = 32.3437461853027 },
--		reward = math.random(500,2000),
--		nameofstore = "24/7. (Sandy Shores)",
--		secondsRemaining = 45, -- seconds
--		lastrobbed = 0
--	},
--	["grapseed_ltd"] = {
--		position = { ['x'] = 1707.4, ['y'] = 4919.82, ['z'] = 42.0637 },
--		reward = math.random(500,2000),
--		nameofstore = "LTD Gasoline. (Grapeseed Main Street)",
--		secondsRemaining = 45, -- seconds
--		lastrobbed = 0
--	},
--	["route68_twentyfoursever"] = {
--		position = { ['x'] = 546.08, ['y'] = 2663.45, ['z'] = 42.1565 },
--		reward = math.random(500,2000),
--		nameofstore = "24/7. (Route 68)",
--		secondsRemaining = 45, -- seconds
--		lastrobbed = 0
--	},
--	["senora_twentyfoursever"] = {
--		position = { ['x'] = 2673.51, ['y'] = 3286.57, ['z'] = 55.2411 },
--		reward = math.random(500,2000),
--		nameofstore = "24/7. (Senora Freeway)",
--		secondsRemaining = 45, -- seconds
--		lastrobbed = 0
--	},
--	["route68_liquor"] = {
--		position = { ['x'] = 1168.65, ['y'] = 2718.35, ['z'] = 37.1576 },
--		reward = math.random(500,2000),
--		nameofstore = "Robs Liquor. (Route 68)",
--		secondsRemaining = 45, -- seconds
--		lastrobbed = 0
--	},
--	["paleto_twentyfourseven"] = {
--		position = { ['x'] = 1736.32092285156, ['y'] = 6419.4970703125, ['z'] = 35.037223815918 },
--		reward = math.random(500,2000),
--		nameofstore = "24/7. (Paleto Bay)",
--		secondsRemaining = 45, -- seconds
--		lastrobbed = 0
--	}
}
