Locales['fr'] = {

	['robbery_cancelled'] = 'le braquage vas être annulé, vous ne gagnerez rien !',
	['robbery_successful'] = 'Braquage réussi',
	['store_robbery'] = 'Braquage de superette',
	['press_to_rob'] = 'Appuyer sur ~INPUT_CONTEXT~ pour ~g~braquer ~b~',
	['robbery_of'] = 'Braquage de magasin : ~r~',
	['seconds_remaining'] = '~w~ secondes restantes',
	['robbery_cancelled_at'] = '~r~ Braquage annulé à : ~b~',
	['robbery_has_cancelled'] = '~r~ Le braquage à été annulé : ~b~',
	['already_robbed'] = 'Ce magasin ~r~a déjà été braqué. Veuillez attendre : ',
	['seconds'] = ' secondes.',
	['rob_in_prog'] = '~r~Braquage en cours à : ~b~',
	['started_to_rob'] = 'Vous avez commencé ~y~~h~à braquer ',
	['do_not_move'] = ', ne vous éloignez pas !',
	['alarm_triggered'] = 'l\'alarme à été déclenché',
	['hold_pos'] = 'Tenez la position pendant ~g~45 sec~s~ et ~g~~h~l\'argent est à vous !',
	['robbery_complete'] = '~r~Braquage terminé.~s~ ~h~Fuie !',
	['robbery_complete_at'] = '~r~Braquage terminé à : ~b~',
    	['min_police'] = 'Il faut minimum ~b~',
	['min_two_police'] = 'Pour braquer il faut minimum ~y~~h~2 policiers',
	['robbery_already'] = '~r~Un braquage est ~r~~h~déjà en cours.',

}
