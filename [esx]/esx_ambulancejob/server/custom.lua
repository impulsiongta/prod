PlayersCrafting    = {}
ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_ambulancejob:putInVehicle')
AddEventHandler('esx_ambulancejob:putInVehicle', function(target)
  TriggerClientEvent('esx_ambulancejob:putInVehicle', target)
end)

RegisterServerEvent('esx_ambulancejob:poche')
AddEventHandler('esx_ambulancejob:poche', function(target)
	TriggerClientEvent('esx_ambulancejob:poche', target)
end)

RegisterServerEvent('esx_ambulancejob:addItem')
AddEventHandler('esx_ambulancejob:addItem', function(item)
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  
  xPlayer.addInventoryItem(item, 1)
  
  if item == 'poche_de_sang' then
    TriggerClientEvent('esx:showNotification', _source, _U('add_poche'))
  end
end)

RegisterServerEvent('esx_ambulancejob:Craft')
AddEventHandler('esx_ambulancejob:Craft', function(source)

	SetTimeout(4000, function()

		if PlayersCrafting[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local PocheQuantity = xPlayer.getInventoryItem('poche_de_sang').count

			if PocheQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~~h~plus de poche de sang')		
			else   
                xPlayer.removeInventoryItem('poche_de_sang', 1)
                xPlayer.addInventoryItem('medikit', 5)

				TriggerEvent("esx_ambulancejob:Craft", source)
			end
		end
	end)
end)

RegisterServerEvent('esx_ambulancejob:startCraft')
AddEventHandler('esx_ambulancejob:startCraft', function()
	local _source = source
	PlayersCrafting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Assemblage de la ~b~~h~Trousse de soin~s~...')
	TriggerEvent('esx_ambulancejob:Craft', _source)
end)

RegisterServerEvent('esx_ambulancejob:stopCraft')
AddEventHandler('esx_ambulancejob:stopCraft', function()
	local _source = source
	PlayersCrafting[_source] = false
end)