Config                           = {}
Config.DrawDistance              = 100.0
Config.MarkerColor               = {r = 0, g = 128, b = 255}
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.4 }
Config.ReviveReward               = 700  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
local second = 1000
local minute = 60 * second

-- How much time before auto respawn at hospital
Config.RespawnDelayAfterRPDeath   = 30 * minute
-- How much time before a menu opens to ask the player if he wants to respawn at hospital now
-- The player is not obliged to select YES, but he will be auto respawn
-- at the end of RespawnDelayAfterRPDeath just above. 
Config.RespawnToHospitalMenuTimer   = true
Config.MenuRespawnToHospitalDelay   = 10 * minute
Config.EnablePlayerManagement       = true
Config.EnableSocietyOwnedVehicles   = true

Config.RemoveWeaponsAfterRPDeath    = false
Config.RemoveCashAfterRPDeath       = false
Config.RemoveItemsAfterRPDeath      = false

-- Will display a timer that shows RespawnDelayAfterRPDeath time remaining
Config.ShowDeathTimer               = true

-- Will allow to respawn at any time, don't use RespawnToHospitalMenuTimer at the same time !
Config.EarlyRespawn                 = false
-- The player can have a fine (on bank account)
Config.RespawnFine                  = true
Config.RespawnFineAmount            = 5000

Config.Locale                       = 'fr'

Config.Blip = {
  Pos     = { x = 307.76, y = -1433.47, z = 28.97 },
  Sprite  = 61,
  Display = 4,
  Scale   = 1.2,
  Colour  = 2,
}

Config.HelicopterSpawner = {
  SpawnPoint  = { x = 313.33, y = -1465.2, z = 45.5 },
  Heading     = 0.0
}

Config.Zones = {

-- entrée extérieure en bas
  HospitalInteriorEntering1 = { -- ok
    Pos  = { x = 294.30, y = -1448.60, z = 29.0},
    Type = 1
  },

-- arrivée entrée extérieure en bas
  HospitalInteriorInside1 = { -- ok
    Pos  = { x = 272.8, y = -1358.8, z = 23.5 },
    Type = -1
  },

-- sortie de arrivée entrée extérieure en bas
  HospitalInteriorOutside1 = { -- ok
    Pos  = { x = 295.8, y = -1446.5, z = 28.9 },
    Type = -1
  },

-- sortie hoptial vers en bas
  HospitalInteriorExit1 = { -- ok
    Pos  = { x = 275.7, y = -1361.5, z = 23.5 },
    Type = 1
  },

-- entrée hopital depuis garage
  HospitalInteriorEntering2 = { -- Ascenseur aller au toit
    Pos  = { x = 247.1, y = -1371.4, z = 23.5 },
    Type = 1
  },

-- arrivée entrée hopital depuis garage
  HospitalInteriorInside2 = { -- Toit sortie
    Pos  = { x = 333.1,  y = -1434.9, z = 45.5 },
    Type = -1
  },

-- arrivée sortie intérieure garage
  HospitalInteriorOutside2 = { -- Ascenseur retour depuis toit
    Pos  = { x = 249.1,  y = -1369.6, z = 23.5 },
    Type = -1
  },

-- sortie intérieure garage
  HospitalInteriorExit2 = { -- Toit entrée
    Pos  = { x = 335.5, y = -1432.0, z = 45.5 },
    Type = 1
  },

-- menu amnulancier 
  AmbulanceActions = { -- CLOACKROOM
    Pos  = { x = 268.4, y = -1365.38, z = 23.5 },
    Type = 1
  },

  VehicleSpawner = {
    Pos  = { x = 307.76, y = -1433.47, z = 28.97 },
    Type = 1
  },

  VehicleSpawnPoint = {
    Pos  = { x = 304.87, y = -1437.69, z = 28.80 },
    Type = -1
  },

  VehicleDeleter = {
    Pos  = { x = 327.67, y = -1481.1, z = 28.83 },
    Type = 1
  },
  
  VehicleDeleter2 = {
    Pos  = { x = 299.50, y = -1453.35, z = 45.5 },
    Type = 1
  },
  
  Pharmacy = {
    Pos  = { x = 258.89, y = -1337.19, z = 23.54 },
    Type = 1
  },

  ParkingDoorGoOutInside = {
    Pos  = { x = 234.56, y = -1373.77, z = 20.97 },
    Type = 1
  },

  ParkingDoorGoOutOutside = {
    Pos  = { x = 320.98, y = -1478.62, z = 28.81 },
    Type = -1
  },

  ParkingDoorGoInInside = {
    Pos  = { x = 238.64, y = -1368.48, z = 23.53 },
    Type = -1
  },

  ParkingDoorGoInOutside = {
    Pos  = { x = 317.97, y = -1476.13, z = 28.97 },
    Type = 1
  },

  StairsGoTopTop = {
    Pos  = { x = 251.91, y = -1363.3, z = 38.53 },
    Type = -1
  },

  StairsGoTopBottom = {
    Pos  = { x = 237.45, y = -1373.89, z = 26.30 },
    Type = -1
  },

  StairsGoBottomTop = {
    Pos  = { x = 256.58, y = -1357.7, z = 37.30 },
    Type = -1
  },

  StairsGoBottomBottom = {
    Pos  = { x = 240.94, y = -1369.91, z = 23.53 },
    Type = -1
  },
  AmbulanceCraft = {
    Pos   = {x = 242.97, y = -1370.35, z = 38.53},
    Type  = 1
  },
  HelicoSpawner = { -- ok
    Pos  = { x = 313.33, y = -1465.2, z = 45.5 },
    Type = 1
  } 
}