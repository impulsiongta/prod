# esx_ambulancejob

[REQUIREMENTS]

* Auto mode
   - [esx_skin](https://github.com/ESX-Org/esx_skin)

* Player management (boss actions **There is no way to earn money for now**)
   - [esx_society](https://github.com/ESX-Org/esx_society)

[INSTALLATION]

1) CD in your resources/[esx] folder
2) Clone the repository
```
git clone [esx_ambulancejob](https://github.com/ESX-Org/esx_ambulancejob)
```
3) Import esx_ambulancejob.sql in your database

4) Add this in your server.cfg :

```
start esx_ambulancejob
```
5) * If you want player management you have to set Config.EnablePlayerManagement to true in config.lua

