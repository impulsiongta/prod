RegisterNetEvent('esx_ambulancejob:SetVehicleMaxMods')
AddEventHandler('esx_ambulancejob:SetVehicleMaxMods', function(vehicle)

  local props = {
    modEngine       = 2,
    modTransmission = 2,
    modTurbo        = true,
  }

  ESX.Game.SetVehicleProperties(vehicle, props)

end)

RegisterNetEvent('esx_ambulancejob:heal2')
AddEventHandler('esx_ambulancejob:heal2', function(_type)
    local playerPed = GetPlayerPed(-1)
    local maxHealth = GetEntityMaxHealth(playerPed)
    if _type == 'small' then
        local health = GetEntityHealth(playerPed)
        local newHealth = math.min(maxHealth , math.floor(health + maxHealth/8))
		TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_KNEEL', 0, true)
		Citizen.Wait(15000)
        SetEntityHealth(playerPed, newHealth)
		ClearPedTasks(playerPed)
    elseif _type == 'big' then
		TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_KNEEL', 0, true)
		Citizen.Wait(15000)
		SetEntityHealth(playerPed, maxHealth)
		ClearPedTasks(playerPed)
    end
    ESX.ShowNotification(_U('healed2'))
end)

--------add effect when the player come back after death-----
RegisterNetEvent('effectrespawn')
AddEventHandler('effectrespawn', function(status)

	Citizen.CreateThread(function()
	
		RequestAnimSet("MOVE_M@DRUNK@SLIGHTLYDRUNK")
		while not HasAnimSetLoaded("MOVE_M@DRUNK@SLIGHTLYDRUNK") do
			Citizen.Wait(0)
		end
	
		Citizen.Wait(1000)
		SetPedMotionBlur(GetPlayerPed(-1), true)
		ShakeGameplayCam("DRUNK_SHAKE", 1.5)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@SLIGHTLYDRUNK", true)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)		

		Citizen.CreateThread(function()
			Citizen.Wait(78000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			StopGameplayCamShaking(1)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedIsDrunk(GetPlayerPed(-1), false)
			SetPedMotionBlur(GetPlayerPed(-1), false)
		end)	
	end)
end)

RegisterNetEvent('effectsang')
AddEventHandler('effectsang', function(status)

	Citizen.CreateThread(function()
	
		RequestAnimSet("MOVE_M@DRUNK@SLIGHTLYDRUNK")
		while not HasAnimSetLoaded("MOVE_M@DRUNK@SLIGHTLYDRUNK") do
			Citizen.Wait(0)
		end
	
		Citizen.Wait(1000)
		SetPedMotionBlur(GetPlayerPed(-1), true)
		ShakeGameplayCam("DRUNK_SHAKE", 1.5)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@SLIGHTLYDRUNK", true)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)		

		Citizen.CreateThread(function()
			Citizen.Wait(120000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			StopGameplayCamShaking(1)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedIsDrunk(GetPlayerPed(-1), false)
			SetPedMotionBlur(GetPlayerPed(-1), false)
		end)	
	end)
end)

RegisterNetEvent('OpenAmbulanceCraftMenu')
AddEventHandler('OpenAmbulanceCraftMenu', function(PlayerData)

	if PlayerData.job ~= nil and PlayerData.job.grade_name ~= 'interne' then

		local elements = {
			{label = 'Trousse de soin', value = 'medkit'},
		}

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'ambulance_craft',
			{
				title    = 'Laboratoire',
				elements = elements
			},
			function(data, menu)
				if data.current.value == 'medkit' then
					menu.close()
					TriggerServerEvent('esx_ambulancejob:startCraft')
				end

			end,
			
			function(data, menu)
				menu.close()
				CurrentAction     = 'ambulance_craft_menu'
				CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~acc�der au laboratoire.'
				CurrentActionData = {}
			end
		)
	else
		ESX.ShowNotification("Vous n'�tes ~r~pas assez exp�riment�~s~ pour effectuer cette action.")
	end
end)

RegisterNetEvent('OpenHelicoSpawnerMenu')
AddEventHandler('OpenHelicoSpawnerMenu', function()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('hel_menu'),
			align    = 'top-left',
			elements = {
				{label = _U('helicopter'), value = 'polmav'},
			},
		},
		function(data, menu)

			menu.close()

			local model = data.current.value

			ESX.Game.SpawnVehicle(model, Config.Zones.HelicoSpawner.Pos, 2.58, function(vehicle)

				local playerPed = GetPlayerPed(-1)
			
				if model == 'polmav' then
					SetVehicleModKit(vehicle, 0)
					SetVehicleLivery(vehicle, 1)
				end

				TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
				TriggerEvent("advancedFuel:setEssence", 100, GetVehicleNumberPlateText(vehicle), GetDisplayNameFromVehicleModel(GetEntityModel(vehicle)))
			end)

		end,
		function(data, menu)

			menu.close()

			CurrentAction     = 'helico_spawner_menu'
			CurrentActionMsg  = _U('hel_spawn')
			CurrentActionData = {}

		end
	)

end)

RegisterNetEvent('esx_ambulancejob:poche')
AddEventHandler('esx_ambulancejob:poche', function()

	Citizen.CreateThread(function()

	local playerPed = GetPlayerPed(-1)
	
		TriggerEvent('effectsang', true)
	end)
	
	ESX.ShowNotification(_U('poche_complete'))
end)

RegisterNetEvent('esx_ambulancejob:putInVehicle')
AddEventHandler('esx_ambulancejob:putInVehicle', function()

  local playerPed = GetPlayerPed(-1)
  local coords    = GetEntityCoords(playerPed)

  if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then

    local vehicle = GetClosestVehicle(coords.x,  coords.y,  coords.z,  5.0,  0,  71)

    if DoesEntityExist(vehicle) then

      local maxSeats = GetVehicleMaxNumberOfPassengers(vehicle)
      local freeSeat = nil

      for i=maxSeats - 1, 0, -1 do
        if IsVehicleSeatFree(vehicle,  i) then
          freeSeat = i
          break
        end
      end

      if freeSeat ~= nil then
        TaskWarpPedIntoVehicle(playerPed,  vehicle,  freeSeat)
      end

    end

  end

end)