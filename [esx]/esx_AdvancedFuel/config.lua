petrolCanPrice = 500

lang = "fr"
-- lang = "fr"

settings = {}
settings["en"] = {
	openMenu = "Press ~g~E~w~ to open the menu.",
	electricError = "~r~You have an electric vehicle.",
	fuelError = "~r~You're not in the good place.",
	buyFuel = "buy fuel",
	liters = "liters",
	percent = "percent",
	confirm = "Confirm",
	fuelStation = "Fuel station",
	boatFuelStation = "Fuel station | Boat",
	avionFuelStation = "Fuel station | Plane ",
	heliFuelStation = "Fuel station | Helicopter",
	getJerryCan = "Press ~g~E~w~ to buy a Petrol can ("..petrolCanPrice.."$)",
	refeel = "Press ~g~E~w~ to refeel the car.",
	YouHaveBought = "You have bought ",
	fuel = " liters of fuel",
	price = "price"
}

settings["fr"] = {
	openMenu = "Appuyez sur ~INPUT_CONTEXT~ ~g~~h~pour faire le plein.",
	electricError = "Vous avez une ~r~~h~voiture électrique.",
	fuelError = "Vous n'êtes pas ~r~~h~au bon endroit.",
	buyFuel = "acheter de l'essence",
	liters = "litres",
	percent = "pourcent",
	confirm = "Valider",
	fuelStation = "Station essence",
	boatFuelStation = "Station d'essence | Bateau",
	avionFuelStation = "Station d'essence | Avions",
	heliFuelStation = "Station d'essence | Hélicoptères",
	getJerryCan = "Appuyez sur ~INPUT_CONTEXT~ pour acheter ~g~~h~un bidon d'essence ("..petrolCanPrice.."$)",
	refeel = "Appuyez sur ~INPUT_CONTEXT~ pour remplir ~g~~h~votre voiture d'essence.",
	YouHaveBought = "Vous avez acheté ",
	fuel = " litres d'essence",
	price = "prix"
}


showBar = true
showText = false


hud_form = 2 -- 1 : Vertical | 2 = Horizontal
hud_x = 0.086
hud_y = 0.742

text_x = 0.2575
text_y = 0.975


electricityPrice = 5 -- NOT RANOMED !!

randomPrice = false --Random the price of each stations
price = 5 --If random price is on False, set the price here for 1 liter