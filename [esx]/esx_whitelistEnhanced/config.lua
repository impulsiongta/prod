Config = {}

Config.Locale = 'fr'

Config.EnableAntiSpam = true
Config.WaitingTime = 5
Config.CommunityLink = "https://impulsiongtavrp.fr/"

Config.ReloadWhitelistCommand = "loadwl"
Config.ReloadWhitelistGroup = "admin"
