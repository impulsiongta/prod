Locales['fr'] = {

	['valid_this_purchase'] = 'Valider cet achat ?',
	['yes'] = 'Oui',
	['no'] = 'Non',
	['name_outfit'] = 'Nom de la tenue ?',
	['not_enough_money'] = 'Vous n\'avez ~r~~h~pas assez d\'argent',
	['press_menu'] = 'Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~accéder à la boutique de vêtements',
	['clothes'] = 'Vêtements',
	['you_paid'] = 'Vous avez payé ~g~$',
	['save_in_dressing'] = 'Voulez-vous donner un nom à votre tenue ?',
	['saved_outfit'] = 'La tenue ~g~~h~a été sauvegardé',
}
