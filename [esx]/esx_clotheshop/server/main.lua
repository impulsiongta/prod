ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_clotheshop:pay')
AddEventHandler('esx_clotheshop:pay', function()

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeMoney(Config.Price)

	TriggerClientEvent('esx:showNotification', source, _U('you_paid') .. Config.Price)

end)

RegisterServerEvent('esx_clotheshop:saveOutfit')
AddEventHandler('esx_clotheshop:saveOutfit', function(label, skin)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)

		local dressing = store.get('dressing')

		if dressing == nil then
			dressing = {}
		end

		table.insert(dressing, {
			label = label,
			skin  = skin
		})

		store.set('dressing', dressing)

	end)

end)

ESX.RegisterServerCallback('esx_clotheshop:checkMoney', function(source, cb)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	if xPlayer.get('money') >= Config.Price then
		cb(true)
	else
		cb(false)
	end

end)

ESX.RegisterServerCallback('esx_clotheshop:checkPropertyDataStore', function(source, cb)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
	local foundStore = false

	TriggerEvent('esx_datastore:getDataStore', 'property', xPlayer.identifier, function(store)
		foundStore = true
	end)

	cb(foundStore)

end)

-------------------------------------------------------------------------------

RegisterServerEvent('esx_clotheshop:saveHelmet')
AddEventHandler('esx_clotheshop:saveHelmet', function(skin)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	TriggerEvent('esx_datastore:getDataStore', 'user_helmet', xPlayer.identifier, function(store)
		
		store.set('hasHelmet', true)
		
		store.set('skin', {
			helmet_1 = skin.helmet_1,
			helmet_2 = skin.helmet_2
		})

	end)

end)

ESX.RegisterServerCallback('esx_clotheshop:getHelmet', function(source, cb)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	TriggerEvent('esx_datastore:getDataStore', 'user_helmet', xPlayer.identifier, function(store)
		
		local hasHelmet = (store.get('hasHelmet') and store.get('hasHelmet') or false)
		local skin    = (store.get('skin')    and store.get('skin')    or {})

		cb(hasHelmet, skin)

	end)

end)

RegisterServerEvent('esx_clotheshop:saveGlasses')
AddEventHandler('esx_clotheshop:saveGlasses', function(skin)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	TriggerEvent('esx_datastore:getDataStore', 'user_glasses', xPlayer.identifier, function(store)
		
		store.set('hasGlasses', true)
		
		store.set('skin', {
			glasses_1 = skin.glasses_1,
			glasses_2 = skin.glasses_2
		})

	end)

end)

ESX.RegisterServerCallback('esx_clotheshop:getGlasses', function(source, cb)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	TriggerEvent('esx_datastore:getDataStore', 'user_glasses', xPlayer.identifier, function(store)
		
		local hasGlasses = (store.get('hasGlasses') and store.get('hasGlasses') or false)
		local skin    = (store.get('skin')    and store.get('skin')    or {})

		cb(hasGlasses, skin)

	end)

end)
