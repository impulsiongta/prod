Locales['fr'] = {
  ['new_message'] = '~b~Nouveau ~b~~h~message :~s~ %s',
  ['press_take_call'] = ' - Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~prendre l\'appel',
  ['taken_call'] = ' a ~b~~h~pris l\'appel',
  ['gps_position'] = '~y~Position~s~ entrée ~g~~h~dans le GPS',
  ['message_sent'] = 'Message ~g~~h~envoyé',
  ['contact_added'] = 'Contact ~g~~h~ajouté',
  ['contact_deleted'] = 'Contact ~r~~h~supprimé',
  ['contact_updated'] = 'Contact ~g~~h~mis à jour',
  ['cannot_add_self'] = 'Vous ne pouvez pas ~r~~h~vous ajouter vous-même',
  ['number_in_contacts'] = 'Ce numéro est ~r~~h~déja dans votre liste de contacts',
  ['number_not_assigned'] = 'Ce numéro ~r~~h~n\'est pas attribué...',
}
