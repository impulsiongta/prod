Config                = {}
Config.DrawDistance   = 100
Config.Size           = { x = 1.5, y = 1.5, z = 1.5 }
Config.Color          = { r = 0, g = 128, b = 255 }
Config.Type           = 1
Config.Locale         = 'fr'
Config.EnableLicense  = true -- only turn this on if you are using esx_license
Config.LicensePrice   = 4000

Config.Zones = {

    GunShop = {
        legal = 1,
        Items = {},
        Pos   = {
            { x = -662.180,   y = -934.961,   z = 20.829 },
            { x = 810.25,     y = -2157.60,   z = 28.62 },
            { x = 1693.44,    y = 3760.16,    z = 33.71 },
            { x = -330.24,    y = 6083.88,    z = 30.45 },
            { x = 252.63,     y = -50.00,     z = 68.94 },
            { x = 22.09,      y = -1107.28,   z = 28.80 },
            { x = 2567.69,    y = 294.38,     z = 107.73 },
            { x = -1117.58,   y = 2698.61,    z = 17.55 },
            { x = 842.44,     y = -1033.42,   z = 27.19 },
            {x = -3172.02,            y = 1087.24,            z = 19.84},
            {x = -1306.1,             y = -393.98,            z = 35.7}
        }
    },

    BlackWeashop = {
        legal = 0,
        Items = {},
        Pos   = {
            {x = 41.6,                y = 3705.78,            z = 40.26}
        }
    },

}
