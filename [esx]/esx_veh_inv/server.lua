ESX = nil
local plate = nil
function sendToDiscord(name, message)
  PerformHttpRequest('https://discordapp.com/api/webhooks/422886159492841497/sk6Wq5tIfSSIBmGiYFMZSYJ9HMO7Qs1Y61L8FH6cCU1zO41P5etNnbWj8LBeDXNBvSHu', function(err, text, headers) end, 'POST', json.encode({username = name, content = message}), { ['Content-Type'] = 'application/json' })
end
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
ESX.RegisterServerCallback('esx_inv_veh:getPlayerInv', function(source, cb)
  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })
end)
ESX.RegisterServerCallback('esx_inv_veh:getVehicleInv', function(source, cb)
	local items = nil
 MySQL.Async.fetchAll(
		'SELECT * FROM `vehicle_inventory` WHERE `plate` = @plate AND `type` = @type' ,
		{
			['@plate'] = plate,
			['@type'] = 0
		},
		function(result)
			
		  cb({
		    items      = result
		  })
		end)
end)

ESX.RegisterServerCallback('esx_inv_veh:getVehicleWeapon', function(source, cb)
	local items = nil
 MySQL.Async.fetchAll(
		'SELECT * FROM `vehicle_inventory` WHERE `plate` = @plate AND `type` = @type' ,
		{
			['@plate'] = plate,
			['@type'] = 1
		},
		function(result)
			
		  cb({
		    items      = result
		  })
		end)
end)

RegisterServerEvent('esx_inv_veh:takeItemFromVehicle')
AddEventHandler('esx_inv_veh:takeItemFromVehicle',function(plate,items, count)
	--print(items)
	--print(plate)
	sendToDiscord('SYSTEM', GetPlayerName(source) .. ' a sorti ' .. count .. ' x '..items..' de la voiture avec la plaque ( '..plate.. ')')
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local countInv = xPlayer.getInventoryItem(items)
	--print(countInv.count)
	if countInv.count + count <= 1000 then
		--print(countInv)
		MySQL.Async.fetchAll(
		'SELECT * FROM `vehicle_inventory` WHERE `plate` = @plate AND items = @items LIMIT 1' ,
		{
			['@plate'] = plate,
			['@items'] = items
		},
		function(result)
			
			if result[1].quantity >= count then
				if result[1].quantity > count then

					local value = result[1].quantity - count
					MySQL.Async.execute(
						'UPDATE `vehicle_inventory` SET `quantity`=@count WHERE  `plate`=@plate AND `items` = @items',
						{
							['@count'] = value,
							['@plate'] = plate,
							['@items'] = items
						}
					)
					xPlayer.addInventoryItem(items, count)
				elseif result[1].quantity == count then
					MySQL.Async.execute(
							'DELETE FROM vehicle_inventory WHERE `plate`=@plate AND `items` = @items',
							{
								['@plate'] = plate,
								['@items'] = items
							}
						)
					xPlayer.addInventoryItem(items, count)
				end		
				
			end
		end)
	else
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous ne pouvez pas prendre autant d\'items')
	end
end)

RegisterServerEvent('esx_inv_veh:takeweaponFromVehicle')
AddEventHandler('esx_inv_veh:takeweaponFromVehicle',function(plate,weapon, ammo)
	--print(items)
	sendToDiscord('SYSTEM', GetPlayerName(source) .. ' a sorti '..weapon..' de la voiture avec la plaque ( '..plate.. ')')
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	MySQL.Async.fetchAll(
		'SELECT * FROM `vehicle_inventory` WHERE `plate` = @plate AND items = @items LIMIT 1' ,
		{
			['@plate'] = plate,
			['@items'] = weapon
		},
		function(result)
			if result[1].items ~= nil then
					MySQL.Async.execute(
						'DELETE FROM vehicle_inventory WHERE `plate`=@plate AND `items` = @weapon LIMIT 1',
						{
						    ['@plate'] = plate,
							['@weapon'] = weapon
						}
					)
				xPlayer.addWeapon(weapon,500)
			end
		end)
end)

RegisterServerEvent('esx_inv_veh:getPlate')
AddEventHandler('esx_inv_veh:getPlate',function(platex)
	plate = platex

end)

RegisterServerEvent('esx_inv_veh:getPlayerInv')
AddEventHandler('esx_inv_veh:getPlayerInv',function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local inventory = xPlayer.getInventory()
	TriggerClientEvent('esx_inv_veh:getPlayerInvCallBack',inventory)
end)

RegisterServerEvent('esx_inv_veh:putItemIntoVehicle')
AddEventHandler('esx_inv_veh:putItemIntoVehicle',function(plate,items,count,useCapacity,maxCapacity)	
	  --print("put "..items.." in vehicle("..plate..")quantity = " ..count)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	sendToDiscord('SYSTEM', GetPlayerName(source) .. ' a déposé ' .. count .. ' x '..items..' dans la voiture avec la plaque ( '..plate.. ')')
	MySQL.Async.fetchAll(
		'SELECT * FROM `vehicle_inventory` WHERE `plate` = @plate AND items = @items LIMIT 1' ,
		{
			['@plate'] = plate,
			['@items'] = items,
		},
		function(result)
			
				if useCapacity <= maxCapacity then

					xPlayer.removeInventoryItem(items, count)
					if result[1] ~= nil then
							count = result[1].quantity + count

							--print(count) -- valeur demandé ok
							MySQL.Async.execute(
								'UPDATE `vehicle_inventory` SET `quantity`=@quantity WHERE  `plate`=@plate AND `items` = @items',
								{
									['@quantity'] = count, 
									['@plate'] = plate,
									['@items'] = items
								}
							)
					else
						MySQL.Async.execute(
							'INSERT INTO `vehicle_inventory` (`plate`, `items`, quantity,type) VALUES (@plate, @items, @quantity,@type)',
							{
								['@plate'] = plate,
								['@items']     = items,
								['@type']     = 0,
								['@quantity']       = count

							}, function(rowsChanged)
								
							end
						)
					end
				else 
					--print("invalide")
				end
		end)
end)

RegisterServerEvent('esx_inv_veh:putWeaponIntoVehicle')
AddEventHandler('esx_inv_veh:putWeaponIntoVehicle',function(weapon,ammo)	
	  --print("put "..items.." in vehicle("..plate..")quantity = " ..count)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	xPlayer.removeWeapon(weapon)
	sendToDiscord('SYSTEM', GetPlayerName(source) .. ' a déposé ' .. weapon .. ' avec '..ammo..' munitions dans la voiture avec la plaque ( '..plate.. ')')
	
	MySQL.Async.execute(
		'INSERT INTO `vehicle_inventory` (`plate`, `items`, `quantity`, type) VALUES (@plate, @items, @quantity, @type)',
		{
			['@plate'] = plate,
			['@items']    = weapon,
			['@quantity'] = ammo,
			['@type']       = 1
		},
		function(rowsChanged)
									
		end
	)		
end)