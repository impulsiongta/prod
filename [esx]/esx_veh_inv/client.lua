ESX = nil
local vehicleInv = nil
local inventory = {}
local plate = nil
local useCapacity = 0
local maxCapacity = 0
local defaultCapacity = 50 -- default if config do not exist for this vehicle
local invWeapon = nil
local v = nil
local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
local config = {
	
--Super
	{model = 'ADDER',maxCapacity = 20},
	{model = 'BANSHEE',maxCapacity = 20},
	{model = 'BULLET',maxCapacity = 20},
	{model = 'CHEETAH',maxCapacity = 20},
	{model = 'ENTITYXF',maxCapacity = 20},
	{model = 'SHEAVA',maxCapacity = 20},	--ETR1
	{model = 'FMJ',maxCapacity = 20},
	{model = 'INFERNUS',maxCapacity = 20},
	{model = 'OSIRIS',maxCapacity = 20},
	{model = 'PFISTER811',maxCapacity = 80},	--AUDI RS5
	{model = 'LE7B',maxCapacity = 20},
	{model = 'REAPER',maxCapacity = 20},
	{model = 'SULTANRS',maxCapacity = 50},
	{model = 'T20',maxCapacity = 20},
	{model = 'TURISMOR',maxCapacity = 20},
	{model = 'TYRUS',maxCapacity = 20},
	{model = 'VACCA',maxCapacity = 20},
	{model = 'VOLTIC',maxCapacity = 20},
	{model = 'PROTOTIPO',maxCapacity = 20},
	{model = 'ZENTORNO',maxCapacity = 20},
	{model = 'XA21',maxCapacity = 20},
	{model = 'NERO',maxCapacity = 20},
	{model = 'TEMPESTA',maxCapacity = 20},
	{model = 'GP1',maxCapacity = 20},
	{model = 'VAGNER',maxCapacity = 20},
	{model = 'SPECTER2',maxCapacity = 20},
	{model = 'ITALIGTB',maxCapacity = 20},
	{model = 'ITALIGTB2',maxCapacity = 20},
	{model = 'BANSHEE2',maxCapacity = 20},
	{model = 'nero2',maxCapacity = 20},

	--Sport Classic
	{model = 'BTYPE',maxCapacity = 50},		--classic
	{model = 'BTYPE2',maxCapacity = 50},	--hotroad
	{model = 'BTYPE3',maxCapacity = 50},	--luxe
	{model = 'CASCO',maxCapacity = 80},
	{model = 'COQUETTE2',maxCapacity = 20},
	{model = 'MANAMA',maxCapacity = 100},
	{model = 'MONROE',maxCapacity = 20},
	{model = 'PIGALLE',maxCapacity = 100},
	{model = 'STINGER',maxCapacity = 120},
	{model = 'STINGERGT',maxCapacity = 20},
	{model = 'ZTYPE',maxCapacity = 20},
	{model = 'FELTZER3',maxCapacity = 120},
	{model = 'TORERO',maxCapacity = 20},
	{model = 'TURISMO2',maxCapacity = 25},
	{model = 'INFERNUS2',maxCapacity = 20},
	{model = 'COMET3',maxCapacity = 20},
	{model = 'CHEETAH2',maxCapacity = 20},


	--Sport
	{model = 'SCHAFTER3',maxCapacity = 70},	--mercedes
	{model = 'rapidgt',maxCapacity = 40},
	{model = 'penumbra',maxCapacity = 40},
	{model = 'omnis',maxCapacity = 52},
	{model = 'massacro2',maxCapacity = 50},
	{model = 'massacro',maxCapacity = 55},
	{model = 'mamba',maxCapacity = 36},
	{model = 'lynx',maxCapacity = 46},
	{model = 'futo',maxCapacity = 48},
	{model = 'rapidgt2',maxCapacity = 35},
	{model = 'seven70',maxCapacity = 37},
	{model = 'sultan',maxCapacity = 65},
	{model = 'surano',maxCapacity = 40},
	{model = 'tropos',maxCapacity = 20},
	{model = 'verlierer2',maxCapacity = 35},
	{model = 'ruston',maxCapacity = 20},
	{model = 'specter',maxCapacity = 30},
	{model = 'kuruma',maxCapacity = 70},
	{model = 'bestiagts',maxCapacity = 45},
	{model = 'elegy',maxCapacity = 65},
	{model = 'ninef',maxCapacity = 20},
	{model = 'ninef2',maxCapacity = 25},
	{model = 'alpha',maxCapacity = 60},
	{model = 'buffalo',maxCapacity = 45},
	{model = 'buffalo2',maxCapacity = 45},
	{model = 'carbonizzare',maxCapacity = 45},
	{model = 'comet2',maxCapacity = 30},
	{model = 'coquette',maxCapacity = 35},
	{model = 'khamelion',maxCapacity = 65},
	{model = 'jester2',maxCapacity = 25},
	{model = 'jester',maxCapacity = 20},
	{model = 'fusilade',maxCapacity = 45},
	{model = 'furoregt',maxCapacity = 40},
	{model = 'tampa2',maxCapacity = 45},
	{model = 'elegy2',maxCapacity = 54},
	{model = 'feltzer2',maxCapacity = 50},

	--SUV
	{model = 'MOONBEAM2',maxCapacity = 50},
	{model = 'granger',maxCapacity = 170},
	{model = 'fq2',maxCapacity = 53},
	{model = 'rocoto',maxCapacity = 100},
	{model = 'seminole',maxCapacity = 100},
	{model = 'xls',maxCapacity = 80},
	{model = 'mesa',maxCapacity = 95},
	{model = 'baller2',maxCapacity = 110},
	{model = 'gresley',maxCapacity = 95},
	{model = 'huntley',maxCapacity = 85},
	{model = 'landstalker',maxCapacity = 95},
	{model = 'patriot',maxCapacity = 115},
	{model = 'mesa3',maxCapacity = 95},
	{model = 'baller3',maxCapacity = 110},
	{model = 'radi',maxCapacity = 56},
	{model = 'dubsta',maxCapacity = 86},
	{model = 'cavalcade2',maxCapacity = 105},
	{model = 'contender',maxCapacity = 175},
	{model = 'dubsta2',maxCapacity = 86},


	--Muscle
	{model = 'BLADE',maxCapacity = 80},
	{model = 'COQUETTE3',maxCapacity = 80},
	{model = 'DOMINATO',maxCapacity = 20},
	{model = 'GAUNTLET',maxCapacity = 80},
	{model = 'HOTKNIFE',maxCapacity = 80},
	{model = 'FACTION',maxCapacity = 100},
	{model = 'FACTION2',maxCapacity = 100},
	{model = 'FACTION3',maxCapacity = 100},
	{model = 'PHOENIX',maxCapacity = 20},
	{model = 'SABREGT',maxCapacity = 100},
	{model = 'SABREGT2',maxCapacity = 100},
	{model = 'TAMPA',maxCapacity = 80},
	{model = 'VIRGO',maxCapacity = 80},
	{model = 'VIRGO2',maxCapacity = 80},
	{model = 'VIRGO3',maxCapacity = 80},
	{model = 'VOODOO',maxCapacity = 100},
	{model = 'VOODOO2',maxCapacity = 100},
	{model = 'NIGHTSHADE',maxCapacity = 30},
	{model = 'PICADOR',maxCapacity = 90},
	{model = 'SLAMVAN3',maxCapacity = 85},
	{model = 'BUCCANEER',maxCapacity = 35},
	{model = 'BUCCANEER2',maxCapacity = 30},
	{model = 'CHINO',maxCapacity = 45},
	{model = 'CHINO2',maxCapacity = 40},
	{model = 'virgo2',maxCapacity = 40},
	{model = 'tornado',maxCapacity = 30},
	{model = 'tornado2',maxCapacity = 35},

	--Off Road
	{model = 'BIFTA',maxCapacity = 10},
	{model = 'BFINJECT',maxCapacity = 20},
	{model = 'DUBSTA3',maxCapacity = 100},
	{model = 'DUNE',maxCapacity = 10},
	{model = 'GUARDIAN',maxCapacity = 150},
	{model = 'REBEL2',maxCapacity = 80},
	{model = 'REBEL2',maxCapacity = 80},
	{model = 'SANDKING',maxCapacity = 100},
	{model = 'SANDKING2',maxCapacity = 100},
	{model = 'MONSTER',maxCapacity = 20},
	{model = 'TROPHYTRUCK',maxCapacity = 20},
	{model = 'TROPHYTRUCK2',maxCapacity = 20},

	--Compact
	{model = 'BLISTA',maxCapacity = 30},
	{model = 'ISSI2',maxCapacity = 45},
	{model = 'PANTO',maxCapacity = 20},
	{model = 'PRAIRIE',maxCapacity = 80},
	{model = 'BRIOSO',maxCapacity = 30},
	{model = 'kalahari',maxCapacity = 46},

	--Moto
	{model = 'AKUMA',maxCapacity = 5},
	{model = 'AVARUS',maxCapacity = 5},
	{model = 'BAGGER',maxCapacity = 5},
	{model = 'BATI',maxCapacity = 5},
	{model = 'BATI2',maxCapacity = 5},
	{model = 'BF400',maxCapacity = 5},
	{model = 'CARBONRS',maxCapacity = 5},
	{model = 'CHIMERA',maxCapacity = 5},
	{model = 'CLIFFHANGER',maxCapacity = 5},
	{model = 'DAEMON',maxCapacity = 5},
	{model = 'DAEMON2',maxCapacity = 5},
	{model = 'DEFILER',maxCapacity = 5},
	{model = 'DOUBLE',maxCapacity = 5},
	{model = 'ENDURO',maxCapacity = 5},
	{model = 'ESSKEY',maxCapacity = 5},
	{model = 'FAGGIO',maxCapacity = 5},
	{model = 'GARGOYLE',maxCapacity = 5},
	{model = 'HAKUCHOU',maxCapacity = 5},
	{model = 'HAKUCHOU2',maxCapacity = 5},
	{model = 'HEXER',maxCapacity = 5},
	{model = 'INNOVATION',maxCapacity = 5},
	{model = 'MANCHEZ',maxCapacity = 5},
	{model = 'NEMESIS',maxCapacity = 5},
	{model = 'NIGHTBLADE',maxCapacity = 5},
	{model = 'SANCHEZ',maxCapacity = 5},
	{model = 'SANCHEZ2',maxCapacity = 5},
	{model = 'SANCTUS',maxCapacity = 5},
	{model = 'SOVEREIGN',maxCapacity = 5},
	{model = 'THRUST',maxCapacity = 5},
	{model = 'VADER',maxCapacity = 5},
	{model = 'FAGGIO2',maxCapacity = 5},
	{model = 'VORTEX',maxCapacity = 5},
	{model = 'WOLFSBANE',maxCapacity = 5},
	{model = 'ZOMBIEA',maxCapacity = 5},
	{model = 'ZOMBIEB',maxCapacity = 5},
	{model = 'PCJ',maxCapacity = 5},
	{model = 'RUFFIAN',maxCapacity = 5},
	{model = 'fcr',maxCapacity = 5},
	{model = 'fcr2',maxCapacity = 5},
	{model = 'diablous',maxCapacity = 5},
	{model = 'diablous2',maxCapacity = 5},

	--Coupe
	{model = 'COGCABRIO',maxCapacity = 85},
	{model = 'EXEMPLAR',maxCapacity = 30},
	{model = 'F620',maxCapacity = 45},
	{model = 'FELON',maxCapacity = 95},
	{model = 'FELON2',maxCapacity = 45},
	{model = 'JACKAL',maxCapacity = 65},
	{model = 'ORACLE2',maxCapacity = 65},
	{model = 'SENTINEL',maxCapacity = 85},
	{model = 'SENTINEL2',maxCapacity = 85},
	{model = 'WINDSOR',maxCapacity = 100},
	{model = 'WINDSOR2',maxCapacity = 100},
	{model = 'ZION',maxCapacity = 70},
	{model = 'ZION2',maxCapacity = 60},

	--Sedans
	{model = 'ASEA',maxCapacity = 60},
	{model = 'COGNOSCENTI',maxCapacity = 85},
	{model = 'EMPEROR',maxCapacity = 100},
	{model = 'FUGITIVE',maxCapacity = 60},
	{model = 'GLENDALE',maxCapacity = 80},
	{model = 'INTRUDER',maxCapacity = 40},
	{model = 'PREMIER',maxCapacity = 55},
	{model = 'PRIMO2',maxCapacity = 85},
	{model = 'REGINA',maxCapacity = 130},
	{model = 'SCHAFTER',maxCapacity = 60},
	{model = 'STRETCH',maxCapacity = 100},
	{model = 'SUPERD',maxCapacity = 90},
	{model = 'TAILGATER',maxCapacity = 70},
	{model = 'WARRENER',maxCapacity = 70},
	{model = 'WASHINGTON',maxCapacity = 80},
	{model = 'SCHWARZER',maxCapacity = 55},

	--Vans
	{model = 'BISON',maxCapacity = 150},
	{model = 'BOBCATXL',maxCapacity = 175},
	{model = 'BURRITO3',maxCapacity = 230},
	{model = 'CAMPER',maxCapacity = 275},
	{model = 'GBURRITO',maxCapacity = 230},
	{model = 'GBURRITO2',maxCapacity = 230},
	{model = 'JOURNEY',maxCapacity = 200},
	{model = 'MINIVAN',maxCapacity = 100},
	{model = 'MOONBEAM',maxCapacity = 110},
	{model = 'MOONBEAM2',maxCapacity = 100},
	{model = 'PARADISE',maxCapacity = 250},
	{model = 'RUMPO',maxCapacity = 230},
	{model = 'RUMPO3',maxCapacity = 250},
	{model = 'SURFER',maxCapacity = 175},
	{model = 'YOUGA',maxCapacity = 200},
	{model = 'YOUGA2',maxCapacity = 200},
	{model = 'minivan2',maxCapacity = 75},
	{model = 'ratloader',maxCapacity = 260},
	{model = 'ratloader2',maxCapacity = 260},

	-- truck
	{model = 'MULE',maxCapacity = 700},	
	{model = 'BENSON',maxCapacity = 720},
	{model = 'PHANTOM',maxCapacity = 50},
	{model = 'BARRACKS3',maxCapacity = 600},
	{model = 'carfish',maxCapacity = 200},

	--Traile
	{model = 'TANKER',maxCapacity = 900},
	{model = 'TRAILER',maxCapacity = 750},
	{model = 'KURUMA2',maxCapacity = 60},
   
	{model = 'BRICKADE',maxCapacity = 950},--brickade
	{model = 'DUNE4',maxCapacity = 10},
	{model = 'DUKE2',maxCapacity = 65},
	{model = 'HAULER2',maxCapacity = 110},
	{model = 'BALLER5',maxCapacity = 80},
	{model = 'BALLER6',maxCapacity = 90},
	{model = 'SCHAFTER3',maxCapacity = 80},
	{model = 'SCHAFTER5',maxCapacity = 75},
	{model = 'COGNOSCENTI2',maxCapacity = 76},--cognoscenti
	{model = 'XLS2',maxCapacity = 85},
	{model = 'INSURGENT2',maxCapacity = 153},   -- dernier sans virgule 
	
	-- Bateau
	{model = 'TUG',maxCapacity = 200}
	
}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
--	TriggerServerEvent("GetAllPersonalVeh")
end)
RegisterNetEvent('esx_inv_veh:getPlayerInv')
AddEventHandler('esx_inv_veh:getPlayerInv',function(inv)
	inv = inventory
end)

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if IsControlJustPressed(0, Keys['F10']) then
			--TriggerServerEvent('esx_inv_veh:getPlayerInv',source)
			
			GetVehicle()

		end
	end
end)
--function

function DisplayHelpText(str)
  SetTextComponentFormat("STRING")
  AddTextComponentString(str)
  DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end
function GetVehicleInDirection( coordFrom, coordTo )
    local rayHandle = CastRayPointToPoint( coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed( -1 ), 0 )
    local _, _, _, _, vehicle = GetRaycastResult( rayHandle )
    return vehicle
end

function notification(message)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(message)
	DrawNotification(0,1)
end
function depositweapon()
	local selectitem = nil
	

  		
    local elements = {}
		local playerPed  = GetPlayerPed(-1)
		local weaponList = ESX.GetWeaponList()
		local ammo = 0
		for i=1, #weaponList, 1 do

			local weaponHash = GetHashKey(weaponList[i].name)

			if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
				ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
				table.insert(elements, {label = weaponList[i].label , value = weaponList[i].name, ammo = ammo})
			end

		end
    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'depositweapon',
      {
        title    = 'Votre inventaire',
        align 	 = 'top-left',
        elements = elements
      },
      function(data, menu)
        local weaponName = data.current.value
        TriggerServerEvent('esx_inv_veh:getPlate',plate)
		TriggerServerEvent('esx_inv_veh:putWeaponIntoVehicle',weaponName,ammo)		
		menu.close()	
      end,
      function(data, menu)
        menu.close()
 
      end
    

  	)
end

function depositItem()
	

	local selectitem = nil
	

 	 ESX.TriggerServerCallback('esx_inv_veh:getPlayerInv', function(inventory)
  		
    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name , countMax = item.count})
      end

    end
       TriggerServerEvent('esx_inv_veh:getPlate',plate)
       ESX.TriggerServerCallback('esx_inv_veh:getVehicleInv', function(inventory)
			useCapacity = 0
		   for i=1, #inventory.items, 1 do
				 local item = inventory.items[i]
				 useCapacity = useCapacity + item.quantity
			end
		end)


    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu2',
      {
        title    = 'Votre inventaire (véhicule: '..useCapacity..'/'..maxCapacity..')' ,
        align 	 = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        local quantity = data.current.countMax
        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count2',
          {
            title = 'quantité'
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil or count < 0 or count > quantity then
              ESX.ShowNotification('quantité invalide')
            else
            		
					    useCapacity = useCapacity + count
					    --ESX.ShowNotification('use capacity: '..useCapacity..' maxCapacity: '.. maxCapacity)
					   	if useCapacity <= maxCapacity then
					   		if GetDistanceBetweenCoords(GetEntityCoords(v, true), GetEntityCoords(GetPlayerPed(-1), true), true) < 5 then
            					TriggerServerEvent('esx_inv_veh:putItemIntoVehicle',plate,itemName, count,useCapacity,maxCapacity)
            				end
            			else
            				 ESX.ShowNotification("Ce véhicule ne possede que "..maxCapacity.." places")
            			end
			
				
            	 menu2.close()
            	menu.close()
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)
end
function deposit()
	local elements = {}
	table.insert(elements, {label = 'Depot d\'arme',             					value = "weapon"})
		table.insert(elements, {label = 'Depot d\'item',             					value = "item"})
 ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = 'Votre inventaire',
        align 	 = 'top-left',
        elements = elements
      },
      	function(data, menu)
      		if data.current.value == "weapon" then
      			depositweapon()
      		else
				depositItem()
	 		end
	 	end,
	 	function(data, menu)
	 		menu.close()
	  	end)
end

function withdrawItem()
	TriggerServerEvent('esx_inv_veh:getPlate',plate)
	ESX.TriggerServerCallback('esx_inv_veh:getVehicleInv', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]
  		if item ~= nil then
        	table.insert(elements, {label = item.items .. ' x' .. item.quantity, type = 'item_standard', value = item.items})
      	end

    end
    local random = math.random(0,1500)
    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu52',
      {
        title    = 'Inventaire véhicule',
        align 	 = 'top-left',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count42',
          {
            title = 'quantité'
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil or count == 0 or count < 0 then
              ESX.ShowNotification('quantité invalide')
            else
              menu2.close()
              menu.close()
             	Citizen.CreateThread(function()
             	 Wait(random)
             	end)
                if GetDistanceBetweenCoords(GetEntityCoords(v, true), GetEntityCoords(GetPlayerPed(-1), true), true) < 5 then 
         			TriggerServerEvent('esx_inv_veh:takeItemFromVehicle',plate,itemName, count)
         		end
            end
          end,
          function(data2, menu2)
            menu2.close()
          end
        )
      end,
      function(data, menu)
        menu.close()
      end
    )

  end)
end

function withdrawWeapon()
	TriggerServerEvent('esx_inv_veh:getPlate',plate)
	ESX.TriggerServerCallback('esx_inv_veh:getVehicleWeapon', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]
  		if item ~= nil then
        	table.insert(elements, {label = ESX.GetWeaponLabel(item.items) , value = item.items})
      	end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'withdrawWeapon',
      {
        title    = 'Inventaire véhicule',
        align 	 = 'top-left',
        elements = elements
      },
      function(data, menu)
      	local random = math.random(0,1500)
      	Citizen.CreateThread(function()
             Wait(random)
        end)
        local itemName = data.current.value
        TriggerServerEvent('esx_inv_veh:takeweaponFromVehicle',plate,itemName, count)
        menu.close()
      end,
      function(data, menu)
        menu.close()
      end
    )

  end)
end

function GetVehicle()
	local playerPos = GetEntityCoords( GetPlayerPed(-1), 1 )
		local x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(PlayerId())))
		local inFrontOfPlayer = GetOffsetFromEntityInWorldCoords( GetPlayerPed(-1), 0.0, 10.000, 0.0 )
	    v = GetVehicleInDirection( playerPos, inFrontOfPlayer )
		if DoesEntityExist(v) then
			local isClose = GetVehicleDoorLockStatus(v)
			if isClose == 1 or isClose == 0 then
				SetVehicleDoorOpen(v, 5, false, false)
				SetPedCanPlayGestureAnims(GetPlayerPed(-1), false)
				ESX.UI.Menu.CloseAll()
				plate = GetVehicleNumberPlateText(v)
				local model = GetDisplayNameFromVehicleModel(GetEntityModel(v))
				local found = false
				for i = 1 , #config ,1 do
					
					if config[i].model == model  then
						maxCapacity = config[i].maxCapacity
						found = true
					end
				end
				if not found then
					maxCapacity = defaultCapacity
				end
				local elements = {}

			

				table.insert(elements, {label = 'déposer',             					value = "depositItem"})
				table.insert(elements, {label = 'Récupérer',             					value = "withdrawItem"})
			
				ESX.UI.Menu.Open(
					'default', GetCurrentResourceName(), 'Inventory',
					{
						title = 'inventaire véhicule ('..maxCapacity..' places)',
						align = 'top-left',
						elements = elements
					},
				    function(data, menu) --Submit Cb
				    	if data.current.value == "depositItem" then
				    		deposit()
				    	elseif data.current.value == "withdrawItem" then
		                	withdraw()
		                end
		        	end,
		       		function(data, menu) --Cancel Cb
		                menu.close()
		                SetVehicleDoorShut(v, 5, false)
		                SetPedCanPlayGestureAnims(GetPlayerPed(-1), true)
		        	end,
		       		function(data, menu) --Change Cb
		                print(data.current.value)
		       		end
				)
			else
				ESX.ShowNotification("le véhicule est fermé")
			end
		else
			ESX.ShowNotification("Aucun véhicule proche") 
		end
end

function withdraw()
		local elements = {}
	table.insert(elements, {label = 'Récupéré arme',             					value = "weapon"})
		table.insert(elements, {label = 'Récupéré item',             					value = "item"})
 ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = 'Votre inventaire',
        align 	 = 'top-left',
        elements = elements
      },
      	function(data, menu)
      		if data.current.value == "weapon" then
      			withdrawWeapon()
      		else
				withdrawItem()
	 		end
	 	end,
	 	function(data, menu)
	 		menu.close()
	  	end)
end