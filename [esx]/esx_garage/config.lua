Config                     = {}
Config.DrawDistance        = 30.0
Config.MarkerType          = 1
Config.MarkerSize          = {x = 2.0, y = 2.0, z = 1.0}
Config.MarkerColor         = {r = 0, g = 128, b = 255}
Config.ParkingMarkerSize   = {x = 2.5, y = 2.5, z = 1.5}
Config.ParkingMarkerColor  = {r = 0, g = 128, b = 255}
Config.ZDiff               = 0.5
Config.EnableOwnedVehicles = true
Config.MinimumHealthPercent = 90

Config.Locale = 'fr'

Config.Zones = {}

Config.Garages = {

	MiltonDrive = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x= -796.542, y = 318.137, z = 84.673},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = -796.501, y = 302.271, z = 85.000},
			Heading = 180.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = 228.930, y = -1000.698, z = -100.000},
			Heading = 0.0
		},

		InteriorExitPoint = {
			Pos = {x = 224.613, y = -1004.769, z = -100.000},
		},

		Parkings = {
			{
				Pos     = {x = 224.500, y = -998.695, z = -100.000},
				Heading = 225.0
			},
		  {
				Pos     = {x = 224.500, y = -994.630, z = -100.000},
				Heading = 225.0
			},
		  {
				Pos     = {x = 224.500, y = -990.255, z = -100.000},
				Heading = 225.0
			},
		  {
				Pos     = {x = 224.500, y = -986.628, z = -100.000},
				Heading = 225.0
			},
		  {
				Pos     = {x = 224.500, y = -982.496, z = -100.000},
				Heading = 225.0
			},
		  {
				Pos     = {x = 232.500, y = -982.496, z = -100.000},
				Heading = 135.0
			},
		  {
				Pos     = {x = 232.500, y = -986.628, z = -100.000},
				Heading = 135.0
			},
		  {
				Pos     = {x = 232.500, y = -990.255, z = -100.000},
				Heading = 135.0
			},
		  {
				Pos     = {x = 232.500, y = -994.630, z = -100.000},
				Heading = 135.0
			},
		  {
				Pos     = {x = 232.500, y = -998.695, z = -100.000},
				Heading = 135.0
			},
		}

	},
	
	GroveStreet = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x= -69.0, y = -1824.0, z = 25.9},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = -59.0, y = -1842.0, z = 25.9},
			Heading = 276.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = 202.0, y = -1005.0, z = -100.0},
			Heading = 320.0
		},

		InteriorExitPoint = {
			Pos = {x = 194.0, y = -1005.0, z = -100.0},
		},

		Parkings = {
			{
				Pos     = {x = 192.0, y = -997.0, z = -100.000},
				Heading = 180.0
			},
		  {
				Pos     = {x = 196.0, y = -997.0, z = -100.000},
				Heading = 180.0
			},
		  {
				Pos     = {x = 200.0, y = -997.0, z = -100.000},
				Heading = 180.0
			},
		  {
				Pos     = {x = 204.0, y = -997.0, z = -100.000},
				Heading = 180.0
			},
		}

	},
	
	SwissStreet = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x= 466.4, y = -576.8, z = 27.4},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = 471.2, y = -579.1, z = 27.0},
			Heading = 173.9
		},

		InteriorSpawnPoint = {
			Pos     = {x = 197.0, y = -1022.0, z = -100.000},
			Heading = 320.0
		},

		InteriorExitPoint = {
			Pos = {x = 194.0, y = -1025.0, z = -100.0},
		},

		Parkings = {
			{
				Pos     = {x = 193.0, y = -1016.0, z = -100.000},
				Heading = 180.0
			},
		  {
				Pos     = {x = 197.0, y = -1016.0, z = -100.000},
				Heading = 180.0
			},
		  {
				Pos     = {x = 201.0, y = -1017.0, z = -100.000},
				Heading = 145.0
			},
		  {
				Pos     = {x = 203.0, y = -1020.0, z = -100.000},
				Heading = 90.0
			},
		  {
				Pos     = {x = 203.0, y = -1024.0, z = -100.000},
				Heading = 90.0
			},
		}

	},
	
	DelPerroFreeway = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x = -1351.0, y = -750.5, z = 21.3},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = -1350.0, y = -755.1, z = 21.3},
			Heading = 276.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = -1359.4, y = -755.2, z = 21.3},
			Heading = 128.6
		},

		InteriorExitPoint = {
			Pos = {x = -1355.2, y = -754.1, z = 21.3},
		},
		InteriorExitPoint2 = {
			Pos = {x = -1354.2, y = -755.4, z = 21.3},
		},
		InteriorExitPoint3 = {
			Pos = {x = -1356.3, y = -752.6, z = 21.3},
		},

		Parkings = {
			{
				Pos     = {x = -1358.0, y = -757.5, z = 21.3},
				Heading = 306.2
			},
		}

	},
	
	ZancudoAvenue = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x = 1967.0, y = 3821.0, z = 30.9},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = 1976.0, y = 3826.0, z = 30.9},
			Heading = 300.0
		},

		InteriorSpawnPoint = {
			Pos     = {x = 175.0, y = -1003.8, z = -100.0},
			Heading = 180.0
		},

		InteriorExitPoint = {
			Pos = {x = 172.0, y = -1007.6, z = -100.0},
		},

		Parkings = {
			{
				Pos     = {x = 171.0, y = -1002.89, z = -100.0},
				Heading = 180.0
			},
		}

	},
	SanAndreasAvenue = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x = -80.42, y = -780.9, z = 37.5},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = -82.32, y = -772.91, z = 38.8},
			Heading = 15.65
		},

		InteriorSpawnPoint = {
			Pos     = {x = -79.34, y = -798.18, z = 36.0},
			Heading = 168.3
		},

		InteriorExitPoint = {
			Pos = {x = -79.0, y = -786.3, z = 37.35},
		},
		InteriorExitPoint2 = {
			Pos = {x = -76.7, y = -785.6, z = 37.35},
		},
		InteriorExitPoint3 = {
			Pos = {x = -81.9, y = -786.9, z = 37.35},
		},

		Parkings = {
			{
				Pos     = {x = -71.2, y = -805.3, z = 35.4},
				Heading = 56.0
			},
			{
				Pos     = {x = -66.6, y = -810.60, z = 34.8},
				Heading = 123.5
			},
			{
				Pos     = {x = -64.9, y = -813.9, z = 34.7},
				Heading = 115.34
			},
			{
				Pos     = {x = -63.2, y = -817.5, z = 34.7},
				Heading = 107.12
			},
			{
				Pos     = {x = -63.0, y = -821.1, z = 34.6},
				Heading = 96.7
			},
			{
				Pos     = {x = -62.7, y = -824.86, z = 34.6},
				Heading = 81.83
			},
			{
				Pos     = {x = -63.5, y = -828.89, z = 34.6},
				Heading = 74.0
			},
			{
				Pos     = {x = -64.2, y = -832.3, z = 34.0},
				Heading = 63.7
			},
			{
				Pos     = {x = -66.4, y = -835.25, z = 34.0},
				Heading = 55.75
			},
			{
				Pos     = {x = -69.18, y = -838.37, z = 33.8},
				Heading = 43.25
			},
			{
				Pos     = {x = -71.7, y = -840.6, z = 33.5},
				Heading = 34.1
			},
			{
				Pos     = {x = -75.08, y = -842.53, z = 33.6},
				Heading = 21.72
			},
			{
				Pos     = {x = -78.78, y = -843.86, z = 33.5},
				Heading = 14.88
			},
			{
				Pos     = {x = -82.30, y = -844.5, z = 33.4},
				Heading = 4.7
			},
			{
				Pos     = {x = -85.85, y = -844.41, z = 33.3},
				Heading = 352.87
			},
			{
				Pos     = {x = -89.94, y = -843.78, z = 33.3},
				Heading = 345.89
			},
			{
				Pos     = {x = -93.09, y = -842.3, z = 33.3},
				Heading = 336.0
			},
			{
				Pos     = {x = -96.43, y = -840.77, z = 33.48},
				Heading = 324.15
			},
			{
				Pos     = {x = -99.43, y = -838.18, z = 33.5},
				Heading = 314.24
			},
			{
				Pos     = {x = -101.6, y = -835.4, z = 33.5},
				Heading = 305.5
			},
			{
				Pos     = {x = -103.73, y = -832.41, z = 33.8},
				Heading = 292.32
			},
			{
				Pos     = {x = -105.13, y = -828.53, z = 33.9},
				Heading = 283.76
			},
			{
				Pos     = {x = -106.0, y = -824.8, z = 34.0},
				Heading = 271.6
			},
			{
				Pos     = {x = -105.58, y = -821.46, z = 34.3},
				Heading = 263.0
			},
			{
				Pos     = {x = -105.0, y = -817.42, z = 34.5},
				Heading = 256.46
			},
			{
				Pos     = {x = -103.6, y = -814.1, z = 34.5},
				Heading = 244.8
			},
			{
				Pos     = {x = -102.0, y = -810.87, z = 34.8},
				Heading = 233.81
			},
			{
				Pos     = {x = -99.4, y = -807.80, z = 35.0},
				Heading = 224.84
			},
			{
				Pos     = {x = -96.7, y = -805.34, z = 35.0},
				Heading = 212.3
			},
			{
				Pos     = {x = -89.9, y = -801.85, z = 35.4},
				Heading = 281.92
			},
			--Cercle intérieur 
			{
				Pos     = {x = -75.07, y = -819.04, z = 34.7},
				Heading = 293.81
			},
			{
				Pos     = {x = -74.29, y = -822.67, z = 34.5},
				Heading = 271.10
			},
			{
				Pos     = {x = -74.95, y = -826.48, z = 34.3},
				Heading = 250.38
			},
			{
				Pos     = {x = -77.04, y = -829.85, z = 34.1},
				Heading = 227.28
			},
			{
				Pos     = {x = -80.24, y = -832.17, z = 34.0},
				Heading = 206.75
			},
			{
				Pos     = {x = -84.02, y = -832.95, z = 33.95},
				Heading = 182.6
			},
			{
				Pos     = {x = -87.87, y = -832.40, z = 33.95},
				Heading = 157.84
			},
			{
				Pos     = {x = -91.19, y = -830.36, z = 34.02},
				Heading = 139.25
			},
			{
				Pos     = {x = -93.40, y = -827.22, z = 34.15},
				Heading = 118.34
			},
			{
				Pos     = {x = -94.38, y = -823.22, z = 34.33},
				Heading = 91.65
			},
			{
				Pos     = {x = -93.75, y = -819.27, z = 34.52},
				Heading = 68.40
			},
			{
				Pos     = {x = -91.5, y = -816.04, z = 34.69},
				Heading = 46.18
			},
			
		},
	},
	PaletoBoulevard = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x = 117.0, y = 6620.5, z = 30.9},
		},

		ExteriorSpawnPoint = {
			Pos     = {x = 119.1, y = 6617.5, z = 30.4},
			Heading = 222.8
		},

		InteriorSpawnPoint = {
			Pos     = {x = 111.5, y = 6627.7, z = 30.3},
			Heading = 45.2
		},

		InteriorExitPoint = {
			Pos = {x = 114.44, y = 6623.10, z = 30.7},
		},
		
		InteriorExitPoint2 = {
			Pos = {x = 115.5, y = 6624.50, z = 30.7},
		},
		
		InteriorExitPoint3 = {
			Pos = {x = 113.11, y = 6622.22, z = 30.7},
		},
		
		InteriorExitPoint4 = {
			Pos = {x = 109.9, y = 6620.3, z = 30.7},
		},
		
		InteriorExitPoint5 = {
			Pos = {x = 108.8, y = 6621.03, z = 30.7},
		},

		Parkings = {
			{
				Pos     = {x = 109.0, y = 6626.1, z = 30.3},
				Heading = 224.3
			},
		},

	},
}
