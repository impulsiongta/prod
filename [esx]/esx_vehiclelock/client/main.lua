ESX               = nil
local playerCars = {}
local key = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

AddEventHandler('esx_key:getVehiclesKey', function(newkey) -- Sorti du garage
	local i = #key
	local duplicatekey = false
	while i>=0 do
		Wait(50)
		if newkey == key[i] then
			duplicatekey = true
			break
		end
		i = i -1
	end
	if duplicatekey == false then
		table.insert(key,newkey)
		TriggerEvent('esx_key:giveVehiclesKey', key)
	end
end)
AddEventHandler('giveKeyAtKeyMaster', function(newkey) -- donner clé
 	table.insert(key, newkey)
end)
-- 
function OpenCloseVehicle()
	local playerPed = GetPlayerPed(-1)
	local coords    = GetEntityCoords(playerPed)

	local vehicle = nil

	if IsPedInAnyVehicle(playerPed,  false) then
		vehicle = GetVehiclePedIsIn(playerPed, false)
	else
		vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 7.0, 0, 70)
	end

	ESX.TriggerServerCallback('esx_vehiclelock:requestPlayerCars', function(isOwnedVehicle)

		if isOwnedVehicle then
			local locked = GetVehicleDoorLockStatus(vehicle)
			if locked == 1 then -- if unlocked
				SetVehicleDoorsLocked(vehicle, 2)
				PlayVehicleDoorCloseSound(vehicle, 1)
				ESX.ShowNotification("Votre véhicule est désormais ~r~~h~fermé.")
				TriggerEvent('esx_key:getVehiclesKey', GetVehicleNumberPlateText(vehicle))
			elseif locked == 2 then -- if locked
				SetVehicleDoorsLocked(vehicle, 1)
				PlayVehicleDoorOpenSound(vehicle, 0)
				ESX.ShowNotification("Votre véhicule est désormais ~g~~h~ouvert.")
				TriggerEvent('esx_key:getVehiclesKey', GetVehicleNumberPlateText(vehicle))
			end
		else
			local currentPlate = GetVehicleNumberPlateText(vehicle)
         
              	local i = #key
         		--Citizen.Trace(key[1].."  "..currentPlate)
              	while i > 0 do -- ouais ouasi boucle while j'ai un bug chelou avec la for mais la while ca marche nickel
              		Wait(50)
              		if key[i] == currentPlate then
              			local lockStatus = GetVehicleDoorLockStatus(vehicle)
              			if lockStatus == 1 or lockStatus == 0 then
              				lockStatus = SetVehicleDoorsLocked(vehicle, 2)
							PlayVehicleDoorCloseSound(vehicle, 1)
                       		ESX.ShowNotification("Votre véhicule est désormais ~r~~h~fermé.")
              			else
              				lockStatus = SetVehicleDoorsLocked(vehicle, 1)
							PlayVehicleDoorOpenSound(vehicle, 0)
              				ESX.ShowNotification("Votre véhicule est désormais ~g~~h~ouvert.")
              			end
              			break
              		end
              		i = i-1
				end
		end
	end, GetVehicleNumberPlateText(vehicle))
end

Citizen.CreateThread(function()
	while true do
		Wait(0)
		if IsControlJustReleased(0, 303) then
			OpenCloseVehicle()
		end
	end
end)