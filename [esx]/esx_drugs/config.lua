Config              = {}
Config.MarkerType   = 1
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 5.0, y = 5.0, z = 3.0}
--Config.MarkerColor  = {r = 100, g = 204, b = 100}
Config.RequiredCopsCoke = 2
Config.RequiredCopsMeth = 2
Config.RequiredCopsMeth2 = 1
Config.RequiredCopsMeth3 = 2
Config.RequiredCopsWeed = 1
Config.RequiredCopsOpium = 2
Config.RequiredCopsOpium2 = 2
Config.RequiredCopsOpium3 = 3
Config.Locale = 'fr'

Config.Zones = {
	CokeFarm = 	{x=3631.07,  y=3739.01, z=28.69},
	CokeTreatment = {x=3561.06, y=3672.6, z=28.12},
	CokeResell = 	{x=-476.91, y=-2688.56,  z=8.76},

	MethFarm = 	{x=2589.91, y=4678.42, z=33.0768},
	MethTreatment = {x=1078.3, y=-2334.11, z=29.2681},
	MethResell = 	{x=-291.23, y=6184.78, z=30.4891},

	WeedFarm = 	{x=2225.16, y=5577.41, z=53.83},
	WeedTreatment = {x=2482.27, y=3725.74, z=43.51},
	WeedResell = 	{x=-1168.78, y=-1572.52, z=4.66},

	OpiumFarm = 	{x=841.27, y=-502.89, z=57.11},
	OpiumTreatment ={x=1218.24, y=2396.94, z=66.07},
	OpiumResell = 	{x=93.84, y=-1292.26, z=29.27},
}
