ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

------------------------------NOURRITURES--------------------------------------
ESX.RegisterUsableItem('bread', function(source)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('bread', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_bread'))

end)

ESX.RegisterUsableItem('tacos', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('tacos', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:sandwich', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_sandwich'))
	
end)

ESX.RegisterUsableItem('gateau', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('gateau', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:donuts', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_gateau'))
	
end)

ESX.RegisterUsableItem('sushi', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('sushi', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:sandwich', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_sushi'))
end)

ESX.RegisterUsableItem('plateau_traiteur', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('plateau_traiteur', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:sandwich', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_plateau_traiteur'))
end)

ESX.RegisterUsableItem('gambas', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('gambas', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:sandwich', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_gambas'))
end)

ESX.RegisterUsableItem('makki', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('makki', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:sandwich', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_makki'))
end)

ESX.RegisterUsableItem('plateau_fruit_mer', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('plateau_fruit_mer', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 400000)
	TriggerClientEvent('esx_basicneeds:sandwich', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_plateau_fruit_mer'))
end)

ESX.RegisterUsableItem('pain_complet', function(source)
	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('pain_complet', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 300000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_pain_complet'))
end)

ESX.RegisterUsableItem('cookies', function(source)
	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('cookies', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:donuts', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_cookie'))	
end)

ESX.RegisterUsableItem('donuts', function(source)
	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('donuts', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:donuts', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_donut'))
end)

ESX.RegisterUsableItem('croissant', function(source)
	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('croissant', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:donuts', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_croissant'))
end)

ESX.RegisterUsableItem('pizza', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('pizza', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 800000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_pizza'))
end)

ESX.RegisterUsableItem('entree', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('entree', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_entree'))
end)

ESX.RegisterUsableItem('plat', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('plat', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 600000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_plat'))
end)

ESX.RegisterUsableItem('dessert', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('dessert', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 200000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_dessert'))
end)

---------------------------BOISSONS----------------------------------------

ESX.RegisterUsableItem('water', function(source)

	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('water', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_water'))

end)

ESX.RegisterUsableItem('eau_minerale', function(source)
	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('eau_minerale', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 300000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_water'))
end)

ESX.RegisterUsableItem('bieresansalcool', function(source)
	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('bieresansalcool', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_basicneeds:bieresansalcool', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_biere_sans_alcool'))
end)


ESX.RegisterUsableItem('coca', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('coca', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:coca', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_coca'))
end)


ESX.RegisterUsableItem('redbull', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('redbull', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:redbull', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_redbull'))
end)

ESX.RegisterUsableItem('jus_de_fruit', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('jus_de_fruit', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_jus_de_fruit'))
end)

ESX.RegisterUsableItem('pur_jus', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('pur_jus', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_pur_jus'))
end)

ESX.RegisterUsableItem('jus_de_raisin', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('jus_de_raisin', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_jus_de_raisin'))
end)

ESX.RegisterUsableItem('cafe', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('cafe', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 150000)
	TriggerClientEvent('esx_basicneeds:cafe', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_cafe'))
end)

ESX.RegisterUsableItem('lait', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('lait', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_status:add', source, 'hunger', 50000)
	TriggerClientEvent('esx_basicneeds:cafe', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_lait'))
end)

----------------------------ALCOOL---------------------------------------

ESX.RegisterUsableItem('vodka', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('vodka', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 200000)
	TriggerClientEvent('esx_basicneeds:vodka', source, 7*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_vodka'))
end)

ESX.RegisterUsableItem('punch', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('punch', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 200000)
	TriggerClientEvent('esx_basicneeds:mojito', source, 7*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_punch'))
end)

ESX.RegisterUsableItem('biere', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('biere', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 100000)
	TriggerClientEvent('esx_basicneeds:biere', source, 3*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_biere'))
end)

ESX.RegisterUsableItem('champagne', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('champagne', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 100000)
	TriggerClientEvent('esx_basicneeds:mojito', source, 3*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_champagne'))
end)

ESX.RegisterUsableItem('punch', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('punch', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 100000)
	TriggerClientEvent('esx_basicneeds:mojito', source, 3*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_punch'))
end)

ESX.RegisterUsableItem('whisky', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('whisky', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:whisky', source, 7*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_whisky'))
end)

ESX.RegisterUsableItem('mojito', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('mojito', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 100000)
	TriggerClientEvent('esx_basicneeds:mojito', source, 5*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_mojito'))
end)


ESX.RegisterUsableItem('vin_rouge', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('vin_rouge', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 100000)
	TriggerClientEvent('esx_basicneeds:vin', source, 2*60*1000)
	TriggerClientEvent('esx:showNotification', source, _U('used_vin_rouge'))
end)

------------------------------------AUTRES---------------------------------------------------------

ESX.RegisterUsableItem('cigarette', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('cigarette', 1)

	TriggerClientEvent('esx_status:remove', source, 'thirst', 70000)
	TriggerClientEvent('esx_basicneeds:cigarette', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_cigarette'))
end)

--------------------------------ÉVÉNEMENTS------------------------------------------

ESX.RegisterUsableItem('cacao_chaud', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('cacao_chaud', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 150000)
	TriggerClientEvent('esx_basicneeds:cafe', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_cacao_chaud'))
end)

ESX.RegisterUsableItem('vin_chaud', function(source)

	
	local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)

	xPlayer.removeInventoryItem('vin_chaud', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 70000)
	TriggerClientEvent('esx_basicneeds:cafe', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_vin_chaud'))

end)