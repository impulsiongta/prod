ESX          = nil
local IsDead = false
local IsAnimated = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

AddEventHandler('esx_basicneeds:resetStatus', function()
	TriggerEvent('esx_status:set', 'hunger', 500000)
	TriggerEvent('esx_status:set', 'thirst', 500000)

end)

AddEventHandler('playerSpawned', function()

	if IsDead then
		TriggerEvent('esx_basicneeds:resetStatus')
	end

	IsDead = false
end)

AddEventHandler('esx_status:loaded', function(status)

	TriggerEvent('esx_status:registerStatus', 'hunger', 1000000, '#CFAD0F',
		function(status)
			return true
		end,
		function(status)
			status.remove(6000)
		end
	)

	TriggerEvent('esx_status:registerStatus', 'thirst', 1000000, '#0C98F1',
		function(status)
			return true
		end,
		function(status)
			status.remove(7500)
		end
	)

	Citizen.CreateThread(function()

		while true do

			Wait(1000)

			local playerPed  = GetPlayerPed(-1)
			local prevHealth = GetEntityHealth(playerPed)
			local health     = prevHealth

			TriggerEvent('esx_status:getStatus', 'hunger', function(status)
				
				if status.val == 0 then

					if prevHealth <= 150 then
						health = health - 5
					else
						health = health - 1
					end

				end

			end)

			TriggerEvent('esx_status:getStatus', 'thirst', function(status)
				
				if status.val == 0 then

					if prevHealth <= 150 then
						health = health - 5
					else
						health = health - 1
					end

				end

			end)

			if health ~= prevHealth then
				SetEntityHealth(playerPed,  health)
			end

		end

	end)

	Citizen.CreateThread(function()

		while true do

			Wait(0)

			local playerPed = GetPlayerPed(-1)
			
			if IsEntityDead(playerPed) and not IsDead then
				IsDead = true
			end

		end

	end)

end)

AddEventHandler('esx_basicneeds:isEating', function(cb)
	cb(IsAnimated)
end)

RegisterNetEvent('esx_basicneeds:onEat')
AddEventHandler('esx_basicneeds:onEat', function(prop_name)
    if not IsAnimated then
		local prop_name = prop_name or 'prop_cs_burger_01'
    	IsAnimated = true
	    local playerPed = GetPlayerPed(-1)
	    Citizen.CreateThread(function()
	        local x,y,z = table.unpack(GetEntityCoords(playerPed))
	        prop = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
	        AttachEntityToEntity(prop, playerPed, GetPedBoneIndex(playerPed, 18905), 0.12, 0.028, 0.001, 10.0, 175.0, 0.0, true, true, false, true, 1, true)
	        RequestAnimDict('mp_player_inteat@burger')
	        while not HasAnimDictLoaded('mp_player_inteat@burger') do
	            Wait(0)
	        end
	        TaskPlayAnim(playerPed, 'mp_player_inteat@burger', 'mp_player_int_eat_burger_fp', 8.0, -8, -1, 49, 0, 0, 0, 0)
	        Wait(3000)
	        IsAnimated = false
	        ClearPedSecondaryTask(playerPed)
	        DeleteObject(prop)
	    end)
	end
end)

RegisterNetEvent('esx_basicneeds:onDrink')
AddEventHandler('esx_basicneeds:onDrink', function(prop_name)
	if not IsAnimated then
		local prop_name = prop_name or 'prop_ld_flow_bottle'
		IsAnimated = true
		local playerPed = GetPlayerPed(-1)
		Citizen.CreateThread(function()
			local x,y,z = table.unpack(GetEntityCoords(playerPed))
			prop = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)			
	        AttachEntityToEntity(prop, playerPed, GetPedBoneIndex(playerPed, 57005), 0.12, -0.01, -0.02, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
			RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")  
			while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
				Wait(0)
			end
			TaskPlayAnim(playerPed, "amb@code_human_wander_drinking@male@idle_a", "idle_a",8.0, -8, -1, 49, 0, 0, 0, 0)
			Wait(3000)
	        IsAnimated = false
	        ClearPedSecondaryTask(playerPed)
			DeleteObject(prop)
		end)
	end
end)

--NEW PROPS--
--NORMAL--
--CAF�--
RegisterNetEvent('esx_basicneeds:cafe')
AddEventHandler('esx_basicneeds:cafe', function()
Citizen.CreateThread(function()
  local prop_name = "ng_proc_coffee_01a"
  local pid = PlayerPedId()
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
  local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
  AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.08, -0.04, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
  RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
	while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
		Citizen.Wait(100)
	end
	TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
	Wait(3000)
	ClearPedSecondaryTask(GetPlayerPed(-1))		
	DeleteObject(boisson)
 end)
end)

--BIERE SANS ALCOOL--
RegisterNetEvent('esx_basicneeds:bieresansalcool')
AddEventHandler('esx_basicneeds:bieresansalcool', function()
Citizen.CreateThread(function()
  local prop_name = "prop_cs_beer_bot_01"
  local pid = PlayerPedId()
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
  local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
  AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.01, -0.02, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
  RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
	while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
		Citizen.Wait(100)
	end
	TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
	Wait(3000)
	ClearPedSecondaryTask(GetPlayerPed(-1))		
	DeleteObject(boisson)
 end)
end)

--COCA--
RegisterNetEvent('esx_basicneeds:coca')
AddEventHandler('esx_basicneeds:coca', function()
Citizen.CreateThread(function()
  local prop_name = "ng_proc_sodacan_01a"
  local pid = PlayerPedId()
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
  local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
  AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.08, -0.04, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
  RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
	while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
		Citizen.Wait(100)
	end
	TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
	Wait(3000)
	ClearPedSecondaryTask(GetPlayerPed(-1))		
	DeleteObject(boisson)
 end)
end)

--REDBULL--
RegisterNetEvent('esx_basicneeds:redbull')
AddEventHandler('esx_basicneeds:redbull', function()
Citizen.CreateThread(function()
  local prop_name = "ng_proc_sodacan_01b"
  local pid = PlayerPedId()
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
  local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
  AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.08, -0.04, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
  RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
	while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
		Citizen.Wait(100)
	end
	TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
	Wait(3000)
	ClearPedSecondaryTask(GetPlayerPed(-1))		
	DeleteObject(boisson)
 end)
end)

--SANDWICH--
RegisterNetEvent('esx_basicneeds:sandwich')
AddEventHandler('esx_basicneeds:sandwich', function()
    local prop_name = "prop_food_bs_burger2"
    local ped = GetPlayerPed(-1)
	Citizen.CreateThread(function()
		local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
        local hamburger = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
		AttachEntityToEntity(hamburger, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 18905), 0.12, 0.028, 0.060, 10.0, 175.0, 0.0, true, true, false, true, 1, true)
		RequestAnimDict("mp_player_inteat@burger")
		while not HasAnimDictLoaded("mp_player_inteat@burger") do
			Citizen.Wait(100)
		end
		TaskPlayAnim(GetPlayerPed(-1),"mp_player_inteat@burger","mp_player_int_eat_burger_fp", 8.0, -8, -1, 49, 0, 0, 0, 0)
		Wait(3000)
		ClearPedSecondaryTask(GetPlayerPed(-1))	
		DeleteObject(hamburger)
	end)
end)

--DONUTS, COOKIES ET CROISSANT--
RegisterNetEvent('esx_basicneeds:donuts')
AddEventHandler('esx_basicneeds:donuts', function()
    local prop_name = "prop_donut_01"
    local ped = GetPlayerPed(-1)
	Citizen.CreateThread(function()
		local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
        local hamburger = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
		AttachEntityToEntity(hamburger, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 18905), 0.12, 0.028, 0.001, 10.0, 175.0, 0.0, true, true, false, true, 1, true)
		RequestAnimDict("mp_player_inteat@burger")
		while not HasAnimDictLoaded("mp_player_inteat@burger") do
			Citizen.Wait(100)
		end
		TaskPlayAnim(GetPlayerPed(-1),"mp_player_inteat@burger","mp_player_int_eat_burger_fp", 8.0, -8, -1, 49, 0, 0, 0, 0)
		Wait(3000)
		ClearPedSecondaryTask(GetPlayerPed(-1))	
		DeleteObject(hamburger)
	end)
end)

--ALCOOL--
--VODKA--
RegisterNetEvent('esx_basicneeds:vodka')
AddEventHandler('esx_basicneeds:vodka', function()
Citizen.CreateThread(function()
		local prop_name = "prop_cherenkov_01"
		local pid = PlayerPedId()
		local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
		local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
		AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.16, -0.25, -0.06, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
		RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
		while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
			Citizen.Wait(100)
		end
		TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
		Wait(3000)
		ClearPedSecondaryTask(GetPlayerPed(-1))		
		DeleteObject(boisson)

		RequestAnimSet("MOVE_M@DRUNK@VERYDRUNK")
		while not HasAnimSetLoaded("MOVE_M@DRUNK@VERYDRUNK") do
			Citizen.Wait(0)
		end
		
		DoScreenFadeOut(1000)
		Citizen.Wait(1000)
		SetPedMotionBlur(GetPlayerPed(-1), true)
		ShakeGameplayCam("DRUNK_SHAKE", 1.5)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@VERYDRUNK", true)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)		

		Citizen.CreateThread(function()
			Citizen.Wait(150000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			StopGameplayCamShaking(1)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedIsDrunk(GetPlayerPed(-1), false)
			SetPedMotionBlur(GetPlayerPed(-1), false)
		end)	
	end)
end)


--VIN ROUGE--
RegisterNetEvent('esx_basicneeds:vin')
AddEventHandler('esx_basicneeds:vin', function()
Citizen.CreateThread(function()
		local prop_name = "prop_cs_beer_bot_01"
		local pid = PlayerPedId()
		local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
		local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
		AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.01, -0.02, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
		RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
		while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
			Citizen.Wait(100)
		end
		TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
		Wait(3000)
		ClearPedSecondaryTask(GetPlayerPed(-1))		
		DeleteObject(boisson)

		RequestAnimSet("MOVE_M@DRUNK@SLIGHTLYDRUNK")
		while not HasAnimSetLoaded("MOVE_M@DRUNK@SLIGHTLYDRUNK") do
			Citizen.Wait(0)
		end
		
		DoScreenFadeOut(1000)
		Citizen.Wait(1000)
		SetPedMotionBlur(GetPlayerPed(-1), true)
		ShakeGameplayCam("DRUNK_SHAKE", 1.0)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@SLIGHTLYDRUNK", true)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)		

		Citizen.CreateThread(function()
			Citizen.Wait(60000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			StopGameplayCamShaking(1)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedIsDrunk(GetPlayerPed(-1), false)
			SetPedMotionBlur(GetPlayerPed(-1), false)
		end)	
	end)
end)

--BIERE--
RegisterNetEvent('esx_basicneeds:biere')
AddEventHandler('esx_basicneeds:biere', function()
Citizen.CreateThread(function()
		local prop_name = "prop_cs_beer_bot_01"
		local pid = PlayerPedId()
		local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
		local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
		AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.01, -0.02, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
		RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
		while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
			Citizen.Wait(100)
		end
		TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
		Wait(3000)
		ClearPedSecondaryTask(GetPlayerPed(-1))		
		DeleteObject(boisson)

		RequestAnimSet("MOVE_M@DRUNK@SLIGHTLYDRUNK")
		while not HasAnimSetLoaded("MOVE_M@DRUNK@SLIGHTLYDRUNK") do
			Citizen.Wait(0)
		end
		
		DoScreenFadeOut(1000)
		Citizen.Wait(1000)
		SetPedMotionBlur(GetPlayerPed(-1), true)
		ShakeGameplayCam("DRUNK_SHAKE", 1.0)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@SLIGHTLYDRUNK", true)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)		

		Citizen.CreateThread(function()
			Citizen.Wait(60000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			StopGameplayCamShaking(1)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedIsDrunk(GetPlayerPed(-1), false)
			SetPedMotionBlur(GetPlayerPed(-1), false)
		end)	
	end)
end)

--WHISKY--
RegisterNetEvent('esx_basicneeds:whisky')
AddEventHandler('esx_basicneeds:whisky', function()
Citizen.CreateThread(function()
		local prop_name = "prop_cs_whiskey_bottle"
		local pid = PlayerPedId()
		local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
		local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
		AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.01, -0.02, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
		RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
		while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
			Citizen.Wait(100)
		end
		TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
		Wait(3000)
		ClearPedSecondaryTask(GetPlayerPed(-1))		
		DeleteObject(boisson)

		RequestAnimSet("MOVE_M@DRUNK@VERYDRUNK")
		while not HasAnimSetLoaded("MOVE_M@DRUNK@VERYDRUNK") do
			Citizen.Wait(0)
		end
		
		DoScreenFadeOut(1000)
		Citizen.Wait(1000)
		SetPedMotionBlur(GetPlayerPed(-1), true)
		ShakeGameplayCam("DRUNK_SHAKE", 1.5)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@VERYDRUNK", true)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)		

		Citizen.CreateThread(function()
			Citizen.Wait(180000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			StopGameplayCamShaking(1)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedIsDrunk(GetPlayerPed(-1), false)
			SetPedMotionBlur(GetPlayerPed(-1), false)
		end)	
	end)
end)

--MOJITO--
RegisterNetEvent('esx_basicneeds:mojito')
AddEventHandler('esx_basicneeds:mojito', function()
Citizen.CreateThread(function()
		local prop_name = "prop_drink_whisky"
		local pid = PlayerPedId()
		local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
		local boisson = CreateObject(GetHashKey(prop_name), x, y, z+0.2,  true,  true, true)
		AttachEntityToEntity(boisson, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.12, -0.01, -0.02, 260.0, 90.0, 0.0, true, true, false, true, 1, true)
		RequestAnimDict("amb@code_human_wander_drinking@male@idle_a")
		while not HasAnimDictLoaded("amb@code_human_wander_drinking@male@idle_a") do
			Citizen.Wait(100)
		end
		TaskPlayAnim(GetPlayerPed(-1),"amb@code_human_wander_drinking@male@idle_a","idle_a", 8.0, -8, -1, 49, 0, 0, 0, 0)
		Wait(3000)
		ClearPedSecondaryTask(GetPlayerPed(-1))		
		DeleteObject(boisson)

		RequestAnimSet("MOVE_M@DRUNK@SLIGHTLYDRUNK")
		while not HasAnimSetLoaded("MOVE_M@DRUNK@SLIGHTLYDRUNK") do
			Citizen.Wait(0)
		end
		
		DoScreenFadeOut(1000)
		Citizen.Wait(1000)
		SetPedMotionBlur(GetPlayerPed(-1), true)
		ShakeGameplayCam("DRUNK_SHAKE", 1.0)
		SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@SLIGHTLYDRUNK", true)
		SetPedIsDrunk(GetPlayerPed(-1), true)
		DoScreenFadeIn(1000)		

		Citizen.CreateThread(function()
			Citizen.Wait(90000)
			DoScreenFadeOut(1000)
			Citizen.Wait(1000)
			DoScreenFadeIn(1000)
			StopGameplayCamShaking(1)
			ClearTimecycleModifier()
			ResetScenarioTypesEnabled()
			ResetPedMovementClipset(GetPlayerPed(-1), 0)
			SetPedIsDrunk(GetPlayerPed(-1), false)
			SetPedMotionBlur(GetPlayerPed(-1), false)
		end)	
	end)
end)

--AUTRE--

RegisterNetEvent('esx_basicneeds:cigarette')
AddEventHandler('esx_basicneeds:cigarette', function()
 	TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_SMOKING", 0, true);
	Wait(180000)
	ClearPedTasks(GetPlayerPed(-1))
end)