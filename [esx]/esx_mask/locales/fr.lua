Locales ['fr'] = {

    ['valid_purchase'] = 'valider cet achat ?',
    ['yes'] = 'oui',
    ['no'] = 'non',
    ['not_enough_money'] = 'vous n\'avez ~r~~h~pas assez d\'argent',
    ['press_access'] = 'appuez sur ~INPUT_CONTEXT~ pour ~g~~h~accéder au magasin de masques',
    ['masks_blip'] = 'masques',
    ['no_mask'] = 'vous n\'avez ~r~~h~pas de masque',
    ['you_paid'] = 'vous avez payé~g~ ',

}