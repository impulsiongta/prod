GUIAction = {
    closeGui () {
        $('#identity').css("display", "none");
        $('#register').css("display", "none");
        $("#cursor").css("display", "none");
    },
    openGuiIdentity (data) {
        data = data || {}
        let infoMissing = 'Erreur'
        if (data.dateNaissance) {
            data.dateNaissance = data.dateNaissance.substr(0,11)
        }
        if (data.sexe !== undefined) {
            $('#identity').css('background-image', "url('carteV3_" + data.sexe +".png')")
            data.sexe = data.sexe === 'm' ? 'Homme' : 'Femme'
        }
        if (data.taille !== undefined){
            data.taille = data.taille + ' cm'
        }
		
		if (data.jobs !== undefined) {
      if (data.jobs == "unemployed")
 				data.jobs = "Chômeur"
 			else if (data.jobs == "ambulance")
 				data.jobs = "EMS"
 			else if (data.jobs == "slaughterer")
 				data.jobs = "Abatteur"
 			else if (data.jobs == "fisherman")
 				data.jobs = "Pêcheur"
 			else if (data.jobs == "miner")
 				data.jobs = "Mineur"
      else if (data.jobs == "lumberjack")
 				data.jobs = "Bûcheron"
 			else if (data.jobs == "mecano")
 				data.jobs = "LS Prestige"
 			else if (data.jobs == "taxi")
 				data.jobs = "Millers Cab&Co"
 			else if (data.jobs == "vigneron")
 				data.jobs = "Gordon’s Wine & Co"
 			else if (data.jobs == "tequilala")
 				data.jobs = "Bahamas Mamas"
      else if (data.jobs == "PLS")
 				data.jobs = "Petrol LS"
 			else if (data.jobs == "fermier")
 				data.jobs = "Ortiz Farm"
 			else if (data.jobs == "police")
 				data.jobs = "LSPD"
 			else if (data.jobs == "couturier")
 				data.jobs = "Old Leather"
 			else if (data.jobs == "tacobell")
 				data.jobs = "Manoir de Jade"
 			else if (data.jobs == "gouv")
 				data.jobs = "Gouvernement"
 			else if (data.jobs == "lossantostimes")
 				data.jobs = "LS Times"
 			else if (data.jobs == "brinks")
 				data.jobs = "Brink's"
        }
		
        ['nom','prenom','jobs', 'dateNaissance', 'sexe', 'taille'].forEach(k => {
            $('#'+k).text(data[k] || infoMissing)
        })

        

        $('#identity').css("display", "block");
    }
}

window.addEventListener('message', function (event){
    let method = event.data.method
    if (GUIAction[method] !== undefined) {
        GUIAction[method](event.data.data)
    }
})



//
// Gestion de la souris
//
$(document).ready(function(){
    var documentWidth = document.documentElement.clientWidth
    var documentHeight = document.documentElement.clientHeight
    var cursor = $('#cursor')
    cursorX = documentWidth / 2
    cursorY = documentHeight / 2
    cursor.css('left', cursorX)
    cursor.css('top', cursorY)
    $(document).mousemove( function (event) {
        cursorX = event.pageX
        cursorY = event.pageY
        cursor.css('left', cursorX + 1)
        cursor.css('top', cursorY + 1)
    })
})