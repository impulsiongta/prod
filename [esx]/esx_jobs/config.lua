Config              = {}
Config.DrawDistance = 1000.0
Config.Locale       = 'fr'

Config.Plates = {
	taxi = "TAXI",
	fisherman = "FISH",
	cop = "LSPD",
	ambulance = "EMS",
	depanneur = "MECA",
	fuel = "FUEL",
	lumberjack = "BUCH",
	miner = "MINE",
	reporter = "JOUR",
	slaughterer = "ABAT",
	textil = "COUT"
}

Config.Jobs = {}

Config.PublicZones = {
	EnterBuilding = {
--		Pos   = { x = -118.21381378174, y = -607.14227294922, z = 35.280723571777 },
		Size  = {x = 3.0, y = 3.0, z = 0.2},
		Color = {r = 0, g = 128, b = 255},
		Marker= -1,
		Blip  = false,
		Name  = "Le Maclerait Libéré",
		Type  = "teleport",
		Hint  = "Appuyez sur ~INPUT_PICKUP~ pour entrer dans l'immeuble.",
		Teleport = { x = -139.09838867188, y = -620.74865722656, z = 167.82052612305 }
	},

	ExitBuilding = {
--		Pos   = { x = -139.45831298828, y = -617.32312011719, z = 167.82052612305 },
		Size  = {x = 3.0, y = 3.0, z = 0.2},
		Color = {r = 0, g = 128, b = 255},
		Marker= -1,
		Blip  = false,
		Name  = "Le Maclerait Libéré",
		Type  = "teleport",
		Hint  = "Appuyez sur ~INPUT_PICKUP~ pour aller à l'entrée de l'immeuble.",
		Teleport = { x = -113.07, y = -604.93, z = 35.28 },
	},
}


takeJob = {}

takeJob["lumberjack"] = {x=1191.974,y=-1269.044,z=35.554,n="Bûcheron"}
takeJob["slaughterer"] = {x=-1042.161,y=-2023.766,z=13.162,n="Abatteur"}
--takeJob["fisherman"] = {x=869.001,y=-1629.374,z=30.414,n="Pêcheur"}
takeJob["miner"] = {x=913.817,y=-2153.596,z=30.495,n="Mineur"}
