
tattoosList = {
	["mpbeach_overlays"] = {

		{nameHash = "MP_Bea_M_Head_000", part = "head", addedX = 0.5, addedY=0.4,addedZ=0.7, rotZ = 136.4, price = 500},
		{nameHash = "MP_Bea_M_Head_001", part = "head", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
		{nameHash = "MP_Bea_M_Head_002", part = "head", addedX = -0.1, addedY=-0.5,addedZ=0.7, rotZ = 334.5, price = 500},


		{nameHash = "MP_Bea_F_Neck_000", part = "Nhead", addedX = 0.2, addedY=-0.5,addedZ=0.7, rotZ = 374.3, price = 500},
		{nameHash = "MP_Bea_M_Neck_000", part = "Nhead", addedX = -0.1, addedY=0.7,addedZ=0.7, rotZ = 537.0, price = 500},
		{nameHash = "MP_Bea_M_Neck_001", part = "Nhead", addedX = 0.3, addedY=-0.3,addedZ=0.6, rotZ = 410.3, price = 500},


		{nameHash = "MP_Bea_F_Back_000", part = "torso_back", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
		{nameHash = "MP_Bea_F_Back_001", part = "torso_back", addedX = -0.5, addedY=-0.3,addedZ=0.3, rotZ = -54.4, price = 500},
		{nameHash = "MP_Bea_F_Back_002", part = "torso_back", addedX = -0.5, addedY=-0.3,addedZ=0.3, rotZ = -54.4, price = 500},
		{nameHash = "MP_Bea_M_Back_000", part = "torso_back", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},


		{nameHash = "MP_Bea_F_Chest_000", part = "torso", addedX = 0.3, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Bea_F_Chest_001", part = "torso", addedX = 0.3, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Bea_F_Chest_002", part = "torso", addedX = 0.4, addedY=0.1,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Bea_M_Chest_000", part = "torso", addedX = 0.3, addedY=0.2,addedZ=0.4, rotZ = 482.0, price = 500},
		{nameHash = "MP_Bea_M_Chest_001", part = "torso", addedX = 0.3, addedY=0.2,addedZ=0.4, rotZ = 482.0, price = 500},


		{nameHash = "MP_Bea_F_Stom_000", part = "torso", addedX = 0.2, addedY=0.3,addedZ=0.1, rotZ = 488.8, price = 500},
		{nameHash = "MP_Bea_F_Stom_001", part = "torso", addedX = 0.3, addedY=0.1,addedZ=0.1, rotZ = 466.4, price = 500},
		{nameHash = "MP_Bea_F_Stom_002", part = "torso", addedX = 0.3, addedY=0.3,addedZ=0.1, rotZ = 485.6, price = 500},
		{nameHash = "MP_Bea_M_Stom_000", part = "torso", addedX = 0.3, addedY=0.3,addedZ=0.1, rotZ = 485.6, price = 500}, 
		{nameHash = "MP_Bea_M_Stom_001", part = "torso", addedX = 0.6, addedY=0.3,addedZ=0.1, rotZ = 485.6, price = 500},


		{nameHash = "MP_Bea_F_RSide_000", part = "torso_right", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},


		{nameHash = "MP_Bea_F_Should_000", part = "torso", addedX = -0.5, addedY=-0.3,addedZ=0.5, rotZ = 654.1, price = 500},
		{nameHash = "MP_Bea_F_Should_001", part = "torso", addedX = -0.5, addedY=-0.3,addedZ=0.5, rotZ = 654.1, price = 500},


		{nameHash = "MP_Bea_F_RArm_001", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "MP_Bea_M_RArm_001", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "MP_Bea_M_RArm_000", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},


		{nameHash = "MP_Bea_F_LArm_000", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Bea_F_LArm_001", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
		{nameHash = "MP_Bea_M_LArm_000", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500}, 


		{nameHash = "MP_Bea_M_Lleg_000", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},


		{nameHash = "MP_Bea_F_RLeg_000",addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500}

	},


	["mpbusiness_overlays"] = {
		{nameHash = "MP_Buis_M_Neck_000", addedX = 0.3, addedY=0.2,addedZ=0.5,rotZ = 119.4, price = 500},
		{nameHash = "MP_Buis_M_Neck_001", addedX = 0.3, addedY=-0.2,addedZ=0.7,rotZ = 56.9, price = 500},
		{nameHash = "MP_Buis_M_Neck_002", addedX = 0.0, addedY=0.3,addedZ=0.6,rotZ = 164.8, price = 500},
		{nameHash = "MP_Buis_M_Neck_003", addedX = -0.3, addedY=-0.2,addedZ=0.6,rotZ = -54.1, price = 500},


		{nameHash = "MP_Buis_M_LeftArm_000", addedX = 0.3, addedY=0.2,addedZ=0.0,rotZ = 115.5, price = 500},
		{nameHash = "MP_Buis_M_LeftArm_001", addedX = -0.7, addedY=0.1,addedZ=0.0,rotZ = -68.4, price = 500},

		{nameHash = "MP_Buis_M_RightArm_000", addedX = 0.3, addedY=-0.7,addedZ=0.5,rotZ = 17.7, price = 500},
		{nameHash = "MP_Buis_M_RightArm_001", addedX = 0.3, addedY=0.3,addedZ=0.0,rotZ = 145.4, price = 500},

		{nameHash = "MP_Buis_M_Stomach_000", addedX = 0.7, addedY=0.4,addedZ=0.3,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_M_Chest_000", addedX = 0.7, addedY=0.4,addedZ=0.3,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_M_Chest_001", addedX = 0.7, addedY=0.4,addedZ=0.3,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_M_Back_000", addedX = -0.7, addedY=-0.3,addedZ=0.3,rotZ = -53.6, price = 500},
		{nameHash = "MP_Buis_F_Chest_000", addedX = 0.5, addedY=0.3,addedZ=0.5,rotZ = 124.9, price = 500},
		{nameHash = "MP_Buis_F_Chest_001", addedX = 0.7, addedY=0.4,addedZ=0.3,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_F_Chest_002", addedX = 0.7, addedY=0.4,addedZ=0.3,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_F_Stom_000", addedX = 0.7, addedY=0.4,addedZ=0.2,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_F_Stom_001", addedX = 0.7, addedY=0.4,addedZ=0.2,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_F_Stom_002", addedX = 0.7, addedY=0.4,addedZ=0.2,rotZ = 117.3, price = 500},
		{nameHash = "MP_Buis_F_Back_000", addedX = -0.4, addedY=-0.3,addedZ=0.2,rotZ = 299.8, price = 500},
		{nameHash = "MP_Buis_F_Back_001", addedX = -0.4, addedY=-0.3,addedZ=0.2,rotZ = 299.8, price = 500},
		{nameHash = "MP_Buis_F_Neck_000", addedX = 0.3, addedY=-0.4,addedZ=0.6,rotZ = 382.8, price = 500},
		{nameHash = "MP_Buis_F_Neck_001", addedX = -0.3, addedY=0.3,addedZ=0.7,rotZ = 214.2, price = 500},
		{nameHash = "MP_Buis_F_RArm_000", addedX = 0.5, addedY=0.0,addedZ=0.0,rotZ = 457.5, price = 500},
		{nameHash = "MP_Buis_F_LArm_000", addedX = 0.2, addedY=0.4,addedZ=0.0,rotZ = 472.0, price = 500},
		{nameHash = "MP_Buis_F_LLeg_000", addedX = -0.3, addedY=-0.1,addedZ=-0.7,rotZ = 332.0, price = 500},
		{nameHash = "MP_Buis_F_RLeg_000", addedX = -0.4, addedY=-0.1,addedZ=-0.7,rotZ = 278.0, price = 500}

	},

	["mphipster_overlays"] = {
		{nameHash = "FM_Hip_M_Tat_000", addedX = -0.7, addedY=-0.4,addedZ=0.5,rotZ = -55.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_001", addedX = -0.4, addedY=-0.7,addedZ=0.2,rotZ = -49.7, price = 500},
		{nameHash = "FM_Hip_M_Tat_002", addedX = 0.4, addedY=0.5,addedZ=0.4,rotZ = 129.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_003", addedX = -0.8, addedY=0.1,addedZ=0.0,rotZ = -71.9, price = 500},
		{nameHash = "FM_Hip_M_Tat_004", addedX = 0.4, addedY=0.2,addedZ=0.0,rotZ = -182.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_005", addedX = -0.1, addedY=0.3,addedZ=0.6,rotZ = -182.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_006", addedX = 0.5, addedY=-0.3,addedZ=0.2,rotZ = 38.9, price = 500},
		{nameHash = "FM_Hip_M_Tat_007", addedX = -0.1, addedY=0.8,addedZ=-0.2,rotZ = 156.7, price = 500},
		{nameHash = "FM_Hip_M_Tat_008", addedX = 0.4, addedY=-0.5,addedZ=0.5,rotZ = 51.8, price = 500},
		{nameHash = "FM_Hip_M_Tat_009", addedX = -0.7, addedY=-0.4,addedZ=-0.5,rotZ = -23.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_010", addedX = 0.5, addedY=-0.4,addedZ=-0.1,rotZ = 37.8, price = 500},
		{nameHash = "FM_Hip_M_Tat_011", addedX = -0.3, addedY=-0.2,addedZ=0.5,rotZ = 290.7, price = 500},
		{nameHash = "FM_Hip_M_Tat_012", addedX = -0.3, addedY=-0.2,addedZ=0.5,rotZ = 290.7, price = 500},
		{nameHash = "FM_Hip_M_Tat_013", addedX = 0.6, addedY=0.3,addedZ=0.5,rotZ = 121.7, price = 500},
		{nameHash = "FM_Hip_M_Tat_014", addedX = 0.5, addedY=0.1,addedZ=0.1,rotZ = 121.7, price = 500},
		{nameHash = "FM_Hip_M_Tat_015", addedX = 0.3, addedY=0.3,addedZ=0.4,rotZ = 114.0, price = 500},
		{nameHash = "FM_Hip_M_Tat_016", addedX = 0.3, addedY=0.3,addedZ=0.0,rotZ = 114.0, price = 500},
		{nameHash = "FM_Hip_M_Tat_017", addedX = -0.2, addedY= -0.5,addedZ= 0.1,rotZ = -27.8, price = 500},
		{nameHash = "FM_Hip_M_Tat_018", addedX = -0.4, addedY=-0.2,addedZ=0.1,rotZ = 247.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_019", addedX = -0.7, addedY=-0.5,addedZ=-0.7,rotZ = 296.0, price = 500},
		{nameHash = "FM_Hip_M_Tat_020", addedX = -0.4, addedY=-0.4,addedZ=0.1,rotZ = 296.0, price = 500},
		{nameHash = "FM_Hip_M_Tat_021", addedX = 0.3, addedY=-0.2,addedZ=0.5,rotZ = 74.3, price = 500},
		{nameHash = "FM_Hip_M_Tat_022", addedX = 0.5, addedY=-0.4,addedZ=0.1,rotZ = 74.3, price = 500},
		{nameHash = "FM_Hip_M_Tat_023", addedX = 0.4, addedY=-0.5,addedZ=-0.1,rotZ = 24.9, price = 500},
		{nameHash = "FM_Hip_M_Tat_024", addedX = -0.3, addedY=-0.3,addedZ=0.5,rotZ = -53.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_025", addedX = -0.5, addedY=-0.4,addedZ=0.5,rotZ = -53.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_026", addedX = -0.7, addedY=0.9,addedZ=0.5,rotZ = -124.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_027", addedX = -0.3, addedY=-0.4,addedZ=0.2,rotZ = -343.4, price = 500},
		{nameHash = "FM_Hip_M_Tat_028", addedX = -1.1, addedY=0.2,addedZ=0.2,rotZ = -431.1, price = 500},
		{nameHash = "FM_Hip_M_Tat_029", addedX = 0.2, addedY=0.5,addedZ=0.0,rotZ = -200.1, price = 500},
		{nameHash = "FM_Hip_M_Tat_030", addedX = -0.7, addedY=-0.5,addedZ=0.3,rotZ = -63.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_031", addedX = -0.7, addedY=-0.5,addedZ=0.3,rotZ = -63.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_032", addedX = -0.7, addedY=-0.5,addedZ=0.3,rotZ = -63.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_033", addedX = 0.6, addedY=0.3,addedZ=0.4,rotZ = 116.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_034", addedX = -0.1, addedY=0.7,addedZ=0.0,rotZ = 191.5, price = 500},
		{nameHash = "FM_Hip_M_Tat_035", addedX = 0.1, addedY=0.7,addedZ=0.0,rotZ = 145.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_036", addedX = 0.4, addedY=-0.8,addedZ=0.4,rotZ = 375.2, price = 500},
		{nameHash = "FM_Hip_M_Tat_037", addedX = -0.5, addedY=0.9,addedZ=0.4,rotZ = 551.7, price = 500},
		{nameHash = "FM_Hip_M_Tat_038", addedX = 0.4, addedY=-0.8,addedZ=-0.6,rotZ = 400.1, price = 500},
		{nameHash = "FM_Hip_M_Tat_039", addedX = 0.1, addedY=1.2,addedZ=0.1,rotZ = 511.9, price = 500},
		{nameHash = "FM_Hip_M_Tat_040", addedX = -0.6, addedY=-0.1,addedZ=-0.7,rotZ = 307.0, price = 500},
		{nameHash = "FM_Hip_M_Tat_041", addedX = -0.5, addedY=0.1,addedZ=0.2,rotZ = 258.8, price = 500},
		{nameHash = "FM_Hip_M_Tat_042", addedX = -0.5, addedY=-0.5,addedZ=-0.7,rotZ = -25.0, price = 500},
		{nameHash = "FM_Hip_M_Tat_043", addedX = -0.3, addedY=0.4,addedZ=0.4, rotZ = -135.4, price = 500},
		{nameHash = "FM_Hip_M_Tat_044", addedX = 0.2, addedY=-0.5,addedZ=0.4,rotZ = 13.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_045", addedX = 0.2, addedY=-0.8,addedZ=0.3,rotZ = 13.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_046", addedX = -0.5, addedY=-0.3,addedZ=0.5,rotZ = -61.2, price = 500},
		{nameHash = "FM_Hip_M_Tat_047", addedX = 0.4, addedY=0.2,addedZ=0.5,rotZ = 104.6, price = 500},
		{nameHash = "FM_Hip_M_Tat_048", addedX = -0.1, addedY=0.6,addedZ=-0.1,rotZ = 181.1, price = 500},
	},
	
	["mpbiker_overlays"] = {
	
		{nameHash = "MP_MP_Biker_Tat_000_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500}, -- Haut torse
		{nameHash = "MP_MP_Biker_Tat_000_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_001_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_001_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_002_M", addedX = 0.4, addedY=0.4,addedZ=-0.3, rotZ = 500.0, price = 500}, -- Avant jambe gauche
		{nameHash = "MP_MP_Biker_Tat_002_F", addedX = 0.4, addedY=0.4,addedZ=-0.2, rotZ = 500.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_003_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500}, -- Ventre
		{nameHash = "MP_MP_Biker_Tat_003_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_004_M", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500}, -- Avant jambe droite
		{nameHash = "MP_MP_Biker_Tat_004_F", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_005_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_005_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_006_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, -- Dos
		{nameHash = "MP_MP_Biker_Tat_006_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_007_M", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500}, -- Bras droit
	--	{nameHash = "MP_MP_Biker_Tat_007_F", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_008_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_008_F", addedX = -0.6, addedY=-0.4,addedZ=0.5, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_009_M", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500}, -- Cot� gauche t�te
	--	{nameHash = "MP_MP_Biker_Tat_009_F", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_010_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_010_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_011_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_011_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},	
		
		{nameHash = "MP_MP_Biker_Tat_012_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500}, -- Bras gauche
	--	{nameHash = "MP_MP_Biker_Tat_012_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_013_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_013_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_014_M", addedX = 0.1, addedY=-0.6,addedZ=0.3, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_014_F", addedX = 0.1, addedY=-0.6,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_015_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500}, -- Jambe gauche bas
	--	{nameHash = "MP_MP_Biker_Tat_015_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_016_M", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_016_F", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_017_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_017_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_018_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_018_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_019_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_019_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_020_M", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_020_F", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_021_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_021_F", addedX = -0.6, addedY=-0.4,addedZ=0.6, rotZ = 309.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_022_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_022_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_MP_Biker_Tat_023_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_023_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_024_M", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
		{nameHash = "MP_MP_Biker_Tat_024_F", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},

		{nameHash = "MP_MP_Biker_Tat_025_M", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_025_F", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},

		{nameHash = "MP_MP_Biker_Tat_026_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_026_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_027_M", addedX = -0.5, addedY=0.1,addedZ=-0.8, rotZ = -75.7, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_027_F", addedX = -0.5, addedY=0.1,addedZ=-0.8, rotZ = -75.7, price = 500},

		{nameHash = "MP_MP_Biker_Tat_028_M", addedX = 0.4, addedY=-0.6,addedZ=-0.4, rotZ = 400.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_028_F", addedX = 0.4, addedY=-0.6,addedZ=-0.4, rotZ = 400.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_029_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_029_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_030_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_030_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_031_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500}, 
		{nameHash = "MP_MP_Biker_Tat_031_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500}, 
		
		{nameHash = "MP_MP_Biker_Tat_032_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_032_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_033_M", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_033_F", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_034_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_034_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_035_M", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},
		{nameHash = "MP_MP_Biker_Tat_035_F", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500},

		{nameHash = "MP_MP_Biker_Tat_036_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_MP_Biker_Tat_036_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_037_M", addedX = 0.4, addedY=0.4,addedZ=-0.5, rotZ = 500.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_037_F", addedX = 0.4, addedY=0.4,addedZ=-0.5, rotZ = 500.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_038_M", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_038_F", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_039_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_039_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_040_M", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_040_F", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_041_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_041_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_042_M", addedX = 0.6, addedY=-0.5,addedZ=0.1, rotZ = 81.1, price = 500},
		{nameHash = "MP_MP_Biker_Tat_042_F", addedX = 0.6, addedY=-0.5,addedZ=0.1, rotZ = 81.1, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_043_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_043_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_044_M", addedX = 0.4, addedY=0.4,addedZ=-0.4, rotZ = 500.0, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_044_F", addedX = 0.4, addedY=0.4,addedZ=-0.4, rotZ = 500.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_045_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_045_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_046_M", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_046_F", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_047_M", addedX = 0.7, addedY=-0.4,addedZ=0.2, rotZ = 80.1, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_047_F", addedX = 0.7, addedY=-0.4,addedZ=0.2, rotZ = 80.1, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_048_M", addedX = 0.2, addedY=-0.8,addedZ=-0.5, rotZ = 400.0, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_048_F", addedX = 0.2, addedY=-0.8,addedZ=-0.5, rotZ = 400.0, price = 500},

		{nameHash = "MP_MP_Biker_Tat_049_M", addedX = 0.7, addedY=-0.4,addedZ=0.2, rotZ = 80.1, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_049_F", addedX = 0.7, addedY=-0.4,addedZ=0.2, rotZ = 80.1, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_050_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_050_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_051_M", addedX = 0.2, addedY=-0.5,addedZ=0.7, rotZ = 374.3, price = 500},
	--	{nameHash = "MP_MP_Biker_Tat_051_F", addedX = 0.2, addedY=-0.5,addedZ=0.7, rotZ = 374.3, price = 500},
				
		{nameHash = "MP_MP_Biker_Tat_052_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_052_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_053_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_MP_Biker_Tat_053_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_054_M", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_Biker_Tat_054_F", addedX = 0.2, addedY=-0.5,addedZ=0.3, rotZ = 21.1, price = 500},
				
		{nameHash = "MP_MP_Biker_Tat_055_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_MP_Biker_Tat_055_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
				
		{nameHash = "MP_MP_Biker_Tat_056_M", addedX = 0.4, addedY=0.4,addedZ=-0.4, rotZ = 500.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_056_F", addedX = 0.4, addedY=0.4,addedZ=-0.4, rotZ = 500.0, price = 500},
						
		{nameHash = "MP_MP_Biker_Tat_057_M", addedX = 0.4, addedY=0.4,addedZ=-0.5, rotZ = 500.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_057_F", addedX = 0.4, addedY=0.4,addedZ=-0.5, rotZ = 500.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_058_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_058_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_059_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_059_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Biker_Tat_060_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Biker_Tat_060_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	},

	["mpchristmas2_overlays"] = {

		{nameHash = "MP_Xmas2_M_Tat_000", addedX = 0.5, addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_000", addedX = 0.5, addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_001", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_001", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_002", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_002", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_003", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_003", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_004", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_004", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_Xmas2_M_Tat_005", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_005", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_006", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_006", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_007", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_007", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_008", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_008", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_009", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_009", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_010", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_010", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_011", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_011", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_012", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_012", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	
		{nameHash = "MP_Xmas2_M_Tat_013", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_013", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
	
		{nameHash = "MP_Xmas2_M_Tat_014", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_014", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_015", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_015", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_016", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_016", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	
		{nameHash = "MP_Xmas2_M_Tat_017", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_017", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	
		{nameHash = "MP_Xmas2_M_Tat_018", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_018", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	
		{nameHash = "MP_Xmas2_M_Tat_019", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_019", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_020", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_020", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_021", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_021", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_022", addedX = -0.4, addedY=-0.5,addedZ=0.2,rotZ = 299.8, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_022", addedX = -0.4, addedY=-0.5,addedZ=0.2,rotZ = 299.8, price = 500},
		
		{nameHash = "MP_Xmas2_M_Tat_023", addedX = -0.4, addedY=-0.5,addedZ=0.2,rotZ = 299.8, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_023", addedX = -0.4, addedY=-0.5,addedZ=0.2,rotZ = 299.8, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_024", addedX = -0.1, addedY=-0.5,addedZ=0.7, rotZ = 334.5, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_024", addedX = -0.1, addedY=-0.5,addedZ=0.7, rotZ = 334.5, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_025", addedX = -0.1, addedY=-0.5,addedZ=0.7, rotZ = 334.5, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_025", addedX = -0.1, addedY=-0.5,addedZ=0.7, rotZ = 334.5, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_026", addedX = 0.5, addedY=-0.4,addedZ=-0.1,rotZ = 37.8, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_026", addedX = 0.5, addedY=-0.4,addedZ=-0.1,rotZ = 37.8, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_027", addedX = 0.5, addedY=-0.4,addedZ=-0.1,rotZ = 37.8, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_027", addedX = 0.5, addedY=-0.4,addedZ=-0.1,rotZ = 37.8, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_028", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_Xmas2_F_Tat_028", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},

		{nameHash = "MP_Xmas2_M_Tat_029", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
	--	{nameHash = "MP_Xmas2_F_Tat_029", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
	},
	
	["multiplayer_overlays"] = {

		{nameHash = "FM_Tat_Award_M_000", addedX = 0.5, addedY=0.4,addedZ=0.7, rotZ = 136.4, price = 500}, -- T�te
	--	{nameHash = "FM_Tat_Award_F_000", addedX = 0.5, addedY=0.4,addedZ=0.7, rotZ = 136.4, price = 500},

		{nameHash = "FM_Tat_Award_M_001", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500}, -- Bras gauche
		{nameHash = "FM_Tat_Award_F_001", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "FM_Tat_Award_M_002", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500}, -- Bras droit
		{nameHash = "FM_Tat_Award_F_002", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "FM_Tat_Award_M_003", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500}, -- Torse
		{nameHash = "FM_Tat_Award_F_003", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "FM_Tat_Award_M_004", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500}, -- Ventre
		{nameHash = "FM_Tat_Award_F_004", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},

		{nameHash = "FM_Tat_Award_M_005", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, -- Dos
		{nameHash = "FM_Tat_Award_F_005", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_Award_M_006", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500}, -- Jambe Droite Arri�re
		{nameHash = "FM_Tat_Award_F_006", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "FM_Tat_Award_M_007", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "FM_Tat_Award_F_007", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "FM_Tat_Award_M_008", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_Award_F_008", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "FM_Tat_Award_M_009", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500}, -- Jambe Gauche Arri�re
		{nameHash = "FM_Tat_Award_F_009", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "FM_Tat_Award_M_010", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_Award_F_010", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "FM_Tat_Award_M_011", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_Award_F_011", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "FM_Tat_Award_M_012", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_Award_F_012", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	
		{nameHash = "FM_Tat_Award_M_013", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_Award_F_013", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "FM_Tat_Award_M_014", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_Award_F_014", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_Award_M_015", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "FM_Tat_Award_F_015", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "FM_Tat_Award_M_016", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_Award_F_016", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_Award_M_017", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_Award_F_017", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_Award_M_018", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_Award_F_018", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_Award_M_019", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_Award_F_019", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "FM_Tat_Award_M_019", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_Award_F_019", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "FM_Tat_M_000", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "FM_Tat_F_000", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "FM_Tat_M_001", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_001", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "FM_Tat_M_002", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_002", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "FM_Tat_M_003",  addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_003",  addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "FM_Tat_M_004", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_004", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "FM_Tat_M_005", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "FM_Tat_F_005", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "FM_Tat_M_006", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "FM_Tat_F_006", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "FM_Tat_M_007", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_007", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "FM_Tat_M_008", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_008", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
				
		{nameHash = "FM_Tat_M_009", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_009", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
				
		{nameHash = "FM_Tat_M_010", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_010", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
				
		{nameHash = "FM_Tat_M_011", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_011", addedX = -0.6, addedY=-0.4,addedZ=0.6, rotZ = 309.0, price = 500},
				
		{nameHash = "FM_Tat_M_012", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_012", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},

		{nameHash = "FM_Tat_M_013", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_013", addedX = -0.6, addedY=-0.4,addedZ=0.6, rotZ = 309.0, price = 500},
		
		{nameHash = "FM_Tat_M_014", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_014", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "FM_Tat_M_015", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500}, 
		{nameHash = "FM_Tat_F_015", addedX = -0.5, addedY=0.7,addedZ=0.4, rotZ = -140.7, price = 500}, 
		
		{nameHash = "FM_Tat_M_016", addedX = -0.6, addedY=-0.4,addedZ=0.5, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_016", addedX = -0.6, addedY=-0.4,addedZ=0.5, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_M_017", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_017", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},

		{nameHash = "FM_Tat_M_018", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_018", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "FM_Tat_M_019", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_019", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_M_020", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_020", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "FM_Tat_M_021", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_021", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},

		{nameHash = "FM_Tat_M_022", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_022", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},

		{nameHash = "FM_Tat_M_023", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_023", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
		
		{nameHash = "FM_Tat_M_024", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_024", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
				
		{nameHash = "FM_Tat_M_025", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_025", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "FM_Tat_M_026", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_026", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},

		{nameHash = "FM_Tat_M_027", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_027", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},

		{nameHash = "FM_Tat_M_028", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_028", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "FM_Tat_M_029", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_029", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "FM_Tat_M_030", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_030", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "FM_Tat_M_031", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "FM_Tat_F_031", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
				
		{nameHash = "FM_Tat_M_032", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_032", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
						
		{nameHash = "FM_Tat_M_033", addedX = -0.5, addedY=-0.2,addedZ=-0.4, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_033", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
						
		{nameHash = "FM_Tat_M_034", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_034", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "FM_Tat_M_035", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "FM_Tat_F_035", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	
		{nameHash = "FM_Tat_M_036", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_036", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
	
		{nameHash = "FM_Tat_M_037", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_037", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
			
		{nameHash = "FM_Tat_M_038", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_038", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
			
		{nameHash = "FM_Tat_M_039", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "FM_Tat_F_039", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
			
		{nameHash = "FM_Tat_M_040", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
		{nameHash = "FM_Tat_F_040", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
					
		{nameHash = "FM_Tat_M_041", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "FM_Tat_F_041", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
					
		{nameHash = "FM_Tat_M_042", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
		{nameHash = "FM_Tat_F_042", addedX = 0.6, addedY=-0.1,addedZ=-0.5, rotZ = 450.0, price = 500},
					
		{nameHash = "FM_Tat_M_043", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "FM_Tat_F_043", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
					
		{nameHash = "FM_Tat_M_044", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "FM_Tat_F_044", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
					
		{nameHash = "FM_Tat_M_045", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_045", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
					
		{nameHash = "FM_Tat_M_046", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "FM_Tat_F_046", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
					
		{nameHash = "FM_Tat_M_047", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "FM_Tat_F_047", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	},
	
	["mpimportexport_overlays"] = {

		{nameHash = "MP_MP_ImportExport_Tat_000_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_000_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_MP_ImportExport_Tat_001_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_001_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_MP_ImportExport_Tat_002_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_002_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_MP_ImportExport_Tat_003_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_003_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "MP_MP_ImportExport_Tat_004_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_004_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_MP_ImportExport_Tat_005_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_005_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "MP_MP_ImportExport_Tat_006_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_006_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_ImportExport_Tat_007_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_007_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_ImportExport_Tat_008_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_MP_ImportExport_Tat_008_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_ImportExport_Tat_009_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, 
		{nameHash = "MP_MP_ImportExport_Tat_009_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, 
		
		{nameHash = "MP_MP_ImportExport_Tat_010_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, 
		{nameHash = "MP_MP_ImportExport_Tat_010_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, 
		
		{nameHash = "MP_MP_ImportExport_Tat_011_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, 
		{nameHash = "MP_MP_ImportExport_Tat_011_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, 

	},
	
	["mpluxe2_overlays"] = {

		{nameHash = "MP_LUXE_TAT_002_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LUXE_TAT_002_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_005_M", addedX = -0.6, addedY=-0.1,addedZ=0.1, rotZ = 309.0, price = 500},
		{nameHash = "MP_LUXE_TAT_005_F", addedX = -0.6, addedY=-0.1,addedZ=0.1, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_010_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_LUXE_TAT_010_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "MP_LUXE_TAT_011_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LUXE_TAT_011_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_LUXE_TAT_012_M", addedX = 0.5, addedY=0.3,addedZ=0.3, rotZ = 482.0, price = 500},
		{nameHash = "MP_LUXE_TAT_012_F", addedX = 0.5, addedY=0.3,addedZ=0.3, rotZ = 482.0, price = 500},

		{nameHash = "MP_LUXE_TAT_016_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LUXE_TAT_016_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_LUXE_TAT_017_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_LUXE_TAT_017_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

		{nameHash = "MP_LUXE_TAT_018_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_LUXE_TAT_018_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_LUXE_TAT_022_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LUXE_TAT_022_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_023_M", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LUXE_TAT_023_F", addedX = -0.5, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_LUXE_TAT_025_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	--	{nameHash = "MP_LUXE_TAT_025_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_026_M", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		{nameHash = "MP_LUXE_TAT_026_F", addedX = 0.2, addedY=-0.8,addedZ=0.3, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_LUXE_TAT_027_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LUXE_TAT_027_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
				
		{nameHash = "MP_LUXE_TAT_028_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LUXE_TAT_028_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LUXE_TAT_029_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LUXE_TAT_029_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
				
		{nameHash = "MP_LUXE_TAT_030_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_LUXE_TAT_030_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_LUXE_TAT_031_M", addedX = -0.8, addedY=0.1,addedZ=0.2, rotZ = 280.0, price = 500},
	--	{nameHash = "MP_LUXE_TAT_031_F", addedX = -0.8, addedY=0.1,addedZ=0.2, rotZ = 280.0, price = 500},

	},
	
	["mpluxe_overlays"] = {

		{nameHash = "MP_LUXE_TAT_000_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LUXE_TAT_000_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_LUXE_TAT_001_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LUXE_TAT_001_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_LUXE_TAT_003_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_LUXE_TAT_003_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_004_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_LUXE_TAT_004_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_LUXE_TAT_006_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LUXE_TAT_006_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_007_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	--	{nameHash = "MP_LUXE_TAT_007_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_008_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LUXE_TAT_008_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_009_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LUXE_TAT_009_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LUXE_TAT_013_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_LUXE_TAT_013_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_LUXE_TAT_014_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LUXE_TAT_014_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_015_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LUXE_TAT_015_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LUXE_TAT_019_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_LUXE_TAT_019_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_LUXE_TAT_020_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_LUXE_TAT_020_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LUXE_TAT_021_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LUXE_TAT_021_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LUXE_TAT_024_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LUXE_TAT_024_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
	
	},
	
	["mplowrider_overlays"] = {

		{nameHash = "MP_LR_Tat_001_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_001_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LR_Tat_002_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_002_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LR_Tat_004_M", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
		{nameHash = "MP_LR_Tat_004_F", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
		
		{nameHash = "MP_LR_Tat_005_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LR_Tat_005_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LR_Tat_007_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LR_Tat_007_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	
		{nameHash = "MP_LR_Tat_009_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_009_F", addedX = -0.6, addedY=-0.4,addedZ=0.6, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LR_Tat_010_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_010_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LR_Tat_013_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_013_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LR_Tat_014_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_014_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LR_Tat_015_M", addedX = 0.5, addedY=-0.05,addedZ=-0.1,rotZ = 117.3, price = 500},
		{nameHash = "MP_LR_Tat_015_F", addedX = 0.5, addedY=-0.05,addedZ=-0.1,rotZ = 117.3, price = 500},
			
		{nameHash = "MP_LR_Tat_017_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LR_Tat_017_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_LR_Tat_020_M", addedX = 0.4, addedY=0.4,addedZ=-0.7, rotZ = 500.0, price = 500},
		{nameHash = "MP_LR_Tat_020_F", addedX = 0.4, addedY=0.4,addedZ=-0.7, rotZ = 500.0, price = 500},

		{nameHash = "MP_LR_Tat_021_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_021_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LR_Tat_023_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_LR_Tat_023_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_LR_Tat_026_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_026_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_LR_Tat_027_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LR_Tat_027_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_LR_Tat_033_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LR_Tat_033_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

	},
	["mplowrider2_overlays"] = {

		{nameHash = "MP_LR_Tat_000_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_000_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LR_Tat_003_M", addedX = 0.5, addedY=0.1,addedZ=-0.1, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_003_F", addedX = 0.5, addedY=0.1,addedZ=-0.1, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LR_Tat_006_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LR_Tat_006_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LR_Tat_008_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_008_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LR_Tat_011_M", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
		{nameHash = "MP_LR_Tat_011_F", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
		
		{nameHash = "MP_LR_Tat_012_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_012_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LR_Tat_016_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_016_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LR_Tat_018_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LR_Tat_018_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LR_Tat_019_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_019_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_LR_Tat_022_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_LR_Tat_022_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_LR_Tat_028_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_LR_Tat_028_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_LR_Tat_029_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LR_Tat_029_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_LR_Tat_030_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_LR_Tat_030_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_LR_Tat_031_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_031_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_LR_Tat_032_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_LR_Tat_032_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_LR_Tat_035_M", addedX = 0.5, addedY=0.1,addedZ=-0.1, rotZ = 482.0, price = 500},
		{nameHash = "MP_LR_Tat_035_F", addedX = 0.5, addedY=0.1,addedZ=-0.1, rotZ = 482.0, price = 500},

	},
	
	["mpgunrunning_overlays"] = {

		{nameHash = "MP_Gunrunning_Tattoo_000_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_000_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_001_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_001_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_Gunrunning_Tattoo_002_M", addedX = 0.5, addedY=-0.5,addedZ=-0.1, rotZ = 400.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_002_F", addedX = 0.5, addedY=-0.5,addedZ=-0.1, rotZ = 400.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_003_M", addedX = 0.2, addedY=-0.5,addedZ=0.7, rotZ = 374.3, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_003_F", addedX = 0.2, addedY=-0.5,addedZ=0.7, rotZ = 374.3, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_004_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_004_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	
		{nameHash = "MP_Gunrunning_Tattoo_005_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_005_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_006_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_Gunrunning_Tattoo_006_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
	--	{nameHash = "MP_Gunrunning_Tattoo_007_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_Gunrunning_Tattoo_007_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_008_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_008_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_009_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_009_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_010_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_010_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
	
		{nameHash = "MP_Gunrunning_Tattoo_011_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_011_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_012_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_012_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_Gunrunning_Tattoo_013_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_013_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_014_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_014_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_Gunrunning_Tattoo_015_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_015_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_016_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_Gunrunning_Tattoo_016_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	
		{nameHash = "MP_Gunrunning_Tattoo_017_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_017_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_018_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_018_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_Gunrunning_Tattoo_019_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_019_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
				
		{nameHash = "MP_Gunrunning_Tattoo_020_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_020_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_021_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_021_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	
		{nameHash = "MP_Gunrunning_Tattoo_022_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_022_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_023_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_Gunrunning_Tattoo_023_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_Gunrunning_Tattoo_024_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_024_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
				
		{nameHash = "MP_Gunrunning_Tattoo_025_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_025_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
				
		{nameHash = "MP_Gunrunning_Tattoo_026_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_Gunrunning_Tattoo_026_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_027_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_027_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	
		{nameHash = "MP_Gunrunning_Tattoo_028_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_028_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},

		{nameHash = "MP_Gunrunning_Tattoo_029_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_029_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_Gunrunning_Tattoo_030_M", addedX = -0.3, addedY=-0.4,addedZ=-0.4, rotZ = -51.7, price = 500},
		{nameHash = "MP_Gunrunning_Tattoo_030_F", addedX = -0.3, addedY=-0.4,addedZ=-0.4, rotZ = -51.7, price = 500},

	},
	
	["mpstunt_overlays"] = {

		{nameHash = "MP_MP_Stunt_Tat_000_M", addedX = 0.2, addedY=-0.5,addedZ=0.7, rotZ = 374.3, price = 500},
	--	{nameHash = "MP_MP_Stunt_Tat_000_F", addedX = 0.2, addedY=-0.5,addedZ=0.7, rotZ = 374.3, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_001_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_001_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_002_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_MP_Stunt_tat_002_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_003_M", addedX = 0.5, addedY=0.1,addedZ=-0.1, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_003_F", addedX = 0.5, addedY=0.1,addedZ=-0.1, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_004_M", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_004_F", addedX = -0.1, addedY=0.4,addedZ=0.7, rotZ = 195.0, price = 500},
	
		{nameHash = "MP_MP_Stunt_tat_005_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_MP_Stunt_tat_005_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_006_M", addedX = -0.1, addedY=0.7,addedZ=0.7, rotZ = 537.0, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_006_F", addedX = -0.1, addedY=0.7,addedZ=0.7, rotZ = 537.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_007_M", addedX = 0.5, addedY=0.3,addedZ=-0.7, rotZ = 482.0, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_007_F", addedX = 0.5, addedY=0.3,addedZ=-0.7, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_008_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_008_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_009_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_Stunt_tat_009_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
			
		{nameHash = "MP_MP_Stunt_tat_010_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_010_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_011_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_011_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_012_M", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_012_F", addedX = 0.4, addedY=-0.4,addedZ=0.1, rotZ = 421.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_013_M", addedX = -0.2, addedY=0.8,addedZ=-0.1, rotZ = -150.2, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_013_F", addedX = -0.2, addedY=0.8,addedZ=-0.1, rotZ = -150.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_014_M", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_014_F", addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500},
			
		{nameHash = "MP_MP_Stunt_tat_015_M", addedX = 0.5, addedY=0.3,addedZ=-0.2, rotZ = 482.0, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_015_F", addedX = 0.5, addedY=0.3,addedZ=-0.2, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_016_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_016_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_017_M", addedX = -0.1, addedY=0.7,addedZ=0.7, rotZ = 537.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_017_F", addedX = -0.1, addedY=0.7,addedZ=0.7, rotZ = 537.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_018_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_018_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_019_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_019_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
	
		{nameHash = "MP_MP_Stunt_tat_020_M", addedX = 0.8, addedY=0.3,addedZ=-0.6, rotZ = 482.0, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_020_F", addedX = 0.8, addedY=0.3,addedZ=-0.6, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_021_M", addedX = 0.8, addedY=0.3,addedZ=-0.6, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_021_F", addedX = 0.8, addedY=0.3,addedZ=-0.6, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_022_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_MP_Stunt_tat_022_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_023_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_023_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_024_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_024_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
	
		{nameHash = "MP_MP_Stunt_tat_025_M", addedX = 0.2, addedY=-0.5,addedZ=-0.4, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_Stunt_tat_025_F", addedX = 0.2, addedY=-0.5,addedZ=-0.4, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_026_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_026_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_027_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_027_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_028_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_MP_Stunt_tat_028_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_029_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_029_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
			
		{nameHash = "MP_MP_Stunt_tat_030_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_030_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_031_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_MP_Stunt_tat_031_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_032_M", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		{nameHash = "MP_MP_Stunt_tat_032_F", addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_033_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_033_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_034_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_034_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
			
		{nameHash = "MP_MP_Stunt_tat_035_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_035_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_036_M", addedX = 0.5, addedY=-0.4,addedZ=-0.1,rotZ = 37.8, price = 500},
		{nameHash = "MP_MP_Stunt_tat_036_F", addedX = 0.5, addedY=-0.4,addedZ=-0.1,rotZ = 37.8, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_037_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_037_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_038_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		{nameHash = "MP_MP_Stunt_tat_038_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_039_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		{nameHash = "MP_MP_Stunt_tat_039_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
			
		{nameHash = "MP_MP_Stunt_tat_040_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_040_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_041_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_041_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_042_M", addedX = -0.1, addedY=0.7,addedZ=0.7, rotZ = 537.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_042_F", addedX = -0.1, addedY=0.7,addedZ=0.7, rotZ = 537.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_043_M", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_043_F", addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_044_M", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_044_F", addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500},
			
		{nameHash = "MP_MP_Stunt_tat_045_M", addedX = 0.5, addedY=-0.6,addedZ=-0.5,rotZ = 37.8, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_045_F", addedX = -0.5, addedY=-0.2,addedZ=-0.6, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_046_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_046_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_047_M", addedX = -0.8, addedY=-0.2,addedZ=-0.7, rotZ = -51.7, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_047_F", addedX = -0.5, addedY=-0.2,addedZ=-0.6, rotZ = -51.7, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_048_M", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		{nameHash = "MP_MP_Stunt_tat_048_F", addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500},
		
		{nameHash = "MP_MP_Stunt_tat_049_M", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},
	--	{nameHash = "MP_MP_Stunt_tat_049_F", addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500},

	},
}


tattoosCategories = {
	{name = "Tatouages Basique", value = "multiplayer_overlays"},
	{name = "Tatouages Plage", value = "mpbeach_overlays"},
	{name = "Tatouages Dollar", value = "mpbusiness_overlays"},
	{name = "Tatouages Malabar", value = "mphipster_overlays"},
	{name = "Tatouages Biker", value = "mpbiker_overlays"},	
	{name = "Tatouages No�l", value = "mpchristmas2_overlays"},
	{name = "Tatouages Voiture", value = "mpimportexport_overlays"},
	{name = "Tatouages Luxe", value = "mpluxe_overlays"},
	{name = "Tatouages Luxe 2", value = "mpluxe2_overlays"},
	{name = "Tatouages Lowrider", value = "mplowrider_overlays"},
	{name = "Tatouages Lowrider 2", value = "mplowrider2_overlays"},
	{name = "Tatouages Armes", value = "mpgunrunning_overlays"},
	{name = "Tatouages Casse-cou", value = "mpstunt_overlays"}
}

		
		--[[
		addedX = 0.5, addedY=0.4,addedZ=0.7, rotZ = 136.4, price = 500}, -- T�te
		addedX = 0.3, addedY=0.8,addedZ=0.2, rotZ = -192.2, price = 500}, -- Bras gauche
		addedX = -0.8, addedY=0.1,addedZ=0.2, rotZ = 280.0, price = 500}, -- Arri�re Bras Gauche
		addedX = 0.2, addedY=-0.8,addedZ=0.1, rotZ = 21.1, price = 500}, -- Bras droit
		addedX = 0.5, addedY=0.3,addedZ=0.5, rotZ = 482.0, price = 500}, -- Torse
		addedX = 0.5, addedY=0.3,addedZ=0.2, rotZ = 482.0, price = 500}, -- Ventre
		addedX = -0.6, addedY=-0.4,addedZ=0.3, rotZ = 309.0, price = 500}, -- Dos
		addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500}, -- Jambe Droite Arri�re
		addedX = -0.5, addedY=-0.2,addedZ=-0.8, rotZ = -51.7, price = 500}, -- Jambe Gauche Arri�re
		addedX = 0.8, addedY=0.3,addedZ=-0.6, rotZ = 482.0, price = 500}, -- Jambe Face
		--]]


tattoosShops = {
	{x=1322.645,y=-1651.976,z=52.275},
	{x=-1153.676,y=-1425.68,z=4.954},
	{x=322.139,y=180.467,z=103.587},
	{x=-3170.071,y=1075.059,z=20.829},
	{x=1864.633,y=3747.738,z=33.032},
	{x=-293.713,y=6200.04,z=31.487}
}