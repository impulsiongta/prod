ESX 				= nil
local defaultsecs   = 300
local maxsecs 		= 18000

-----------------------------

--ESX base
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

local xPlayers 		= ESX.GetPlayers()

AddEventHandler('chatMessage', function(source, n, message)
	cm = stringsplit(message, " ")
	local xPlayer 		= ESX.GetPlayerFromId(source)
		
		if cm[1] == "/unjail" then
			if xPlayer.job.name == 'police' then
				CancelEvent()
				local tPID = tonumber(cm[2])
				if GetPlayerName(tPID) ~= nil then
					print("Vous avez êtes libéré ".. GetPlayerName(tPID).. " par ".. GetPlayerName(source))
					TriggerClientEvent("UnJP", tPID)
				end
			else
		--		TriggerClientEvent('chatMessage', -1, 'SYSTEM', { 0, 0, 0 }, "Vous n'avez pas le droit de libérer des citoyens de prison !")
			end
		elseif cm[1] == "/jail1" then
			if xPlayer.job.name == 'police' then
				CancelEvent()
				local tPID = tonumber(cm[2])
				local jT = defaultsecs
					if cm[3] ~= nil then
						jT = tonumber(cm[3])				
					end
				if jT > maxsecs then
					jT = maxsecs
				end
				if GetPlayerName(tPID) ~= nil then
					print("Vous êtes en prison ".. GetPlayerName(tPID).. " pour ".. jT .." seconds par ".. GetPlayerName(source))
					TriggerClientEvent("JP1", tPID, jT)
			--		TriggerClientEvent('chatMessage', -1, 'JUGE', { 0, 0, 0 }, GetPlayerName(tPID) ..' est en prison pour '.. jT ..' seconds')
				end
			else
			--	TriggerClientEvent('chatMessage', -1, 'SYSTEM', { 0, 0, 0 }, "Vous n'avez pas le droit de mettre des citoyens en prison !")
			end
		elseif cm[1] == "/jail2" then
			if xPlayer.job.name == 'police' then
				CancelEvent()
				local tPID = tonumber(cm[2])
				local jT = defaultsecs
					if cm[3] ~= nil then
						jT = tonumber(cm[3])				
					end
				if jT > maxsecs then
					jT = maxsecs
				end
				if GetPlayerName(tPID) ~= nil then
					print("Vous êtes en prison ".. GetPlayerName(tPID).. " pour ".. jT .." seconds par ".. GetPlayerName(source))
					TriggerClientEvent("JP2", tPID, jT)
			--		TriggerClientEvent('chatMessage', -1, 'JUGE', { 0, 0, 0 }, GetPlayerName(tPID) ..' est en prison pour '.. jT ..' seconds')
				end
			else
			--	TriggerClientEvent('chatMessage', -1, 'SYSTEM', { 0, 0, 0 }, "Vous n'avez pas le droit de mettre des citoyens en prison !")
			end
		elseif cm[1] == "/jail3" then
			if xPlayer.job.name == 'police' then
				CancelEvent()
				local tPID = tonumber(cm[2])
				local jT = defaultsecs
					if cm[3] ~= nil then
						jT = tonumber(cm[3])				
					end
				if jT > maxsecs then
					jT = maxsecs
				end
				if GetPlayerName(tPID) ~= nil then
					print("Vous êtes en prison ".. GetPlayerName(tPID).. " pour ".. jT .." seconds par ".. GetPlayerName(source))
					TriggerClientEvent("JP3", tPID, jT)
			--		TriggerClientEvent('chatMessage', -1, 'JUGE', { 0, 0, 0 }, GetPlayerName(tPID) ..' est en prison pour '.. jT ..' seconds')
				end
			else
			--	TriggerClientEvent('chatMessage', -1, 'SYSTEM', { 0, 0, 0 }, "Vous n'avez pas le droit de mettre des citoyens en prison !")
			end
		elseif cm[1] == "/jail4" then
			if xPlayer.job.name == 'police' and xPlayer.job.grade_name == "boss" then
				CancelEvent()
				local tPID = tonumber(cm[2])
				local jT = defaultsecs
					if cm[3] ~= nil then
						jT = tonumber(cm[3])				
					end
				if jT > maxsecs then
					jT = maxsecs
				end
				if GetPlayerName(tPID) ~= nil then
					print("Vous êtes en prison fédéral ".. GetPlayerName(tPID).. " pour ".. jT .." seconds par ".. GetPlayerName(source))
					TriggerClientEvent("JP4", tPID, jT)
				--	TriggerClientEvent('chatMessage', -1, 'JUGE', { 0, 0, 0 }, GetPlayerName(tPID) ..' est en prison fédéral pour '.. jT ..' seconds')
				end
			else
			--	TriggerClientEvent('chatMessage', -1, 'SYSTEM', { 0, 0, 0 }, "Vous n'avez pas le grade sufficient pour mettre des citoyens en fédéral !")
			end
		end
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end