ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_itemillegal:handcuff')
AddEventHandler('esx_itemillegal:handcuff', function(target)
  TriggerClientEvent('esx_itemillegal:handcuff', target)
end)

ESX.RegisterUsableItem('Serflex', function(source)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('Serflex', 1)

  TriggerClientEvent('esx_itemillegal:serflex', _source)
    TriggerClientEvent('esx:showNotification', _source, _U('used_one_serflex'))

end)

ESX.RegisterUsableItem('Pince_coupante', function(source)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('Pince_coupante', 1)

  TriggerClientEvent('esx_itemillegal:pince', _source)
    TriggerClientEvent('esx:showNotification', _source, _U('used_one_pince'))

end)