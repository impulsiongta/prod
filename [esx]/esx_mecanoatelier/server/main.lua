ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('esx_mecanoatelier:buyMod')
AddEventHandler('esx_mecanoatelier:buyMod', function(price)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	price = tonumber(price)
	if price > xPlayer.getMoney() then
		TriggerClientEvent('esx_mecanoatelier:cancelInstallMod', _source)
		TriggerClientEvent('esx:showNotification', _source, _U('not_enough_money'))
	else
		xPlayer.removeMoney(price)
		TriggerClientEvent('esx_mecanoatelier:installMod', _source)
		TriggerClientEvent('esx:showNotification', _source, _U('purchased'))
	end
end)

RegisterServerEvent('esx_mecanoatelier:refreshOwnedVehicle')
AddEventHandler('esx_mecanoatelier:refreshOwnedVehicle', function(myCar)

	MySQL.Async.execute(
		'UPDATE `owned_vehicles` SET `vehicle` = @vehicle WHERE `vehicle` LIKE "%' .. myCar['plate'] .. '%"',
		{
			['@vehicle'] = json.encode(myCar)
		}
	)
end)