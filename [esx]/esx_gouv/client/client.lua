local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


local gouv = {x=-429.525,y=1109.5,z=327.682}
local office = {x=3069.9,y=-4632.4,z=16.2}
local sortie = {x=-77.85,y=-833.65,z=243.39}
local garage = {x=-403.84,y=1075.78,z=323.86}
local garagesortie = {x=-77.17,y=-826.46,z=243.39}

local accountMoney = {x=-81.883,y=-808.073,z=243.39}

local playerJob = ""
local playerGrade = ""

local PlayerData = {}
local JobAPI = nil

ESX = nil
local GUI                     = {}
GUI.Time                      = 0
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)

   playerJob = xPlayer.job.name
   playerGrade = xPlayer.job.grade
   PlayerData = xPlayer
   
  	if PlayerData.job.name == 'gouv' then

		Config.Zones.GouvActions.Type = 1
		Config.Zones.VehicleSpawner.Type   = 1
		Config.Zones.VehicleDeleter.Type   = 1
		Config.Zones.HelicoSpawner.Type   = 1
		Config.Zones.HelicoDeleter.Type   = 1
		Config.Zones.Coffre.Type	   = 1
	else

		Config.Zones.GouvActions.Type = -1
		Config.Zones.VehicleSpawner.Type   = -1
		Config.Zones.VehicleDeleter.Type   = -1
		Config.Zones.HelicoSpawner.Type   = -1
		Config.Zones.HelicoDeleter.Type   = -1
		Config.Zones.Coffre.Type	   = -1
	end
   
   	if(PlayerData ~= {} and PlayerData.job.name == "gouv") then
		--JobAPI.createCoffre(-67.3, -812.24, 243.39, "gouv")
		if (PlayerData.job.grade_name == "boss") then
			--JobAPI.createSocietyMenu(-63.03, -814.29, 243.39, "gouv", "Gouvernement")
		end
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

	PlayerData.job = job

	if PlayerData.job.name == 'gouv' then

		Config.Zones.GouvActions.Type = 1
		Config.Zones.VehicleSpawner.Type   = 1
		Config.Zones.VehicleDeleter.Type   = 1
		Config.Zones.HelicoSpawner.Type   = 1
		Config.Zones.HelicoDeleter.Type   = 1
		Config.Zones.Coffre.Type	   = 1
	else

		Config.Zones.GouvActions.Type = -1
		Config.Zones.VehicleSpawner.Type   = -1
		Config.Zones.VehicleDeleter.Type   = -1
		Config.Zones.HelicoSpawner.Type   = -1
		Config.Zones.HelicoDeleter.Type   = -1
		Config.Zones.Coffre.Type	   = -1
	end

end)

Citizen.CreateThread(function()

	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
	
	while JobAPI == nil do
		Citizen.Wait(10)
		TriggerEvent('API:getJobAPI', function(obj) JobAPI = obj end)
	end
end)

Citizen.CreateThread(function()

	company = AddBlipForCoord(gouv.x, gouv.y, gouv.z)
	SetBlipSprite(company, 419)
	SetBlipAsShortRange(company, true)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString("Gouvernement")
	EndTextCommandSetBlipName(company)

	while playerJob == "" do
		Citizen.Wait(10)
	end

	TriggerServerEvent("gouv:addPlayer", playerJob)

	while true do
		Citizen.Wait(0)

		DrawMarker(1,gouv.x,gouv.y,gouv.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
		DrawMarker(1,sortie.x,sortie.y,sortie.z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)

		if(isNear(gouv)) then
			if(playerJob == "gouv") then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~entrer")

				if(IsControlJustPressed(1, 38)) then
					SetEntityCoords(GetPlayerPed(-1),-76.79, -830.14, 243.39)
				end
			else
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~sonner")

				if(IsControlJustPressed(1, 38)) then
					TriggerServerEvent("gouv:sendSonnette")
					SendNotification("Vous sonnez au gouvernement")
				end
			end
		end

		if(isNear(sortie)) then
			Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~sortir.")

			if(IsControlJustPressed(1, 38)) then
				SetEntityCoords(GetPlayerPed(-1),gouv.x,gouv.y,gouv.z)
			end
		end

		-- if(playerGrade == "boss" and playerJob == "gouv") then
			-- DrawMarker(1,accountMoney.x,accountMoney.y,accountMoney.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)

			-- if(isNear(accountMoney)) then
				-- Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~ouvrir le coffre.")

				-- if(IsControlJustPressed(1, 38)) then
					-- renderMenu("gouv", "Gouvernement")
				-- end
			-- end
		-- end
		
	end

end)

Citizen.CreateThread(function()
	while playerJob == "" do
		Citizen.Wait(10)
	end

	TriggerServerEvent("gouv:addPlayer", playerJob)
	
	while true do
		Citizen.Wait(0)

		DrawMarker(1,garage.x,garage.y,garage.z-1,0,0,0,0,0,0,2.001,2.0001,0.5001,0,128,255,200,0,0,0,0)
		DrawMarker(1,garagesortie.x,garagesortie.y,garagesortie.z-1,0,0,0,0,0,0,1.001,1.0001,0.5001,0,128,255,200,0,0,0,0)

		if(isNear(garage)) then
			if(playerJob == "gouv") then
				Info("Appuyez sur ~INPUT_CONTEXT~ pour ~g~~h~entrer")

				if(IsControlJustPressed(1, 38)) then
					SetEntityCoords(GetPlayerPed(-1),-78.79, -829.53, 243.39)
				end
			end
		end

		if(isNear(garagesortie)) then
			Info("Appuyez sur ~INPUT_CONTEXT~ pour ~r~~h~sortir.")

			if(IsControlJustPressed(1, 38)) then
				SetEntityCoords(GetPlayerPed(-1),garage.x,garage.y,garage.z)
			end
		end
	end
end)

function OpenGouvActionsMenu()

	local elements = {
		{label = _U('cloakroom'), value = 'cloakroom'}
	}

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'gouv_actions',
		{
			title    = _U('gouvernement'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			if data.current.value == 'cloakroom' then
				OpenCloakroomMenu()
			end
		end,
		function(data, menu)

			menu.close()

			CurrentAction     = 'gouv_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end
	)

end

function OpenCloakroomMenu()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = {
				{label = _U('clothes_civil'), value = 'citizen_wear'},
				{label = _U('clothes_gouv'), value = 'gouv_wear'},
			},
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'citizen_wear' then

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
					TriggerEvent('skinchanger:loadSkin', skin)
				end)

			end

			if data.current.value == 'gouv_wear' then

				ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

					if skin.sex == 0 then
						TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
					else
						TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
					end
				end)
			end

			CurrentAction     = 'gouv_actions_menu'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		function(data, menu)
			menu.close()
		end
	)

end

function OpenVehicleSpawnerMenu()

	local elements = {}
	
	  if PlayerData.job.grade_name == 'secretaire' then
	    table.insert(elements, {label = _U('schafter'),   value = 'schafter5'})
	  end

	  if PlayerData.job.grade_name == 'garde' then
	    table.insert(elements, {label = _U('kuruma'), value = 'kuruma2'})
	    table.insert(elements, {label = _U('xls'), value = 'xls2'})	    
	  end

	  if PlayerData.job.grade_name == 'juge' then
	    table.insert(elements, {label = _U('schafter'),   value = 'schafter5'})
	    table.insert(elements, {label = _U('xls'), value = 'xls2'})
	  end

	  if PlayerData.job.grade_name == 'procureur' then
	    table.insert(elements, {label = _U('kuruma'), value = 'kuruma2'})
	    table.insert(elements, {label = _U('cog'), value = 'cog552'})
	  end
	  
	  if PlayerData.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('kuruma'), value = 'kuruma2'})
	    table.insert(elements, {label = _U('cog'), value = 'cog552'})
	  end
	  
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('veh_menu'),
			align    = 'top-left',
			elements = elements,
		},
		function(data, menu)

			menu.close()

			local model = data.current.value

			ESX.Game.SpawnVehicle(model, Config.Zones.VehicleSpawner.Pos, 345.56, function(vehicle)

				local playerPed = GetPlayerPed(-1)
			
				TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
				TriggerEvent("advancedFuel:setEssence", 100, GetVehicleNumberPlateText(vehicle), GetDisplayNameFromVehicleModel(GetEntityModel(vehicle)))
			end)

		end,
		function(data, menu)

			menu.close()

			CurrentAction     = 'vehicle_spawner_menu'
			CurrentActionMsg  = _U('veh_spawn')
			CurrentActionData = {}


		end
	)
end

function OpenHelicoSpawnerMenu()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('hel_menu'),
			align    = 'top-left',
			elements = {
				{label = _U('helicopter'), value = 'supervolito2'},
			},
		},
		function(data, menu)

			menu.close()

			local model = data.current.value

			ESX.Game.SpawnVehicle(model, Config.Zones.HelicoSpawner.Pos, 104.14, function(vehicle)

				local playerPed = GetPlayerPed(-1)

				TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
				TriggerEvent("advancedFuel:setEssence", 100, GetVehicleNumberPlateText(vehicle), GetDisplayNameFromVehicleModel(GetEntityModel(vehicle)))
			end)
		end,
		function(data, menu)

			menu.close()

			CurrentAction     = 'helico_spawner_menu'
			CurrentActionMsg  = _U('hel_spawn')
			CurrentActionData = {}
		end
	)
end
		
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(0)

		if CurrentAction ~= nil then

			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)

			if IsControlPressed(0,  Keys['E']) and PlayerData.job and PlayerData.job.name == 'gouv' and (GetGameTimer() - GUI.Time) > 150 then

				if CurrentAction == 'gouv_actions_menu' then
					OpenGouvActionsMenu()
				end

				if CurrentAction == 'vehicle_spawner_menu' then
					OpenVehicleSpawnerMenu()
				end

				if CurrentAction == 'helico_spawner_menu' then
					OpenHelicoSpawnerMenu()
				end
				
				if CurrentAction == 'delete_vehicle' then
					ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
				end
				
				if CurrentAction == 'gouv_actions_menu_coffre' then
				    CoffreMenu()
				end
				CurrentAction = nil
				GUI.Time      = GetGameTimer()

			end
		end
		
		if IsControlPressed(0,  Keys['F6']) and PlayerData.job ~= nil and PlayerData.job.name == 'gouv' and (PlayerData.job.grade_name == "secretaire" or PlayerData.job.grade_name == "garde") and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'gouv_mobile_actions') and (GetGameTimer() - GUI.Time) > 150 then
			OpenGouvMobileActionsMenu()
			GUI.Time = GetGameTimer()
		end
	end
end)		
			
AddEventHandler('esx_gouv:hasEnteredMarker', function(zone)

	if zone == 'GouvActions' and PlayerData.job and PlayerData.job.name == 'gouv' then
		CurrentAction     = 'gouv_actions_menu'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end

	if zone == 'VehicleSpawner' and PlayerData.job and PlayerData.job.name == 'gouv' then
		CurrentAction     = 'vehicle_spawner_menu'
		CurrentActionMsg  = _U('veh_spawn')
		CurrentActionData = {}
	end

	if zone == 'VehicleDeleter' and PlayerData.job and PlayerData.job.name == 'gouv' then

		local playerPed = GetPlayerPed(-1)
		local coords    = GetEntityCoords(playerPed)

		if IsPedInAnyVehicle(playerPed,  false) then

			local vehicle, distance = ESX.Game.GetClosestVehicle({
				x = coords.x,
				y = coords.y,
				z = coords.z
			})

			if distance ~= -1 and distance <= 1.0 then

				CurrentAction     = 'delete_vehicle'
				CurrentActionMsg  = _U('store_veh')
				CurrentActionData = {vehicle = vehicle}
			end
		end
	end

	if zone == 'HelicoSpawner' and PlayerData.job and PlayerData.job.name == 'gouv' then
		CurrentAction     = 'helico_spawner_menu'
		CurrentActionMsg  = _U('hel_spawn')
		CurrentActionData = {}
	end
	
	if zone == 'HelicoDeleter' and PlayerData.job and PlayerData.job.name == 'gouv' then

		local playerPed = GetPlayerPed(-1)
		local coords    = GetEntityCoords(playerPed)

		if IsPedInAnyVehicle(playerPed,  false) then

			local vehicle, distance = ESX.Game.GetClosestVehicle({
				x = coords.x,
				y = coords.y,
				z = coords.z
			})

			if distance ~= -1 and distance <= 1.0 then

				CurrentAction     = 'delete_vehicle'
				CurrentActionMsg  = _U('store_hel')
				CurrentActionData = {vehicle = vehicle}
			end
		end
	end
	
	if zone == 'Coffre' and PlayerData.job and PlayerData.job.name == 'gouv' then
		CurrentAction     = 'gouv_actions_menu_coffre'
		CurrentActionMsg  = _U('open_menu')
		CurrentActionData = {}
	end
	
end)
			
AddEventHandler('esx_gouv:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)
			
Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords      = GetEntityCoords(GetPlayerPed(-1))
		local isInMarker  = false
		local currentZone = nil

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone                = currentZone
			TriggerEvent('esx_gouv:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('esx_gouv:hasExitedMarker', lastZone)
		end
	end
end)

Citizen.CreateThread(function()
	while true do

		Wait(0)

		local coords = GetEntityCoords(GetPlayerPed(-1))

		for k,v in pairs(Config.Zones) do

			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
			end
		end
	end
end)
			
function isNear(tabl)
	local distance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),tabl.x,tabl.y,tabl.z, true)

	if(distance<3) then
		return true
	end

	return false
end


function Info(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end

local stopRequest = false
local timeout = 0

RegisterNetEvent("gouv:sendRequest")
AddEventHandler("gouv:sendRequest", function(name,id)
	stopRequest = true
	
	SendNotification("~b~"..name.."~w~ a sonné à la porte du gouvernement.")
	SendNotification("~b~Y~w~ pour ~g~accepter~w~ / ~b~G~w~ pour ~r~refuser~w~.")

	stopRequest = false
	
	while not stopRequest do
		Citizen.Wait(0)
			
		if(IsControlJustPressed(1, 246)) then
			TriggerServerEvent("gouv:sendStatusToPoeple", id, 1)
			stopRequest = true
		end
			
		if(IsControlJustPressed(1, 47)) then
			TriggerServerEvent("gouv:sendStatusToPoeple", id, 0)
			stopRequest = false
		end
	end
end)

RegisterNetEvent("gouv:sendStatus")
AddEventHandler("gouv:sendStatus", function(status)
	if(status == 1) then
		SendNotification("Quelqu'un est venu vous ~g~~h~ouvrir la porte.")
		SetEntityCoords(GetPlayerPed(-1),-76.79, -830.14, 243.39)
	else
		SendNotification("Personne n'a voulu vous ~r~~h~ouvrir la porte...")
	end
end)

function SendNotification(message)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(message)
	DrawNotification(false, false)
end


function CoffreMenu()

	local elements = {
	{label = _U('deposit_stock'), value = 'put_stock'},
    	{label = _U('withdraw_stock'), value = 'get_stock'}
	}
	if PlayerData.job ~= nil and PlayerData.job.grade_name == 'boss' then
	    table.insert(elements, {label = _U('boss_action'), value = 'boss_actions'})
	end

	ESX.UI.Menu.CloseAll()
	
	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'cloakroom',
		{
			title    = _U('cloakroom'),
			align    = 'top-left',
			elements = elements
		},
		function(data, menu)

			menu.close()

			if data.current.value == 'put_stock' then
        		OpenPutStocksMenu()
      		end
			if data.current.value == 'get_stock' then
		        OpenGetStocksMenu()
		    end
		    if data.current.value == 'boss_actions' then
		        TriggerEvent('esx_society:openBossMenu', 'gouv', function(data, menu)
		        menu.close()
				end)
			end

			CurrentAction     = 'gouv_actions_menu_coffre'
			CurrentActionMsg  = _U('open_menu')
			CurrentActionData = {}

		end,
		
		function(data, menu)
			menu.close()
		end
	)

end


function OpenGetStocksMenu()

  ESX.TriggerServerCallback('gouv:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
    	if items[i].count > 0 then
			table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
		end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('gouv_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('gouv:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('gouv:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('gouv:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenIdentityCardMenu(player)

  if Config.EnableESXIdentity then

    ESX.TriggerServerCallback('esx_gouv:getOtherPlayerData', function(data)

      local jobLabel    = nil
      local sexLabel    = nil
      local sex         = nil
      local dobLabel    = nil
      local heightLabel = nil
      local idLabel     = nil

      if data.job.grade_label ~= nil and  data.job.grade_label ~= '' then
        jobLabel = 'Job : ' .. data.job.label .. ' - ' .. data.job.grade_label
      else
        jobLabel = 'Job : ' .. data.job.label
      end

      if data.sex ~= nil then
        if (data.sex == 'm') or (data.sex == 'M') then
          sex = 'Male'
        else
          sex = 'Female'
        end
        sexLabel = 'Sex : ' .. sex
      else
        sexLabel = 'Sex : Unknown'
      end

      if data.dob ~= nil then
        dobLabel = 'DOB : ' .. data.dob
      else
        dobLabel = 'DOB : Unknown'
      end

      if data.height ~= nil then
        heightLabel = 'Height : ' .. data.height
      else
        heightLabel = 'Height : Unknown'
      end

      if data.name ~= nil then
        idLabel = 'ID : ' .. data.name
      else
        idLabel = 'ID : Unknown'
      end

      local elements = {
        {label = _U('name') .. data.firstname .. " " .. data.lastname, value = nil},
        {label = sexLabel,    value = nil},
        {label = dobLabel,    value = nil},
        {label = heightLabel, value = nil},
        {label = jobLabel,    value = nil},
        {label = idLabel,     value = nil},
      }

      if data.drunk ~= nil then
        table.insert(elements, {label = _U('bac') .. data.drunk .. '%', value = nil})
      end

      if data.licenses ~= nil then

        table.insert(elements, {label = '--- Licenses ---', value = nil})

        for i=1, #data.licenses, 1 do
          table.insert(elements, {label = data.licenses[i].label, value = nil})
        end

      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'citizen_interaction',
        {
          title    = _U('citizen_interaction'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)

        end,
        function(data, menu)
          menu.close()
        end
      )

    end, GetPlayerServerId(player))

  else

    ESX.TriggerServerCallback('esx_gouv:getOtherPlayerData', function(data)

      local jobLabel = nil

      if data.job.grade_label ~= nil and  data.job.grade_label ~= '' then
        jobLabel = 'Job : ' .. data.job.label .. ' - ' .. data.job.grade_label
      else
        jobLabel = 'Job : ' .. data.job.label
      end

        local elements = {
          {label = _U('name') .. data.name, value = nil},
          {label = jobLabel,              value = nil},
        }

      if data.drunk ~= nil then
        table.insert(elements, {label = _U('bac') .. data.drunk .. '%', value = nil})
      end

      if data.licenses ~= nil then

        table.insert(elements, {label = '--- Licenses ---', value = nil})

        for i=1, #data.licenses, 1 do
          table.insert(elements, {label = data.licenses[i].label, value = nil})
        end

      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'citizen_interaction',
        {
          title    = _U('citizen_interaction'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)

        end,
        function(data, menu)
          menu.close()
        end
      )

    end, GetPlayerServerId(player))

  end

end

function OpenGouvMobileActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'gouv_mobile_actions',
    {
      title    = 'Gouvernement',
      align    = 'top-left',
      elements = {
        {label = _U('citizen_interaction'), value = 'citizen_interaction'},
      },
    },
    function(data, menu)

      if data.current.value == 'citizen_interaction' then

        ESX.UI.Menu.Open(
          'default', GetCurrentResourceName(), 'citizen_interaction',
          {
            title    = _U('citizen_interaction'),
            align    = 'top-left',
            elements = {
         --     {label = _U('id_card'),       value = 'identity_card'},
              {label = _U('search'),        value = 'body_search'}
            },
          },
          function(data2, menu2)

            local player, distance = ESX.Game.GetClosestPlayer()

            if distance ~= -1 and distance <= 3.0 then

              if data2.current.value == 'identity_card' then
                OpenIdentityCardMenu(player)
              end

              if data2.current.value == 'body_search' then
                OpenBodySearchMenu(player)
              end

            else
              ESX.ShowNotification(_U('no_players_nearby'))
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end
    end,
    function(data, menu)

      menu.close()

    end
  )

end

function OpenBodySearchMenu(player)

  ESX.TriggerServerCallback('esx_gouv:getOtherPlayerData', function(data)

    local elements = {}

    table.insert(elements, {label = _U('guns_label'), value = nil})

    for i=1, #data.weapons, 1 do
      table.insert(elements, {
        label          = _U('confiscate') .. ESX.GetWeaponLabel(data.weapons[i].name),
        value          = data.weapons[i].name,
        itemType       = 'item_weapon',
        amount         = data.ammo,
      })
    end

    table.insert(elements, {label = _U('inventory_label'), value = nil})

    for i=1, #data.inventory, 1 do
      if data.inventory[i].count > 0 then
        table.insert(elements, {
          label          = _U('confiscate_inv') .. data.inventory[i].count .. ' ' .. data.inventory[i].label,
          value          = data.inventory[i].name,
          itemType       = 'item_standard',
          amount         = data.inventory[i].count,
        })
      end
    end


    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'body_search',
      {
        title    = _U('search'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        local itemType = data.current.itemType
        local itemName = data.current.value
        local amount   = data.current.amount

        if data.current.value ~= nil then

          TriggerServerEvent('esx_gouv:confiscatePlayerItem', GetPlayerServerId(player), itemType, itemName, amount)

          OpenBodySearchMenu(player)

        end

      end,
      function(data, menu)
        menu.close()
      end
    )

  end, GetPlayerServerId(player))

end
