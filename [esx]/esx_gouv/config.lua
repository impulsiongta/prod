Config                           = {}
Config.DrawDistance              = 100.0
Config.MarkerColor               = {r = 0, g = 128, b = 255}
Config.Locale                    = 'fr'
Config.EnableESXIdentity         = true -- only turn this on if you are using esx_identity

Config.Zones = {

	GouvActions = {
		Pos   = {x = -78.73, y = -811.7, z = 242.39},
		Size = {x = 2.001, y = 2.001, z = 0.5001},
		Type  = 1
	},

	VehicleSpawner = {
		Pos   = {x = -403.01,y=1061.69, z = 322.84},
		Size = {x = 2.001, y = 2.001, z = 0.5001},
		Type  = 1
	},

	VehicleDeleter = {
		Pos   = {x = -412.32,y=1063.26,z=322.84},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Type  = 1
	},
	
	HelicoSpawner = {
		Pos   = {x = -459.59,y=1143.42, z = 326.74},
		Size = {x = 2.001, y = 2.001, z = 0.5001},
		Type  = 1
	},

	HelicoDeleter = {
		Pos   = {x = -461.42,y=1137.08, z = 326.74},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Type  = 1
	},
	Coffre = {
		Pos   = {x = -67.3, y = -812.24, z = 242.39},
		Size  = {x = 1.501, y = 1.501, z = 0.5001},
		Type  = 1
	}
}