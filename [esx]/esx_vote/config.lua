Config                           = {}
Config.DrawDistance              = 100.0
Config.MarkerColor               = {r = 0, g = 128, b = 255}

Config.Zones = {

	VoteBooth1 = {
		MarkerPos  = {x = -266.18, y = -2035.95, z = 29.15 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1

	},
	
	VoteBooth2 = {
		MarkerPos  = {x = -247.89, y = -2014.19, z = 29.15 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1

	},
	
	VoteBooth3 = {
		MarkerPos  = {x = -271.61, y = -2039.17, z = 29.15 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1

	},
	
	VoteBooth4 = {
		MarkerPos  = {x = -245.55, y = -2008.34, z = 29.15 },
		Size       = {x = 1.5, y = 1.5, z = 1.0},
		Type       = 1

	}
}

Config.Candidates = {
	{label = "Etienne ROTOLA", value = 'rotola'},
	{label = "Vote blanc", value = 'blanc'}
}
