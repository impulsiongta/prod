ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('elec:vote')
AddEventHandler('elec:vote', function(candidate,value)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local candidate = candidate
	local value = value
	local count = 0

	local result = MySQL.Sync.fetchAll('SELECT COUNT(*) as count FROM vote WHERE vote IS NOT NULL AND identifier = @identifier', {
		['@identifier'] = xPlayer.getIdentifier()
	})

	count = tonumber(result[1].count)

	if count == 0 then
		MySQL.Async.execute('INSERT INTO vote (identifier, vote) VALUES (@identifier, @vote) ON DUPLICATE KEY UPDATE vote = @vote;',{
			['@identifier'] = xPlayer.getIdentifier(),
			['@vote'] = value
		})
		
		if value == 'blanc' then
			TriggerClientEvent('esx:showNotification', _source, 'Vous avez voté ~g~~h~blanc~s~')
		else
			TriggerClientEvent('esx:showNotification', _source, 'Vous avez voté pour ~g~~h~' .. candidate .. '~s~')
		end
	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous avez ~r~~h~déjà voté.')
	end
end)