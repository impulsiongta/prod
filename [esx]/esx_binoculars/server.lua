ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('esx_binoculars:itemChecker', function(source, cb)
	if ESX.GetPlayerFromId(source).getInventoryItem('jumelles').count > 0 then
		cb(true)
	else
		TriggerClientEvent('esx:showNotification', source, "~r~Vous n'avez pas de jumelles sur vous.")
		cb(false)
	end
end)