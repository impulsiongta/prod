Locales['fr'] = {

	['invoices'] = 'factures',
	['received_invoice'] = 'vous avez ~r~reçu~s~ une facture',
	['paid_invoice'] = 'vous avez ~g~payé~s~ une facture de ~r~$',
	['received_payment'] = 'vous avez ~g~reçu~s~ un paiement de ~g~$',
	['player_not_logged'] = 'le joueur ~r~~h~n\'est pas en ville',
	['no_money'] = 'vous n\'avez pas assez d\'argent pour payer cette facture',
	['target_no_money'] = 'Cette personne n\'a ~r~pas assez~w~ d\'argent pour payer cette facture!',
	['negative_bill'] = 'Vous ne pouvez pas faire de factures négatives!'
}
